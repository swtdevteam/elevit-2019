function SlideList(t){var t=t||[];this.init(t)}Function.prototype.bind||(Function.prototype.bind=function(t){if("function"!=typeof this)throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");var e=Array.prototype.slice.call(arguments,1),i=this,n=function(){},s=function(){return i.apply(this instanceof n&&t?this:t,e.concat(Array.prototype.slice.call(arguments)))};return n.prototype=this.prototype,s.prototype=new n,s}),function(t,e){try{t.querySelector(":scope body")}catch(i){["querySelector","querySelectorAll"].forEach(function(i){var n=e[i];e[i]=function(e){if(/(^|,)\s*:scope/.test(e)){var s=this.id;this.id="ID_"+Date.now(),e=e.replace(/((^|,)\s*):scope/g,"$1#"+this.id);var r=t[i](e);return this.id=s,r}return n.call(this,e)}})}}(window.document,Element.prototype);var app=app||function(){"use strict";function t(){if(w.wrapper){var t=s(),e=app.config.get("maxScale"),i=app.config.get("minScale");w.slides.style.width=t.slideWidth+"px",w.slides.style.height=t.slideHeight+"px",y=Math.min(t.availableWidth/t.slideWidth,t.availableHeight/t.slideHeight),y=Math.max(y,i),y=Math.min(y,e),"undefined"==typeof w.slides.style.zoom||navigator.userAgent.match(/(iphone|ipod|ipad|android)/gi)?c(w.slides,"translate(-50%, -50%) scale("+y+") translate(50%, 50%)"):w.slides.style.zoom=y,app.trigger("update:layout",{scale:y})}}function e(){return y}function i(){function t(){!window.Promise&&window.ES6Promise&&(window.Promise=window.ES6Promise.Promise),n.length&&head.js.apply(null,n),app.start.init()}function e(e){head.ready(e.src.match(/([\w\d_\-]*)\.?js$|[^\\\/]*$/i)[0],function(){"function"==typeof e.callback&&e.callback.apply(this),0===--s&&t()})}var i=[],n=[],s=0,r=app.config.get("dependencies"),a=app.config.get("transition"),o=app.config.get("transitionSpeed"),p=app.config.get("monitoringAPI");a?w.wrapper.classList.add(a):w.wrapper.classList.add("linear"),o&&w.wrapper.setAttribute("data-transition-speed",o),app.start||r.unshift({src:"accelerator/js/core.js"}),window.touchy||r.push({src:"accelerator/lib/touchy.js"}),!window.ag&&p&&r.unshift({src:p}),window.Promise||r.unshift({src:"accelerator/lib/promise.min.js"});for(var l=0,c=r.length;l<c;l++){var d=r[l];d.condition&&!d.condition()||(d.async?n.push(d.src):i.push(d.src),e(d))}i.length?(s=i.length,head.js.apply(null,i)):t()}function n(){var t=w.slides.querySelectorAll(".slide");return t.length>0&&p(t).forEach(function(t,e){t.id&&(w[t.id]=t)}),t}function s(){var t=w.wrapper.offsetWidth,e=w.wrapper.offsetHeight,i=app.config.get();t-=e*i.margin,e-=e*i.margin;var n=i.width,s=i.height,r=20;return"string"==typeof n&&/%$/.test(n)&&(n=parseInt(n,10)/100*t),"string"==typeof s&&/%$/.test(s)&&(s=parseInt(s,10)/100*e),{availableWidth:t,availableHeight:e,slideWidth:n,slideHeight:s,slidePadding:r}}function r(e){var s=app.config.get();e&&app.config.add(e),e.setup&&e.setup(app.config.get()),app.env&&s[app.env]&&app.config.add(s[app.env]),!app.lang&&s.lang&&(app.lang=s.lang),t();n();app.config.add({cachedElements:w}),setTimeout(function(){i()},50)}function a(t,e){t=t||null,e=e||{},app.initialize=!1;window.location.port||null;w.theme=document.querySelector("#theme"),w.wrapper=document.querySelector(".accelerator"),w.slides=document.querySelector(".accelerator .slides"),w.template=document.querySelector(".accelerator .template"),app.queryParams=f(window.location.search),app.queryParams&&(app.queryParams.env&&(app.env=app.queryParams.env),app.queryParams.lang&&(app.lang=app.queryParams.lang),app.queryParams.mode&&(app.mode=app.queryParams.mode)),t?app.config.fetch(t,function(){r(e)}):r(e)}function o(t){return S.call(arguments,1).forEach(function(e){for(var i in e)t[i]=e[i]}),t}function p(t){return Array.prototype.slice.call(t)}function l(t){var e=0;if(t){var i=0;p(t.childNodes).forEach(function(t){"number"==typeof t.offsetTop&&t.style&&("absolute"===t.style.position&&(i+=1),e=Math.max(e,t.offsetTop+t.offsetHeight))}),0===i&&(e=t.offsetHeight)}return e}function c(t,e){t.style.WebkitTransform=e,t.style.MozTransform=e,t.style.msTransform=e,t.style.OTransform=e,t.style.transform=e}function d(){if(window.XMLHttpRequest)return new XMLHttpRequest;try{return new ActiveXObject("Microsoft.XMLHTTP")}catch(t){}try{return new ActiveXObject("Msxml2.XMLHTTP.6.0")}catch(t){}try{return new ActiveXObject("Msxml2.XMLHTTP.3.0")}catch(t){}try{return new ActiveXObject("Msxml2.XMLHTTP")}catch(t){}return!1}function u(t,e){var i=d();if(!i)throw new Error("XMLHttpRequest or ActiveXObject is not available. Cannot get file.");i.open("GET",t),i.onreadystatechange=function(){4===i.readyState&&(i.onreadystatechange=function(){},i.status>=200&&i.status<300||304==i.status||0==i.status&&"file:"==window.location.protocol?e(null,i.responseText):400===i.status?e({error:"Could not locate file"},null):e({error:i.status},null))},i.send(null)}function h(t){var e,i=document.createElement("div"),n=i.style;return t.toLowerCase()in n?e="":"Webkit"+t in n?e="-webkit-":"Moz"+t in n?e="-moz-":"ms"+t in n?e="-ms-":"O"+t in n&&(e="-o-"),e}function f(t){return(t||document.location.search).replace(/(^\?)/,"").split("&").map(function(t){return t=t.split("="),this[t[0]]=t[1],this}.bind({}))[0]}function v(){function t(t){var e=_.template(t);return e()}return t}var g,m="1.4.7",w={},y=1,b=[],S=b.slice;return window.head||(g=document.createElement("script"),g.setAttribute("src","accelerator/lib/head.min.js"),document.head.appendChild(g)),window.addEventListener("resize",t,!1),setTimeout(function(){app.initialize&&app.initialize("config.json")},100),{initialize:a,layout:t,getScale:e,util:{extend:o,getAbsoluteHeight:l,toArray:p,transformElement:c,getFile:u,getBrowserPrefix:h,_templateParser:v},version:m}}();!function(){function t(t){var e=++s+"";return t?t+e:e}var e=[],i=e.push,n=e.slice,s=({}.toString,0),r=app.events={on:function(t,e,i){if(!o(this,"on",t,[e,i])||!e)return this;this._events||(this._events={});var n=this._events[t]||(this._events[t]=[]);return n.push({callback:e,context:i,ctx:i||this}),this},once:function(t,e,i){if(!o(this,"once",t,[e,i])||!e)return this;var n,s=this,r=function(){n||(n=!0,s.off(t,r),e.apply(this,arguments))};return r._callback=e,this.on(t,r,i)},off:function(t,e,i){var n,s,r,a,p,l,c,d;if(!this._events||!o(this,"off",t,[e,i]))return this;if(!t&&!e&&!i)return this._events=void 0,this;for(a=t?[t]:Object.keys(this._events),p=0,l=a.length;p<l;p++)if(t=a[p],r=this._events[t]){if(this._events[t]=n=[],e||i)for(c=0,d=r.length;c<d;c++)s=r[c],(e&&e!==s.callback&&e!==s.callback._callback||i&&i!==s.context)&&n.push(s);n.length||delete this._events[t]}return this},trigger:function(t){if(!this._events)return this;var e=n.call(arguments,1);if(!o(this,"trigger",t,e))return this;var i=this._events[t],s=this._events.all;return i&&p(i,e),s&&p(s,arguments),this},stopListening:function(t,e,i){var n=this._listeningTo;if(!n)return this;var s=!e&&!i;i||"object"!=typeof e||(i=this),t&&((n={})[t._listenId]=t);for(var r in n)t=n[r],t.off(e,i,this),!s&&Object.keys(t._events).length||delete this._listeningTo[r];return this}},a=/\s+/,o=function(t,e,n,s){if(!n)return!0;var r;if("object"==typeof n){for(var o in n)r=[o,n[o]],i.apply(r,s),t[e].apply(t,r);return!1}if(a.test(n)){for(var p=n.split(a),l=0,c=p.length;l<c;l++)r=[p[l]],i.apply(r,s),t[e].apply(t,r);return!1}return!0},p=function(t,e){var i,n=-1,s=t.length,r=e[0],a=e[1],o=e[2];switch(e.length){case 0:for(;++n<s;)(i=t[n]).callback.call(i.ctx);return;case 1:for(;++n<s;)(i=t[n]).callback.call(i.ctx,r);return;case 2:for(;++n<s;)(i=t[n]).callback.call(i.ctx,r,a);return;case 3:for(;++n<s;)(i=t[n]).callback.call(i.ctx,r,a,o);return;default:for(;++n<s;)(i=t[n]).callback.apply(i.ctx,e)}},l={listenTo:"on",listenToOnce:"once"};Object.keys(l).forEach(function(e){var i=l[e];r[e]=function(e,n,s){var r=this._listeningTo||(this._listeningTo={}),a=e._listenId||(e._listenId=t("l"));return r[a]=e,s||"object"!=typeof n||(s=this),e[i](n,s,this),this}})}(),app.util.extend(app,app.events),app.config=function(){function t(t,e){for(var i in e)t[i]=e[i]}function e(t,e){var n=app.cache.get(t);n?(i(JSON.parse(n)),e()):app.util.getFile(t,function(n,s){var r;if(n)throw new Error("Unable to fetch configuration "+t,n);r=JSON.parse(s),r&&i(r),e()})}function i(e){t(p,e)}function n(t){return t?!!p[t]&&p[t]:p}function s(t,e){return!(!t||!e)&&(!(!p[t]||"object"!=typeof p[t]||!p[t][e])&&p[t][e])}function r(t,e){p[t]=e}function a(t,e,i){p[t]&&"object"==typeof p[t]?p[t][e]=i:(p[t]={},p[t][e]=i)}function o(t){p[t]&&i(p[t])}var p={paths:{slides:"slides/<id>/",modules:"modules/<id>/",scripts:"slides/<id>/<id>.js",styles:"slides/<id>/<id>.css",thumbs:"slides/<id>/<id>.png",placeholderThumb:"placeholder_thumb.png",references:"shared/references/<id>.pdf"},width:960,height:700,margin:.1,minScale:.2,maxScale:1,history:!1,keyboard:!0,center:!0,touch:!0,loop:!1,rtl:!1,fragments:!0,embedded:!1,autoSlide:0,mouseWheel:!1,rollingLinks:!1,hideAddressBar:!0,theme:null,transition:"linear",transitionSpeed:"default",backgroundTransition:"default",viewDistance:3,preload:!1,pathToSlides:"slides/<id>/",pathToModules:"modules/<id>/",monitoringAPI:"accelerator/lib/agnitio.js",dependencies:[],lazy:!1,remote:!1,editableTag:"data-ag-editable",csvHeaders:{fieldName:"field_name",originalContent:"original_value",localContent:"localized_value"},iPlanner:{lockedOrientation:null,bounce:!1}};return{fetch:e,add:i,get:n,pluck:s,set:r,update:a,storyboardSetup:o}}(),app.registry=function(){function t(t,e,i){!n[t]||i?(n[t]=e(app),app.registry.trigger("register",t)):window.console&&console.log("Registration of "+t+" ignored. It's already registered.")}function e(t){return!!n[t]}function i(t){return t?e(t)?n[t]:null:n}var n={},s={add:t,exist:e,get:i};return app.util.extend(s,app.events),s}(),app.register=app.registry.add,app.util.extend(app.registry,app.events),app.cache=function(){function t(t,e){s[t]=e}function e(t){return!!s[t]}function i(t){return t?e(t)?s[t]:void 0:s}function n(t){return e(t)&&(s[t]=null),!0}var s={};return{put:t,exist:e,get:i,remove:n}}(),app.remote=function(){function t(t){i=t.role,n=t.path||null,app.config.set("remote",!0)}function e(){if(window.ag&&i){var t=function(){};app.slideshow.on("update:current",function(t){ag.msg.send({name:"slideEnter",value:app.getPath()})}),app.slide.on("state:enter",function(t){ag.msg.send({name:"stateEnter",value:JSON.stringify(t)})}),app.slide.on("reset",function(t){ag.msg.send({name:"stateExit",value:JSON.stringify(t)})}),ag.on("goTo",function(t){app.slideshow.__load__(t)}),"contact"===i&&(app.removeNavListeners(),app.slideshow.load=t,app.slideshow.goTo=t,app.slideshow.step=t,app.slideshow.next=t,app.slideshow.prev=t,app.slideshow.left=t,app.slideshow.right=t,app.slideshow.up=t,app.slideshow.down=t,ag.on("enterState",function(t){var e=JSON.parse(t),i=app.slide.get(e.view);i&&i.goTo(e.id,e.data)}),ag.on("resetState",function(t){var e=JSON.parse(t),i=app.slide.get(e.view);i&&i.reset()})),n&&app.slideshow.__load__(n)}}var i,n;return window.ag&&window.ag.on&&ag.on("registerUser",function(e){t(e)}),{init:t,setup:e}}(),SlideList.prototype.init=function(t){this.list=t,this.current={h:0,v:0}},SlideList.prototype.size=function(){var t=[];return t=t.concat.apply(t,this.list),t.length},SlideList.prototype.get=function(t,e){var i;if(void 0===t&&(t=this.current.h,e=this.current.v),e=e||0,i=this.getType(t)){if("list"===i)return this.list[t][e];if(!e)return this.list[t]}},SlideList.prototype.getIndex=function(t){var e,i=this.list.indexOf(t);return t?i>-1?{h:i,v:0}:(this.list.forEach(function(n,s){"string"!=typeof n&&(i=n.indexOf(t),i>-1&&(e={h:s,v:i}))}),e):this.current},SlideList.prototype.getType=function(t){if(t>-1&&t<this.list.length)return"string"==typeof this.list[t]?"item":"list"},SlideList.prototype.inRange=function(t){return t>-1&&t<this.list.length},SlideList.prototype.isEqual=function(t,e){return JSON.stringify(t)===JSON.stringify(e)},SlideList.prototype._set=function(t){this.current;this.current=t},SlideList.prototype.getList=function(){return this.list},SlideList.prototype.setList=function(t){var e,i=this.get();this.list=t,e=this.getIndex(i),this.inRange(this.current.h)&&e?this._set(e):this._set({h:0,v:0})},SlideList.prototype.goTo=function(t){var t=t||{h:0,v:0};"string"==typeof t&&(t=this.getIndex(t)),t.h=t.h||0,t.v=t.v||0,this.get(t.h,t.v)&&this._set(t)},SlideList.prototype.left=function(){var t={h:this.current.h-1,v:0};this.get(t.h)&&this._set(t)},SlideList.prototype.right=function(){var t={h:this.current.h+1,v:0};this.get(t.h)&&this._set(t)},SlideList.prototype.down=function(){var t={h:this.current.h,v:this.current.v+1};this.get(t.h,t.v)&&this._set(t)},SlideList.prototype.up=function(){var t={h:this.current.h,v:this.current.v-1};this.get(t.h,t.v)&&this._set(t)},SlideList.prototype.getUp=function(){var t={h:this.current.h,v:this.current.v-1},e=this.get(t.h,t.v);if(e)return t},SlideList.prototype.getDown=function(){var t={h:this.current.h,v:this.current.v+1},e=this.get(t.h,t.v);if(e)return t},SlideList.prototype.getRight=function(){var t={h:this.current.h+1,v:0},e=this.get(t.h,0);if(e)return t},SlideList.prototype.getLeft=function(){var t={h:this.current.h-1,v:0},e=this.get(t.h,0);if(e)return t},SlideList.prototype.getNeighbors=function(t){var e=[];return e[0]=this.getLeft(),e[1]=this.getUp(),e[2]=this.getRight(),e[3]=this.getDown(),e},SlideList.prototype.getNext=function(){var t,e={h:this.current.h,v:this.current.v+1},i={h:this.current.h+1,v:0},n=this.get(e.h,e.v);return n?e:(t=this.get(i.h,0))?i:void 0},SlideList.prototype.next=function(){var t=this.getNext();t&&this._set(t)},SlideList.prototype.previous=function(){var t,e={h:this.current.h,v:this.current.v-1},i={h:this.current.h-1,v:0},n=this.get(e.h,e.v);n?this._set(e):(t=this.getType(i.h),"item"===t?this._set(i):"list"===t&&(i.v=this.list[i.h].length-1,this._set(i)))},SlideList.prototype.gotoFirst=function(){this._set({h:0,v:0})},SlideList.prototype.gotoLast=function(){this._set({h:this.list.length-1,v:0})},SlideList.prototype.append=function(t){this.list.push(t)},SlideList.prototype.prepend=function(t){this.list.unshift(t),this._set({h:this.current.h+1,v:0})},SlideList.prototype.insert=function(t,e){this.getType(e.h);this.list.splice(e.h,0,t),e.h<=this.current.h&&this._set({h:this.current.h+1,v:0})},SlideList.prototype.insertNested=function(t,e){var i=this.getType(e.h);"list"===i&&"string"==typeof t&&(this.list[e.h].splice(e.v,0,t),e.h===this.current.h&&e.v<=this.current.v&&this._set({h:e.h,v:this.current.v+1}))},SlideList.prototype.replace=function(t,e){var i,n;"string"==typeof t?(i=t,t=this.getIndex(t)):i=this.get(t.h,t.v),n=this.getType(t.h),"list"===n&&"string"==typeof e?this.list[t.h].splice(t.v,1,e):"item"!==n&&"list"!==n||this.list.splice(t.h,1,e)},SlideList.prototype.remove=function(t){var e,i;"string"==typeof t?(e=t,t=this.getIndex(t)):e=this.get(t.h,t.v),i=this.getType(t.h),"list"===i&&void 0!==t.v?this.list[t.h].splice(t.v,1):"item"!==i&&"list"!==i||this.list.splice(t.h,1)},SlideList.prototype.move=function(t,e){var i,n,s,r;"string"==typeof t?(i=t,t=this.getIndex(t)):i=this.get(t.h,t.v),"string"==typeof e?(n=e,e=this.getIndex(e)):n=this.get(e.h,e.v),s=this.getType(t.h),r=this.getType(e.h),"item"===s&&"item"===r?(t.h<e.h&&(e.h=e.h-1),this.list.splice(t.h,1),this.list.splice(e.h,0,i)):"item"===s&&"list"===r?void 0!==e.v?(t.h<e.h&&(e.h=e.h-1),this.list.splice(t.h,1),this.list[e.h].splice(e.v,0,i)):(t.h<e.h&&(e.h=e.h-1),this.list.splice(t.h,1),this.list.splice(e.h,0,i)):"list"===s&&"item"===r?void 0!==t.v?(this.list[t.h].splice(t.v,1),this.list.splice(e.h,0,i)):(t.h<e.h&&(e.h=e.h-1),this.list.splice(t.h,1),this.list.splice(e.h,0,i)):"list"===s&&"list"===r&&(void 0!==e.v&&void 0!==t.v?(t.h===e.h&&t.v<e.v&&(e.v=e.v-1),this.list[t.h].splice(t.v,1),this.list[e.h].splice(e.v,0,i)):void 0===e.v&&void 0===t.v?(t.h<e.h&&(e.h=e.h-1),this.list.splice(t.h,1),this.list.splice(e.h,0,i)):void 0===e.v&&void 0!==t.v&&(this.list[t.h].splice(t.v,1),this.list.splice(e.h,0,i)))},app.model=function(){function t(t,e){var n=app.cache.get(t);e=e||function(){},n?(i(JSON.parse(n)),e()):app.util.getFile(t,function(n,s){if(n)throw new Error("Unable to fetch model "+t,n);i(JSON.parse(s)),e()})}function e(t){t=t||"local",app.model.trigger("save",{model:j,storage:t})}function i(t){if(!(t.slides&&t.structures&&t.storyboard))throw new Error("Presentation model is incorrectly formatted");j=t,app.model.trigger("set:model")}function n(){return j}function s(t){return!!j.slides[t]&&j.slides[t]}function r(t){return!!j.modules[t]&&j.modules[t]}function a(t){return j.slides[t]?j.slides[t]:!!j.modules[t]&&j.modules[t]}function o(t){return j.slides[t]?j.slides[t]:j.modules[t]?j.modules[t]:j.structures[t]?j.structures[t]:!!j.storyboards[t]&&j.storyboards[t]}function p(t){return!!j.structures[t]&&j.structures[t]}function l(t){return!t&&j.storyboard?j.storyboard:!(!j.storyboards||!j.storyboards[t])&&j.storyboards[t]}function c(t,e){if(!t)return!1;var i=p(t)||l(t);return i?i.content.indexOf(e):void 0}function d(t){return!!(j.slides[t]||j.structures[t]||j.modules[t]||j.storyboards[t])}function u(t){return!!j.slides[t]}function h(t){return!!j.structures[t]}function f(t){return!!j.storyboards[t]}function v(t){return!(!j.storyboards[t]||!j.storyboards[t].linear)}function g(t,e){j.storyboards[t]&&(j.storyboards[t].linear=e)}function m(t){var e=o(t);return!!e.shareable}function w(t){var e;j.slides[t]?e=j.slides[t]:j.structures[t]?e=j.structures[t]:j.storyboards[t]&&(e=j.storyboards[t]),e.shareable||(e.shareable={})}function y(t){var e;j.slides[t]?e=j.slides[t]:j.structures[t]?e=j.structures[t]:j.storyboards[t]&&(e=j.storyboards[t]),e.shareable&&delete e.shareable}function b(t,e){u(t)||(j.slides[t]=e)}function S(t,e){if(!e.name||!e.content)throw new Error("Name and content must be specified when adding structure");h(t)||(j.structures[t]=e)}function E(t,e){if(!e.name||!e.content)throw new Error("Name and content must be specified when adding storyboard");f(t)||(j.storyboards[t]=e)}function _(t){f(t)&&delete j.storyboards[t]}function L(t,e,i){var n=l(t)||p(t);b(e.id,e),n&&n.content.splice(i,0,e.id)}function x(t,e,i,n){var s=l(t);b(e.id,e),s&&s.content.splice(i,1,n)}function T(t,e,i){var n,s=l(t)||p(t);s&&(n=s.content,"string"==typeof n[i.h]?h(n[i.h])?(s=p(n[i.h]),s.content.splice(i.v,1)):n.splice(i.h,1):(n=n[i.h],n.splice(i.v,1)))}function k(t,e,i){var n=o(t);n&&n[e]&&(n[e]=i)}function P(t,e){var i=l(t)||p(t);i&&e&&(i.content=e,app.model.trigger("update:content",{id:t,content:e}))}var j={slides:{},modules:{},structures:{},storyboard:[],storyboards:{}};return{save:e,set:i,get:n,getSlide:s,getModule:r,getView:a,getItem:o,getStructure:p,getStoryboard:l,getSlidePosition:c,fetch:t,exist:d,hasSlide:u,hasStructure:h,hasStoryboard:f,isLinear:v,setLinear:g,isShareable:m,setShareable:w,removeShareable:y,addSlide:b,addStructure:S,addStoryboard:E,insertSlide:L,insertNested:x,removeSlide:T,update:k,updateContent:P,deleteStoryboard:_}}(),app.util.extend(app.model,app.events),app.slideshow=function(t){function e(e){var i=t.getStoryboard(e),n=i.content||[];n.length&&(B[e]={},n.forEach(function(i,n){t.hasStructure(i)&&(t.hasSlide(i)||(B[e][n]=i))}))}function i(e,i){i=i||t.getStoryboard(e);var n=i.content||[],s=n.slice(0);if(B[e]={},i)s.forEach(function(i,n){t.hasStructure(i)&&(t.hasSlide(i)||(s.splice(n,1,t.getStructure(i).content.slice(0)),B[e][n]=i))}),R[e]=new SlideList(s),app.dom.add(e,app.dom.get("wrapper")),app.dom.trigger("new:elements",{views:[{id:e,parent:"presentation"}]});else{if(i=t.getItem(e),!i)throw new Error('Not able to find content with id "'+e+'"');i.content?R[e]=new SlideList(i.content):R[e]=new SlideList([e])}if(R[e])return R[e]}function n(e){var n,s,r={},a={},o=b(e),c=o.slideshow,d=o.chapter,u=o.slide,h={h:0,v:0},f=!1;c!==F&&(s=app.view.get(F),f=!0,s&&s.onExit&&s.onExit(),app.slideshow.trigger("unload",{id:F})),F&&(r={index:l(),id:p(),classes:["present","past"]}),R[c]||i(c),R[c]&&(F=c,u&&!d?h=l(u):d&&!u?h.h=y(d,c):d&&u&&(t.isLinear(c)?h=l(u):(h.h=y(d,c),h.v=t.getSlidePosition(d,u),h.v=h.v>-1?h.v:0)),R[c].goTo(h),a={index:l(),id:p(),classes:["past future","present"]},f&&(app.slideshow.trigger("load",{id:c}),n=app.view.get(c),n&&n.onEnter&&n.onEnter()),app.slideshow.trigger("update:current",{current:a,prev:r}))}function s(t){n(t)}function r(t){var t=t||F;if(t)return R[t]}function a(t){var e=l();R[t.id]&&(i(t.id),g({h:0,v:0},e))}function o(){if(F)return F}function p(t,e){if(F)return R[F].get(t,e)}function l(t){if(F)return R[F].getIndex(t)}function c(){if(F)return R[F].list.length}function d(){if(F)return R[F].size()}function u(t){if(F)return R[F].getType(t)}function h(t,e){return JSON.stringify(t)===JSON.stringify(e)}function f(t,e,i){var n="",s="";return i||t.h===e.h&&(t.v>0||e.v>0)&&(n+="stack"),h(e,t)?(s+=" past future archived",n+=" present"):t.h<e.h?(s+=" past present stack archived",n+=" future"):t.h===e.h&&t.v<e.v?(s+=" past present archived",n+=" future"):(s+=t.h===e.h?" past present archived":" present future stack archived",n+=" past"),[s,n]}function v(t){var e=R[F].getNeighbors(),i=[],n=app.slideshow.getIndex(),s=R[F].getIndex(t.prevId),r=app.model.isLinear(F);e.forEach(function(e){var a,o;e&&!h(s,e)&&(a=R[F].get(e.h,e.v),o={index:e,id:a,classes:f(n,e,r)},a!==t.id&&i.push(o))}),app.slideshow.trigger("load:neighbors",{updates:i})}function g(t,e){var i,n,s=app.model.isLinear(F);i={index:t,id:R[F].get(),classes:f(t,t,s)},e&&(n={index:e,id:R[F].get(e.h,e.v),classes:f(t,e,s)}),app.slideshow.trigger("update:current",{current:i,prev:n})}function m(){var t=F,e=B[F][l().h]||null;return e&&(t+="/"+e),t+="/"+R[F].get()}function w(t,e,i){var n,s,i=i||F,r=app.model.isLinear(i);if(!R[i])throw new Error("Slideshow is not initialized. Use 'app.slideshow.init(id)' to initialize.");s=l(),r&&("right"===t&&(t="next"),"left"===t&&(t="previous")),R[i][t].apply(R[i],e),n=l(),g(n,s),R[i].isEqual.apply(R[i],[s,n])?("next"!==t&&"right"!==t||app.slideshow.trigger("slideshow:end",{id:i}),"down"===t&&app.slideshow.trigger("chapter:end",{id:i}),"previous"!==t&&"left"!==t||app.slideshow.trigger("slideshow:start",{id:i}),"up"===t&&app.slideshow.trigger("chapter:start",{id:i})):app.slideshow.trigger("navigate")}function y(e,i){if(!e)return-1;var i=i||F,n=t.getStoryboard(i);return n?n.content.indexOf(e):-1}function b(e){e=e||app.getPath();var i,n,s,r=e.split("/");return""===r[0]&&r.splice(0,1),i=r[0],r[2]?(n=r[1],s=r[2]):r[1]?t.hasStructure(r[1])&&!t.hasSlide(r[1])?(n=r[1],s=null):(n=null,s=r[1]):(n=null,s=null),{slideshow:i,chapter:n,slide:s}}function S(e){if(!e)return!1;var i=!0,n=b(e);return n.slideshow?(t.hasStoryboard(n.slideshow)||(i=!1),t.hasSlide(n.slideshow)&&(i=!0)):i=!1,n.chapter&&!t.hasStructure(n.chapter)&&(i=!1),n.slide&&!t.hasSlide(n.slide)&&(i=!1),i}function E(t,e,i){var n,s,i=i||F,r={h:0,v:0};if(e){if(n=y(e,i),n<0)return!1;if(r.h=n,s=R[i].list[n].indexOf(t),s<0)return!1;r.v=s}else if(s=R[i].getIndex(t))r=s;else{if(n=y(t,i),n<0)return!1;r.h=n}return w("goTo",[r],i),!0}function _(t,e){w(t,[],e)}function L(){_("next")}function x(){_("previous")}function T(){_("left")}function k(){_("right")}function P(){_("up")}function j(){_("down")}function I(){_("gotoFirst")}function N(){_("gotoLast")}function q(t,e,i){var i=i||F;R[i]&&R[i][t].apply(R[i],e)}function A(t,e){q("append",[t],e)}function M(t,e){q("prepend",[t],e)}function O(t,e,i){q("insert",[t,e],i)}function C(t,e,i){q("insertNested",[t,e],i)}function $(t,e,i){q("move",[t,e],i)}function H(t,e,i){q("replace",[t,e],i)}function z(t,e){q("remove",[t],e),p()?update({index:l(),id:p(),classes:["past future","present"]}):x()}if(!t)throw new Error("app.model module is required for app.slideshow");var s,R={},F="",B={};return app.listenTo(app.model,"update:content",a),app.on("enter:element",v),{init:i,load:s,__load__:n,inspect:r,getId:o,get:p,getIndex:l,getPath:m,getLength:c,getSize:d,getType:u,resolve:b,pathExist:S,updateChapterMap:e,goTo:E,step:_,next:L,prev:x,left:T,right:k,up:P,down:j,first:I,last:N,prepend:M,append:A,insert:O,insertNested:C,move:$,remove:z,replace:H}}(app.model),app.util.extend(app.slideshow,app.events),app.dom=function(t,e,i){function n(t){return t&&T[t]?T[t]:t?void 0:T}function s(t,e){e&&!T[t]&&(T[t]=e)}function r(){return P}function a(t){P=t}function o(t,e,i){var n=[];return t?(n=t.className.split(" "),e.forEach(function(t){var e=n.indexOf(t);e>-1&&n.splice(e,1)}),i.forEach(function(t){var e=n.indexOf(t);e===-1&&t&&n.push(t)}),n):[]}function p(){var t=app.slideshow.get(),e=O?O.id:null;M&&M.id===t||(O=M||null,M=T[t],$&&clearTimeout($),P.push(t),app.dom.trigger("element:enter",{id:t,prevId:e}),app.trigger("enter:element",{id:t,prevId:e}))}function l(t){$&&clearTimeout($),$=setTimeout(function(){p()},q)}function c(t){return new Promise(function(n,s){var r,a=t.id,o=t.model||{},p=t.type||"slide",l="",c=null,h=null,f=app.registry.get(a)||{},v=e.get("lazy");T[t.id]&&n(T[t.id]);var g=function(t){if(t&&"<"===t.trim().charAt(0)||n(null),app.lang){var e="module"===p?I:j;e=e.replace("<id>",a)+"translations/"+app.lang+".json";var i=app.cache.get(e);i&&(t=app.template(t,JSON.parse(i)))}else H&&(t=H(t));if(h=z.parse(t),c=h.querySelector("#"+a)||h,!c.tagName&&(c=document.createElement("article"),c.setAttribute("id",a),"slide"===p&&c.classList.add("slide"),c.appendChild(h),c.querySelector("title")))return console.error("Could not get template for "+a),void n(null);if(i){i=JSON.parse(i);var s=c.querySelectorAll("[data-ag-local]");app.util.toArray(s).forEach(function(t){var e=t.getAttribute("data-ag-local");i[e]&&(t.innerHTML=i[e])})}n(c)};f.hasOwnProperty("template")?g(f.template):(f.hasOwnProperty("templateUrl")?l=f.templateUrl:o.files&&o.files.templates&&(l=o.files.templates[0],/\.html$/.test(l)||(l=l.split("."),l[l.length-1]="html",l=l.join("."))),app.cache.exist(l)?g(app.cache.get(l)):l?i.getFile(l,function(t,e){t||!e?("slide"===p&&(c=document.createElement("div"),c.id=a,c.classList.add("slide"),c.innerHTML="<h2>Missing template: "+l+"</h2>"),n(c)):v?g(e):(r=u([{id:a}],!0),r.length?d(r).then(function(){g(e)}):g(e))}):n(null))})}function d(t){return new Promise(function(e,i){t.reduce(function(t,e){return t.indexOf(e)<0&&t.push(e),t},[]);head.load(t,function(){e()})})}function u(t,i){var n=[];return i=i||e.get("lazy"),i&&t.forEach(function(t){var e=t.id;if(t.getAttribute&&(e=t.getAttribute("data-module")||t.getAttribute("data-partial")),!k[e]){var i=app.model.getView(e)||null,s=[];i&&i.files&&(i.files.styles=i.files.styles||[],i.files.scripts=i.files.scripts||[],s=s.concat(i.files.styles),s=s.concat(i.files.scripts)),n=n.concat(s)}}),n}function h(t){return new Promise(function(e,i){var n=t.getAttribute("data-module")||t.getAttribute("data-partial"),s=app.model.getView(n),r=n+"_"+(new Date).getTime(),a=t.id||r,o=t.hasAttribute("data-module")?"module":"slide";c({id:n,model:s,type:o}).then(function(i){i&&(i.id&&i.removeAttribute("id"),t.appendChild(i)),t.id=a,T[a]=t,e({id:a,moduleId:n,el:i})},function(i){t.id=a,T[a]=t,e({id:a,moduleId:n,el:null})})})}function f(t){var e=Array.prototype.slice.call(t.querySelectorAll("[data-module]")),i=Array.prototype.slice.call(t.querySelectorAll("[data-partial]")),n=e.concat(i);return n}function v(t,e){var i,n=f(t);n.length&&(i=u(n),i.length?d(i).then(function(){n.forEach(function(t){h(t).then(function(t){t.el&&v(t.el,t.id),app.dom.trigger("new:elements",{views:[{id:t.id,scriptId:t.moduleId,parent:e,el:t.el}]})})})}):n.forEach(function(t){h(t).then(function(t){t.el&&v(t.el,t.id),app.dom.trigger("new:elements",{views:[{id:t.id,scriptId:t.moduleId,parent:e,el:t.el}]})})}))}function g(t,e){var i=n(t);e=e||T.slides,i&&(e.appendChild(i),app.dom.trigger("new:elements",{views:[{id:t}]}))}function m(t,e){e=e||T.slides;var i=T[t]||null;i&&e.removeChild(i),T[t]=null,k[t]=!1}function w(t){T[t]&&T[t].classList.add("archived")}function y(t,e,i){e=e||!1,i=i||T.slides,S(t).then(function(t){b(t.elements,t.views,e,i)})}function b(t,e,i,n){n.appendChild(t),app.dom.trigger("new:elements",{views:e}),i&&(E(),$&&clearTimeout($),$=setTimeout(function(){p()},q))}function S(t){return new Promise(function(e,i){var n=document.createDocumentFragment(),s=0,r=t.length,a=[],p=u(t)||[],l=function(){s+=1,s===r&&(p.length?d(p).then(function(){e({elements:n,views:a})}):e({elements:n,views:a}))},g=function(t,e){var i=f(t);i.length&&(r+=i.length,p=p.concat(u(i)),i.forEach(function(t){h(t).then(function(t){t.el&&v(t.el,t.id),a.push({id:t.id,scriptId:t.moduleId,parent:e}),l()},function(){l()})}))};t.forEach(function(t){var e=app.model.getSlide(t.id),i=[];k[t.id]?(T[t.id]&&t.classes&&(i=o(T[t.id],t.classes[0].split(" "),t.classes[1].split(" ")),T[t.id].className=i.join(" ")),l()):(k[t.id]=!0,c({id:t.id,model:e,type:"slide"}).then(function(e){return e?(t.classes&&(i=o(e,t.classes[0].split(" "),t.classes[1].split(" ")),e.className=i.join(" ")),T[t.id]=e,g(e,t.id),n.appendChild(e),a.push({id:t.id,parent:"presentation"}),void l()):new Error("Not able to get element for "+t.id)},function(e){l(),console.log("Not able to get element for",t.id)}))})})}function E(t){var t=t||app.slideshow.get(),e=T[t],i=e.getAttribute("data-state");C&&C.split(" ").forEach(function(t){document.documentElement.classList.remove(t)}),i&&(C=i,i.split(" ").forEach(function(t){document.documentElement.classList.add(t)}))}function _(t){var e=[];T[t.current.id]||y([t.current],!0);var i=function(t,e,i,n){if(!n){t.setAttribute("data-incorrect","ultra");var s=function(t){t.classList.contains("past")&&(t.classList.remove("past"),t.classList.add("future"))},r=function(t){t.classList.contains("future")&&(t.classList.remove("future"),t.classList.add("past"))},a=function(t){t.classList.contains("stack")||t.classList.add("stack")},o=function(t){t.classList.contains("stack")&&t.classList.remove("stack")};i.h>e.h?(o(t),s(t)):i.h<e.h?(o(t),r(t)):i.v<e.v?(a(t),r(t)):i.v>e.v&&(a(t),s(t))}},n=function(){l(t.current.id),e=o(T[t.current.id],t.current.classes[0].split(" "),t.current.classes[1].split(" ")),T[t.current.id].className=e.join(" "),E(t.current.id)},s=function(t){t?document.getElementById("presentation").setAttribute("data-transition-speed",app.config.get("transitionSpeed")):document.getElementById("presentation").setAttribute("data-transition-speed","ultra")};if(T[t.prev.id]&&(s(!0),e=o(T[t.prev.id],t.prev.classes[0].split(" "),t.prev.classes[1].split(" ")),T[t.prev.id].className=e.join(" ")),M&&T[t.current.id]){var r=new Date,a=T[t.current.id];if(r-A<400)s(!1),x&&clearTimeout(x),n();else{var c=t.prev.id==t.current.id;i(a,t.prev.index,t.current.index,c),x=setTimeout(function(){s(!0),a.removeAttribute("data-incorrect"),n()},20)}A=r}else T[t.current.id]&&($&&clearTimeout($),p(),E(t.current.id))}function L(t){var e,i=[],n=!1,s=t.updates.length,r=function(t){var e=T[t.id],n=[];e?(n=o(e,t.classes[0].split(" "),t.classes[1].split(" ")),e.className=n.join(" ")):(i.push(t),P.push(t.id))};for(e=0;e<s;e++)r(t.updates[e]);y(i,n)}if(!t||!e||!i)throw new Error("app.dom requires following modules to be loaded: app.slideshow, app.config, app.util");app.listenTo(app.slideshow,"update:current",function(t){_(t)}),app.listenTo(app.slideshow,"load:neighbors",function(t){L(t)});var x,T=e.get("cachedElements"),k={},P=[],j=e.pluck("paths","slides")||"slides/<id>/",I=e.pluck("paths","modules")||"modules/<id>/",N=e.get("transitionSpeed")||"default",q=800,A=(document.createElement("div"),new Date);T||(T={},T.theme=document.querySelector("#theme"),T.wrapper=document.querySelector(".accelerator"),T.slides=document.querySelector(".accelerator .slides"));var M,O,C,$;"default"!==N&&("fast"===N?q=300:"slow"===N&&(q=1100));var H=(function(){var t=document.createElement("div");return void 0!==t.style.transform?"transform":void 0!==t.style.webkitTransform?"-webkit-transform":void 0!==t.style.MozTransform?"-moz-transform":void 0!==t.style.msTransform?"-ms-transform":void 0!==t.style.OTransform?"-o-transform":"transform"}(),function(){var t=app.registry.get("templateParser");return t}()),z=function(){function t(t,e){if("string"!=typeof t)throw new TypeError("String expected");e||(e=document);var i=/<([\w:]+)/.exec(t);
if(!i)return e.createTextNode(t);t=t.replace(/^\s+|\s+$/g,"");var s=i[1];if("body"==s){var r=e.createElement("html");return r.innerHTML=t,r.removeChild(r.lastChild)}var a=n[s]||n._default,o=a[0],p=a[1],l=a[2],r=e.createElement("div");for(r.innerHTML=p+t+l;o--;)r=r.lastChild;if(r.firstChild==r.lastChild)return r.removeChild(r.firstChild);for(var c=e.createDocumentFragment();r.firstChild;)c.appendChild(r.removeChild(r.firstChild));return c}var e=document.createElement("div");e.innerHTML='  <link/><table></table><a href="/a">a</a><input type="checkbox"/>';var i=!e.getElementsByTagName("link").length;e=void 0;var n={legend:[1,"<fieldset>","</fieldset>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],_default:i?[1,"X<div>","</div>"]:[0,"",""]};return n.td=n.th=[3,"<table><tbody><tr>","</tr></tbody></table>"],n.option=n.optgroup=[1,'<select multiple="multiple">',"</select>"],n.thead=n.tbody=n.colgroup=n.caption=n.tfoot=[1,"<table>","</table>"],n.text=n.circle=n.ellipse=n.line=n.path=n.polygon=n.polyline=n.rect=[1,'<svg xmlns="http://www.w3.org/2000/svg" version="1.1">',"</svg>"],{parse:t}}();return{get:n,add:s,getElement:c,getHistory:r,setHistory:a,load:S,insert:y,remove:m,archive:w,render:g,resolveModules:v,parse:z.parse}}(app.slideshow,app.config,app.util),app.util.extend(app.dom,app.events),app.slide=app.view=app.module=function(t){function e(t,e){return Object.hasOwnProperty.call(t,e)}function i(t,e){var i=t?t[e]:void 0;return"function"==typeof i?t[e]():i}function n(t){var t=t||T;if(t)return k[t]}function s(t){if(!t)throw new Error("Need to supply slide id when creating new instance");var e=app.model.getView(t);if(e)return e}function r(t){t.views.length&&t.views.forEach(function(t){p(t)})}function a(t){return new Promise(function(e,i){k[t]?e(k[t]):app.dom.load([{id:t,classes:["present","future"]}]).then(function(t){e(t.views[0])})})}function o(t,e){var i,n=k[e.id];n&&(e.parent&&(i=k[e.parent],i||(i=k.presentation),n.states&&i._injectStates(e.id,n.states),n.parentId=e.parent,n.parent=i,i.children&&i.children.push(e.id)),n.onRender&&n.onRender(t),app.slide.trigger("slide:render",{id:e.id}))}function p(e){var i,n,s=t.get(e.id)||null,r={},a=e.scriptId||e.id;if(!k[e.id]){if(s||(s=document.getElementById(e.id)),r.el=s,r.id=e.id,r.scriptId=a,e.el&&(r.$el=e.el),n=app.registry.get(a)||{},n.children=[],n&&app.util.extend(r,n),i=_.extend(r),k[e.id]=new i,k[e.id].publish){k[e.id].props=app.util.extend({},k[e.id].publish);for(prop in k[e.id].props)k[e.id].props[prop]&&k[e.id].props[prop].push&&(k[e.id].props[prop]=k[e.id].props[prop][0])}else k[e.id].props={};app.util.toArray(s.attributes).forEach(function(t){var i=t.name.replace(/-([a-z])/g,function(t){return t[1].toUpperCase()});k[e.id].props[i]=t.value||!0})}window.setTimeout(function(){o(s,e)})}function l(e,i){var n=e.id,s=k[n]||{},r=t.get(n)||null,i=i||!1;return T&&!i&&c(T),s&&(0===s._state&&r.classList.add("state-default"),s.onEnter&&s.onEnter(r),s.children&&s.children.length&&s.children.forEach(function(t){l({id:t},!0)})),i||(T=n,app.slide.trigger("slide:enter",e)),s}function c(e,i){var n=k[e]||{},s=t.get(e)||null,i=i||!1;return n&&(n.onExit&&n.onExit(s),n.children&&n.children.length&&n.children.forEach(function(t){c(t,!0)})),i||app.slide.trigger("slide:exit",{id:e}),n}function d(){var t;if(T)return t=k[T],!!t&&t.next()}function u(){var t;if(T)return t=k[T],!!t&&t.previous()}function h(t){var e;T&&(e=k[T],e.goTo(t))}function f(t){var e;T&&(e=k[T],e.toggle(t))}function v(){var t;T&&(t=k[T],t.reset())}function g(t,e){var i=k[t]||null;i&&(e||i.el.classList.contains("archived"))&&(i.children&&i.children.forEach(function(t){app.slide.remove(t,!0)}),i._removeElement())}var m=/^\s*</,w="undefined"!=typeof Element&&Element.prototype||{},y=w.addEventListener||function(t,e){return this.attachEvent("on"+t,e)},b=w.removeEventListener||function(t,e){return this.detachEvent("on"+t,e)},S=function(t,e){for(var i=0,n=t.length;i<n;i++)if(t[i]===e)return i;return-1},E=w.matches||w.webkitMatchesSelector||w.mozMatchesSelector||w.msMatchesSelector||w.oMatchesSelector||function(t){var e=(this.parentNode||document).querySelectorAll(t)||[];return~S(e,this)};app.extend=function(t,i){var n,s=this;n=t&&e(t,"constructor")?t.constructor:function(){return s.apply(this,arguments)},app.util.extend(n,s,i);var r=function(){this.constructor=n};return r.prototype=s.prototype,n.prototype=new r,t&&app.util.extend(n.prototype,t),n.__super__=s.prototype,n};var _=function(t){t&&Object.keys(t).forEach(function(e){x.indexOf(e)!==-1&&(this[e]=t[e])},this),this._elements={},this._ensureElement(),this._createStateMap(),this.initialize.apply(this,arguments)},L=/^(\S+)\s*(.*)$/,x=["model","collection","el","id","attributes","className","tagName","events"];if(app.util.extend(_.prototype,app.events,{initialize:function(){},_state:0,states:[],rotate:!1,_domEvents:[],$:function(t){var e="querySelectorAll";return this._elements[t]?this._elements[t]:("#"===t[0]&&(e="querySelector"),"<"===t[0]?app.dom.parse(t):(this._elements[t]=this.el[e](t),this._elements[t]))},getState:function(){return this.states[this._state-1]?this.states[this._state-1]:null},addState:function(t){t.id&&this.states.push(t),this._createStateMap()},stateIs:function(t){var e=this.states[this._state-1];return!(!e||e.id!==t)},stateIsnt:function(t){var e=this.states[this._state-1];return!e||e.id!==t},next:function(){return this._setState(this._state+1)},previous:function(){return this._setState(this._state-1)},goTo:function(t,e){var i;"string"==typeof t?(i=this._stateMap[t],i>0&&this._setState(i,e)):this._setState(t,e)},getIndex:function(t){return t?this._stateMap[t]?this._stateMap[t]:-1:this._state},reset:function(){this._setState(0)},toggle:function(t){var e=this._stateMap[t];e===this._state?this.reset():this.goTo(t)},_setState:function(t,e){var i,n=this._state,s={},r=this.states[t-1];if(t!==n)return this.states.length&&(r||0===t)?(i=r||{id:null},n&&(s=this.states[n-1],this.el.classList.remove("state-"+s.id),s.onExit&&s.onExit.bind(this)(i.id),0===t&&(this.el.classList.add("state-default"),this.trigger("reset",{id:s.id,next:i.id,view:this.id}),app.slide.trigger("reset",{id:s.id,next:i.id,view:this.id}))),this._state=t,0!==t&&(s.id=s.id||null,this.el.classList.remove("state-default"),this.el.classList.add("state-"+i.id),i.onEnter&&i.onEnter.bind(this)(s.id,e),this.trigger("state:enter",{id:i.id,previous:s.id,view:this.id,data:e}),app.slide.trigger("state:enter",{id:i.id,previous:s.id,view:this.id,data:e})),!0):!(!this.rotate||t!==this.states.length+1)&&(this._setState(1),!0)},_injectStates:function(t,e){var i=this._stateMap[t],n=this,s=1;i&&(e.forEach(function(e,r){n.states.splice(i+r-1,s,{id:t+"-"+e.id,onEnter:function(e,i){var n=app.slide.get(t);n.goTo(r+1)},onExit:function(e,i){var n=app.slide.get(t);n.reset()}}),0===r&&(s=0)}),this._createStateMap())},_createStateMap:function(){var t=this;this._stateMap={},this.states.forEach(function(e,i){e.id&&(t._stateMap[e.id]=i+1),e.include&&(t._stateMap[e.include]=i+1)})},_removeElement:function(){var e=this;this.onRemove&&this.onRemove(this.el),this.undelegateEvents(),Object.keys(this._elements).forEach(function(t){e._elements[t]=null}),t.remove(this.id,this.el.parentNode),k[this.id]=null},_setElement:function(t){if("string"==typeof t)if(m.test(t)){var e=document.createElement("div");e.innerHTML=t,this.el=e.firstChild}else this.el=document.querySelector(t);else this.el=t},setElement:function(t){return this.undelegateEvents(),this._setElement(t),this.delegateEvents(),this},_setAttributes:function(t){for(var e in t)e in this.el?this.el[e]=t[e]:this.el.setAttribute(e,t[e])},delegate:function(t,e,i){"function"==typeof e&&(i=e,e=null);var n=this.el,s=e?function(t){for(var s=t.target||t.srcElement;s&&s!=n;s=s.parentNode)E.call(s,e)&&(t.delegateTarget=s,i(t))}:i;return y.call(this.el,t,s,!1),this._domEvents.push({eventName:t,handler:s,listener:i,selector:e}),s},delegateEvents:function(t){if(!t&&!(t=i(this,"events")))return this;this.undelegateEvents();for(var e in t){var n=t[e];"function"!=typeof n&&(n=this[t[e]]?this[t[e]]:function(){});var s=e.match(L);this.delegate(s[1],s[2],n.bind(this))}return this},undelegate:function(t,e,i){if("function"==typeof e&&(i=e,e=null),this.el)for(var n=this._domEvents.slice(),s=0,r=n.length;s<r;s++){var a=n[s],o=!(a.eventName!==t||i&&a.listener!==i||e&&a.selector!==e);o&&(b.call(this.el,a.eventName,a.handler,!1),this._domEvents.splice(S(n,a),1))}return this},undelegateEvents:function(){if(this.el){for(var t=0,e=this._domEvents.length;t<e;t++){var i=this._domEvents[t];b.call(this.el,i.eventName,i.handler,!1)}this._domEvents.length=0}return this},_createElement:function(t){return document.createElement(t)},_ensureElement:function(){if(this.el)this.setElement(i(this,"el"));else{var t=app.util.extend({},i(this,"attributes"));this.id&&(t.id=i(this,"id")),this.className&&(t["class"]=i(this,"className")),this.setElement(this._createElement(i(this,"tagName"))),this._setAttributes(t)}}}),_.extend=app.extend,!t)throw new Error("app.dom module is required for app.slide");var T,k={};return app.listenTo(t,"new:elements",r),app.listenTo(t,"element:enter",l),{get:n,getInstance:s,load:a,remove:g,views:k,enter:l,exit:c,next:d,prev:u,goTo:h,toggle:f,reset:v}}(app.dom),app.util.extend(app.slide,app.events),app.history=function(t){function e(t){a||(r=t?t+"&":"?",app.listenTo(app.slide,"slide:enter",s),app.listenTo(app.slide,"state:enter",i),app.listenTo(app.slide,"reset",n),a=!0)}function i(t){var e=app.slide.get();if(e&&e.stateIs(t.id)){var i={stateId:t.id,path:app.getPath()};window.history.replaceState(i,"",r+"path="+i.path+"&state="+t.id)}}function n(t){var e={stateId:"default",path:app.getPath()};window.history.replaceState(e,"",r+"path="+e.path)}function s(t){var e=app.slide.get().getState(),i={stateId:"default",path:app.getPath()};t.view&&(e=t);var n=r+"path="+i.path;e&&(i.stateId=e.id,n+="&state="+e.id),window.history.pushState(i,"",n)}if(!t)throw new Error("app.slide module is required for app.history");var r,a=!1;return window.addEventListener("popstate",function(t){t.state&&t.state.path!==app.getPath()&&app.goTo(t.state.path)}),{init:e,setStates:i}}(app.slide),app.template=function(){"use strict";var t={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g},e=/.^/,i={"\\":"\\","'":"'",r:"\r",n:"\n",t:"\t",u2028:"\u2028",u2029:"\u2029"};for(var n in i)i[i[n]]=n;var s=/\\|'|\r|\n|\t|\u2028|\u2029/g,r=function(n,r,a){t.variable=a;var o="__p+='"+n.replace(s,function(t){return"\\"+i[t]}).replace(t.escape||e,function(t,e){return"'+\n_.escape("+unescape(e)+")+\n'"}).replace(t.interpolate||e,function(t,e){return"'+\n("+unescape(e)+")+\n'"}).replace(t.evaluate||e,function(t,e){return"';\n"+unescape(e)+"\n;__p+='"})+"';\n";t.variable||(o="with(obj||{}){\n"+o+"}\n"),o="var __p='';var print=function(){__p+=Array.prototype.join.call(arguments, '')};\n"+o+"return __p;\n";var p=new Function(t.variable||"obj",o);if(r)return p(r);var l=function(t){return p.call(this,t)};return l.source="function("+(t.variable||"obj")+"){\n"+o+"}",l};return window._&&window._.template?window._.template:(window._={template:r},r)}(),app.msg=function(){function t(){var t=app.dom.get("wrapper"),e=document.createElement("ul");return e.classList.add("app-messages"),t.appendChild(e),e}function e(t,e){var i='<div class="notice-msg">'+e+"</div>";return i+='<div class="notice-accept"><button class="pure-button ag-button-success" onclick="app.msg.accept(\''+t+"')\">Got it!</button></div>"}function i(t,e){var i='<div class="confirm-msg">'+e+"</div>";return i+='<div class="confirm-cancel"><button class="pure-button ag-button-warning" onclick="app.msg.cancel(\''+t+"')\">Cancel</button></div>",i+='<div class="confirm-confirm"><button class="pure-button ag-button-success" onclick="app.msg.accept(\''+t+"')\">Confirm</button></div>"}function n(t){var e='<div class="notice-msg">'+t+"</div>";return e+='<div class="notice-accept"><button class="pure-button ag-button-success" onclick="app.msg.reload()">Refresh</button></div>'}function s(t){var e=document.getElementById("app-msg-"+t);app.msg.trigger("msg:confirmed",{id:t}),window.localStorage.setItem(t,!0),e&&u(e)}function r(t){var e=document.getElementById("app-msg-"+t);app.msg.trigger("msg:cancelled",{id:t}),e&&u(e)}function a(){window.location.reload()}function o(t,i){var n=window.localStorage.getItem(t);n||(i=e(t,i),h(t,i))}function p(t){var e="confirm_"+(new Date).getTime();return t=i(e,t),h(e,t),e}function l(t){t=n(t),f(t)}function c(t){f(t,"alert")}function d(t){f(t)}function u(t){t.classList.remove("active-msg"),setTimeout(function(){v.removeChild(t)},500)}function h(e,i,n){var s=document.createElement("li");s.id="app-msg-"+e,n&&s.classList.add(n+"-msg"),s.innerHTML=i,v||(v=t()),v.insertBefore(s,v.firstChild),setTimeout(function(){s.classList.add("active-msg")},100)}function f(e,i){var n=document.createElement("li");i&&n.classList.add(i+"-msg"),n.innerHTML=e,v||(v=t()),v.insertBefore(n,v.firstChild),setTimeout(function(){n.classList.add("active-msg")},100),setTimeout(function(){n.classList.remove("active-msg")},4e3),setTimeout(function(){v.removeChild(n)},4500)}var v;return{alert:c,notice:o,send:d,accept:s,confirm:p,update:l,cancel:r,reload:a}}(),app.util.extend(app.msg,app.events),app.$=function(){function t(t){t=t||"builder";var i="agnitio-info.js",n=window.location.pathname.match(/\/presentations\/([a-z0-9]+)\/index.html/);l=document.createElement("div"),l.setAttribute("id","builder-plugins"),document.body.appendChild(l),n&&n.length&&(p=n[1]),p&&(i="../viewer/js/agnitio-info.js"),window.CKEDITOR_BASEPATH="ckeditor/",a="https:"===window.location.protocol?"prod":"dev","prod"===a&&/sqa/.test(window.location.host)&&(a="qa"),head.load([i],function(){var i;window.agnitioInfo&&("Builder Online"===window.agnitioInfo.applicationName?i=window.agnitioInfo.socket[a]+"socket.io/socket.io.js":"Builder CLI"===window.agnitioInfo.applicationName&&(i=["http://"+(location.host||"localhost")+window.agnitioInfo.socket[a]+"socket.io/socket.io.js"],"develop"===t&&i.push("http://"+(location.host||"localhost").split(":")[0]+":35729/livereload.js")),i&&head.load(i,function(){e(t)}))})}function e(t){var e,i="?mode="+t;window.agnitioInfo.socket[a].length>1&&(e=window.agnitioInfo.socket[a]),app.lang&&(i+="&lang="+app.lang),o=app.$.socket=window.io(e),app.$.send=function(t,e){e=e||{},app.$.session&&app.$.session.user&&(e.user=app.$.session.user),p&&(e.pid=p),c&&(e.bundle=c),o.emit(t,e)},"builder"===t&&app.$.send("init:builder"),app.history.init(i),o.on("server:error",function(t){app.msg.alert(t.msg)}),o.on("server:confirmation",function(t){app.msg.alert(t.msg)}),o.on("load:plugins",function(t){app.$.trigger("load:plugins",t)}),o.on("initialize:builder",function(t){app.$.isBuilder=!0,document.childNodes[1].classList.add("mode-builder"),t.basePath&&(window.CKEDITOR_BASEPATH=t.basePath),t.tmpPath&&(app.$.tmpPath=t.tmpPath),app.$.trigger("load:plugins",t)})}function i(t){return new Promise(function(e,i){var n=app.registry.get(t);l=l||document.body,n&&!app.$[t]?(app.dom.insert([{id:t}],!1,l),e(!0)):app.$[t]?e(app.$[t]):i("No plugin named "+t+" is registered")})}function n(t,e){ag&&ag.msg.send({name:t,value:e})}function s(){var t={presentation:{styles:["accelerator/css/styles.css","templates/master/**/*.{css,styl}","modules/**/*.{css,styl}","slides/**/*.{css,styl}","shared/**/*.{css,styl}"],scripts:["accelerator/lib/head.min.js","accelerator/js/init.js","templates/master/**/*.{js,coffee}","modules/**/*.{js,html,json,coffee,md,jade}","slides/**/*.{js,html,json,coffee,md,jade}","slides/**/translations/*.json","shared/**/*.{js,html,json,coffee,md,jade}","config.json","presentation.json","references.json"]}},e=app.config.get("bundle")||t;return e}function r(t){t.data&&("string"==typeof t.data&&(t.data=JSON.parse(t.data)),app.$.trigger(t.data.name,t.data))}var a,o,p,l,c=s();return window.addEventListener("message",r,!1),{isBuilder:!1,init:t,load:i,send:n}}(),app.util.extend(app.$,app.events),app.$.on("load:plugins",function(t){t.files&&head.load(t.files,function(){setTimeout(function(){app.$.trigger("plugins:ready")},500)})}),app.$.on("navigate",function(t){t.data&&t.data.path&&app.goTo(t.data.path)}),function(){function t(t){var e=location.hash.substring(1);app.goTo(e)}function e(t){var e=(document.activeElement,!(!document.activeElement||!document.activeElement.type&&!document.activeElement.href&&"inherit"===document.activeElement.contentEditable)),i=app.config.get("keyboard");if(!(e||t.shiftKey&&32!==t.keyCode||t.altKey||t.ctrlKey||t.metaKey)){if(app.isPaused&&[66,190,191].indexOf(t.keyCode)===-1)return!1;var n=!1;if(i&&"object"==typeof i)for(var s in i)if(parseInt(s,10)===t.keyCode){var r=i[s];"function"==typeof r?r.apply(null,[t]):"string"==typeof r&&"function"==typeof app[r]&&app[r].call(),n=!0}if(n===!1)switch(n=!0,t.keyCode){case 33:app.prev();break;case 34:app.next();break;case 37:app.slideshow.step("left");break;case 39:app.slideshow.step("right");break;case 38:app.slideshow.step("up");break;case 40:app.slideshow.step("down");break;case 36:app.slideshow.first();break;case 35:app.slideshow.last();break;case 32:t.shiftKey?app.prev():app.next();break;case 190:case 191:app.togglePause();break;case 70:app.fullScreen();break;default:n=!1}n&&t.preventDefault()}}function i(){document.addEventListener("swipeleft",app.slideshow.right),document.addEventListener("swiperight",app.slideshow.left),document.addEventListener("swipeup",app.slideshow.down),document.addEventListener("swipedown",app.slideshow.up)}function n(){document.removeEventListener("swipeleft",app.slideshow.right),document.removeEventListener("swiperight",app.slideshow.left),document.removeEventListener("swipeup",app.slideshow.down),document.removeEventListener("swipedown",app.slideshow.up)}function s(t){var e=app.config.get();app.model.isLinear(t.id)?(document.removeEventListener("swipeup",app.slideshow.down),document.removeEventListener("swipedown",app.slideshow.up)):e.touch&&(document.removeEventListener("swipeup",app.slideshow.down),document.removeEventListener("swipedown",app.slideshow.up),document.addEventListener("swipeup",app.slideshow.down),document.addEventListener("swipedown",app.slideshow.up))}function r(t,n){t&&document.addEventListener("keydown",e,!1),n&&i()}app.isPaused=!1,app.goTo=function(t,e){e&&app.slide.once("slide:enter",function(){app.slide.goTo(e)}),app.slideshow.load(t)},app.getPath=app.slideshow.getPath,app.addNavListeners=function(){var t=app.config.get(),e=app.slideshow.getId();r(t.keyboard,t.touch),s({id:e})},app.removeNavListeners=function(){document.removeEventListener("keydown",e,!1),n()},app.lock=function(){app.removeNavListeners(),app.trigger("locked")},app.unlock=function(){app.addNavListeners(),app.trigger("unlocked")},app.dispatchEvent=function(t,e){var i=document.createEvent("HTMLEvents",1,2),n=app.dom.get("wrapper")||document;i.initEvent(t,!0,!0),app.util.extend(i,e),n.dispatchEvent(i)},app.fullScreen=function(){var t=document.body,e=t.requestFullScreen||t.webkitRequestFullscreen||t.webkitRequestFullScreen||t.mozRequestFullScreen||t.msRequestFullScreen;e&&e.apply(t)},app.togglePause=function(){var t=app.dom.get("wrapper");t.classList.toggle("paused"),app.isPaused=!app.isPaused,app.isPaused?n():i()},app.autoRun=function(t){setInterval(function(){app.next()},t)},app.next=function(){app.slide.next()||app.slideshow.step("next")},app.prev=function(){app.slide.prev()||app.slideshow.step("previous")},app.start=function(t,e,i,n){function a(){var a=e.get();app.queryParams&&app.queryParams.mode&&app.$.init(app.queryParams.mode),app.initialize?app.listenTo(i,"register",function(t){app.slide.enter({id:t},!0)}):(a.history&&app.history.init(),a.preload&&app.listenTo(n,"load",function(e){var i=[],n=[],s=app.slideshow.get(),r="past",a="future present";i=i.concat.apply(i,app.slideshow.inspect().list),i.forEach(function(e){var i=t.getSlide(e);i.id=i.id||e,e===s?(i.classes=["past future","present"],r="future",a="past present"):i.classes=[a,r],n.push(i)}),app.dom.insert(n)}),a.model=a.model||"presentation.json",r(a.keyboard,a.touch),app.listenTo(n,"load",s),app.model.once("set:model",function(t){var e=window.location.hash,i=(window.location.search,a.startPath||null);e&&e.length>2?i=e.replace("#",""):app.queryParams&&(app.queryParams.path&&app.slideshow.pathExist(app.queryParams.path)?i=app.queryParams.path:app.queryParams.slide&&(i=app.queryParams.slide.replace(/%2F/g,"/"))),app.start.at(i),app.dispatchEvent("ready"),app.trigger("presentation:ready"),app.dom.trigger("new:elements",{views:[{id:"presentation"}]}),a.autoSlide&&app.autoRun(a.autoSlide)}),/\.json$/.test(a.model)?t.fetch(a.model):"object"==typeof a.model?t.set(a.model):(slides=a.model.replace(/,/g," ").replace("  "," ").split(" "),t.set({slides:{},modules:{},structures:{},storyboard:["presentation"],storyboards:{presentation:{content:slides}}}))),window.ag&&window.ag.on&&(app.config.get("remote")?app.remote.setup():ag.on("registerUser",function(t){app.remote.init(t),app.remote.setup()}))}function o(e){var i,s=app.dom.get("slides"),r=app.queryParams.state||null;if(!e)if(app.model.hasStoryboard(app.env))e=app.env;else{if(i=t.getStoryboard(),!i||!i[0])throw new Error("No default storyboard or start path defined");e=i[0]}r?app.goTo(e,r):n.__load__(e),app.dom.resolveModules(app.dom.get("wrapper"),"accelerator"),s&&s.classList.remove("no-transition"),app.config.get("startPath")||app.config.set("startPath",e)}return{init:a,at:o}}(app.model,app.config,app.registry,app.slideshow),"onhashchange"in window&&(window.onhashchange=t)}();
/*! head.core - v1.0.2 */
(function(n,t){"use strict";function r(n){a[a.length]=n}function k(n){var t=new RegExp(" ?\\b"+n+"\\b");c.className=c.className.replace(t,"")}function p(n,t){for(var i=0,r=n.length;i<r;i++)t.call(n,n[i],i)}function tt(){var t,e,f,o;c.className=c.className.replace(/ (w-|eq-|gt-|gte-|lt-|lte-|portrait|no-portrait|landscape|no-landscape)\d+/g,"");t=n.innerWidth||c.clientWidth;e=n.outerWidth||n.screen.width;u.screen.innerWidth=t;u.screen.outerWidth=e;r("w-"+t);p(i.screens,function(n){t>n?(i.screensCss.gt&&r("gt-"+n),i.screensCss.gte&&r("gte-"+n)):t<n?(i.screensCss.lt&&r("lt-"+n),i.screensCss.lte&&r("lte-"+n)):t===n&&(i.screensCss.lte&&r("lte-"+n),i.screensCss.eq&&r("e-q"+n),i.screensCss.gte&&r("gte-"+n))});f=n.innerHeight||c.clientHeight;o=n.outerHeight||n.screen.height;u.screen.innerHeight=f;u.screen.outerHeight=o;u.feature("portrait",f>t);u.feature("landscape",f<t)}function it(){n.clearTimeout(b);b=n.setTimeout(tt,50)}var y=n.document,rt=n.navigator,ut=n.location,c=y.documentElement,a=[],i={screens:[240,320,480,640,768,800,1024,1280,1440,1680,1920],screensCss:{gt:!0,gte:!1,lt:!0,lte:!1,eq:!1},browsers:[{ie:{min:6,max:11}}],browserCss:{gt:!0,gte:!1,lt:!0,lte:!1,eq:!0},html5:!0,page:"-page",section:"-section",head:"head"},v,u,s,w,o,h,l,d,f,g,nt,e,b;if(n.head_conf)for(v in n.head_conf)n.head_conf[v]!==t&&(i[v]=n.head_conf[v]);u=n[i.head]=function(){u.ready.apply(null,arguments)};u.feature=function(n,t,i){return n?(Object.prototype.toString.call(t)==="[object Function]"&&(t=t.call()),r((t?"":"no-")+n),u[n]=!!t,i||(k("no-"+n),k(n),u.feature()),u):(c.className+=" "+a.join(" "),a=[],u)};u.feature("js",!0);s=rt.userAgent.toLowerCase();w=/mobile|android|kindle|silk|midp|phone|(windows .+arm|touch)/.test(s);u.feature("mobile",w,!0);u.feature("desktop",!w,!0);s=/(chrome|firefox)[ \/]([\w.]+)/.exec(s)||/(iphone|ipad|ipod)(?:.*version)?[ \/]([\w.]+)/.exec(s)||/(android)(?:.*version)?[ \/]([\w.]+)/.exec(s)||/(webkit|opera)(?:.*version)?[ \/]([\w.]+)/.exec(s)||/(msie) ([\w.]+)/.exec(s)||/(trident).+rv:(\w.)+/.exec(s)||[];o=s[1];h=parseFloat(s[2]);switch(o){case"msie":case"trident":o="ie";h=y.documentMode||h;break;case"firefox":o="ff";break;case"ipod":case"ipad":case"iphone":o="ios";break;case"webkit":o="safari"}for(u.browser={name:o,version:h},u.browser[o]=!0,l=0,d=i.browsers.length;l<d;l++)for(f in i.browsers[l])if(o===f)for(r(f),g=i.browsers[l][f].min,nt=i.browsers[l][f].max,e=g;e<=nt;e++)h>e?(i.browserCss.gt&&r("gt-"+f+e),i.browserCss.gte&&r("gte-"+f+e)):h<e?(i.browserCss.lt&&r("lt-"+f+e),i.browserCss.lte&&r("lte-"+f+e)):h===e&&(i.browserCss.lte&&r("lte-"+f+e),i.browserCss.eq&&r("eq-"+f+e),i.browserCss.gte&&r("gte-"+f+e));else r("no-"+f);r(o);r(o+parseInt(h,10));i.html5&&o==="ie"&&h<9&&p("abbr|article|aside|audio|canvas|details|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|progress|section|summary|time|video".split("|"),function(n){y.createElement(n)});p(ut.pathname.split("/"),function(n,u){if(this.length>2&&this[u+1]!==t)u&&r(this.slice(u,u+1).join("-").toLowerCase()+i.section);else{var f=n||"index",e=f.indexOf(".");e>0&&(f=f.substring(0,e));c.id=f.toLowerCase()+i.page;u||r("root"+i.section)}});u.screen={height:n.screen.height,width:n.screen.width};tt();b=0;n.addEventListener?n.addEventListener("resize",it,!1):n.attachEvent("onresize",it)})(window);
/*! head.css3 - v1.0.0 */
(function(n,t){"use strict";function a(n){for(var r in n)if(i[n[r]]!==t)return!0;return!1}function r(n){var t=n.charAt(0).toUpperCase()+n.substr(1),i=(n+" "+c.join(t+" ")+t).split(" ");return!!a(i)}var h=n.document,o=h.createElement("i"),i=o.style,s=" -o- -moz- -ms- -webkit- -khtml- ".split(" "),c="Webkit Moz O ms Khtml".split(" "),l=n.head_conf&&n.head_conf.head||"head",u=n[l],f={gradient:function(){var n="background-image:";return i.cssText=(n+s.join("gradient(linear,left top,right bottom,from(#9f9),to(#fff));"+n)+s.join("linear-gradient(left top,#eee,#fff);"+n)).slice(0,-n.length),!!i.backgroundImage},rgba:function(){return i.cssText="background-color:rgba(0,0,0,0.5)",!!i.backgroundColor},opacity:function(){return o.style.opacity===""},textshadow:function(){return i.textShadow===""},multiplebgs:function(){i.cssText="background:url(https://),url(https://),red url(https://)";var n=(i.background||"").match(/url/g);return Object.prototype.toString.call(n)==="[object Array]"&&n.length===3},boxshadow:function(){return r("boxShadow")},borderimage:function(){return r("borderImage")},borderradius:function(){return r("borderRadius")},cssreflections:function(){return r("boxReflect")},csstransforms:function(){return r("transform")},csstransitions:function(){return r("transition")},touch:function(){return"ontouchstart"in n},retina:function(){return n.devicePixelRatio>1},fontface:function(){var t=u.browser.name,n=u.browser.version;switch(t){case"ie":return n>=9;case"chrome":return n>=13;case"ff":return n>=6;case"ios":return n>=5;case"android":return!1;case"webkit":return n>=5.1;case"opera":return n>=10;default:return!1}}};for(var e in f)f[e]&&u.feature(e,f[e].call(),!0);u.feature()})(window);
/*! head.load - v1.0.3 */
(function(n,t){"use strict";function w(){}function u(n,t){if(n){typeof n=="object"&&(n=[].slice.call(n));for(var i=0,r=n.length;i<r;i++)t.call(n,n[i],i)}}function it(n,i){var r=Object.prototype.toString.call(i).slice(8,-1);return i!==t&&i!==null&&r===n}function s(n){return it("Function",n)}function a(n){return it("Array",n)}function et(n){var i=n.split("/"),t=i[i.length-1],r=t.indexOf("?");return r!==-1?t.substring(0,r):t}function f(n){(n=n||w,n._done)||(n(),n._done=1)}function ot(n,t,r,u){var f=typeof n=="object"?n:{test:n,success:!t?!1:a(t)?t:[t],failure:!r?!1:a(r)?r:[r],callback:u||w},e=!!f.test;return e&&!!f.success?(f.success.push(f.callback),i.load.apply(null,f.success)):e||!f.failure?u():(f.failure.push(f.callback),i.load.apply(null,f.failure)),i}function v(n){var t={},i,r;if(typeof n=="object")for(i in n)!n[i]||(t={name:i,url:n[i]});else t={name:et(n),url:n};return(r=c[t.name],r&&r.url===t.url)?r:(c[t.name]=t,t)}function y(n){n=n||c;for(var t in n)if(n.hasOwnProperty(t)&&n[t].state!==l)return!1;return!0}function st(n){n.state=ft;u(n.onpreload,function(n){n.call()})}function ht(n){n.state===t&&(n.state=nt,n.onpreload=[],rt({url:n.url,type:"cache"},function(){st(n)}))}function ct(){var n=arguments,t=n[n.length-1],r=[].slice.call(n,1),f=r[0];return(s(t)||(t=null),a(n[0]))?(n[0].push(t),i.load.apply(null,n[0]),i):(f?(u(r,function(n){s(n)||!n||ht(v(n))}),b(v(n[0]),s(f)?f:function(){i.load.apply(null,r)})):b(v(n[0])),i)}function lt(){var n=arguments,t=n[n.length-1],r={};return(s(t)||(t=null),a(n[0]))?(n[0].push(t),i.load.apply(null,n[0]),i):(u(n,function(n){n!==t&&(n=v(n),r[n.name]=n)}),u(n,function(n){n!==t&&(n=v(n),b(n,function(){y(r)&&f(t)}))}),i)}function b(n,t){if(t=t||w,n.state===l){t();return}if(n.state===tt){i.ready(n.name,t);return}if(n.state===nt){n.onpreload.push(function(){b(n,t)});return}n.state=tt;rt(n,function(){n.state=l;t();u(h[n.name],function(n){f(n)});o&&y()&&u(h.ALL,function(n){f(n)})})}function at(n){n=n||"";var t=n.split("?")[0].split(".");return t[t.length-1].toLowerCase()}function rt(t,i){function e(t){t=t||n.event;u.onload=u.onreadystatechange=u.onerror=null;i()}function o(f){f=f||n.event;(f.type==="load"||/loaded|complete/.test(u.readyState)&&(!r.documentMode||r.documentMode<9))&&(n.clearTimeout(t.errorTimeout),n.clearTimeout(t.cssTimeout),u.onload=u.onreadystatechange=u.onerror=null,i())}function s(){if(t.state!==l&&t.cssRetries<=20){for(var i=0,f=r.styleSheets.length;i<f;i++)if(r.styleSheets[i].href===u.href){o({type:"load"});return}t.cssRetries++;t.cssTimeout=n.setTimeout(s,250)}}var u,h,f;i=i||w;h=at(t.url);h==="css"?(u=r.createElement("link"),u.type="text/"+(t.type||"css"),u.rel="stylesheet",u.href=t.url,t.cssRetries=0,t.cssTimeout=n.setTimeout(s,500)):(u=r.createElement("script"),u.type="text/"+(t.type||"javascript"),u.src=t.url);u.onload=u.onreadystatechange=o;u.onerror=e;u.async=!1;u.defer=!1;t.errorTimeout=n.setTimeout(function(){e({type:"timeout"})},7e3);f=r.head||r.getElementsByTagName("head")[0];f.insertBefore(u,f.lastChild)}function vt(){for(var t,u=r.getElementsByTagName("script"),n=0,f=u.length;n<f;n++)if(t=u[n].getAttribute("data-headjs-load"),!!t){i.load(t);return}}function yt(n,t){var v,p,e;return n===r?(o?f(t):d.push(t),i):(s(n)&&(t=n,n="ALL"),a(n))?(v={},u(n,function(n){v[n]=c[n];i.ready(n,function(){y(v)&&f(t)})}),i):typeof n!="string"||!s(t)?i:(p=c[n],p&&p.state===l||n==="ALL"&&y()&&o)?(f(t),i):(e=h[n],e?e.push(t):e=h[n]=[t],i)}function e(){if(!r.body){n.clearTimeout(i.readyTimeout);i.readyTimeout=n.setTimeout(e,50);return}o||(o=!0,vt(),u(d,function(n){f(n)}))}function k(){r.addEventListener?(r.removeEventListener("DOMContentLoaded",k,!1),e()):r.readyState==="complete"&&(r.detachEvent("onreadystatechange",k),e())}var r=n.document,d=[],h={},c={},ut="async"in r.createElement("script")||"MozAppearance"in r.documentElement.style||n.opera,o,g=n.head_conf&&n.head_conf.head||"head",i=n[g]=n[g]||function(){i.ready.apply(null,arguments)},nt=1,ft=2,tt=3,l=4,p;if(r.readyState==="complete")e();else if(r.addEventListener)r.addEventListener("DOMContentLoaded",k,!1),n.addEventListener("load",e,!1);else{r.attachEvent("onreadystatechange",k);n.attachEvent("onload",e);p=!1;try{p=!n.frameElement&&r.documentElement}catch(wt){}p&&p.doScroll&&function pt(){if(!o){try{p.doScroll("left")}catch(t){n.clearTimeout(i.readyTimeout);i.readyTimeout=n.setTimeout(pt,50);return}e()}}()}i.load=i.js=ut?lt:ct;i.test=ot;i.ready=yt;i.ready(r,function(){y()&&u(h.ALL,function(n){f(n)});i.feature&&i.feature("domloaded",!0)})})(window);
/*
//# sourceMappingURL=/accelerator/lib/head.min.js.map
*/
(function(){
    'use strict';

    document.addEventListener('ready', function() {

      // Prevent vertical bouncing of slides if tablet or bigger
      document.ontouchmove = function(event){
        var currentWidth = app.dom.get('wrapper').getBoundingClientRect().width;
        if(currentWidth >= 768) event.preventDefault();
      };

      if (window.ag && window.ag.data) {
        // window.debug = ag.debug(true); // To see what's captured during development
        ag.data.getPresenter(); // data available through ag.data.presenter
        ag.data.getCallContacts(); // data available through ag.data.call_contacts
      }

    })

})();

app.register('ag-auto-menu', function () {
	var self;
	return {
		template: '<div class="menu-container"><ul class="menu"></ul></div>',
		current: '',
		fallback: '', // if no menu is built
		publish: {
			hide: false, // Should we initially hide menu?
			placement: ['top', 'bottom'], // top or bottom?
			exclude: '', // Some content that should not be in the menu?
			slideshows: '',
			binding: 77,
			trigger: ''
		},
		events: {
			'tap li': 'navigate',
			'tap .menu-container': function () {
				if (this.stateIsnt('hidden')) {
					this.goTo('hidden');
					app.$.toolbar.hide();
				}
				else
					this.goTo('open');
			},
			'swipeleft': function (event) { event.stopPropagation(); },
			'swiperight': function (event) { event.stopPropagation(); }
		},
		states: [
			{
				id: 'hidden',
				onEnter: function () {
					if (this.props.placement === 'bottom') {
						app.util.transformElement(this.$el, 'translate(0,62px)');
					} else {
						app.util.transformElement(this.$el, 'translate(0,-62px)');
					}
				},
				onExit: function () {
					app.util.transformElement(this.$el, 'translate(0,0)');
				}
			},
			{
				id: 'open'
			}
		],
		onRender: function (el) {
			self = this;
			self.appWidth = $(window).width();
			if (app.env === 'ag-microsites') {
				$(el).hide();
			}

			self.pathLength = 2; // Default to menu of structures
			app.$.menu = this;
			if (this.props.hide) {
				this.hide();
				if (this.props.binding) {
					app.config.update('keyboard', parseInt(this.props.binding, 10), function () {
						app.$.trigger("toggle:menu");
					});
				}
			}
			app.$.on('toggle:menu', function () {
				this.toggle('hidden');
			}.bind(this));
			if (this.props.placement === 'bottom') el.classList.add('placement-bottom');
			// Are we using this menu with specific slideshows?
			if (this.props.slideshows) {
				this.props.slideshows.replace(/\s+/g, ''); // "one, two" => "one,two"
				this.props.slideshows = this.props.slideshows.split(',');
			}
			app.listenTo(app.slideshow, 'update:current', this.updateCurrent);
			app.listenTo(app.slideshow, 'load', function (data) {
				self.setup(data.id);
			});
			this.layout({scale: app.getScale()});
			app.on('update:layout', this.layout);
			this.setup();
			this.goTo('hidden');
		},
		hide: function () {
			this.goTo('hidden');
		},
		setup: function (idv) {
			var id = idv || app.slideshow.getId();
			// If scroller already exist, reset it
			if (self.scroller) self.scroller.moveTo(0, 0);
			if (!this.props.slideshows || this.props.slideshows.indexOf(id) > -1) {
				this.createLinks(id);
				this.updateCurrent();
			} else {
				this.removeLinks();
			}
			this.setTrigger();
		},
		setTrigger: function () {
			if (this.props.trigger) {
				var parts = this.props.trigger.split(' ');
				var e = parts[0];
				var selector = parts[1] || null;
				var el = document;
				if (selector) {
					el = document.body.querySelector(selector);
				}
				if (el) {
					el.addEventListener(e, function () {
						self.toggle('hidden');
					});
				}
			}
		},
		createLinks: function (structureVar) {
			var list = this.$('.menu')[0];
			var structure = structureVar || app.slideshow.getId();
			var html = '';
			var chapter;
			var links;
			var data = structure === 'storyboard' ? app.model.getStoryboard() : app.model.getStoryboard(structure);
			var pathPrefix = structure + '/';
			var excludedLinks = this.props.exclude.split(' ');

			if (data && data.content) {
				links = data.content;

				// If a single item in menu, let's try to dive down and get more links
				if (links.length === 1) {
					chapter = data.content[0];
					data = app.model.getStructure(chapter);
					if (data && data.content) {
						links = data.content;
						pathPrefix += chapter + '/';
						this.pathLength = 3;
					}
				}
			}

			if (!list) {
				list = document.createElement('ul');
				list.classList.add('menu');
				this.$el.appendChild(list);
			} else {
				list.innerHTML = '';
			}

			if (links) {
				links.forEach(function (item) {
					var lItem = item;
					if (typeof lItem !== 'string') {
						lItem = lItem[0];
					}
					if (excludedLinks.indexOf(lItem) === -1) {
						var name = app.model.getItem(lItem).name;
						html += '<li data-goto="' + pathPrefix + lItem + '">' + name + '</li>';
					}
				});

				list.appendChild(app.dom.parse(html));
				this.createScroller(list);
			}
		},
		setFallback: function (html) {
			if (html) this.fallback = html;
		},
		removeLinks: function () {
			var list = this.$('.menu')[0];
			list.innerHTML = this.fallback;
		},
		updateCurrent: function () {
			var path = app.getPath();
			var parts = path.split('/');
			if (parts.length > 2 && self.pathLength === 2) path = parts[0] + '/' + parts[1];
			if (self.current) self.current.classList.remove('selected');
			self.current = self.el.querySelector('.menu [data-goto="' + path + '"]');
			if (self.current) self.current.classList.add('selected');
			if (self.scroller) {
				var moveTo = 0;
				var pos, outerWidth;
				if (self.getWidth().menu > self.appWidth) {
					if (self.current) {
						pos = $(self.current).position();
						outerWidth = $(self.current).outerWidth();
					}

					if (pos) {
						if ((pos.left + outerWidth) > self.appWidth) {
							moveTo = self.scroller.config.limitsX[0];

						}
						else if ((pos.left + outerWidth) < self.getWidth().menu) {
							moveTo = 0;
						}

						setTimeout(function () {
							return self.scroller.moveTo(moveTo, 0)
						}, 500);
					}
				}
			}
		},
		navigate: function (event) {
			var link = event.target;
			var path;

			if (link) {
				path = link.getAttribute('data-goto');
				if (path) {
					app.goTo(path);
					self.updateCurrent(); // Immediate update of menu
				}
				if (self.props.hide) app.$.trigger("toggle:menu");
			}
		},
		createScroller: function (menu) {
			// TODO: listen to window resize and update limits
			var widths = this.getWidth();
			var appWidth = app.dom.get('wrapper').getBoundingClientRect().width;
			var scrollLimit = appWidth - widths.menu;
			// No scroller necessary if menu isn't bigger than width of view
			if (scrollLimit < 0) {
				menu.style.transform = 'translate(' + scrollLimit + 'px, 0px)';
				this.scroller = new Draggy(menu, {
					restrictY: true,
					limitsX: [scrollLimit, 0]
				});
			} else {
				this.scroller = null;
			}
		},
		getWidth: function () {
			var links = this.el.querySelectorAll('.menu li');
			var menuWidth = 0;
			var linkWidths = [];
			Array.prototype.slice.call(links).forEach(function (link) {
				var width = link.getBoundingClientRect().width;
				menuWidth += width;
				linkWidths.push(width);
			});
			return {
				menu: menuWidth,
				links: linkWidths
			};
		},
		layout: function (data) {
			// Only apply if zoom is supported
			if (typeof self.el.style.zoom !== 'undefined' && !navigator.userAgent.match( /(iphone|ipod|ipad|android)/gi) ) {
				self.el.style.zoom = data.scale;
			} else {
				app.util.transformElement( self.el, 'translate(-50%, -50%) scale(' + data.scale + ') translate(50%, 50%)' );
			}
		}
	};
});

app.register('ag-overlay', function () {
	return {
		publish: {
			width: '80%',
			height: '80%',
			noBackground: false,
			noCloseBtn: false,
			content: 'No content available',
			delay: 0,
			type: 'popup',
			defaultClassSlide: 'state-default'
		},
		events: {
			'tap .ag-overlay-close': 'close'
		},
		states: [
			{
				id: 'ag-overlay-open',
				onEnter: function () {
					app.lock();
					app.trigger('opening:overlay', {id: this.id, slideId: this.slideId});

					if (this.slideId) {
						setTimeout(function () {
							this.runSlideMethod(this.slideId, 'onEnter');
							app.slide.trigger('slide:enter', {id: this.slideId, type: this.props.type});
							app.trigger('opened:overlay', {id: this.id, slideId: this.slideId});
						}.bind(this), this.props.delay);
					} else {
						app.trigger('opened:overlay', {id: this.id});
					}
				},
				onExit: function () {
					app.trigger('closing:overlay', {id: this.id, slideId: this.slideId});

					setTimeout(function (slideId) {
						if (slideId) {
							this.runSlideMethod(slideId, 'onExit');
							app.slide.trigger('slide:exit', {id: slideId, type: this.props.type});
							app.slide.remove(slideId, true);
						}
						delete this.slideId;
						app.unlock();
						app.trigger('closed:overlay', {id: this.id, slideId: slideId});
					}.bind(this, this.slideId), this.props.delay);
				}
			}
		],
		runSlideMethod: function (slideId, methodName) {
			var slideData = app.slide.get(slideId);
			var slideElement;

			if (slideData && slideData[methodName]) {
				slideElement = slideData.el;
				slideData[methodName](slideElement);
				if (!slideData.getState()) slideElement.classList.add(this.props.defaultClassSlide);
			}
		},
		close: function () {
			this.reset();
		},
		// Load a slide into the overlay
		_loadPopup: function (slideId) {
			this.slideId = slideId;
			this.$('.ag-overlay-content')[0].innerHTML = '';
			app.dom.insert([{id: this.slideId}], false, this.$('.ag-overlay-content')[0]);
			this.goTo('ag-overlay-open');
		},
		load: function (slideId) {
			if (slideId === this.slideId) return;

			if (this.slideId) {
				this.reset();
				if (this._currentLoad) {
					app.off('close:overlay', this._currentLoad, app);
				}
				this._currentLoad = this._loadPopup.bind(this, slideId);
				app.once('close:overlay', this._currentLoad, app);
			} else {
				this._loadPopup(slideId);
			}
		},
		// Open provided HTML
		open: function (content) {
			var localContent = content || this.props.content;
			if (localContent) {
				this.$('.ag-overlay-content')[0].innerHTML = localContent;
			}
			this.goTo('ag-overlay-open');
		},
		setDimensions: function (width, height) {
			var contentEl = this.$('.ag-overlay-content')[0];
			var closeBtn = this.$('.ag-overlay-x')[0];
			var wMargin;
			var hMargin;
			var wUnit = '%';
			var hUnit = '%';
			// Assume percentage
			if (width < 101) {
				wMargin = (100 - width) / 2;
			}
			// Assume percentage
			if (height < 101) {
				hMargin = (100 - height) / 2;
			}

			if (contentEl) {
				contentEl.style.top = hMargin + hUnit;
				contentEl.style.bottom = hMargin + hUnit;
				contentEl.style.left = wMargin + wUnit;
				contentEl.style.right = wMargin + wUnit;
			}
			if (contentEl) {
				closeBtn.style.top = (hMargin - 1) + hUnit;
				closeBtn.style.right = (wMargin - 1) + wUnit;
			}
		},
		onRender: function () {
			var content = this.el.innerHTML;
			var html = '';
			if (!this.props.noBackground) {
				html = '<div class="ag-overlay-background ag-overlay-close"></div>';
			}
			html += '<div class="ag-overlay-content">';
			html += content;
			html += '</div>';
			if (!this.props.noCloseBtn) {
				html += '<div class="ag-overlay-x ag-overlay-close" data-popover="closePopup/bottomCenterCustom1/bottom"></div>';
			}
			this.el.innerHTML = html;
		},
		onRemove: function () {
		},
		onEnter: function () {
		},
		onExit: function () {
		}
	};
});

app.register('ag-slide-analytics', function () {
	/**
	 * Slide Analytics Module
	 *
	 * This module will save data about
	 * the slides visited.
	 *
	 * Usage:
	 * Include <div data-modules="ag-slide-analytics"></div>
	 * in index.html. See docs for more info.
	 */

	return {
		template: false,
		// The interface to this module
		publish: {
			debug: false,
			map: '', // Provide the namespace for map, e.g. "monitorMap" => window.monitorMap
			offset: 0, // e.g. if 1: default/safety/study => safety/study
			skip: ''
		},
		onRender: function () {
			app.listenTo(app.slide, 'slide:enter', this.save.bind(this));
			if (this.props.debug) ag.debug(true);
		},
		/**
		 * Assign correct id or name
		 * Get correct id and name for chapter, subchapter and slide
		 * Lookup order:
		 * 1. map[id]
		 * 2. app.json[type]['name']
		 * 3. id
		 * @private
		 * @param {string} id STRING Id of structure to find label for
		 * @param {string} itemType STRING One of 'slide', 'chapter', or 'slideshow'
		 * @returns {object} obj
		 */
		assignValues: function (id, itemType) {
			var data;
			var name;
			var localId = id;

			if (itemType === 'slide') {
				data = app.model.getSlide(localId);
			} else if (itemType === 'chapter') {
				data = app.model.getStructure(localId);
			}
			if (itemType === 'slideshow') {
				data = app.model.getStoryboard(localId);
			}

			if (data) {
				// If specific id and name has been specified
				// for monitoring in presentation.json
				if (data.monitoring) {
					localId = data.monitoring.id || localId;
					name = data.monitoring.name || data.name;
				} else if (this.map && this.map[localId]) {
					localId = this.map[localId].id || localId;
					name = this.map[localId].name || data.name;
				} else {
					name = data.name;
				}
				return {id: localId, name: name};
			}
			return {id: localId, name: null};
		},
		save: function (data) {
			var id = data.id;
			var path = app.getPath();
			var index = app.slideshow.getIndex();
			var slideIndex = index.v ? index.v : index.h;
			var components = app.slideshow.resolve();
			var subChapterId = components.chapter || null;
			var chapterId = components.slideshow || null;
			var chapter = {id: null, name: null};
			var subChapter = {id: null, name: null};
			var slide = this.assignValues(id, 'slide');

			if (subChapterId) subChapter = this.assignValues(subChapterId, 'chapter');
			if (chapterId) chapter = this.assignValues(chapterId, 'slideshow');

			// Slide id and name are required
			if (!slide.name) {
				// eslint-disable-next-line
				if (console.error) {
					// eslint-disable-next-line
					console.error('Slide will not be monitored! Name must be specified for "' + data.id + '"');
				}
				return;
			}

			if (window.ag) {
				window.ag.submit.slide({
					id: slide.id,
					name: slide.name,
					path: path,
					slideIndex: slideIndex,
					chapter: chapter.name,
					chapterId: chapter.id,
					subChapter: subChapter.name,
					subChapterId: subChapter.id
				});
			}
		}
	};
});

/* global app */
/* global ag */
app.register("ag-viewer", function() {

  /**
   * Agnitio Viewer Module
   *
   * This module will open URLs or PDF documents
   * in iFrame on top of presentation.
   *
   * Usage:
   * - Call ag.openPDF in non-agnitio app or on the web
   * - Call ag.openURL in non-agnitio app or on the web
   * - Add 'data-viewer="browser"' to a link (<a>)
   */

  return {
    template: false,
    publish: {
        
    },
    events: {
      "tap .close": "closeViewer"
    },
    states: [],
    onRender: function(el) {

        // app.on('ready', this.init.bind(this));

        this.frame = null;
        this.content = [];
        this.inDevice = true;

        var info = ag.platform.info();

        // If non-Engager, let's open PDFs in viewer
        if (!info || (info.localizedModel !== "iPad" && info.platform !== "Windows")) {
            this.inDevice = false;
            ag.on('openPDF', this.openContent.bind(this));
        }
        ag.on('openURL', this.openContent.bind(this));
        ag.on('openSlide', this.openSlide.bind(this));

        document.addEventListener('click', this.handleClick.bind(this));
      
    },
    onRemove: function(el) {
        this._removeElement(); // Will undelegate events
    },
    handleClick: function(event) {
        var el = event.target;
        var attr = el.getAttribute('data-viewer') || el.hasAttribute('data-viewer');
        var href = el.getAttribute('href') || attr;
        if (attr) {
            event.preventDefault();
            event.stopPropagation();
            if (attr === "slide") {
                if (!href) return;
                ag.publish('openSlide', {slide: href});
            }
            else if (href) {
                if (/.pdf/.test(href)) {
                    ag.openPDF(href);
                }
                else if (ag.openURL && typeof href === 'string') {
                    ag.openURL(href);
                }
            }
        }
    },
    openLink: function(link) {
      var href = link.getAttribute('href');
      if (ag.openURL) ag.openURL(href);
    },
    closeViewer: function() {
        app.unlock();
        var last = this.content.length - 1;
        var view = this.content[last];
        if (view.slide) {
            app.slide.remove(view.slide, true);
        }
        view.container.classList.remove('loaded');
        view.container.classList.remove('visible');
        this.el.removeChild(view.container);
        this.frame = null;
        view.container = null;
        this.content.pop();
    },
    openContent: function(path) {
        var view = {};
        var markup = [
            '<header>',
                '<a class="close" href="#"><span class="icon"></span></a>',
            '</header>',
            '<div class="spinner"></div>',
            '<div class="viewport">',
                '<iframe src="'+ path +'"></iframe>',
            '</div>'
        ];
        if (!this.inDevice) markup.splice(2, 0, '<a class="external" href="'+ path +'" target="_blank"><span class="icon"></span></a>');
        view.container = document.createElement('div');
        view.container.classList.add('preview-link-overlay');
        this.el.appendChild(view.container);
        view.container.innerHTML = markup.join('');
        this.frame = this.el.querySelector('iframe');
        this.frame.addEventListener('load', this.load.bind(this));
        this.content.push(view);
        setTimeout(function() {
            view.container.classList.add('visible');
        },1);
        app.lock();
    },
    openSlide: function(data) {
        var viewer = document.createElement('div');
        var view = {};
        viewer.classList.add('viewport');
        // Need to remove slide if already loaded in presentation
        app.slide.remove(data.slide, true);
        view.slide = data.slide;
        view.container = document.createElement('div');
        view.container.classList.add('preview-link-overlay');
        this.el.appendChild(view.container);
        view.container.innerHTML = [
            '<header>',
                '<a class="close" href="#"><span class="icon"></span></a>',
            '</header>',
            '<div class="spinner"></div>'
        ].join('');
        app.dom.insert([{id: data.slide}], false, viewer);
        view.container.appendChild(viewer);
        this.content.push(view);
        setTimeout(function() {
            view.container.classList.add('visible');
            view.container.classList.add('loaded');
        },1);
        app.lock();
    },
    load: function() {
        var last = this.content.length - 1;
        this.content[last].container.classList.add('loaded');
        this.frame.removeEventListener('load', this.load.bind(this));
    }
  }

});
app.register("ag-video", function() {

    // This module makes videos stateful so that they'll work remotely
    // ... and fix some layering issues that default controls cause
    // Usage: <div src="modules/ag-video/assets/myvideo.mp4" data-module="ag-video">

    var self = null;
    var videoEles = [];
    var initialized = false;
    return {
        publish: {
            src: "",
            poster: "",
            type: "video/mp4",
            monitor: false
        },
        events: {
            "playing video": function() {this.goTo('play')},
            "pause video": function() {this.goTo('pause')},
            "tap .ag-video-play-toggle": 'toggleVideo',
            "tap .ag-upper-click-layer": 'toggleVideo',
            "tap .ag-video-fullscreen-button": 'toggleFullscreen',
            "swipeleft .ag-video-controls": 'noSwipe',
            "swiperight .ag-video-controls": 'noSwipe',
            "swipeleft": 'checkIfSwipe',
            "swiperight": 'checkIfSwipe',
            "swipeup": 'checkIfSwipe',
            "swipedown": 'checkIfSwipe',
            "onDrop .ag-video-seek-ele": 'handleDrop'
        },
        states: [
            {
                id: 'play',
                onEnter: function() {
                    this.el.videoEle.play();
                }
            },
            {
                id: 'pause',
                onEnter: function() {
                    this.el.videoEle.pause();
                    if (this.props.monitor) {
                        window.ag.submit.data({
                            category: "Video viewed",
                            label: this.props.src,
                            value: this.formatTime(this.el.videoEle.currentPlayTime),
                            valueType: "time",
                            path: app.getPath(),
                            unique: true
                        });
                    }
                }
            }
        ],
        onRender: function(el) {
            self = this;

            this.el.videoEle = this.el.querySelector('.ag-video-element');
            this.el.videoEle.currentPlayTime = 0;
            this.el.videoEle.progressBar = this.el.querySelector('.ag-video-progress-bar');
            this.el.videoEle.currentTimeText = this.el.querySelector('.ag-video-current-time');
            this.el.videoEle.endTimeText = this.el.querySelector('.ag-video-total-time');
            this.el.videoEle.videoContainer = this.el.querySelector('.ag-video-container');
            this.el.videoEle.seekEle = this.el.querySelector('.ag-video-seek-ele');
            this.el.videoEle.progressBarContainer = this.el.querySelector('.ag-video-progress-container');

            // this.el.videoEle.addEventListener('error', this.fallBackSrc.bind(this));
            this.el.videoEle.src = this.props.src;
            this.el.videoEle.poster = this.props.poster;
            this.el.videoEle.type = this.props.type;
            this.el.videoEle.preload = "none";

            // Make sure play and pause is passed on if handled with default controls
            this.el.videoEle.addEventListener('playing', function(e) {
                if(e.target == self.el.videoEle){
                    if (self.stateIsnt('play')){
                        self.goTo('play');
                    }
                }
            });
            this.el.videoEle.addEventListener('pause', function(e) {
                if(e.target == self.el.videoEle){
                    if (self.stateIsnt('pause')){
                        self.goTo('pause');
                    }
                }
            });
            this.el.videoEle.addEventListener('timeupdate', this.setTime);

            self.el.videoEle.seekHandle = new Draggy(self.el.videoEle.seekEle, {
                restrictY: true, limitsX: [0, self.el.videoEle.progressBarContainer.offsetWidth], onChange: self.moveSeekHandle
            });

            videoEles.push(this.el.videoEle);

            setTimeout(function(){
                self.updateSeekHandle();
            },100);

            if(!initialized){
                app.listenTo(app, 'close:overlay', this.cleanOnOverlayExit);
                document.addEventListener('webkitfullscreenchange', this.addFullscreenClass, false);
                document.addEventListener('mozfullscreenchange', this.addFullscreenClass, false);
                document.addEventListener('fullscreenchange', this.addFullscreenClass, false);
                document.addEventListener('MSFullscreenChange', this.addFullscreenClass, false);
                window.addEventListener('resize', this.updateSeekHandle, false);
                initialized = true;
            }
        },
        // If can't load src as specified, then try to load from "slides/[parentId]/[src specified]"
        // This is to make it work if lazyloaded
        // TODO: use slides path in config
        fallBackSrc: function(event) {
            var el = event.target;
            // el.removeEventListener('error', this.fallBackSrc.bind(this));
            el.src = "slides/" + this.parentId + "/" + this.props.src;
        },
        cleanOnOverlayExit: function(e) {
            // Checks that the exited slide from the overlay has videoelements and if so removes them from videoEles on exit
            var slideEle = null, videoEle = null, videos = null;
            if(e.slideId){
                slideEle = app.slide.get(e.slideId);
                if(slideEle){
                    videos = slideEle.el.querySelectorAll('.ag-video-element');
                    if(videos[0]){
                        for (var i = 0; i < videos.length; i++) {
                            videos[i].removeEventListener('timeupdate', self.setTime);
                            videoEles.pop();
                        };
                    }
                }
            }
        },
        onEnter: function(el) {

        },
        onExit: function(el) {
            if (this.stateIs('play')) this.goTo('pause');
            this.reset();
        },
        onRemove: function() {
        },
        noSwipe: function(e){
            e.stopPropagation();
        },
        checkIfSwipe: function(e){
            // prevent swiping in fullscreen mode
            for(ele in videoEles){
                if(videoEles[ele].classList.contains('ag-video-fullscreen'))
                    self.noSwipe(e);
            }
        },
        toggleVideo: function(e){
            if(this.stateIs('play'))
                this.goTo('pause');
            else
                this.goTo('play');
        },
        setTime: function(e) {
            e.target.currentPlayTime = e.target.currentTime;
            self.updateProgress(e.target);
        },
        updateProgress: function(ele, time) {
            ele.currentPlayTime = time || ele.currentPlayTime;
            var seekHandlePos = 0;
            var value = 0;

            if (ele.currentPlayTime > 0) {
                value = Math.floor((100 / ele.duration) * ele.currentPlayTime);
            }

            var videoCurrentTime = this.formatTime(ele.currentPlayTime);
            var videoEndTime = this.formatTime(ele.duration);

            ele.currentTimeText.innerHTML = videoCurrentTime;
            ele.endTimeText.innerHTML = videoEndTime;

            ele.progressBar.style.width = value + "%";

            seekHandlePos = value * 0.01 * ele.progressBarContainer.offsetWidth;

            ele.seekHandle.moveTo(seekHandlePos);
        },
        formatTime: function(seconds) {
            var s = Math.floor(seconds % 60),
                m = Math.floor(seconds / 60 % 60),
                h = Math.floor(seconds / 3600);

            if (h > 0) {
                h = ((h < 10) ? "0" + h : h) + ":";
            }
            else {
                h = "00:";
            }

            if (m > 0) {
                m = ((m < 10) ? "0" + m : m) + ":";
            }
            else {
                m = "00:";
            }

            // Check if leading zero is need for seconds
            s = (s < 10) ? "0" + s : s;

            return h + m + s;
        },
        toggleFullscreen: function(e){
            var videoEle = self.getVideoEle(e.target);
            var fullscreenEle = document.fullscreenElement || document.msFullscreenElement ||
                                document.mozFullScreenElement || document.webkitCurrentFullScreenElement;

            var userAgent = navigator.userAgent || navigator.vendor || window.opera;

            if(userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i )){
                if (videoEle.webkitEnterFullscreen){
                    videoEle.webkitEnterFullscreen();
                }
            }
            else{
                if (videoEle.videoContainer.requestFullscreen) {
                    if(fullscreenEle)
                        document.exitFullscreen();
                    else
                        videoEle.videoContainer.requestFullscreen();
                }
                else if (videoEle.videoContainer.msRequestFullscreen) {
                    if(fullscreenEle)
                        document.msExitFullscreen();
                    else
                        videoEle.videoContainer.msRequestFullscreen();
                }
                else if (videoEle.videoContainer.mozRequestFullScreen) {
                    if(fullscreenEle)
                        document.mozCancelFullScreen();
                    else
                        videoEle.videoContainer.mozRequestFullScreen();
                }
                else if (videoEle.videoContainer.webkitRequestFullscreen) {
                    if(fullscreenEle)
                        document.webkitCancelFullScreen();
                    else
                        videoEle.videoContainer.webkitRequestFullscreen();
                }
            }
        },
        addFullscreenClass: function(){
            var fullscreenEle = document.fullscreenElement || document.msFullscreenElement ||
                                document.mozFullScreenElement || document.webkitCurrentFullScreenElement;
            for(ele in videoEles){
                if (fullscreenEle) {
                    videoEles[ele].classList.add('ag-video-fullscreen');
                }
                else{
                    videoEles[ele].classList.remove('ag-video-fullscreen');
                    self.updateSeekHandle();
                }
            }
        },
        updateSeekHandle: function(){
            // draggy - changes limits and position on window resize
            for(ele in videoEles){
                videoEles[ele].seekHandle.config.limitsX[1] = videoEles[ele].progressBarContainer.offsetWidth;
                var seekHandlePos = videoEles[ele].currentTime / videoEles[ele].duration * videoEles[ele].progressBarContainer.offsetWidth;
                videoEles[ele].seekHandle.moveTo(seekHandlePos);
            }
        },
        moveSeekHandle: function(x, y){
            // updates progressbar
            var videoEle = self.getVideoEle(this);
            var currentPos = x / videoEle.progressBarContainer.offsetWidth;
            self.updateProgress(videoEle, videoEle.duration * currentPos);
        },
        handleDrop: function(e) {
            // moves handle
            var videoEle = self.getVideoEle(e.target);
            var pos = e.target.position;
            var currentPos = pos[0] / videoEle.progressBarContainer.offsetWidth;
            videoEle.currentTime = videoEle.duration * currentPos;
        },
        getVideoEle: function (el) {
            while ((el = el.parentElement) && !el.classList.contains("ag-video-container"));
            var vid = el.querySelector('.ag-video-element');
            return vid;
        }
    }

});

app.register('ah-cleanup', function () {
	// Based on the ag-cleanup module
	// Get the unique entries of an array
	function onlyUnique(value, index, self) {
		return self.indexOf(value) === index;
	}
	return {
		publish: {
			purgeOnLoad: false,
			timeout: 40, // seconds to wait since last purge.
			limit: 10
		},
		events: {},
		states: [],
		history: [],
		archived: [],
		lastPurge: 0,
		onRender: function () {
			var self = this;
			app.slideshow.on('update:current', function (data) {
				var clean;
				var cleanupLength = parseInt(self.props.limit / 2, 10); // We'll keep half our limit as active
				// Add current slide to beginning of history
				if (data.current) {
					self.history.unshift(data.current.id);
				}
				// Allow animation to run first
				setTimeout( function () {
					// Make sure we don't have the new slide in history already
					self.history = self.history.filter(onlyUnique);
					// console.log('SLIDE HISTORY:', self.history);
					// If our history is larger than limit, then send for cleanup
					if (self.history.length > parseInt(self.props.limit, 10)) {
						clean = self.history.splice(cleanupLength - 1, self.history.length - cleanupLength);
						// console.log('CLEAN FROM HISTORY:', clean);
						self.clean(clean);
					}
				}, 800);
			});
			// Fix for bug in Accelerator when slideshow is loaded and first slide has been archived
			app.slideshow.on('update:anthill-current', function (data) {
				var el = app.dom.get(data.current.id);
				if (el) el.classList.remove('archived');
			});
			app.slideshow.on('load', function () {
				var info = app.slideshow.resolve();
				var el = app.dom.get(info.slide);
				if (el) el.classList.remove('archived');
			});
			if (this.props.purgeOnLoad) {
				app.slideshow.on('load', function () {
					// console.log('Unloading slideshow');
					// Remove all slides except the newly loaded one
					// TODO: implement
				});
			}
		},
		onRemove: function () {},
		// Archive old slides and remove previously archived ones
		clean: function (slides) {
			var now = new Date().getTime();
			var currentSlide = app.slideshow.get();
			slides.forEach(function (id) {
				app.dom.archive(id);
			});
			// Only remove prevously archived if timeout has expired
			if (this.archived.length && now > (this.lastPurge + parseInt(this.props.timeout * 1000, 10))) {
				this.archived.forEach(function (id) {
					if (id !== currentSlide) app.slide.remove(id, true);
				});
				this.archived = slides;
				this.lastPurge = now;
			} else {
				this.archived = this.archived.concat(slides);
			}
		}
	};
});


/**
 * History module
 * -------------------------------------
 *
 * This module triggers event when current view (slide or popup) is updated.
 *
 * @module ah-history.js
 * @requires ag-overlay.js
 * @author Agent Developer
 */
app.register('ah-history', function () {
	return {
		config: {
			debug: true,
			slides: [
				{
					events: {
						target: app.slide,
						enter: 'slide:enter',
						exit: 'slide:exit'
					}
				}, {
					events: {
						target: app,
						enter: 'slideEnter:inlineSlideshow',
						exit: 'slideExit:inlineSlideshow'
					}
				}
			],
			wrappers: [
				{
					events: {
						target: app,
						enter: 'opening:overlay',
						exit: 'closed:overlay'
					}
				}, {
					events: {
						target: app,
						enter: 'opening:inlineSlideshow',
						exit: 'closed:inlineSlideshow'
					}
				}
			]
		},
		onRender: function () {
			this.stack = [];
			this.currentView = null;
			this.previousView = null;
			this.setFutureViewId();
			this.init();
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-history',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		init: function () {
			this.config.wrappers.forEach(function (data) {
				app.listenTo(data.events.target, data.events.enter, this.wrapperEnter.bind(this));
				app.listenTo(data.events.target, data.events.exit, this.wrapperExit.bind(this));
			}.bind(this));
			this.config.slides.forEach(function (data) {
				app.listenTo(data.events.target, data.events.enter, this.viewAdd.bind(this));
				app.listenTo(data.events.target, data.events.exit, this.viewRemove.bind(this));
			}.bind(this));
			app.listenTo(app.slideshow, 'update:current', function (data) {
				if (!this.currentView || this.currentView.id !== data.prev.id) {
					this.setFutureViewId(data.current.id);
					this.freeze(this.getInactiveViews(), true);
				}
			}.bind(this));
			app.listenTo(app.slideshow, 'load', function () {
				this.setFutureViewId(app.slideshow.resolve());
				this.freeze(this.getInactiveViews(), true);
			}.bind(this));
			return this._initLogs();
		},
		_initLogs: function () {
			var viewEvents;
			viewEvents = this._getViewConfig().viewEvents;
			app.listenTo(viewEvents.target, viewEvents.enter, function (data) {
				this._log(viewEvents.enter, data);
			}.bind(this));
			app.listenTo(viewEvents.target, viewEvents.exit, function (data) {
				this._log(viewEvents.exit, data);
			}.bind(this));
		},
		_getViewConfig: function () {
			return window.View.prototype.config;
		},
		setFutureViewId: function (futureViewId) {
			this.futureViewId = futureViewId || '';
		},
		getInactiveViews: function () {
			return this.stack.slice(0, this.stack.length - 1);
		},
		freeze: function (stack, isFreeze) {
			stack.forEach(function (view) {
				view.freeze(isFreeze);
			});
		},
		wrapperEnter: function () {
			var current = this.getCurrent();
			if (current) {
				this.trigger('exit', current);
			}
		},
		wrapperExit: function () {
			var current = this.getCurrent();
			if (current) {
				this.trigger('enter', current);
			}
		},
		viewAdd: function (data) {
			var view = new window.View(data.id, data.type);
			this.stack.push(view);
			this.trigger('enter', view);
		},
		viewRemove: function (data) {
			var view = this.getView(data.id);
			if (view) {
				this.stack.splice(this.stack.indexOf(view), 1);
				this.trigger('exit', view);
			}
		},
		getView: function (id) {
			return this.stack.find(function (view) {
				return view.id === id;
			});
		},
		getCurrent: function () {
			return this.stack[this.stack.length - 1];
		},
		dispatch: function (type, view) {
			switch (type) {
			case 'enter':
				if (this.currentView !== view) {
					this.previousView = this.currentView;
					this.currentView = view;
					if (this.previousView) {
						this.previousView.exit();
					}
					this.currentView.enter();
				}
				break;
			case 'exit':
				if (this.currentView === view && this.previousView !== view) {
					this.currentView = null;
					this.previousView = view;
					this.previousView.exit();
				}
				break;
			default:
				break;
			}
		},
		trigger: function (type, view) {
			if (!view.isFreeze) {
				this.dispatch(type, view);
			} else if (view.id === this.futureViewId) {
				this.setFutureViewId();
				this.freeze(this.stack, false);
				this.dispatch(type, view);
			}
		},
		_log: function (message, data) {
			if (this.config.debug) {
				// eslint-disable-next-line
				console.log('%c history log: ' + message, 'font-weight:bold; color: aqua; background: gray;', data);
			}
		}
	};
});

function View(id, type) {
	this.id = id;
	this.type = type || this.config.viewType;
	this.isFreeze = false;
}

View.prototype.config = {
	viewType: 'slide',
	viewEvents: {
		target: app,
		enter: 'view-enter',
		exit: 'view-exit'
	}
};

View.prototype.freeze = function (isFreeze) {
	this.isFreeze = isFreeze || false;
};

View.prototype.enter = function () {
	this.config.viewEvents.target.trigger(this.config.viewEvents.enter, this);
};

View.prototype.exit = function () {
	this.config.viewEvents.target.trigger(this.config.viewEvents.exit, this);
};

app.register('ah-popup-management', function () {
	var popupsQueue = [];
	var popupOverlay;
	return {
		publish: {
			overlay: 'ag-overlay-for-bhc'
		},
		events: {},
		states: [],
		onRender: function (el) {
			popupOverlay = app.module.get(this.props.overlay);
			app.listenTo(app, 'closed:overlay', this._closePopup.bind(this));
		},
		_openPopup: function (popupId) {
			clearTimeout(this.timeout);
			this.timeout = setTimeout(function () {
				popupOverlay.load(popupId);
			}, popupOverlay.props.delay);
		},
		openPopup: function (popupId) {
			if (popupOverlay.slideId) {
				popupOverlay.close();
			}
			popupsQueue.push(popupId);
			this._openPopup(popupId);
		},
		_closePopup: function (params) {
			if (popupsQueue[popupsQueue.length - 1] === params.slideId) {
				popupsQueue.pop();
				if (popupsQueue.length)
					this._openPopup(popupsQueue[popupsQueue.length - 1]);
			}
		},
		onRemove: function () {
		},
		onEnter: function (el) {
		},
		onExit: function () {
		}
	};
});

/**
 * Storage data module
 * -------------------------------------
 *
 * Module for saving and loading data from local storage.
 *
 * @module ah-storage-data.js
 * @author Agent Developer
 */
app.register('ah-storage-data', function () {
	return {
		publish: {
			storageId: 'BHCTemplateDefaultStorageId'
		},
		onRender: function () {
			this.init();
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-storage-data',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		init: function () {
			if (!this.getStorageData()) {
				sessionStorage.setItem(this.props.storageId, '{}');
			}
		},
		getStorageData: function (key) {
			var data = JSON.parse(sessionStorage.getItem(this.props.storageId));
			if (key) {
				data = data[key];
			}
			return data;
		},
		add: function (key, data) {
			var sd = this.getStorageData();
			if (typeof key !== 'string' && typeof key !== 'number') {
				// eslint-disable-next-line
				console.error('Wrong key');
				return;
			}
			sd[key] = data;
			this.save(sd);
		},
		save: function (data) {
			try {
				sessionStorage.setItem(this.props.storageId, JSON.stringify(data));
			} catch (e) {
				// eslint-disable-next-line
				console.error('Storage is not available!');
			}
		},
		remove: function (key) {
			var sd = this.getStorageData();
			if (!sd[key]) {
				// eslint-disable-next-line
				console.error('Wrong key');
				return;
			}
			delete sd[key];
			this.save(sd);
		}
	};
});

/**
 * Training overlay tour tab group
 * -------------------------------------
 *
 * This module allows to create Tab Group in Tutorial Tour.
 * Training overlay modules:
 *    ap-exit-popup
 *    ah-tab-group
 *    ah-tour
 *    ah-tour-configurator
 *    ah-tutorial
 *    ah-tutorial-controls
 *
 * @module ah-tab-group.js
 * @requires training overlay modules
 * @author Agent Developer
 */
app.register('ah-tab-group', function () {
	return {
		publish: {
			activeClass: 'active',
			tabMenuClass: 'tg-tab',
			tabsSegmentClass: 'tg-content',
			tabDataAttr: 'data-tab'
		},
		states: [],
		_findSegment: function (tabId) {
			return Array.prototype.filter.call(this.tabsSegments, function (tabSegment) {
				return tabSegment.dataset[this._getDatasetProp(this.props.tabDataAttr)] === tabId;
			}.bind(this))[0];
		},
		_getPropsValue: function (el, attr) {
			return el.dataset[this._getDatasetProp(attr)];
		},
		_init: function () {
			this.change = function () {
			};
			this.map = {};
			return Array.prototype.forEach.call(this.tabsMenu, function (tabItem) {
				var tabId;
				tabId = this._getPropsValue(tabItem, this.props.tabDataAttr);
				this.map[tabId] = {
					tabMenu: tabItem,
					tabSegment: this._findSegment(tabId),
					isScroll: false
				};
				tabItem.addEventListener('tap', this.goTo.bind(this, tabId));
			}.bind(this));
		},
		_getDatasetProp: function (attr) {
			return attr.slice(5, attr.length).replace(/-([a-z])/g, function (letter) {
				return letter[1].toUpperCase();
			});
		},
		onRender: function (el) {
			this.wrapper = el;
			this.tabsMenu = this.wrapper.getElementsByClassName(this.props.tabMenuClass);
			this.tabsSegments = this.wrapper.getElementsByClassName(this.props.tabsSegmentClass);
			this._init();
			this.goTo(Object.keys(this.map)[0]);
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-tab-group',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		initIscroll: function (wrapper) {
			return new IScroll(wrapper, {
				scrollY: true,
				scrollbars: 'custom'
			});
		},
		goTo: function (tabId) {
			if (!tabId && this.map[tabId]) {
				throw Error(tabId + ' is missing');
			}
			if (this.currentTabId && this.currentTabId !== tabId) {
				this.map[this.currentTabId].tabMenu.classList.remove(this.props.activeClass);
				this.map[this.currentTabId].tabSegment.classList.remove(this.props.activeClass);
			}
			this.map[tabId].tabMenu.classList.add(this.props.activeClass);
			this.map[tabId].tabSegment.classList.add(this.props.activeClass);
			if (!this.map[tabId].isScroll) {
				this.initIscroll(this.map[tabId].tabSegment.querySelector('.content-wrap'));
			}
			this.map[tabId].isScroll = true;
			if (this.currentTabId !== tabId) {
				this.change(tabId, this.map[tabId].tabSegment);
			}
			this.currentTabId = tabId;
		}
	};
});

function TabStates(tabInstanceId, slideId) {
	this.tabInstanceId = tabInstanceId;
	this.slideId = slideId;
	this.slide = app.slide.get(this.slideId);
	this.tabGroup = app.module.get(this.tabInstanceId);
	this.references = app.module.get('references');
	setTimeout(function () {
		this._init();
	}.bind(this), 0);
}

TabStates.prototype._init = function () {
	this.slideStatesIds = this.slide.states.map(function (state) {
		return state.id;
	});
	this.tabGroupStatesIds = Object.keys(this.tabGroup.map);
	this._curTime = new Date();
	this.tabGroup.change = function (tabId) {
		if (this.slideStatesIds.indexOf(tabId) !== -1) {
			this.slide.goTo(tabId);
		}
	}.bind(this);
	return app.slide.on('state:enter', function (data) {
		if (data.view === this.slideId && this.tabGroupStatesIds.indexOf(data.id) !== -1) {
			this.tabGroup.goTo(data.id);
			app.trigger('tabstate:change', this);
		}
	}.bind(this));
};
/**
 * Creates Tutorial instance.
 * ---------------------------------
 * This module allows to create Tutorial tour.
 * Training overlay modules:
 *    ap-exit-popup
 *    ah-tab-group
 *    ah-tour
 *    ah-tour-configurator
 *    ah-tutorial
 *    ah-tutorial-controls
 *
 * @module ah-tutorial.js
 * @requires tutorial.js, knockout.js, training overlay modules
 * @author Serhii.Zv, Mykola Fant
 */
app.register('ah-tutorial', function () {
	return {
		onRender: function () {
			var tutorialStrings = {};
			var getData = window.TutorialTour.prototype.getData;
			getData('modules/ah-tutorial/tutorial-strings.json', function (data) {
				tutorialStrings = data;
			});
			app.tutorial = new window.Tutorial(tutorialStrings);
			window.ko.applyBindings(app.tutorial, this.el);
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-tutorial',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		}
	};
});

function Tutorial(data) {
	this.data = data;
	this.tour = app.module.get('ahTour');
	this.tutorialStep = window.ko.observable();
	this.activeTutorial = window.ko.observable();
	this.tutorialWrapper = document.querySelector('#tutorial');
	app.on('toggle:tour', function () {
		this.toggleTutorial();
	}.bind(this));
	app.on('close:tour', function () {
		this.closeTour();
	}.bind(this));
}

Tutorial.prototype.closeTour = function () {
	app.tutorialTour.hide();
	this.trigger();
};

Tutorial.prototype.goTour = function () {
	this.closeTutorial();
	event.stopPropagation();
	this.tour.openTour();
};

Tutorial.prototype.goInfoStep = function () {
	this.tutorialStep('tutorial_1');
};

Tutorial.prototype.toggleTutorial = function () {
	if (this.tour.activeTour()) {
		return;
	}
	this.activeTutorial(!this.activeTutorial());
	this.goInfoStep();
	this.tutorialWrapper.classList.toggle('active-tutorial');
	this.trigger();
};

Tutorial.prototype.closeTutorial = function () {
	this.activeTutorial(false);
	this.tutorialStep('');
	this.tutorialWrapper.classList.remove('active-tutorial');
	this.trigger();
};

Tutorial.prototype.fastCloseTour = function () {
	this.closeTutorial();
	app.trigger('close:tour');
};

Tutorial.prototype.trigger = function () {
	return app.trigger('tutorial:toggle', {
		isActive: this.activeTutorial()
	});
};

/**
 * Training overlay tour functionality
 * -------------------------------------
 *
 * This module allows to create Tutorial Tour.
 * Training overlay modules:
 *    ap-exit-popup
 *    ah-tab-group
 *    ah-tour
 *    ah-tour-configurator
 *    ah-tutorial
 *    ah-tutorial-controls
 *
 * @module ah-tour.js
 * @requires tutorial-tour.js, knockout.js, bootstrap.min.js, ah-history, training overlay modules
 * @author Serhii.Zv, Mykola Fant
 */
app.register('ah-tour', function () {
	return {
		onRender: function (el) {
			this.$el = el;
			this.activeTour = window.ko.observable();
			this.isOpened = window.ko.observable();
			this.history = app.module.get('ahHistory');
			this.$closeButton = this.createCloseButton();
			this.$el.appendChild(this.$closeButton);
			app.tutorialTour = this.createTutorialTour();
			this.$closeButton.addEventListener('tap', this.exitTour.bind(this));
			this.initListeners();
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-tour',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		createTutorialTour: function () {
			return new window.TutorialTour({
				viewport: {
					selector: '#presentation',
					padding: 10
				},
				getActiveContainers: app.tutorialConfigurator.getActiveContainers.bind(app.tutorialConfigurator)
			});
		},
		initListeners: function () {
			app.on('toggle:tour', this.toggleTour.bind(this));
		},
		createCloseButton: function () {
			var button = document.createElement('button');
			button.classList.add('close-tour');
			return button;
		},
		exitTour: function () {
			this.activeTour(false);
			this.closeTour();
			this.trigger('tour:exit', {});
		},
		toggleTour: function () {
			if (this.isOpened()) {
				this.closeTour();
			}
			else {
				if (app.isVeevaWide) {
					if (app.tutorialConfigurator.storageData.isTour) {
						this.openTour();
					}
				} else if (this.activeTour()) {
					this.openTour();
				}
			}
		},
		trigger: function (eventName, data) {
			app.trigger(eventName, data);
		},
		openTour: function () {
			this.activeTour(true);
			this.isOpened(true);
			app.tutorialTour.show(app.dom.get(this.history.getCurrent().id));
			this.$el.classList.add('show');
			this.trigger('tour:show', {});
		},
		closeTour: function () {
			this.isOpened(false);
			app.tutorialTour.hide();
			app.tutorial.toggleTutorial();
			this.$el.classList.remove('show');
			this.trigger('tour:hide', {});
		}
	};
});

var toSnakeCase = function (str) {
	return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
};
var setUnit = function (value, unit) {
	var unit_ = unit || 'px';
	return value + unit_;
};

function TutorialTour(options) {
	var $wrapper;
	this.ahHistory = app.module.get('ahHistory');
	this.viewport = {
		selector: options.viewport.selector || 'body',
		padding: options.viewport.padding || 0
	};
	this.popovers = [];
	this.readData();
	this.getActiveContainers = options.getActiveContainers || function () {
		return [app.slideElements[app.currentSlide]];
	};
	this.attr = 'data-popover'; // Should be prefix 'data-'
	this.attrShower = 'data-state-popover'; // Should be prefix 'data-'
	this.attrReferences = 'data-overlay-reference';
	this.$overlayWrapper = this.createOverlayWrapper('overlayWrapper');
	this.$overlayCustomWrapper = this.createOverlayWrapper('overlayCustomWrapper');
	$wrapper = $(this.viewport.selector);
	$wrapper.append(this.$overlayCustomWrapper);
	$wrapper.append(this.$overlayWrapper);
}

TutorialTour.prototype.createOverlayWrapper = function (id) {
	var wrapper = $('<div>');
	wrapper.prop('id', id);
	return wrapper;
};
TutorialTour.prototype.getData = function (path, callback, isHTML) {
	var isHTML_ = isHTML || false;
	var _metadata = app.cache.get(path);
	if (_metadata) {
		callback(!isHTML_ ? JSON.parse(_metadata) : _metadata);
	} else {
		$.ajax({
			url: path,
			dataType: isHTML_ ? 'html' : 'json',
			success: function (data) {
				callback(data);
			},
			error: function (jqXHR, textStatus) {
				var err = {
					current: 'Error loading tutorial tour metadata file \'' + path + '\': ' + textStatus
				};
				throw err.current;
			},
			async: false
		});
	}
};
TutorialTour.prototype.readData = function () {
	if (app.isVeevaWide) {
		this.template = app.cache.get('modules/veeva_modules/ah-tour/template.html');
		this.customPopover = app.cache.get('modules/veeva_modules/ah-tour/custom-popover.html');
		this.tips = JSON.parse(app.cache.get('modules/veeva_modules/ah-tour/tour-tips.json'));
		this.references = JSON.parse(app.cache.get('modules/veeva_modules/ah-tour/overlay-references-strings.json'));
		this.strings = JSON.parse(app.cache.get('modules/veeva_modules/ah-tour/tour-strings.json'));
	}
	this.getData('modules/ah-tour/template.html', function (data) {
		this.template = data;
	}.bind(this), true);
	this.getData('modules/ah-tour/custom-popover.html', function (data) {
		this.customPopover = data;
	}.bind(this), true);
	this.getData('modules/ah-tour/tour-strings.json', function (data) {
		this.strings = data;
	}.bind(this));
	this.getData('modules/ah-tour/tour-tips.json', function (data) {
		this.tips = data;
	}.bind(this));
	this.getData('modules/ah-tour/overlay-references-strings.json', function (data) {
		this.references = data;
	}.bind(this));
};
TutorialTour.prototype.getTemplate = function (className, customClasslist) {
	var $template = $(this.template);
	$template.addClass(className);
	if (customClasslist) {
		$template.addClass(customClasslist);
	}
	return $template;
};
TutorialTour.prototype.getPosition = function ($parent, pointOffset) {
	var slideParameters = $('.present').get(0).getBoundingClientRect();
	var parentParameters = $parent.get(0).getBoundingClientRect();
	var y = pointOffset.y ? +pointOffset.y : 0;
	var x = pointOffset.x ? +pointOffset.x : 0;
	return {
		top: (parentParameters.top - slideParameters.top) + y + (parentParameters.height * pointOffset.top / 100),
		left: (parentParameters.left - slideParameters.left) + x + (parentParameters.width * pointOffset.left / 100)
	};
};
TutorialTour.prototype.getAutoPlacement = function ($parent) {
	var ICON_HEIGHT = 60;
	var $element = $parent.get(0);
	var className = $element.dataset.placement;
	var deviceParameters = document.getElementsByTagName('body')[0].getBoundingClientRect();
	var parentParameters = $element.getBoundingClientRect();
	return className || (parentParameters.bottom + ICON_HEIGHT > deviceParameters.bottom ? 'right-placement' : void 0);
};
TutorialTour.prototype.checkPlacement = function ($parent) {
	var placement = $parent.get(0).dataset.placement;
	if (placement) {
		return (placement + '-placement') || this.getAutoPlacement($parent);
	}
	return '';
};
TutorialTour.prototype.setPosition = function ($pointElement, $parent, pointOffset) {
	var position = this.getPosition($parent, pointOffset);
	$pointElement.addClass(this.checkPlacement($parent));
	if (pointOffset.placement) {
		$pointElement.addClass(pointOffset.placement);
	}
	position.top = setUnit(position.top);
	position.left = setUnit(position.left);
	$pointElement.css(position);
};
TutorialTour.prototype.createPoint = function (isCustomType) {
	var $pointElement = $('<div>');
	var className = isCustomType ? 'tour-custom-point' : 'tour-point';
	$pointElement.addClass(className);
	if (isCustomType) {
		this.$overlayCustomWrapper.append($pointElement);
	} else {
		this.$overlayWrapper.append($pointElement);
	}
	return $pointElement;
};
TutorialTour.prototype.addEvent = function ($element, isCustomType) {
	if (isCustomType) {
		$element.on('shown.bs.popover', function () {
			this.initScroll();
		}.bind(this));
	} else {
		$element.bind('tap', function (event) {
			event.stopPropagation();
			$element.popover('toggle');
		});
		$element.on('show.bs.popover', function () {
			var currentOpenedPopover = this.$currentOpenedPopover;
			if (!currentOpenedPopover || !currentOpenedPopover.is($element)) {
				if (currentOpenedPopover) {
					this.$currentOpenedPopover.popover('hide');
				}
				this.$currentOpenedPopover = $element;
				this.$currentOpenedPopover.addClass('show');
			}
		}.bind(this));
		$element.on('hide.bs.popover', function () {
			if (this.$currentOpenedPopover) {
				this.$currentOpenedPopover.removeClass('show');
			}
			this.$currentOpenedPopover = null;
		}.bind(this));
	}
};
TutorialTour.prototype.initPoint = function ($element, itemParams, offset, placement) {
	var className;
	var customClassList;
	var isCustomType = itemParams.type === 'custom';
	var $pointElement = this.createPoint(isCustomType);
	this.setPosition($pointElement, $element, offset);
	this.addEvent($pointElement, isCustomType);
	className = toSnakeCase(itemParams.gestures);
	customClassList = itemParams.classList;
	$pointElement.popover({
		placement: placement || 'auto',
		title: this.strings[itemParams.title],
		html: true,
		template: this.getTemplate(className, customClassList),
		trigger: 'manual',
		content: this.getContent(itemParams, isCustomType),
		container: this.viewport.selector,
		viewport: this.viewport
	});
	this.popovers.push($pointElement);
	if (isCustomType) {
		if (this.timerCustomPopover) {
			clearTimeout(this.timerCustomPopover);
		}
		this.timerCustomPopover = setTimeout(function () {
			$pointElement.popover('show');
		}, 150);
	}
};
TutorialTour.prototype.closeAll = function () {
	this.popovers.forEach(function ($pointElement) {
		$pointElement.popover('destroy');
		$pointElement.unbind();
		$pointElement.remove();
	});
};
TutorialTour.prototype.overlayToggle = function (isShow) {
	this.toggleShow(this.$overlayWrapper, isShow);
	this.toggleShow(this.$overlayCustomWrapper, isShow);
};
TutorialTour.prototype.toggleShow = function (wrapper, isShow) {
	wrapper[isShow ? 'addClass' : 'removeClass']('show');
};
TutorialTour.prototype.isPointInContainer = function ($element, offset, container) {
	var containerParameters;
	var pointPosition;
	var container_ = $element.closest(container).get(0);
	if (!container_) {
		throw new Error('Parent is not found');
	}
	pointPosition = this.getPosition($element, offset);
	containerParameters = container_.getBoundingClientRect();
	return pointPosition.top >= containerParameters.top && pointPosition.top <= containerParameters.bottom && pointPosition.left >= containerParameters.left && pointPosition.left <= containerParameters.right;
};
TutorialTour.prototype.checkIsVisible = function (element) {
	var elementComputedStyle = getComputedStyle(element);
	var anyInvisibleCase = [
		{
			name: 'display',
			value: 'none'
		}, {
			name: 'opacity',
			value: '0'
		}, {
			name: 'visibility',
			value: 'hidden'
		}
	].filter(function (data) {
		return elementComputedStyle[data.name] === data.value;
	})[0];
	return !anyInvisibleCase;
};
TutorialTour.prototype.show = function (element) {
	this.isShow = true;
	this.overlayToggle(this.isShow);
	this.getActiveContainers(element).forEach(function (wrapper) {
		var $wrapper = $(wrapper);
		var elements = $wrapper.find('[' + this.attr + ']').toArray();
		if ($wrapper.attr(this.attr)) {
			elements.push(wrapper);
		}
		elements.forEach(function (element_) {
			var showerAttr;
			var slide;
			var slideId;
			var arg = element_.dataset[$.camelCase(this.attr.replace('data-', ''))].split('/');
			var $element = $(element_);
			var itemParams = this.tips.points[arg[0]];
			var offset = this.tips.offsets[arg[1]];
			var placement = arg[2];
			if (element_.dataset.visibilityArea) {
				// eslint-disable-next-line
				console.log(this.isPointInContainer($element, offset, element_.dataset.visibilityArea));
				if (!this.isPointInContainer($element, offset, element_.dataset.visibilityArea)) {
					return;
				}
			}
			if (!this.checkIsVisible(element_)) {
				return;
			}
			showerAttr = element_.dataset[$.camelCase(this.attrShower.replace('data-', ''))];
			slideId = this.ahHistory.getCurrent() ? this.ahHistory.getCurrent().id : '';
			slide = app.slide.get(slideId);
			if (showerAttr && slide.getState(slide._state) && showerAttr.split(',').indexOf(slide.getState(slide._state).id) < 0) {
				return;
			}
			this.initPoint($element, itemParams, offset, placement);
		}.bind(this));
	}.bind(this));
};
TutorialTour.prototype.hide = function () {
	this.isShow = false;
	this.overlayToggle(this.isShow);
	this.closeAll();
};
TutorialTour.prototype.getContent = function (itemParams, isCustomType) {
	return isCustomType ? this.getCustomContent(itemParams.content) : this.strings[itemParams.content];
};
TutorialTour.prototype.getCustomContent = function (key) {
	return this.generateCustomContent({
		content: this.generateCustomString(key)
	});
};
TutorialTour.prototype.generateCustomString = function (key) {
	var content = this.strings[key];
	var result = [];
	Object.keys(content).forEach(function (key_) {
		content[key_].classes = this.parseClasses(key_);
		result.push(content[key_]);
	}.bind(this));
	return result;
};
TutorialTour.prototype.parseClasses = function (key) {
	var array = key.split('-');
	array.shift();
	return array.join(' ') || '';
};
TutorialTour.prototype.generateCustomContent = function (myViewModel) {
	var htmlObject = document.createElement('div');
	htmlObject.innerHTML = this.customPopover;
	window.ko.applyBindings(myViewModel, htmlObject);
	this.injectReferences(htmlObject);
	return htmlObject.innerHTML;
};
TutorialTour.prototype.initScroll = function () {
	var scrollsElements = Array.prototype.slice.call(document.querySelectorAll('.custom-popover-wraptutorialper'), 0);
	var scroll;
	var scrolls = [];
	// eslint-disable-next-line
	console.log(scrollsElements);
	scrollsElements.forEach(function (element) {
		var scroller = element.children[0];
		if (scroller.offsetHeight > element.offsetHeight) {
			/* eslint no-new: "error"*/
			scroll = new IScroll(element, {
				scrollY: true,
				scrollbars: 'custom',
				interactiveScrollbars: true,
				snap: '.item'
			});
			scrolls.push(scroll);
			['swipeup', 'swipedown'].forEach(function (eventName) {
				element.addEventListener(eventName, function (event) {
					event.stopPropagation();
				});
			});
			element.classList.add('show-scroll');
		}
	});
};
TutorialTour.prototype.injectReferences = function (el) {
	var refEls = el.querySelectorAll('[' + this.attrReferences + ']');
	Array.prototype.forEach.call(refEls, function (refEl) {
		var refKey = refEl.getAttribute(this.attrReferences);
		refEl.innerText = this.references[refKey];
	}.bind(this));
};

/**
 * Training overlay tour configuration
 * -------------------------------------
 *
 * This module allows to set Tutorial Tour configuration.
 * Training overlay modules:
 *    ap-exit-popup
 *    ah-tab-group
 *    ah-tour
 *    ah-tour-configurator
 *    ah-tutorial
 *    ah-tutorial-controls
 *
 * @module ah-tour-configurator.js
 * @requires tutorial-configurator.js, ah-storage-data, ah-history, training overlay modules
 * @author Serhii.Zv, Mykola Fant
 */
app.register('ah-tour-configurator', function () {
	return {
		publish: {},
		modulesToCheck: [],
		onRender: function () {
			app.tutorialConfigurator = new window.TutorialConfigurator(this.modulesToCheck);
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-tour-configurator',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		}
	};
});

function TutorialConfigurator(modulesToCheck) {
	this.modulesToCheck = modulesToCheck || [];
	this.popupDelay = 200;
	this.slideDelay = 200;
	this.store = app.module.get('apStorageData');
	this.ahHistory = app.module.get('ahHistory');
	if (app.isVeevaWide) {
		app.once('view-enter', function () {
			if (app.tutorialConfigurator.storageData.isActiveTour) {
				app.module.get('ahTour').openTour();
			} else if (app.tutorialConfigurator.storageData.isActiveTutorial) {
				app.trigger('toggle:tour');
			}
			return this.addHandlers();
		}.bind(this));
		this.initStorageState();
	} else {
		this.addHandlers();
	}
}

TutorialConfigurator.prototype.addHandlers = function () {
	app.on('view-enter', this.setAction.bind(this));
	app.slideshow.on('view-exit', this.close.bind(this));
	app.slide.on('state:enter', this.checkState.bind(this));
	app.slide.on('custom-state:enter', this.checkCustomState.bind(this));
};
TutorialConfigurator.prototype.initStorageState = function () {
	var _storageData = this.store.getStorageData();
	this.storageData = {
		isActiveTutorial: !!_storageData.isActiveTutorial,
		isActiveTour: !!_storageData.isActiveTour,
		isTour: !!_storageData.isTour
	};
	app.listenTo(app, 'tour:show', function () {
		this.storageData.isActiveTour = true;
		this.storageData.isTour = true;
		this.store.save(this.storageData);
	}.bind(this));
	app.listenTo(app, 'tour:hide', function () {
		this.storageData.isActiveTour = false;
		this.store.save(this.storageData);
	}.bind(this));
	app.listenTo(app, 'tour:exit', function () {
		this.storageData.isTour = false;
		this.store.save(this.storageData);
	}.bind(this));
	return app.listenTo(app, 'tutorial:toggle', function (data) {
		this.storageData.isActiveTutorial = data.isActive;
		this.store.save(this.storageData);
	}.bind(this));
};
TutorialConfigurator.prototype.updateState = function () {
	return {
		isActiveTutorial: false,
		isActiveTour: false
	};
};
TutorialConfigurator.prototype.close = function () {
	app.tutorialTour.closeAll();
};
TutorialConfigurator.prototype.checkState = function (data) {
	var slide = app.model.get().slides[data.view];
	if (slide) {
		this.setAction(slide);
	}
};
TutorialConfigurator.prototype.checkCustomState = function (slideId) {
	this.checkState({
		view: slideId
	});
};
TutorialConfigurator.prototype.show = function (event) {
	var element;
	if (event) {
		element = app.dom.get(event.id);
	}
	if (app.tutorialTour) {
		app.tutorialTour.closeAll();
		if (app.tutorialTour.isShow) {
			app.tutorialTour.show(element);
		}
	}
};
TutorialConfigurator.prototype.setAction = function (event) {
	var time = event && (event.type === 'inline' || event.type === 'popup') ? this.popupDelay : this.slideDelay;
	clearTimeout(this.timer);
	this.timer = setTimeout(function () {
		var slideId = this.ahHistory.getCurrent() ? this.ahHistory.getCurrent() : '';
		clearTimeout(this.timer);
		this.show(slideId);
	}.bind(this), time);
};
TutorialConfigurator.prototype.getActiveContainers = function (element) {
	var slideElement = this.getElementWrapper(element);
	var containers = [];
	var modules = this.getActiveElModules();
	if (modules.length > 0) {
		containers = modules;
	} else if (slideElement) {
		containers.push(slideElement);
	}
	return containers;
};
TutorialConfigurator.prototype.getActiveElModules = function () {
	return this.modulesToCheck.filter(function (moduleId) {
		return app.module.get(moduleId);
	}).filter(function (module) {
		return !!module.getState() || module.el.classList.contains('active');
	}).map(function (module) {
		return this.getElementWrapper(module.el);
	}.bind(this));
};
TutorialConfigurator.prototype.getElementWrapper = function (element) {
	if (element && element.parentElement.parentElement.classList.contains('state-ag-overlay-open')) {
		return element.parentElement.parentElement;
	}
	return element;
};

/**
 * Training overlay button functionality on menu toolbar
 * -------------------------------------
 *
 * This module allows to set button functionality on menu toolbar for training overlay.
 * Training overlay modules:
 *    ap-exit-popup
 *    ah-tab-group
 *    ah-tour
 *    ah-tour-configurator
 *    ah-tutorial
 *    ah-tutorial-controls
 *
 * @module ah-tutorial-controls.js
 * @requires ap-toolbar.js, training overlay modules
 * @author Serhii.Zv, Mykola Fant
 */
app.register('ah-tutorial-controls', function () {
	return {
		publish: {
			'active': 'active',
			'disable': 'disable-bhc'
		},
		events: {
			'tap .tutorial': 'toggleTour'
		},
		states: [],
		onRender: function () {
			this.tutorial = this.el.getElementsByClassName('tutorial')[0];
			this.presentation = document.getElementById('presentation');
			this.toolbarModule = app.module.get('apToolbar');
			app.on('close:tour', function () {
				this.resetTour();
			}.bind(this));
			if (app.isVeevaWide) {
				app.once('view-enter', function () {
					if (this.isActiveTour()) {
						this.setControlActive();
					}
				}.bind(this));
			}
			this.monitorUsage();
		},
		resetTour: function () {
			this.resetActive();
			this.enableBhc();
		},
		isActiveTour: function () {
			var storageData;
			storageData = app.tutorialConfigurator.storageData;
			return storageData.isActiveTour || storageData.isActiveTutorial;
		},
		setControlActive: function () {
			this.toggleActive();
			this.toggleDisableBhc();
		},
		toggleTour: function () {
			app.trigger('toggle:tour');
			this.toolbarModule.hide();
			this.setControlActive();
		},
		toggleDisableBhc: function () {
			this.presentation.classList.toggle(this.props.disable);
		},
		enableBhc: function () {
			this.presentation.classList.remove(this.props.disable);
		},
		toggleActive: function () {
			this.tutorial.classList.toggle(this.props.active);
		},
		setActive: function () {
			this.tutorial.classList.add(this.props.active);
		},
		resetActive: function () {
			this.tutorial.classList.remove(this.props.active);
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-tutorial-controls',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		}
	};
});

app.register('ap-auto-menu-handle', function () {
	return {
		publish: {},
		events: {},
		states: [],
		onRender: function () {},
		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {}
	};
});

/**
 * Implements auto reference popups.
 * ---------------------------------
 *
 * Adds tap handlers to every element that has the [data-reference-id] data attribute.
 * On tap a popup with a list of all references in the slide appears.
 *
 * The module is loaded within index.html
 *
 * @module ap-auto-references-popup.js
 * @requires jquery.js, touchy.js
 * @author Andreas Tietz, antwerpes ag
 */
/**
 * Modified ap-auto-references-popup module to read keys instead of numbers.
 * Grouping them when connected references are used.
 * Adding reference-numbers to the sup-elements
 */


app.register("ap-auto-references", function () {
	var GROUP_SEPARATOR = '-';
	var REF_SEPARATOR = ',';
	var GROUP_SEPARATOR_TO_SHOW = '-';
	var REF_SEPARATOR_TO_SHOW = ',';
	var REF_KEY = 'referenceKey';
	var REF_ATTR = 'data-reference-id';
	var REF_CLASS = 'references';
	var REF_SELECTOR = '[' + REF_ATTR + ']';
	var SIDE_CLIP = 'auto-side-clip';
	var SIDE_CLIP_SELECTOR = '.' + SIDE_CLIP;

	return {
		publish: {},
		events: {},
		states: [],

		onRender: function () {
			if (this.props.injectrefstopopup) {
				app.listenTo(app.slide, 'slide:render',
					this.autoReferencePopupHandlerUpdated.bind(this));
				var slide = app.slideshow.get();
				if(slide)
					this.autoReferencePopupHandlerUpdated({id: slide});
			}
			else {
				app.listenToOnce(app.slide, 'slide:enter', this.autoReferencePopupHandler.bind(this));
				app.listenTo(app.slideshow, 'load', this.autoReferencePopupHandler.bind(this));
			}
		},

		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {},

		getReferences: function(refIds, data, refProp = REF_KEY) {
			var media = Object.keys(data)
				.filter(function(e) {return data[e].hasOwnProperty(refProp)})
				.map(function(e) {return String(data[e][refProp])});

			return refIds.split(REF_SEPARATOR).map(function (e) {
				var group = e.split(GROUP_SEPARATOR);
				var res = group;
				if (group.length > 1) {
					var firstGroupIndex = media.indexOf(group[0]);
					var lastGroupIndex = media.indexOf(group[group.length - 1]) + 1;
					res = media.slice(firstGroupIndex, lastGroupIndex);
				}
				return res;
			}).reduce(function(accumulator, e) {
				return accumulator.concat(e)
			}, []);
		},

		getRefindexes: function(reflist, data, refProp = REF_KEY) {
			var refindexes = [];
			$.each(data, function(file, meta) {
				if (reflist.indexOf(String(meta[refProp])) > -1) {
					refindexes.push(meta.referenceId);
				}
			});
			return refindexes;
		},

		getGroups: function (refindexes) {
			var groups = [];
			var point = 0;
			if (refindexes.length > 0) {
				refindexes.reduce(function(prev, next, index) {
					if ((next - prev) !== 1) {
						groups.push(refindexes.slice(point, index));
						point = index;
					}
					return next;
				});
				groups.push(refindexes.slice(point));
			}
			return groups;
		},

		autoReferencePopupHandlerUpdated: function(data) {
			var $slide = $("#" + data.id);
			var self = this;
			var mediaData = window.mediaRepository.metadata();
			var allRefindexes = [];
			$slide.find(REF_SELECTOR).each(function() {
				var referenceIds = $(this).attr(REF_ATTR);
				var reflist = self.getReferences(referenceIds, mediaData);
				var refindexes = self.getRefindexes(reflist, mediaData);
				var groups = self.getGroups(refindexes);

				allRefindexes = allRefindexes.concat(refindexes.filter(
					function (e) {return !allRefindexes.includes(e);}
				));

				$(this)[0].textContent = groups.map(function(group) {
					var result = group;
					if(group.length > 2)
						result = [
							group[0] +
							GROUP_SEPARATOR_TO_SHOW +
							group[group.length - 1]
						];
					return result;
				}).reduce(function(prev, next) {
					return prev.concat(next);
				}).join(REF_SEPARATOR_TO_SHOW);
			});

			app.trigger('refindexes:created', allRefindexes, data.id);
		},

		autoReferencePopupHandler: function() {
			// Load all slide html files of the currently loaded collection
			// and gather all unique reference ids (asynchronously):
			$('article.slide').each(function () {
				var $slide = $(this);
				var referenceIds = {}; // unique reference ids
				$slide.find(REF_SELECTOR).each(function () {
					referenceIds[$(this).attr(REF_ATTR)] = true;
				});
				referenceIds = Object.keys(referenceIds);
				// Find media resources associated with the collected reference ids:
				var references = {};
				$.each(window.mediaRepository.metadata(), function (file, meta) {
					if (referenceIds.indexOf('' + meta.referenceId) > -1) {
						references[file] = meta;
					}
				});
				var $list = $('<ul class=' + REF_CLASS + '/>');
				// Render all references into the list:
				$list.append($.map(references, function (meta, file) {
					return window.mediaRepository.render(file, meta);
				}));
				$slide.find(SIDE_CLIP_SELECTOR).prepend($list);
			});
		}
	}
});


app.register('ap-auto-references-popup', function () {
	var REF_ATTR = 'data-reference-id';
	var PRESENTATION_ID = '#presentation';
	var SCROLL_CLASS = 'scroll';
	var REF_POPUP_CLASS = 'auto-references-popup';
	var REF_POPUP_OVERLAY_CLASS = 'auto-references-popup-overlay';
	var REF_LIST_CLASS = 'references';
	var HIDE_CLASS = 'hidden';
	var CLOSE_EL_CLASS = 'x';
	var CLOSE_ICON = '⊗';
	var REF_SELECTOR = '[' + REF_ATTR + ']';
	var CLOSE_EL_SELECTOR = '.' + REF_POPUP_CLASS + ' .' + CLOSE_EL_CLASS;
	var REF_STR = 'References';

	return {
		publish: {},
		events: {},
		states: [],

		onRender: function () {
			this.$scroll = $('<div class=' + SCROLL_CLASS + '/>');
			this.$list = $('<ul class=' + REF_LIST_CLASS + '/>');
			this.$popup = $('<div class=' + REF_POPUP_CLASS + '/>')
				.append('<header><div class=' + CLOSE_EL_CLASS + '>'
					+ CLOSE_ICON+ '</div><h1>' + REF_STR + '</h1></header>');
			this.$overlay = $('<div/>',
				{'class': REF_POPUP_OVERLAY_CLASS + ' ' + HIDE_CLASS});
			this.refindexesList = {};

			this.$scroll.append(this.$list);
			this.$popup.append(this.$scroll);
			this.$overlay.append(this.$popup)
				.on("swipedown swipeup swiperight swipeleft", function (e) {
					e.stopPropagation();
				})
				.on("tap", function (event) {
					if ($(event.target).is(CLOSE_EL_SELECTOR))
						$(this).addClass(HIDE_CLASS);
				}).appendTo(PRESENTATION_ID);

			app.listenTo(app.slide, 'slide:enter',
				this.autoReferencePopupHandler.bind(this));
			app.on('refindexes:created', this.addRefindexes.bind(this));
		},
		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {},

		addRefindexes: function(refindexes, slideID) {
			refindexes = refindexes.sort(function(a, b) {
				return a > b ? 1 : -1;
			});
			this.refindexesList[slideID] = refindexes;
		},

		getMediaResources: function (ids, mediaData) {
			var references = {};
			$.each(mediaData, function (file, meta) {
				if (ids && ids.indexOf(meta.referenceId) > -1) {
					references[file] = meta;
				}
			});
			return references;
		},

		autoReferencePopupHandler: function (data) {
			var $slide = $('#' + data.id);
			var self = this;
			var mediaData = window.mediaRepository.metadata();
			var localRefindexes = self.refindexesList[data.id];
			var references = this.getMediaResources(localRefindexes, mediaData);

			this.$overlay.addClass(HIDE_CLASS);
			$slide.find(REF_SELECTOR)
				.off('tap.' + REF_POPUP_CLASS)
				.on('tap.' + REF_POPUP_CLASS, function () {
					self.$list.empty();
					self.$list.append($.map(references, function (meta, file) {
						return window.mediaRepository.render(file, meta);
					}));
					self.$overlay.removeClass(HIDE_CLASS);

					if(self.scroll) self.scroll.destroy();
					self.scroll = new IScroll(self.$scroll[0], {scrollbars: true});
				});
		}
	};
});

app.register('ap-auto-side-clip', function () {
	return {
		publish: {},
		events: {},
		states: [],
		onRender: function () {
			app.listenTo(app.slide, 'slide:enter', this.autoSideClipHandler.bind(this));
		},
		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {},
		autoSideClipHandler: function (data) {
			var $slide = $('#' + data.id);
			// search for auto-side-clip elements not already set up
			$slide.find('.auto-side-clip:not(.configured)').each(function () {
				// mark thise auto-side-clip as already set up
				var $content = $(this).addClass('configured');
				var $sideClipHandle = $('<div class=\'sideClipHandle\'/>');
				$content.wrap('<div class=\'contentContainer\'/>'); // fixes webkit scroll render bug
				var $contentContainer = $content.parent();
				$contentContainer.wrap('<div class=\'sideClipContainer\'/>');
				var $sideClipContainer = $contentContainer.parent();
				$sideClipHandle.appendTo($sideClipContainer);
				var $sideClipOverlay = $('<div class=\'sideClipOverlay\'/>');
				$sideClipOverlay.insertBefore($sideClipContainer);
				function toggleSideClip() {
					$sideClipContainer.toggleClass('active');
					$sideClipOverlay.toggleClass('active');
				}
				$sideClipHandle.add($sideClipOverlay).on('tap', toggleSideClip);
				$sideClipOverlay.on('swipedown swipeup swiperight swipeleft', function (e) {
					toggleSideClip();
					e.stopPropagation();
				});
				$sideClipContainer.on('swipedown swipeup swiperight swipeleft', function (e) {
					e.stopPropagation();
				});
			});
		}
	};
});

/**
 * Provides a function to navigate backwards
 * ---------------------------------------------------------
 *
 * Navigates backwards through the sequence of app.goTo calls.
 *
 * @module ap-back-navigation.js
 * @requires agnitio.js
 * @author David Buezas, antwerpes ag
 */
app.register('ap-back-navigation', function () {
	var self;
	return {
		publish: {},
		events: {
			'tap': 'back'
		},
		states: [],
		history: [],
		active: false,
		path: null,
		onRender: function () {
			self = this;
			app.$.BackNavigation = this;
			app.slideshow.on('update:current', function (data) {
				if (data.prev.id !== data.current.id) {
					this.save();
				}
			}.bind(this));
			app.slideshow.on('load', function () {
				this.clearHistory();
			}.bind(this));
			this.update();
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-back-navigation',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		// Save the path of the slide we are currently on
		update: function () {
			this.path = app.getPath();
		},
		save: function () {
			if (!this.active) {
				this.history.push(this.path);
			}
			this.active = false;
			this.update();
		},
		storeLastCollection: function (data) {
			self.prevCollection = data.id;
		},
		storePrevSlide: function (data) {
			self.prevSlide = data.prev.id;
		},
		setPrevCollection: function (id) {
			self.prevCollection = id;
		},
		back: function () {
			if (this.nextSlideHistory()) {
				this.active = true;
				var path = this.history.pop();
				if (path) {
					app.goTo(path);
				}
			}
		},
		nextSlideHistory: function () {
			return !this.history.length < 1;
		},
		clearHistory: function () {
			this.history = [];
			this.update();
			this.active = true;
		}
	};
});

/**
 * Provides a function to lock and connect slides
 * ---------------------------------------------------------
 * *
 * * This module allows the creation of groups of different slides.
 * These will be connected either horizontally or vertically and stay connected when creating custom collections.
 *
 * Usage:
 *
 * To define content groups, you need to create a `contentGroups.json` next to the `presentation.json` file.
 *
 * Remember to make sure that all slideshows in the `presentation.json` file match the rules defined by the different content groups.
 *
 * If they don't match, the group definition will be ignored and a warning will be given in the javascript console.
 *
 * e.g.:
 *
 * 'test-group-1': {
 *       'orientation': 'horizontal',
 *       'slides': ['radiology_slide_1', 'radiology_slide_2']
 *   },
 * 'test-group-2': {
 *       'orientation': 'vertical',
 *       'slides': ['radiology_slide_1', 'radiology_slide_2', 'radiology_slide_3']
 *   }
 *
 * The content groups will be checked on each start
 *
 * The module is loaded within index.html
 *
 * @module ap-content-groups.js
 * @requires agnitio.js, contentGroups.json, ap-custom-collections.js
 * @author Marc Lutz, antwerpes ag
 */
app.register('ap-content-groups', function () {
	var self;
	return {
		definition: null,
		json: {},
		publish: {},
		events: {},
		states: [],
		onRender: function () {
			self = this;
			app.$.contentGroups = this;
			$.get('contentGroups.json').done(function (def) {
				var groupDefinition = def;
				if (typeof groupDefinition === 'string') {
					groupDefinition = JSON.parse(groupDefinition);
				}
				self.json = groupDefinition;
			});
			this.validateContentgroups();
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (!app.isVeevaWide) {
				if (window.ag) {
					window.ag.submit.data({
						label: 'Registered Module',
						value: 'ap-content-groups',
						category: 'BHC Template Modules',
						isUnique: true,
						labelId: 'bhc_registered_module',
						categoryId: 'bhc_template_modules'
					});
				}
			}
		},
		collectionContainsSlides: function (collection, groupDefinition) {
			/**
			 * Implements a function to check if the collection contains a slide from the contentGroup.json
			 * --------------------------------------------------------------------------------------------
			 *
			 * Tests if the collection needs validation
			 * by checking if the collection has any slide from the contentGroup
			 *
			 * @class validateContentgroups
			 * @constructor
			 */
			// get all slides which are used in this collection
			var collectionClone = $.extend(true, {}, collection);
			var slidesUsed;
			if (collection.content) {
				if (collectionClone.content.length > 0) {
					$.each(collectionClone.content, function (i, slideshowName) {
						collectionClone.content[i] = app.model.getStructure(slideshowName).content;
					});
					slidesUsed = collectionClone.content.reduce(function (a, b) {
						return a.concat(b);
					});
				}
			} else {
				slidesUsed = collectionClone.slides;
			}
			// check if the collection contains any slide from this contentGroup
			var collectionContainsSlide = false;
			if (slidesUsed) {
				$.each(groupDefinition.slides, function (i, slide) {
					if (~slidesUsed.indexOf(slide)) {
						collectionContainsSlide = true;
					}
				});
			}
			return collectionContainsSlide;
		},
		validateContentgroups: function () {
			// validate groups
			/**
			 * Implements a function to validate the content groups
			 * -----------------------------------------------------------
			 *
			 * @class validateContentgroups
			 * @constructor
			 */
			// var customCollections = app.$.customCollectionsStorage.getAll();
			// $.extend(true, app.model.get().storyboards, customCollections);
			$.get('contentGroups.json').done(function (def) {
				var groupDefinition = def;
				if (typeof groupDefinition === 'string') {
					groupDefinition = JSON.parse(groupDefinition);
				}
				function createHorizontalCollection() {
					$.each(app.model.get().storyboards, function (i, collection) {
						if (collection.type === 'collection') {
							var collectionContainsSlide = self.collectionContainsSlides(collection, definition);
							// if it contains a slide, validate if its been used correctly
							if (collectionContainsSlide) {
								var firstSlides = [];
								if (collection.content) {
									// normal Collections
									$.each(collection.content, function (item, slideshow) {
										if (definition.slides.indexOf(app.model.getStructure(slideshow).content[0]) > -1) {
											if (app.model.getStructure(slideshow).content.length === 1) {
												// okay
											} else {
												// eslint-disable-next-line
												console.warn('horizontal contentGroups may not have more than one vertical slides @%s', slideshow);
											}
										}
										if (typeof app.model.getStructure(slideshow).content === 'string') {
											firstSlides.push(app.model.getStructure(slideshow).content);
										} else {
											firstSlides.push(app.model.getStructure(slideshow).content[0]);
										}
									});
									if (~firstSlides.join(':').indexOf(definition.slides.join(':'))) {
										// eslint-disable-next-line
										console.log('contentGroup %s valid for presentation %s', groupName, collection.id);
										// write the matching contentgroups to the structures for use in the custom-collections
										var storyboard = app.model.hasStoryboard(collection.id) ? app.model.getStoryboard(collection.id) : app.model.getStoryboard(collection.name);
										if (storyboard) {
											if (!storyboard.contentGroups) {
												storyboard.contentGroups = {};
											}
											storyboard.contentGroups[groupName] = definition;
										}
									} else {
										// eslint-disable-next-line
										console.warn('contentGroup %s is invalid for presentation %s', groupName, collection.id);
										// eslint-disable-next-line
										console.warn('the group definition will be ignored!');
										delete groupDefinition[groupName];
									}
								} else {
									// custom Collections
									$.each(collection.slideshows, function (item, slideshow) {
										// if the slideshow contains a slide from the group definition
										if (definition.slides.indexOf(slideshow.content[0]) > -1) {
											// and is only 1 slide long vertically
											if (slideshow.content.length === 1) {
												// okay
											} else {
												// eslint-disable-next-line
												console.warn('horizontal contentGroups may not have more than one vertical slides @%s', slideshow);
											}
										}
										if (typeof app.model.getStructure(slideshow).content === 'string') {
											firstSlides.push(app.model.getStructure(slideshow).content);
										} else {
											firstSlides.push(app.model.getStructure(slideshow).content[0]);
										}
									});
									if (~firstSlides.join(':').indexOf(definition.slides.join(':'))) {
										// eslint-disable-next-line
										console.log('contentGroup %s valid for presentation %s', groupName, collection.id);
										// write the matching contentgroups to the structures for use in the custom-collections
										var $storyboard = app.model.hasStoryboard(collection.id) ? app.model.getStoryboard(collection.id) : app.model.getStoryboard(collection.name);
										if ($storyboard) {
											if (!$storyboard.contentGroups) {
												$storyboard.contentGroups = {};
											}
											$storyboard.contentGroups[groupName] = definition;
										}
									} else {
										// eslint-disable-next-line
										console.warn('contentGroup %s is invalid for presentation %s', groupName, collection.id);
										// eslint-disable-next-line
										console.warn('the group definition will be ignored!');
										delete groupDefinition[groupName];
									}
								}
							}
						}
					});
				}
				function createVerticalCollection() {
					$.each(app.model.get().storyboards, function (i, collection) {
						if (collection.type === 'collection') {
							var collectionContainsSlide = self.collectionContainsSlides(collection, definition);
							if (collectionContainsSlide) {
								var allSlides = [];
								if (collection.content) {
									$.each(collection.content, function (item, slideshow) {
										var slideshowSlides = app.model.getStructure(slideshow).content;
										allSlides.push(slideshowSlides);
									});
								} else {
									// custom Collection
									$.each(collection.slideshows, function (item, slideshow) {
										allSlides.push(slideshow.content);
									});
								}
								if (~allSlides.join(':').indexOf(definition.slides.join(','))) {
									// eslint-disable-next-line
									console.log('contentGroup %s valid for presentation %s', groupName, collection.id);
									// write the matching contentgroups to the structures for use in the custom-collections
									var storyboard = app.model.hasStoryboard(collection.id) ? app.model.getStoryboard(collection.id) : app.model.getStoryboard(collection.name);
									if (storyboard) {
										if (!storyboard.contentGroups) {
											storyboard.contentGroups = {};
										}
										storyboard.contentGroups[groupName] = definition;
									}
								} else {
									// eslint-disable-next-line
									console.warn('contentGroup %s is invalid for presentation %s', groupName, collection.id);
									// eslint-disable-next-line
									console.warn('the group definition will be ignored!');
									delete groupDefinition[groupName];
								}
							}
						}
					});
				}
				for (var property in groupDefinition) {
					if (groupDefinition.hasOwnProperty(property)) {
						var groupName = property;
						var definition = groupDefinition[property];
						if (definition.orientation === 'horizontal') {
							createHorizontalCollection();
						} else if (definition.orientation === 'vertical') {
							createVerticalCollection();
						} else {
							// eslint-disable-next-line
							console.log('unknown ContentGroup definition type');
						}
					}
				}
				self.definition = groupDefinition;
			});
		}
	};
});

/**
 * Implements Custom Collections Menu.
 * -----------------------------------
 *
 * This module allows the creation / naming / deletion and loading of custom collections.
 *
 * @module ap-custom-collections-menu.js
 * @requires jquery.js, iscroll.js, custom-collections-storage.js
 * @author David Buezas, antwerpes ag
 */
app.register('ap-custom-collections-menu', function () {
	var translation = {
		'ENTER_PRESENTATION_NAME': 'Please enter a name for your presentation',
		'PRESENTATION_NAME': 'Presentation name',
		'NAME_ALREADY_EXISTS': 'Another presentation exists with the same name, please choose a new one',
		'$PRESENTATION_NAME$_WILL_BE_REMOVED': '\'$PRESENTATION_NAME$\' will be erased',
		'OK': 'OK',
		'CANCEL': 'Cancel',
		'YES': 'Yes',
		'NO': 'No',
		'PRESENTATION_IS_EMPTY': 'This presentation is empty.',
		'NOT_ALLOWED_TO_DELETE': 'Deleting the current presentation is not allowed'
	};
	/**
	 * Implements Custom Collections Menu
	 * ------------------------------------
	 * This module allows the creation / naming / deletion and loading of custom collections.
	 *
	 * @class ap-custom-collections-menu
	 * @constructor
	 */
	var self;
	var daDefaultDataPath = './da-default-data.json';
	var daDefaultData = {};
	return {
		publish: {},
		events: {
			'tap .newPresentation': 'createNewPresentation',
			'tap .trash': 'deletePresentation',
			'tap .rename': 'renamePresentation',
			'tap .presentation': 'openPresentation',
			'tap .edit': 'editPresentation',
			'tap .favorite': 'favorisePresentation'
		},
		states: [
			{
				id: 'visible'
			}
		],
		onRender: function () {
			self = this;
			$.ajax(daDefaultDataPath)
				.done(function (data) {
					var parsedData = typeof data === 'string' ? JSON.parse(data) : data;
					generateDefaultStructure(parsedData);
				});
			function generateDefaultStructure (data) {
				daDefaultData = data;
				var structures = app.model.get().structures;
				var slideshows = daDefaultData.slideshows.map(function (slideshowId) {
					return structures[slideshowId] || false;
				});
				daDefaultData.slideshows = slideshows;
			}
			app.$.customCollectionsMenu = this;
			app.$.on('open:ap-custom-collections-menu', function () {
				this.show();
			}.bind(this));
			app.$.on('close:ap-custom-collections-menu', function () {
				this.hide();
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				this.hide();
			}.bind(this));
			self.appriseDefaults = {
				textOk: translation.OK,
				textCancel: translation.CANCEL,
				textYes: translation.YES,
				textNo: translation.NO
			};
			var presentationsList = $('.presentationsContainer');
			presentationsList.sortable({
				axis: 'y',
				items: '.row:not(.newPresentationButtonContainer)',
				scroll: true,
				update: function () {
					self.updateCustomPresentationOrder();
				}
			});
			function checkTouchOutX(element, eventName, e) {
				var customEvent = eventName || 'out:touchout';
				var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
				var elementPosition = $(element).offset();
				var x = Math.floor(touch.pageX - elementPosition.left);
				if (x >= $(element).width() || x <= 0) {
					element.trigger(customEvent);
				}
			}
			presentationsList.on('touchmove', checkTouchOutX.bind(this, presentationsList, 'out-x:sortable-list'));
			presentationsList.on('out-x:sortable-list', presentationsList.trigger.bind(presentationsList, 'mouseup'));
			$.each(app.$.customCollectionsStorage.getAll(), function (index, presentationObject) {
				self._addPresentationToView(presentationObject.name, presentationObject, /* animated: */false);
			});
		},
		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {},
		hide: function () {
			app.unlock();
			this.reset();
		},
		show: function () {
			app.lock();
			this.goTo('visible');
		},
		/**
		 * Adds a new entry to the menu
		 * -----------------------------------
		 *
		 * @param {string} presentationName - presentation name
		 * @param {object} presentationObject - presentation object
		 * @private
		 * @method _addPresentationToView
		 * @returns {void} - returns nothing
		 */
		_addPresentationToView: function (presentationName, presentationObject) {
			var $self = this;
			var $row = $('.row.template').clone();
			$row.removeClass('template');
			$row.data('presentationName', presentationName);
			$row.find('.presentation .name').text(presentationName);
			if (presentationObject.isFavorite) {
				$row.find('.favorite').addClass('selected');
			}
			$row.insertBefore($self.$('.newPresentationButtonContainer'));
		},
		/**
		 * Updates the current order of custom presentations
		 * @public
		 * @method updateCustomPresentationOrder
		 * @returns {void} - returns nothing
		 */
		updateCustomPresentationOrder: function () {
			var rowsArray = [];
			$('.presentationsContainer').children('.row').each(function () {
				var presentationName = $(this).data('presentationName');
				if (presentationName) {
					if (app.$.customCollectionsStorage.getFavorites().length < 3) {
						var isFavorite = $(this).find('.favorite').hasClass('selected');
						var collection = app.$.customCollectionsStorage.get(presentationName);
						collection.isFavorite = isFavorite;
						app.$.customCollectionsStorage.add(presentationName, collection);
					}
					rowsArray.push(presentationName);
				}
			});
			if (rowsArray.length > 1) {
				app.$.customCollectionsStorage.updateOrder(rowsArray);
			}
			app.$.trigger('update:favorites');
		},
		/**
		 * creates a new presentation
		 * @public
		 * @method createNewPresentation
		 * @returns {void} - returns nothing
		 */
		createNewPresentation: function () {
			function askName(defaultName) {
				var optn = $.extend({}, self.appriseDefaults, {input: defaultName});
				window.apprise(translation.ENTER_PRESENTATION_NAME, optn, function (newName) {
					if (newName === false || newName === '') {
						return; // user canceled
					} else if (app.$.customCollectionsStorage.get(newName)) {
						window.apprise(translation.NAME_ALREADY_EXISTS, self.appriseDefaults, function () {
							askName(newName); // ask again
						});
					} else {
						var collectionId = 'custom-collection-' + Math.floor((Math.random() * 10000) + 1);
						var presentationObject = {
							id: collectionId,
							name: newName,
							type: 'collection',
							slideshows: daDefaultData.slideshows,
							slides: daDefaultData.slides,
							presentation: daDefaultData.presentation,
							isCustomPresentation: true,
							isFavorite: false
						};
						app.$.customCollectionsStorage.add(newName, presentationObject);
						self._addPresentationToView(newName, presentationObject, true);
					}
				});
			}
			var standardName = translation.PRESENTATION_NAME;
			askName(standardName);
		},
		/**
		 * deletes the presentation
		 * @public
		 * @method deletePresentation
		 * @param {object} event - event object
		 * @returns {void} - returns nothing
		 */
		deletePresentation: function (event) {
			var $row = $(event.target).parent();
			var presentationName = $row.data('presentationName');
			var confirmationTitle = translation.$PRESENTATION_NAME$_WILL_BE_REMOVED.replace('$PRESENTATION_NAME$', presentationName);
			var optn = $.extend({}, self.appriseDefaults, {confirm: true});
			var current = app.slideshow.getId();
			var collections = app.$.customCollectionsStorage.getAll();
			var collectionData = collections[presentationName];
			if (collectionData && collectionData.id !== current) {
				window.apprise(confirmationTitle, optn, function (answer) {
					if (answer) {
						app.$.customCollectionsStorage.delete(presentationName);
						$row.animate({height: 0}, function () {
							$(this).remove();
						});
					}
				});
				app.$.trigger('update:favorites');
			} else {
				window.apprise(translation.NOT_ALLOWED_TO_DELETE, self.appriseDefaults);
			}
		},
		/**
		 * renames the presentation
		 * @public
		 * @method renamePresentation
		 * @param {object} event - event object
		 * @returns {void} - returns nothing
		 */
		renamePresentation: function (event) {
			var $row = $(event.target).parent();
			function askName(defaultName) {
				var optn = $.extend({}, self.appriseDefaults, {input: defaultName});
				window.apprise(translation.ENTER_PRESENTATION_NAME, optn, function (newName) {
					if (newName === false || newName === oldName || newName === '') {
						return; // user canceled
					} else if (app.$.customCollectionsStorage.get(newName)) {
						window.apprise(translation.NAME_ALREADY_EXISTS, self.appriseDefaults, function () {
							askName(newName); // ask again
						});
					} else {
						app.$.customCollectionsStorage.rename(oldName, newName);
						$row.data('presentationName', newName);
						var $name = $row.find('.name');
						$name.css({opacity: 0});
						setTimeout(function () {
							$name.text(newName);
							$name.css({opacity: 1});
						}, 500);
					}
				});
			}
			var oldName = $row.data('presentationName');
			askName(oldName);
		},
		/**
		 * open the presentation
		 * @public
		 * @method openPresentation
		 * @param {object} event - event object
		 * @returns {void} - returns nothing
		 */
		openPresentation: function (event) {
			var presentationName = $(event.target).parent().parent().data('presentationName');
			if (!presentationName) {
				presentationName = $(event.target).parent().data('presentationName');
			}
			var collection = app.$.customCollectionsStorage.get(presentationName);
			// app.menu.linksConfig[homeSlideName] = {title: '<div class='homeIcon' />', classname: 'home'}
			if (collection.slideshows.length > 0) {
				var slideshowIdArray = [];
				$.each(collection.slideshows, function (i, slideshow) {
					slideshowIdArray.push(slideshow.id);
					var slidesArrary = [];
					$.each(slideshow.content, function (item, slide) {
						slidesArrary.push(slide);
					});
					var temp = {
						id: slideshow.id,
						name: slideshow.name,
						content: slidesArrary,
						type: 'slideshow'
					};
					if (!app.model.hasStructure(slideshow.id)) {
						app.model.addStructure(slideshow.id, temp);
					}
				});
				var storyboardData = {
					id: collection.id,
					name: collection.name,
					content: slideshowIdArray
				};
				if (!app.model.hasStoryboard(collection.id)) {
					app.model.addStoryboard(collection.id, storyboardData);
				}
				if (app.slideshow.resolve().slideshow === collection.id) {
					app.slideshow.first();
				} else {
					app.slideshow.init(collection.id);
					app.slideshow.load(collection.id);
				}
				app.$.toolbar.hide();
				app.unlock();
			} else {
				window.apprise(translation.PRESENTATION_IS_EMPTY, self.appriseDefaults);
			}
		},
		/**
		 * edit the presentation
		 * @public
		 * @method editPresentation
		 * @param {object} event - event object
		 * @returns {void} - returns nothing
		 */
		editPresentation: function (event) {
			// load custom collections editor into own container
			/*
			 new CustomCollections({
			 $container: self.$container,
			 presentationName: $(this).parent().data('presentationName')
			 });
			 */
			self.hide();
			var trigger = 'open:ap-custom-collections';
			var presentationName = $(event.target).parent().data('presentationName');
			app.$.trigger(trigger, {presentationName: presentationName});
		},
		/**
		 * favorise the presentation
		 * @public
		 * @method favorisePresentation
		 * @param {object} event - event object
		 * @returns {void} - returns nothing
		 */
		favorisePresentation: function (event) {
			// set favorite
			var $this = $(event.target);
			var presentationName = $this.parent().data('presentationName');
			var collection = app.$.customCollectionsStorage.get(presentationName);
			if (app.$.customCollectionsStorage.getFavorites().length < 3 || collection.isFavorite) {
				$this.toggleClass('selected');
				collection.isFavorite = !collection.isFavorite;
				app.$.customCollectionsStorage.add(presentationName, collection);
			} else {
				var $popup = $('#maxFavoritesPopUp');
				$popup.fadeIn();
				$popup.delay(1600).fadeOut();
			}
			app.$.trigger('update:favorites');
		}
	};
});

/**
 * Implements Custom Collections Editor.
 * -------------------------------------
 *
 * This module allows the edition of custom collections directly on the client (iPad / Browser).
 *
 * @module ap-custom-collections.js
 * @requires jquery.js, iscroll.js, ap-custom-collections-storage.js, ap-overview.js
 * @author David Buezas, antwerpes
 */
app.register('ap-custom-collections', function () {
	var self;
	var eventNamespace = '.custom-collections';
	var _mouseMoveEvent;
	var _mouseUpEvent;
	var currentTouchPos = 0;
	var id;
	var isAnimationFinish = true;
	var daDefaultDataPath = './da-default-data.json';
	var daDefaultData = {};
	return {
		presentationName: null,
		dragData: null,
		publish: {},
		events: {
			'mousedown .o-slide': 'startDragEventHandler',
			'touchstart .o-slide': 'startDragEventHandler',
			'MSPointerDown .o-slide': 'startDragEventHandler',
			'longTouch .overview .o-slide': 'slidePreview',
			'longTouch .edit-zone .o-slide': 'slidePreview',
			'tap .collection-name': function () {
				self.markAlreadyUsedSlides();
			}
		},
		states: [
			{
				id: 'visible'
			}
		],
		onRender: function () {
			self = this;
			self.dragData = null;
			$.ajax(daDefaultDataPath)
				.done(function (data) {
					var parsedData = typeof data === 'string' ? JSON.parse(data) : data;
					generateDefaultStructure(parsedData);
				});
			function generateDefaultStructure(data) {
				daDefaultData = data;
				var structures = app.model.get().structures;
				var slideshows = daDefaultData.slideshows.map(function (slideshowId) {
					return structures[slideshowId] || false;
				});
				daDefaultData.slideshows = slideshows;
			}
			function checkContentGroup(contentGroup, slides) {
				return contentGroup.slides.some(function (slide) { return slides.indexOf(slide) > -1; });
			}
			function getSavedContentGroups(slides) {
				var allContentGroups = app.$.contentGroups.json;
				var savedContentGroups = {};
				Object.keys(allContentGroups).forEach(function (key) {
					if (checkContentGroup(allContentGroups[key], slides)) {
						savedContentGroups[key] = allContentGroups[key];
					}
				});
				return savedContentGroups;
			}
			app.$.customCollections = this;
			app.$.on('open:ap-custom-collections', function (data) {
				this.show();
				this.load(data.presentationName);
			}.bind(this));
			app.$.on('close:ap-custom-collections', function () {
				this.hide();
				this.unload();
			}.bind(this));
			// Fix: above listener is never triggered because it's not a 'button'
			// in bottom bar
			// This is called from ap-toolbar module
			app.$.on('close:ap-custom-collections-menu', function () {
				this.hide();
				this.unload();
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				this.hide();
			}.bind(this));
			var $editZone = $('.edit-zone');
			$('.cancel').on('tap', function () {
				// user canceled, so go back to the custom collection menu without saving
				self.unload();
				self.hide();
				app.$.overview.hide();
				app.$.customCollectionsMenu.show();
			});
			$('.save').on('tap', function () {
				// save and then go back to the custom collection menu
				var presentation = $.map($editZone.find('.o-slideshow'), function (slideshow) {
					return [$.map($(slideshow).find('.o-slide'), function (slide) {
						return $(slide).attr('data-id');
					})];
				});
				var slides = [];
				$.each(presentation, function (index, slideshows) {
					$.each(slideshows, function (ind, slideId) {
						slides.push(slideId);
					});
				});
				var slideshowArray = [];
				var slideshowIdArray = [];
				var currentChapter = 1;
				$.each(presentation, function (i, slideshow) {
					var slideshowId = 'custom-slideshow-' + Math.floor((Math.random() * 10000) + 1);
					var defaultSlideshow = self._getDefaultSlideshow(slideshow);
					var slideShowName = defaultSlideshow ? defaultSlideshow.name : 'Chapter ' + currentChapter++;
					slideshowIdArray.push(slideshowId);
					var slideshowObj = {id: slideshowId, name: slideShowName, type: 'slideshow', content: slideshow};
					slideshowArray.push(slideshowObj);
					app.model.addStructure(slideshowId, slideshowObj);
				});
				// var homeSlideName = slideshowArray[0].id;
				var existingCollection = app.$.customCollectionsStorage.get(self.presentationName) || {};
				var collectionId = existingCollection.id || null;
				if (!collectionId) {
					collectionId = 'custom-collection-' + Math.floor((Math.random() * 10000) + 1);
				}
				// If updating current slideshow, let's move back to first slide
				// This will prevent presentation to get stuck on removed slide
				if (collectionId === app.slideshow.getId()) {
					app.model.once('update:content', function () {
						app.slideshow.first();
						app.slideshow.trigger('update:anthill-current', {current: {id: app.slideshow.get()}, prev: {id: null}});
					});
				}
				var existingStoryboard = app.model.getStoryboard(collectionId);
				if (existingStoryboard) {
					app.model.updateContent(collectionId, slideshowIdArray);
				} else {
					app.model.addStoryboard(collectionId, {
						id: collectionId,
						name: self.presentationName,
						content: slideshowIdArray,
						type: 'collection'
					});
				}
				var customCollection = {
					id: collectionId,
					name: self.presentationName,
					type: 'collection',
					slideshows: slideshowArray,
					slides: slides,
					presentation: presentation,
					isCustomPresentation: true,
					savedContentGroups: getSavedContentGroups(slides)
				};
				app.$.customCollectionsStorage.add(self.presentationName, customCollection);
				app.$.contentGroups.validateContentgroups();
				self.unload();
				self.hide();
				app.$.overview.hide();
				app.$.customCollectionsMenu.show();
				// Need to update the menu in case current slideshow was changed
				if (app.slideshow.getId() === collectionId) {
					app.$.menu.setup(collectionId);
				}
			});
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-custom-collections',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		getTouch: function (event) {
			var touch;
			if (touchy.isTouch) {
				// 11/16/2017 Updates to custom collections. Making it more stable but does not work in windows
				if (event.originalEvent) {
					// Modern mobile browsers
					if (event.originalEvent.touches) {
						touch = event.originalEvent.touches[currentTouchPos];
					}
				} else if (event.touches) { // Modern mobile browsers
					touch = event.touches[currentTouchPos];
				} else {
					touch = event;
				}
			} else {
				touch = event;
			}
			return touch;
		},
		getTouches: function (event) {
			var touches = [];
			if (event.originalEvent) {
				// Modern mobile browsers
				if (event.originalEvent.touches) {
					touches = Array.from(event.originalEvent.touches);
				}
			} else if (event.touches) { // Modern mobile browsers
				touches = Array.from(event.touches);
			}
			return touches;
		},
		updateCurrentTouchPos: function (event) {
			self.getTouches(event).forEach(function (e, i) {
				if (e.identifier === id) currentTouchPos = i;
			});
		},
		onEnter: function () {},
		onExit: function () {},
		hide: function () {
			app.unlock();
			this.reset();
			this.unload();
		},
		show: function () {
			app.lock();
			this.goTo('visible');
		},
		load: function (presentationName) {
			self.presentationName = presentationName;
			_mouseMoveEvent = touchy.events.move + eventNamespace;
			_mouseUpEvent = touchy.events.end + eventNamespace;
			var $document = $(document);
			var slideHTML = '<div class="o-slide"></div>'; // thumbnail template
			var slideshowHTML = '<figure class="o-slideshow"></figure>'; // slideshow template
			var collectionHTML = '<div class="o-collection"></div>'; // collection template
			$('.presentationName').text(presentationName);
			var $editZone = $('.edit-zone');
			var storage = app.$.customCollectionsStorage.get(presentationName);
			// add home and summary slides to the new presentation
			if (storage.presentation.length === 0) {
				Object.keys(daDefaultData).map(function (key) {
					storage[key] = daDefaultData[key];
				});
			}
			function checkImageAddText(src, ele, slide) {
				var image = new Image();
				image.onload = function () {
					if ('naturalHeight' in this) {
						if (this.naturalHeight + this.naturalWidth === 0) {
							this.onerror();
							return false;
						}
					} else if (this.width + this.height === 0) {
						this.onerror();
						return false;
					}
					ele.text('');
					return true;
				};
				image.onerror = function () {
					ele.text(app.model.get().slides[slide].name);
					// eslint-disable-next-line
					console.log('thumbnail not loaded');
					return false;
				};
				image.src = src;
			}
			// create the initial DOM representation from stored presentation
			var $oCollection = $(collectionHTML).append($.map(storage.presentation, function (slideshow) {
				var $slideshow = $(slideshowHTML);
				$slideshow.append($.map(slideshow, function (slideId) {
					var $slide = $(slideHTML).attr('data-id', slideId);
					$slide.css({'background-image': 'url(slides/' + slideId + '/' + slideId + '.png)'});
					var $thumbText = $('<p class="o-text"></p>').appendTo($slide);
					checkImageAddText('slides/' + slideId + '/' + slideId + '.png', $thumbText, slideId);
					$.each(app.$.contentGroups.json, function (groupName, group) {
						if (group.slides.indexOf(slideId) < 0) {
							return;
						} else if (group.slides.indexOf(slideId) === 0) {
							$slide.addClass('first-in-group');
						}
						$slide.data('contentGroup', group);
						$slide.addClass('grouped');
						$slide.addClass(groupName);
						if (group.orientation === 'vertical') {
							$slide.addClass('vertical-group');
						}
						if (group.orientation === 'horizontal') {
							$slideshow.addClass('grouped');
							if (group.slides.indexOf(slideId) === 0) {
								$slideshow.addClass('first-in-group');
							}
						}
					});
					return $slide;
				}));
				return $slideshow;
			}));
			$editZone.append($oCollection);
			self._setupPlaceholders();
			self.markAlreadyUsedSlides();
			var slideWidth = 136;
			var slideHeight = 102;
			app.$.overview.showCustomOverview();
			self.markAlreadyUsedSlides();
			// each time a new collection is selected on the overview, new slide thumbnails are created
			// so we have to re-mark the slides which are already present in the edit zone
			self.scroll = new IScroll($editZone[0], {
				scrollbars: true,
				mouseWheel: true,
				scrollX: true,
				directionLockThreshold: Number.MAX_VALUE
			});
			/*
			 Stores information about the dragged slide while it is being dragged.
			 At runtime it looks like this:
			 {
			 startPos: {
			 x: number,
			 y: number
			 },
			 $clone: (jQ object) draggable slide clone,
			 $original: (jQ object) original slide,
			 currentTarget: DOM element
			 */
			$document.on(_mouseMoveEvent, function (event) {
				// slide comes from the overview and is already present in the edit zone
				// so we forbid it by returning now
				// if drag data is undefined, the user is not dragging anything
				if (event.target.classList.contains('already-used') ||
					!self.dragData) return;

				var touch = self.getTouch(event);
				var target = document.elementFromPoint(touch.pageX, touch.pageY);
				var diffX = touch.pageX - self.dragData.startPos.x;
				var diffY = touch.pageY - self.dragData.startPos.y;
				// If dragged less than 20 pixels, then we don't consider it a drag
				if (Math.abs(diffX) > 20 || Math.abs(diffY) > 20) {
					self.dragData.didDrag = true;
				} else {
					self.dragData.didDrag = false;
				}
				/* Drag Move*/
				self.dragData.$clone.css('transform',
					'translate3d(' +
					(diffX) + 'px,' +
					(diffY) + 'px,' +
					'0' +
					')'
				);
				self.dragData.$clone.css('z-index', '9999');
				/* Drag over */
				var newTarget = null;
				var contentGroup = self.dragData.$original.data('contentGroup');
				if (target && target.classList.contains('o-slide')) {
					// if dragging over a slide, decide which placeholder (over, under, left, right) to mark
					// based on over which quartile the finger (mouse) is
					var $underlyingSlide = $(target);
					var overSlidePos = $underlyingSlide.offset();
					var x = (overSlidePos.left + slideWidth / 2) - touch.pageX;
					var y = (overSlidePos.top + slideHeight / 2) - touch.pageY;
					var angle = Math.atan2(y, -x);
					var PI = Math.PI;
					var $underlyingSlideshow = $underlyingSlide.parent();
					if (angle > -PI / 4 && angle < PI / 4) { // right
						newTarget = $underlyingSlideshow.next('.placeholder')[0];
					} else if (angle > PI / 4 && angle < PI * 3 / 4) { // over
						newTarget = $underlyingSlide.prev('.placeholder')[0];
					} else if (angle > PI * 3 / 4 || angle < -PI * 3 / 4) { // left
						newTarget = $underlyingSlideshow.prev('.placeholder')[0];
					} else if (angle > -PI * 3 / 4 && angle < -PI / 4) { // under
						newTarget = $underlyingSlide.next('.placeholder')[0];
					}
					if (!(contentGroup && contentGroup.orientation === 'horizontal' && $(target).parent('.o-collection').length > 0)) {
						newTarget = null;
					} else {
						$editZone.addClass('highlighted');
					}
					// if (target && target.classList.contains('source')) {
					//  newTarget = target; // TO DO: Make another solution, because this can break.
					// }
				} else if (target && target.classList.contains('placeholder') && self.dragData.didDrag) {
					newTarget = target;
					if (contentGroup && contentGroup.orientation === 'horizontal' && !($(target).parent('.o-collection').length > 0)) {
						newTarget = null;
					}
				}
				if (self.dragData.currentTarget !== newTarget) {
					if (self.dragData.currentTarget) self.dragData.currentTarget.classList.remove('target');
					self.dragData.currentTarget = null;
					if (newTarget && isAnimationFinish) {
						self.dragData.currentTarget = newTarget;
						self.dragData.currentTarget.classList.add('target');
					}
				}
			});
			$document.on(_mouseUpEvent, function () {
				/* Drop */
				// updating a position of a touch related to our slide that is being dragged
				if (event.type === 'touchend') self.updateCurrentTouchPos(event);

				// if drag data is undefined, the user is not dragging anything
				// forbid an execution if touchend came from a touch that is not related to a slide that is being dragged
				if (!self.dragData ||
					event.type === 'touchend' &&
					Array.from(event.touches).map(function (e) {return e.identifier;}).includes(id)) return;

				var dragData = self.dragData;
				dragData.$original.removeClass('source');
				if(isAnimationFinish) {
					if (dragData.currentTarget && dragData.didDrag) {
						/* Drop Insert */
						var $newSlide = dragData.$original.clone(true).off().removeClass('highlighted');
						$(dragData.currentTarget).replaceWith($newSlide);
						var contentGroup = $newSlide.data('contentGroup');
						if (contentGroup) {
							$newSlide.addClass('grouped');
							$newSlide[0].classList.add('first-in-group');
							if (contentGroup.orientation === 'vertical') {
								$newSlide.addClass('vertical-group');
							}
							if (contentGroup && contentGroup.orientation === 'vertical' && !$newSlide.parent().is('.o-slideshow')) {
								$newSlide.wrapAll(slideshowHTML);
							}
						}
						if ($newSlide.parent().is('.o-slideshow')) {
							isAnimationFinish = false;
							$newSlide.css({
								height: 0
							});
							$newSlide
								.animate({
									height: slideHeight
								}, 500, 'easeOutBounce', function () {
									self.scroll.refresh();
									isAnimationFinish = true;
								});
						} else {
							$newSlide.wrap(slideshowHTML);
							var $slideshow = $newSlide.parent();
							$slideshow.css({
								width: 0
							});
							$slideshow
								.animate({
									width: slideWidth
								}, 500, 'easeOutBounce', function () {
									self.scroll.refresh();
								});
						}
						if (contentGroup && contentGroup.orientation === 'horizontal') {
							$newSlide.parents('.o-slideshow').addClass('grouped');
							$newSlide[0].parentNode.classList.add('first-in-group');
						}
					}
					var comesFromCustomPresentation = $editZone.find(dragData.$original).length > 0;
					if (comesFromCustomPresentation && dragData.didDrag) {
						/* Delete original */
						var originalIsAlone = (dragData.$original.siblings('.o-slide').length === 0);
						if (originalIsAlone) {
							var $slideshow_ = dragData.$original.parent();
							var placeholder = $slideshow_.prev('.placeholder');
							placeholder.css('transition', 'none');
							$slideshow_.add(placeholder)
								.animate({
									width: 0
								}, 500, 'easeOutBounce', function () {
									$(this).remove();
									self._setupPlaceholders();
									self.markAlreadyUsedSlides();
									self.scroll.refresh();
								});
						} else {
							var $placeholder = dragData.$original.next('.placeholder');
							$placeholder.css('transition', 'none');
							dragData.$original.add($placeholder)
								.animate({
									height: 0
								}, 500, 'easeOutBounce', function () {
									$(this).remove();
									self._setupPlaceholders();
									self.markAlreadyUsedSlides();
									self.scroll.refresh();
								});
						}
					}
				}
				self.dragData.$clone.remove();
				self._setupPlaceholders();
				self.dragData = null;
				self.markAlreadyUsedSlides();
				self.scroll.enable();
			});
		},
		unload: function () {
			$('.edit-zone').empty();
			app.$.overview.unload();
			self.dragData = null;
			$(document).off(_mouseUpEvent);
			$(document).off(_mouseMoveEvent);
		},
		/**
		 * Adds and removes placeholders as needed.
		 * Thumbnails are dropped into placeholders.
		 * @private
		 * @method _setupPlaceholders
		 * @returns {void}
		 */
		_setupPlaceholders: function () {
			var placeholderHTML = '<div class="placeholder">'; // placeholder template, slides can be dragged into placeholders
			var $editZone = $('.edit-zone');
			var oSlideshowSelector = '.o-slideshow:only-child .o-slide:only-child';
			var oSlideshowFirstChild = '.o-slideshow:first-child';
			var oSlideshowLastChild = '.o-slideshow:last-child';
			// .o-slide+o.slide --> vertically grouped slides
			$editZone.find('.o-slide:first-child,' +
				'.o-slide+.o-slide, .o-slideshow+.o-slideshow')
				.not('.o-slide.grouped+.o-slide.grouped, .o-slideshow.grouped + .o-slideshow.grouped')
				.before(placeholderHTML);
			// console.log('QUERY', $editZone.find('.o-slide+.o-slide'));
			// Add placeholder before horizontal group
			$editZone.find('.o-slideshow.first-in-group')
				.before(placeholderHTML);
			$editZone.find('.first-in-group.vertical-group').before(placeholderHTML);
			$editZone.find('.o-slide:last-child')
				.after(placeholderHTML);
			$editZone.find('.o-collection:empty').append(placeholderHTML);
			// Remove first placeholders
			// $editZone.find('.o-slide.start-group.first-in-group').closest('.o-slideshow').prev('.placeholder').remove();
			// $editZone.find('.o-slide.start-group.first-in-group').next('.placeholder').remove();
			$editZone.find('.placeholder+.placeholder').prev().remove();
			$editZone.find('.o-slideshow > .placeholder:only-child, .o-slideshow:empty').remove();
			$editZone.find('.placeholder+.placeholder').prev().remove(); // must be repeated!

			$editZone.find('.o-collection >' + oSlideshowLastChild).after(placeholderHTML);
			$editZone.find('.o-collection >' + oSlideshowFirstChild).before(placeholderHTML);
			if (this.isInEditZone($editZone, oSlideshowSelector)) {
				$editZone.find(oSlideshowFirstChild).after(placeholderHTML);
			}
		},
		/**
		 * Adds the class 'already-used' to all the overview slides which were added to the edit zone.
		 * This class will forbid marked slides from being added again
		 * @private
		 * @method markAlreadyUsedSlides
		 * @returns {void}
		 */
		markAlreadyUsedSlides: function () {
			var scope = '#' + self.id;
			var $editZone = $('.edit-zone');
			var $overview = $(scope + ' .custom-collections .overview .collection-overview');
			$overview.find('.already-used').removeClass('already-used');
			$editZone.find('.o-slide').each(function () {
				$overview.find('[data-id="start_slide"]').addClass('already-used');
				$overview.find('[data-id="' + $(this).attr('data-id') + '"]').addClass('already-used');
			});
		},
		// Under normal circumstances we would get a custom
		// event from Touchy library.
		// TODO: refactor into an API
		showTooltip: function (event) {
			// console.log('showTooltip event: ', event);
			var el = event.target;
			// Getting coordinates from user's click/tap
			var xPos = event.clientX || event.detail.clientX;
			var yPos = event.clientY || event.detail.clientY;
			var message = 'This slide cannot be added twice';
			// Start slide get's special treatment
			if ($(el).attr('data-id') === 'start_slide') {
				message = 'Start slide cannot be added';
			}
			var d = document.createElement('div');
			d.className = 'o-tip';
			d.innerHTML = message;
			document.body.appendChild(d);
			el.removeEventListener('drag', self.showTooltip);
			// Setting the 'left' and 'top' position from coordinates
			d.style.left = xPos + 'px';
			d.style.top = yPos + 'px';
			// Show the label
			d.style.display = 'block';
			setTimeout(function () {
				document.body.removeChild(d);
			}, 1500);
		},
		isInEditZone: function ($editZone, selector) {
			return $editZone.find(selector).length > 0;
		},
		getOriginalSlideOffsetCompensation: function (group, $originalSlide, originalSlideId) {
			var originalSlideOffsetCompensation = {x: 0, y: 0};
			var isVerticalOrientation = group.orientation === 'vertical';
			var offsetType = isVerticalOrientation ? 'height' : 'width';
			var offset = (group.slides.indexOf(originalSlideId) * $originalSlide[offsetType]()) * -1;
			if (isVerticalOrientation) {
				originalSlideOffsetCompensation.y = offset;
			} else {
				originalSlideOffsetCompensation.x = offset;
			}
			return originalSlideOffsetCompensation;
		},
		getOriginalGroupSlides: function (group, scope) {
			var selector = [];
			$.each(group.slides, function (ind, slide) {
				selector.push(scope + ' .o-slide[data-id="' + slide + '"]');
			});
			return $(selector.join(','));
		},
		_getDefaultSlideshow: function (slideshow) {
			var index = daDefaultData.presentation.findIndex(function (item) {
				return item[0] === slideshow[0];
			});
			return index > -1 ? daDefaultData.slideshows[index] : null;
		},
		startDragEventHandler: function (event) {
			/* Drag Start */
			/* Drag Start */
			// 11/16/2017 Updates to custom collections. Making it more stable but does not work in windows
			// slide comes from the overview and is already present in the edit zone
			// so we forbid it by returning now
			// forbid an mousedown event handling on touch device
			// forbid execution of function on already used slide
			function isMandatorySlide(slideElement) {
				return app.$.customCollectionsStorage.props.mandatory &&  slideElement.classList.contains('defaultSlide');
			}
			if (self.dragData ||
				isLastSlides(event) ||
				touchy.isTouch &&
				event.type === 'mousedown') return;

			function isLastSlides() {
				var isEditZoneAction = $(event.target).parents('.edit-zone').length;
				return isEditZoneAction ? getAllUniqueSlides().length === 1 : false;
			}
			function getAllUniqueSlides() {
				var allSlides = $('.edit-zone').find('.o-slide');
				var filteredSlides = allSlides.filter(function (index, item) {
					return !$(item).hasClass('grouped') || $(item).hasClass('first-in-group');
				});
				return filteredSlides;
			}

			// updating a position of a touch related to our slide that is being dragged
			if (event.type === 'touchstart') {
				id = Array.from(event.targetTouches)[0].identifier;
				self.updateCurrentTouchPos(event);
			}

			var touch = self.getTouch(event);

			if (event.target.classList.contains('already-used')) {
				event.target.addEventListener('drag', self.showTooltip);
				return;
			}
			var selectors = '';
			daDefaultData.slides.map(function (slide, index) {
				selectors += '.custom-collections .edit-zone .o-collection > .o-slideshow .o-slide[data-id="' + slide + '"]';
				if (index < daDefaultData.slides.length - 1) {
					selectors += ', ';
				}
			});
			var defaultSlides = document.querySelectorAll(selectors);
			var dsLength = defaultSlides.length;
			for (var i = 0; i < dsLength; i++) {
				defaultSlides[i].classList.add('defaultSlide');
			}
			// defaultSlides.forEach(function(ds) {
			// 	ds.classList.add('defaultSlide');
			// });
			if (isMandatorySlide(event.target)) return;
			self.scroll.disable();
			var $originalSlide = $(event.target);
			var $clone = $originalSlide.clone().removeClass('highlighted');
			var contentGroups = $originalSlide.parents('.o-collection').data('contentGroups');
			var originalSlideOffsetCompensation = {
				x: 0,
				y: 0
			};
			var contentGroup = $originalSlide.data('contentGroup');
			if (this.isInEditZone($('.edit-zone'), $originalSlide) && contentGroup) {
				$originalSlide = this.getOriginalGroupSlides(contentGroup, '.edit-zone');
				originalSlideOffsetCompensation = this.getOriginalSlideOffsetCompensation(contentGroup, $originalSlide);
				$clone = $originalSlide.clone(true).off();
			} else if (contentGroups) {
				var originalSlideId = $originalSlide.data('id');
				var scope = '#' + this.id;
				$.each(contentGroups, function (groupName, group) {
					if (group.slides.indexOf(originalSlideId) < 0) {
						return;
					}
					originalSlideOffsetCompensation = this.getOriginalSlideOffsetCompensation(group, $originalSlide, originalSlideId);
					$originalSlide = this.getOriginalGroupSlides(group, scope);
					$clone = $originalSlide.clone(true).off();
				}.bind(this));
			}
			self.dragData = {
				startPos: {
					x: touch.pageX + originalSlideOffsetCompensation.x,
					y: touch.pageY + originalSlideOffsetCompensation.y
				},
				$clone: $clone,
				$original: $originalSlide,
				currentTarget: null,
				didDrag: false
			};
			$clone.remove();
			self.dragData.$original.addClass('source');
			var offset = self.dragData.$original.offset();
			self.dragData.$clone
				.css({
					left: offset.left,
					top: offset.top
				});
			// the clone must be inserted at the end of the body, so that it can 'float' over everything else
			// without having to use z-indexes and overflow visibility
			$('body').append(self.dragData.$clone);
			event.preventDefault(); // prevent text selection on desktop
		},
		slidePreview: function (event) {
			event.preventDefault();
			var slide = $(event.target).attr('data-id');
			if (slide) {
				var ele = event.target.outerHTML;
				var eleInPopup;
				app.view.get('ag-overlay').open(ele);
				eleInPopup = document.querySelector('#ag-overlay .o-slide');
				eleInPopup.style.height = '100%';
			}
		}
	};
});

/**
 * Implements Custom Collections Storage.
 * -------------------------------------
 * This module manages the storage (localStorage) for custom collections.
 *
 * The module is loaded within index.html
 *
 * @module ap-custom-collections-storage.js
 * @requires jquery.js
 * @author David Buezas, antwerpes
 */
app.register('ap-custom-collections-storage', function () {
	/**
	 * Implements Custom Collections Storage.
	 * -------------------------------------
	 * This class manages the storage (localStorage) for custom collections.
	 *
	 * @class ap-custom-collections-storage
	 * @constructor
	 */
	var self;
	var translation;
	return {
		publish: {
			mandatory: false
		},
		events: {},
		onRender: function () {
			if (app.config.get('name') === 'Bayer Health Care Rainmaker Template (replace with the name of your presentation)') {
				// eslint-disable-next-line
				console.error('### Please update the name of the presentation in the config.json ###');
			}
			this.storageNamespace = this.getStorageNamespace();
			self = this;
			app.$.customCollectionsStorage = this;
		},
		/**
		* returns storageNamespace
		*
		* @method getStorageNamespace
		* @returns {String} - returns storageNamespace
		*
		*/
		getStorageNamespace: function () {
			return app.config.get('name') + ':' + app.model.getStoryboard()[0] + ':CustomCollections';
		},
		/**
		 * Deletes the custom collection named collectionName from the localStorage
		 *
		 * @method delete
		 * @param {string} collectionName - collection name
		 * @returns {void} - returns nothing
		 */
		delete: function (collectionName) {
			var customCollections = self.getAll();
			var current = app.slideshow.getId();
			// eslint-disable-next-line
			console.log('Delete collection', customCollections[collectionName]);
			if (customCollections[collectionName] !== current) {
				delete customCollections[collectionName];
				app.model.deleteStoryboard(collectionName);
				localStorage[self.storageNamespace] = JSON.stringify(customCollections);
			} else {
				// alert();
			}
		},
		/**
		 * returns all custom collections as an associative array from collection name to collection representation
		 *
		 * @method getAll
		 * @returns {void} - returns nothing
		 */
		getAll: function () {
			try {
				return JSON.parse(localStorage[self.storageNamespace] || '{}');
			} catch (e) {
				// eslint-disable-next-line
				console.log('Custom Presentations storage is corrupt or empty and will be reseted: ' + JSON.stringify(e));
				delete localStorage[self.storageNamespace];
			}
			return {};
		},
		/**
		 * Adds a custom collection to the localStorage
		 *
		 * @method add
		 * @param {string} collectionName name of the collection to store
		 * @param {string} collectionObject object representation of the collection
		 * @returns {void} - returns nothing
		 */
		add: function (collectionName, collectionObject) {
			var customCollections = self.getAll();
			customCollections[collectionName] = collectionObject;
			localStorage[self.storageNamespace] = JSON.stringify(customCollections);
		},
		/**
		 * returns the stored collection with the provided name
		 *
		 * @method get
		 * @param {string} collectionName name of the collection to store
		 * @returns {object} - collection returned
		 */
		get: function (collectionName) {
			var collection = self.getAll()[collectionName];
			if (collection === void 0 || Array.isArray(collection.presentation) === false) {
				return void 0;
			}
			var invalidSlidesCount = 0;
			collection.presentation = collection.presentation.map(function (slideshow) {
				return slideshow.filter(function (slideId) {
					if (app.model.get().slides[slideId] === void 0) {
						invalidSlidesCount++;
						return false;
					}
					return true;
				});
			}).filter(function (slideshow) {
				return slideshow.length > 0;
			});
			if (invalidSlidesCount > 0) {
				if (translation) {
					var errorMsg = translation.MISSING_PRESSENTATIONS_TITLE
						.replace('$INVALID_SLIDES_COUNT$', invalidSlidesCount);
					window.apprise(errorMsg, {textOk: translation.OK});
				}
				self.add(collectionName, collection);
			}
			return collection;
		},
		/**
		 * renames a stored collection without changing its position (e.g. collections order returned by getAll)
		 *
		 * @method rename
		 * @param {string} oldName - old name
		 * @param {string} newName - new name
		 * @returns {void} - returns nothing
		 */
		rename: function (oldName, newName) {
			// rename presentation keeping the presentation order
			var customCollections = {};
			$.each(self.getAll(), function (name, object) {
				if (name === oldName) {
					$.each(object.slideshows, function ($name, slideshow) {
						slideshow.name = slideshow.name.replace(oldName, newName);
					});
					object.name = newName;
				}
				customCollections[(name === oldName) ? newName : name] = object;
			});
			// var isFavorite = customCollectionsStorage.isFavorite(oldName);
			// customCollectionsStorage.isFavorite(oldName, false);
			// customCollectionsStorage.isFavorite(newName, isFavorite);
			localStorage[self.storageNamespace] = JSON.stringify(customCollections);
		},
		/**
		 * returns all stored favorite collections
		 *
		 * @method getFavorites
		 * @returns {void} - returns nothing
		 *
		 */
		getFavorites: function () {
			var collections = self.getAll();
			var favorites = [];
			$.each(collections, function (collectionName, collectionObject) {
				if (collectionObject.isFavorite) {
					favorites.push(collectionObject);
				}
			});
			return favorites;
		},
		/**
		 * returns all stored favorite collections
		 *
		 * @method updateOrder
		 * @param {Array} order - order
		 * @returns {void} - returns nothing
		 *
		 */
		updateOrder: function (order) {
			var customCollections = self.getAll();
			var newCustomCollections = {};
			// clear old order
			var backUp = localStorage[self.storageNamespace];
			localStorage[self.storageNamespace] = JSON.stringify({});
			// set new order
			try {
				$.each(order, function (index, name) {
					newCustomCollections[name] = customCollections[name];
				});
			} catch (e) {
				localStorage[self.storageNamespace] = backUp;
			}
			localStorage[self.storageNamespace] = JSON.stringify(newCustomCollections);
		},
		/**
		 * store all custom-collections
		 *
		 * @method saveAll
		 * @param {string} customPresentations - custom presentations
		 * @returns {void} - returns nothing
		 *
		 */
		saveAll: function (customPresentations) {
			localStorage[self.storageNamespace] = JSON.stringify(customPresentations);
		}
	};
});

app.register('ap-favorite-presentations-buttons', function () {
	var self;
	return {
		publish: {},
		events: {},
		states: [],
		onRender: function () {
			self = this;
			app.listenTo(app.slideshow, 'load', this.favoritePresentationHandler.bind(this));
			app.$.on('update:favorites', function () {
				self.favoritePresentationHandler();
			});
		},
		onRemove: function () {},
		onEnter: function () {
			this.favoritePresentationHandler();
		},
		onExit: function () {},
		favoritePresentationHandler: function () {
			var content = app.slideshow.resolve();
			var $slide = $('#' + content.slide);
			var $favoritePresentationsContainer = $slide.find('.favoritePresentationsContainer');
			// remove the favorits block if its already a custom presentation
			if (app.slideshow.resolve().slideshow.indexOf('custom-collection') > -1) {
				$favoritePresentationsContainer.empty();
				return;
			}
			if ($favoritePresentationsContainer) {
				$favoritePresentationsContainer.empty();
				$.each(app.$.customCollectionsStorage.getFavorites(), function (index, orderObject) {
					$favoritePresentationsContainer.append('<div class="button" id="button' + index + '">' + orderObject.name + '</div>');
					$favoritePresentationsContainer.find(('#button' + index)).on('tap', function () {
						var collectionObject = app.$.customCollectionsStorage.get(orderObject.name);
						if (!app.model.hasStoryboard(collectionObject.id)) {
							var slideshowIdArray = [];
							$.each(collectionObject.slideshows, function (item, slideshow) {
								slideshowIdArray.push(slideshow.id);
								var slidesArrary = [];
								$.each(slideshow.content, function (i, slide) {
									slidesArrary.push(slide);
								});
								var temp = {
									id: slideshow.id,
									name: slideshow.name,
									content: slidesArrary,
									type: 'slideshow'
								};
								app.model.addStructure(slideshow.id, temp);
							});
							var storyboardData = {
								id: collectionObject.id,
								name: collectionObject.name,
								content: slideshowIdArray
							};
							app.model.addStoryboard(collectionObject.id, storyboardData);
							// app.slideshow.init(collectionObject.id);
							app.goTo(collectionObject.id);
							app.$.toolbar.hide();
							// set home icon
							// app.menu.linksConfig[orderObject.slideshows[0].id] = {title: '<div class="homeIcon" />', classname: 'home'}
						} else {
							app.goTo(collectionObject.id);
						}
					});
				});
			}
		}
	};
});

/**
 * Provides a follow up mail section.
 * ----------------------------------
 * Provides a user interface for composing a follow up email.
 * Focuses on adding and removing media-entries from the media library as attachments.
 *
 * <b>WARNING: Please be responsible and mark only those media entries as attachments whose size
 * can be handled by common mail servers (using the allowDistribution property on entries within media.json).
 * Exceeding the mail servers size limitations may result in attachments not being able to be transmitted.</b>
 *
 * @module ap-follow-up-mail.js
 * @requires jquery.js, media-library.js, agnitio.js, touchy.js
 * @author Alexander Kals, Andreas Tietz, David Buezas, antwerpes ag
 */
app.register('ap-follow-up-mail', function () {
	/**
	 * Implements a follow up mail section.
	 * ------------------------------------
	 * Implements a user interface for composing a follow up email.
	 * Focuses on adding and removing media-entries from the media library as attachments.
	 *
	 * <b>WARNING: Please be responsible and mark only those media entries as attachments whose size
	 * can be handled by common mail servers (using the allowDistribution property on entries within media.json).
	 * Exceeding the mail servers size limitations may result in attachments not being able to be transmitted.</b>
	 *
	 * @class ap-follow-up-mail
	 * @constructor
	 *
	 */
	var self;
	var address = '';
	var subject = 'Reference to our meeting'; // 'Dateien/Referenzen aus unserem Gespräch';
	return {
		publish: {
			allowCustomText: false
		},
		events: {
			'tap .sendButton': 'sendMailHandler',
			'tap .library .emailAttachmentToggler': 'emailAttachmentHandler',
			'tap .clearButton': 'clear'
		},
		states: [
			{
				id: 'visible',
				onEnter: function () {
					app.$.trigger('media-library:refresh');
					app.$.mediaLibrary.refreshScroll();
				}
			}
		],
		onRender: function () {
			self = this;
			app.$.on('open:ap-follow-up-mail', function () {
				if (this.stateIsnt('visible')) {
					this.show();
				}
			}.bind(this));
			app.$.on('close:ap-follow-up-mail', function () {
				this.hide();
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				this.hide();
			}.bind(this));
			// param data.key - The media repository key to attach
			app.$.on('attach:ap-follow-up-mail', function (data) {
				var el = $('.library .MediaLibrary li[data-file="' + data.key + '"]')[0];
				if (el) {
					window.mediaRepository.toggleAttachment(el);
					self.attachContent(el);
				}
			});
			self.bodyText = 'Dear Customer,<br /><br />in reference to our meeting, please find attached the following documents.<br /><br />Best regards<br />Your Bayer team <br /><br /><hr />';
			var presentationName = app.config.get('name');
			localStorage.removeItem(presentationName + ':attachmentStorage');
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-follow-up-mail',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		hide: function () {
			app.unlock();
			this.reset();
		},
		show: function () {
			app.lock();
			this.goTo('visible');
		},
		sendMailHandler: function () {
			// var generalBodyText = 'Dear Customer,<br /><br />in reference to our meeting, please find attached the following documents.<br /><br />Best regards<br />Your Bayer team'; //Sehr geehrter Kunde,<br /><br />anbei finden Sie wie gewünscht die folgenden Dateien/Referenzen aus unserem Gespräch.<br /><br />Mit freundlichen Grüßen,<br />Ihr Bayer Team',
			var presentationName = app.config.get('name');
			var storageNamespace = presentationName + ':attachmentStorage';
			var collectedAttachments = Object.keys(JSON.parse(localStorage[storageNamespace] || '{}'));
			var fileAttachments = [];
			var media = window.mediaRepository.updateAndGetRenderedMetadata(window.mediaRepository.metadata());
			self.bodyText = 'Dear Customer,<br /><br />in reference to our meeting, please find attached the following documents.<br /><br />Best regards<br />Your Bayer team <br /><br /><hr />';
			// Attachments that don't represent real files are writen into the body text,
			// all others are being attached as files:
			$.each(collectedAttachments, function (index, attachment) {
				var contentRegex = /^content:\/\//;
				var urlRegex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=+$,\w]+@)?[A-Za-z0-9.-]+|(?:www\.|[-;:&=+$,\w]+@)[A-Za-z0-9.-]+)((?:\/[+~%/.\w-_]*)?\??(?:[-+=&;%@.\w_]*)#?(?:[.!/\\\w]*))?)/;
				if (contentRegex.test(attachment)) {
					self.bodyText += '<br /><br />';
					self.bodyText += attachment.replace(contentRegex, '');
					// Do we have content to write out?
					if (media[attachment].content) {
						self.bodyText += ': <br /><br />';
						self.bodyText += media[attachment].content;
						self.bodyText += '<hr />';
					}
				} else if (urlRegex.test(attachment)) {
					self.bodyText += '<br /><br />';
					self.bodyText += attachment;
				} else {
					fileAttachments.push(attachment);
				}
			});
			if (self.props.allowCustomText && app.$.customEmailText) {
				self.bodyText += app.$.customEmailText;
			}
			// Hand over to agnitio native mail dialog:
			// eslint-disable-next-line
			console.log('ag.sendMail("' + address + '", "' + subject + '", "' + self.bodyText + '",', fileAttachments, ')');
			ag.sendMail(address, subject, self.bodyText, fileAttachments);
		},
		// param el - The media library list element
		attachContent: function (el) {
			var filename = $(el).attr('data-file');
			var $movedDown = $('.attachments ul li[data-file="' + filename + '"]');
			$('.attachments ul').append($movedDown);
		},
		emailAttachmentHandler: function (event) {
			// var filename = $(event.target).parent('li').attr('data-file');
			self.attachContent($(event.target).parent('li')[0]);
		},
		clear: function () {
			$('.mail [data-file][data-is-attached="true"] .emailAttachmentToggler').each(function () {
				// touchy custom events doesn't support jquery trigger
				var tapEvent = document.createEvent('UIEvents');
				tapEvent.initEvent('tap', true, true);
				this.dispatchEvent(tapEvent);
			});
		}
	};
});

/**
 * Implements a frequently asked questions section.
 * ------------------------------------------------
 * Questions and answeres are maintained directly inside the frequently-asked-questions.html file.
 * Tapping on a question or answer shows/hides the answer.
 *
 *
 * @module ap-frequently-asked-questions.js
 * @requires jquery.js, iscroll.js, module.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register('ap-frequently-asked-questions', function () {
	/**
	 * Implements a frequently asked questions section.
	 * ------------------------------------------------
	 * Questions and answeres are maintained directly inside the frequently-asked-questions.html file.
	 * Tapping on a question or answer shows/hides the answer.
	 *
	 * @class ap-frequently-asked-questions
	 * @constructor
	 */
	var self;
	return {
		publish: {},
		events: {},
		states: [
			{
				id: 'visible'
			}
		],
		onRender: function () {
			self = this;
			app.$.on('open:ap-frequently-asked-questions', function () {
				this.show();
			}.bind(this));
			app.$.on('close:ap-frequently-asked-questions', function () {
				if (this.stateIs('visible')) {this.hide();}
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				if (this.stateIs('visible')) {this.hide();}
			}.bind(this));
			// Initialize scrolling:
			if (self.scroll) self.scroll.destroy();
			var $scroll = $(self.el).find('.scroll');
			self.scroll = new IScroll($scroll[0], {scrollbars: true});
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-frequently-asked-questions',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		hide: function () {
			this.unload();
			app.unlock();
			this.reset();
		},
		show: function () {
			$('.FrequentlyAskedQuestions li').on('tap', function () {
				$(this).toggleClass('maximized');
				self.scroll.refresh();
				if ($(this).hasClass('maximized')) {self.scroll.scrollToElement(this);}
			});
			app.lock();
			this.goTo('visible');
		},
		unload: function () {
			// Cleanup:
			$(self.el).find('li').off('tap');
		}
	};
});

/**
 * Provides a media library section.
 * ---------------------------------
 * Provides a user interface for searching and viewing arbitrary
 * media meta data and content from the media repository.
 *
 * @module ap-media-library.js
 * @requires jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register('ap-media-library', function () {
	/**
	 * Implements a media library section.
	 * -----------------------------------
	 * Implements a user interface for searching and viewing arbitrary
	 * media meta data and content from the media repository.
	 * Shows all media entries by default.
	 * The basic set of listed entries may be optionally limited by some constraint,
	 * e.g. 'all media entries that are attachable to emails'
	 * or 'all media entries that have the 'pdf' and 'reference' tags in it'.
	 *
	 * @class ap-media-library
	 * @constructor
	 * @param {object} options Valid properties are:
	 *     - preFilterSearchTerms: limits basic set of entries: what is to be searched (leave undefined to show all)
	 *     - preFilterAttributesToBeSearched: limits basic set of entries: where is it to be found
	 *     - renderOptions: options to be passed to renderers
	 */
	return {
		publish: {
			prefiltersearchterms: void 0,
			prefilterattributestobesearched: void 0,
			metadatafiltrationopts: void 0,
			renderOptions: {},
			attachment: false,
			followupmail: false,
			hide: false
		},
		events: {},
		states: [
			{
				id: 'hide'
			},
			{
				id: 'visible'
			},
			{
				id: 'followupmail'
			},
			{
				id: 'attachment'
			}
		],
		onRender: function (el) {
			app.$.mediaLibrary = this;
			this.load(el);
			if (this.props.hide) {
				this.hide();
			}
			if (this.props.attachment) {
				this.goTo('attachment');
			}
			if (this.props.followupmail) {
				this.goTo('followupmail');
			}

			app.$.on('open:ap-media-library', function () {
				if (this.stateIs('hide')) {
					this.show();
				}
			}.bind(this));

			app.$.on('close:ap-media-library', function () {
				if (this.stateIs('visible')) this.hide();
			}.bind(this));

			app.$.on('toolbar:hidden', function () {
				if (this.stateIs('visible')) this.hide();
				this.unload();
			}.bind(this));

			app.$.on('media-library:refresh', function () {
				if (this.stateIs('attachment')) this.scroll.refresh();
			}.bind(this));

			app.listenTo(app.slideshow, 'load', function () {
				this.clearSearchInput();
				this.load(this.el);
			}.bind(this));

			this.monitorUsage();
		},
		onRemove: function () {
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-media-library',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},

		hide: function () {
			app.unlock();
			this.goTo('hide');
		},

		show: function () {
			app.lock();
			this.goTo('visible');
			this.load();
		},
		unload: function () {
			if (this.stateIs('visible')) {
				if (this.scroll) this.scroll.destroy();
			}
		},
		refreshScroll: function () {
			this.scroll.refresh();
		},
		clearSearchInput: function (){
			$(this.el).find('input').val('');
		},
		load: function (el) {
			var $list = $(el).find('ul');
			$list.empty();
			if (this.scroll) this.scroll.destroy();
			this.scroll = new IScroll(this.$('.scroll')[0], {scrollbars: true});


			// Fill the list with a basic set of media entries (e.g. 'all media entries that are attachable to emails'):


			/**
			 * Implements a media library section.
			 * -----------------------------------
			 * Implements a user interface for searching and viewing arbitrary
			 * media meta data and content from the media repository.
			 * Shows all media entries by default.
			 * The basic set of listed entries may be optionally limited by some constraint,
			 * e.g. 'all media entries that are attachable to emails'
			 * or 'all media entries that have the 'pdf' and 'reference' tags in it'.
			 *
			 * @param {object} options Valid properties are:
			 *     - preFilterSearchTerms: limits basic set of entries: what is to be searched (leave undefined to show all)
			 *     - preFilterAttributesToBeSearched: limits basic set of entries: where is it to be found
			 *     - renderOptions: options to be passed to renderers
			 */

			var media = window.mediaRepository.find(this.props.prefiltersearchterms, this.props.prefilterattributestobesearched);
			if (media) {
				media = window.mediaRepository.updateAndGetRenderedMetadata(media, this.props.metadatafiltrationopts);
				$.each(media, function (file, meta) {
					var listElement = window.mediaRepository.render(file, meta, this.renderOptions);
					if (listElement) {
						$list.append(listElement);
					}
				});
			}

			// Set up live-search (on each key stroke):
			var $listElements = $(this.el).find('ul li');
			var $input = $(this.el).find('input');
			$input.keyup(function (e) {
				// Dismiss on enter:
				if (e.keyCode === 13) {
					$input.blur();
					return;
				}
				// Search and update list:
				var searchString = $input.val().toLowerCase();
				var searchTerms = searchString.split(/\s+/g);
				$listElements.each(function () {
					var $listElement = $(this);
					var found = searchTerms.reduce(function (foundEl, searchTerm) {
						return foundEl && ($listElement.text().toLowerCase().indexOf(searchTerm) !== -1);
					}, true);
					$listElement.toggleClass('hidden', !found);
				});
				this.scroll.refresh();
			}.bind(this));
			this.scroll.refresh();
		}
	};
});

/**
 * Provides a database interface for accessing meta information of media content.
 * ------------------------------------------------------------------------------
 *
 * The module is loaded within index.html
 *
 * @module ap-media-repository.js
 * @requires jquery.js, media.json
 * @author Andreas Tietz, David Buezas, antwerpes ag
 */
app.register('ap-media-repository', function () {
	/**
	 * Implements a database interface for accessing meta information of media content.
	 * --------------------------------------------------------------------------------
	 *
	 * The media repository provides search and rendering capability
	 * of arbitrary media information and content based on meta data
	 * defined in a json file. Within this json file arbitrary meta
	 * data can be associated with any type of file or content.
	 * The solution is relying heavily the 'convention over configuration' principle.
	 *
	 * Anatomy of a media entry:
	 *
	 *     // It's key can be any kind of unique string:
	 *     // An example convention might be to put in the file path
	 *     // of a real file the meta data should be associated with:
	 *     'content/pdf/reference_01.pdf': {
	 *         // Meta data is defined as attributes. Those can really
	 *         // be completely arbitrary as long as there is a renderer
	 *         // implemented that is capable of processing these values.
	 *         // Currently string, number and boolean are the types of
	 *         // object values supported by the search/find functionality.
	 *         'title': 'Doe J, Lorem Ipsum 1. 2005',
	 *         'referenceId': 3,
	 *         'allowDistribution': true,
	 *         'tags': 'document pdf publication reference 3'
	 *     },
	 *
	 * Apart from searching, each media entry can also be rendered into
	 * DOM via 'renderers', e.g. in order to be displayed inside a list.
	 * For each 'type' of media (which is completely up to the developer to be defined),
	 * a separate renderer must be implemented and registered at the media repository.
	 * When a media entry is about to be rendered, the media repository uses the
	 * renderer that matches first the media type of the file or content of the entry
	 * (first come first serve at registration time).
	 *
	 * Anatomy of a media renderer:
	 *
	 *     MediaRepository.addRenderer({
	 *         // Regular expression used to determine what 'type' of
	 *         // media entries are accepted by this renderer:
	 *         regex: <some regular expression>,
	 *         // Function returning a jQuery DOM element representing that
	 *         // media entry based on the filename or content as well as meta data
	 *         render: function (fileOrContent, meta, options) {
	 *             return <some generated jQuery DOM element>;
	 *         }
	 *     });
	 *
	 *
	 * @class ap-media-repository
	 * @constructor
	 *
	 *
	 **/


	var _metadata;
	var _renderedMetadata;
	var _renderers;
	var self;
	var config = {
		metadataFiltrationHandlers: {
			'filterbytrack': 'filterTrack'
		},
		useReferencesListNumeration: false
	};


	return {
		publish: {
			usereferenceslistnumeration: false
		},
		events: {},
		states: [],
		onRender: function () {
			window.mediaRepository = this; // export globally
			window.mediaRepository.load('media.json'); // load database file
			if (this.props.usereferenceslistnumeration) this.enableReferencesListNumeration();
			var createBasicMediaEntry; // forward declaration

			self = this;

			// Content:
			window.mediaRepository.addRenderer({
				regex: /^content:\/\//,
				render: function (file, meta) {
					return createBasicMediaEntry(file, meta, $.noop)
						.addClass('content')
						.append('<span class=\'title\'>' + file.replace('content://', '') + '</span>')
						.append('<span class=\'tags\'>' + (meta.tags ? '[' + meta.tags + ']' : '') + '</span>');
				}
			});

			// PDF:
			window.mediaRepository.addRenderer({
				regex: /\.(pdf)$/,
				render: function (file, meta, options) {
					// Clean title from any html tags
					var cleanTitle = meta.title.replace(/(<([^>]+)>)/ig, '');
					var renderOptions = options;
					renderOptions = $.extend({
						onTap: function () {
							// eslint-disable-next-line
							console.log('ag.openPDF(\'' + file + '', '' + cleanTitle + '\')');
							ag.openPDF(file, cleanTitle);
						}
					}, renderOptions);
					return createBasicMediaEntry(file, meta, renderOptions.onTap)
						.addClass('pdf')
						.append('<span class=\'title\'>' + (meta.title || '') + '</span>')
						.append('<span class=\'tags\'>' + (meta.tags ? '[' + meta.tags + ']' : '') + '</span>');
				}
			});

			// URL:
			window.mediaRepository.addRenderer({
				/* http://blog.mattheworiordan.com/post/13174566389 */
				regex: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=+$,\w]+@)?[A-Za-z0-9.-]+|(?:www\.|[-;:&=+$,\w]+@)[A-Za-z0-9.-]+)((?:[+~%.\w-_]*)?\??(?:[-+=&;%@.\w_]*)#?(?:[.\x21/\\\w]*))?)/,
				render: function (file, meta, options) {
					var cleanTitle = meta.title.replace(/(<([^>]+)>)/ig, '');
					var renderOptions = options;
					renderOptions = $.extend({
						shouldAllowTap: function () {
							return true;
						},
						onTap: function () {
							// eslint-disable-next-line
							console.log('window.open(\'' + file.replace(/^(https?|ftp)/, 'agnitiodefaultbrowser') + '', '' + cleanTitle + '\')');
							// iOS Fix until Agnitio fixes their ag.openUrl function
							// window.open(file.replace(/^(https?|ftp)/, 'agnitiodefaultbrowser'));
							ag.openURL(file, cleanTitle);
						}
					}, renderOptions);
					return createBasicMediaEntry(file, meta, renderOptions.onTap)
						.addClass('url')
						.append('<span class=\'title\'>' + (meta.title || '') + '</span>')
						.append('<span class=\'url\'>(' + file + ')</span>')
						.append('<span class=\'tags\'>' + (meta.tags ? '[' + meta.tags + ']' : '') + '</span>');
				}
			});

			// Video (with thumbnail):
			// Expecting a PNG thumbnail image by convention
			// e.g. thumbnail for 'content/video/my_movie.mp4' is expected to be 'content/video/my_movie.mp4.png'
			window.mediaRepository.addRenderer({
				regex: /\.(mov|mp4|m4v)$/,
				render: function (file, meta, options) {
					var renderOptions = options;
					renderOptions = $.extend({
						onTap: function () {
							$('<div class=\'videoPopup\'><video src=\'' + file + '\' controls/><div class=\'close\'></div></div>')
								.on('swipedown swipeup swiperight swipeleft', function (e) {
									e.stopPropagation();
								})
								.on('tap', function (event) {
									if ($(event.target).is(':not(video)')) $(this).remove();
								}).appendTo('#presentation');
							var v = $('#presentation').find('.videoPopup video').get(0);
							if (v.load) v.load();
						}
					}, renderOptions);
					var entry = createBasicMediaEntry(file, meta, renderOptions.onTap)
						.addClass('video')
						.append('<span class=\'title\'>' + meta.title + '</span>')
						.append('<span class=\'tags\'>' + (meta.tags ? '[' + meta.tags + ']' : '') + '</span>');
					entry.find('.icon').css({
						'background-image': 'url(\'' + file + '.png\')',
						'background-size': 'contain'
					});
					return entry;
				}
			});

			createBasicMediaEntry = function (file, meta, onTap) {
				var presentationName = app.config.get('name');
				var storageNamespace = presentationName + ':attachmentStorage';
				var attachmentStorage = JSON.parse(localStorage[storageNamespace] || '{}');
				var metadataItemId = self._getMetadataItemId(file, meta);

				var $emailAttachmentToggler = null;
				if (meta.allowDistribution) {
					$emailAttachmentToggler = $('<div class=\'emailAttachmentToggler\' />');
					$emailAttachmentToggler.on('tap', function (e) {
						self.toggleAttachment($(e.target).parent());
					});
				}

				return $('<li/>')
					.addClass('mediaEntry')
					.attr('data-file', file)
					.attr('data-is-attached', !!attachmentStorage[file])
					.append($emailAttachmentToggler)
					.append('<div class=\'icon\' />')
					.append(metadataItemId ? '<div class=\'referenceId\'><span>[</span>' + metadataItemId + '<span>]</span></div>' : '')
					.on('tap', ':not(.emailAttachmentToggler)', function () {
						onTap.apply(this);
					});
			};
		},

		// param el - The media entry list element
		toggleAttachment: function (el) {
			var presentationName = app.config.get('name');
			var storageNamespace = presentationName + ':attachmentStorage';
			var attachmentStorage = JSON.parse(localStorage[storageNamespace] || '{}');

			var file = $(el).attr('data-file');
			var isAttached = !attachmentStorage[file];
			if (isAttached) {
				attachmentStorage[file] = true;
			} else {
				delete (attachmentStorage[file]);
			}
			localStorage[storageNamespace] = JSON.stringify(attachmentStorage);
			$('[data-file=\'' + file + '\']').attr('data-is-attached', isAttached);
			app.$.trigger('media-library:refresh');
		},
		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {},
		_isDataInObject: function (object) {
			return Object.keys(object).length !== 0;
		},
		filterTrack: function (references) {
			var currentTrack = app.slideshow.getId();
			var filteredReferences = {};
			for (var key in references) {
				if (references.hasOwnProperty(key)) {
					var currentFlow = references[key].flow || '';
					var currentFlowValues = currentFlow.split(',');
					if (currentFlowValues.indexOf(currentTrack) !== -1) filteredReferences[key] = references[key];
				}
			}
			return this._isDataInObject(filteredReferences) ? filteredReferences : references;
		},
		updateAndGetRenderedMetadata: function (data, filtrationOptions) {
			var filtrationOptionsChecked = filtrationOptions || '';
			var filtrationOptionsSplitter = ',';
			var options = filtrationOptionsChecked.split(filtrationOptionsSplitter);
			_renderedMetadata = {};

			options.forEach(function (optionName) {
				var optionNameTrimmed = optionName.trim();
				optionNameTrimmed.length && (_renderedMetadata = this[config.metadataFiltrationHandlers[optionNameTrimmed]](data));
			}, this);

			!this._isDataInObject(_renderedMetadata) && (_renderedMetadata = $.extend({}, data));

			return _renderedMetadata;
		},
		enableReferencesListNumeration: function () {
			config.useReferencesListNumeration = true;
		},
		disableReferencesListNumeration: function () {
			config.useReferencesListNumeration = false;
		},
		_getMetadataItemId: function (file, meta) {
			var referenceIdPropAttribute = 'referenceId';
			var renderedMetadataIndexValue = '';

			if (meta.hasOwnProperty(referenceIdPropAttribute)) {
				if (config.useReferencesListNumeration) {
					renderedMetadataIndexValue = String(this._getFilteredByPropertyRenderedMetadataKeys(referenceIdPropAttribute).indexOf(file) + 1);
				} else {
					renderedMetadataIndexValue = meta[referenceIdPropAttribute];
				}
			}

			return renderedMetadataIndexValue;
		},
		_getFilteredByPropertyRenderedMetadataKeys: function (propAttribute) {
			var filteredRenderedMetadataKeys = [];

			for (var key in _renderedMetadata) {
				if (this._isMetadataItemHasPropertyAttribute(_renderedMetadata[key], propAttribute)) {
					filteredRenderedMetadataKeys.push(key);
				}
			}

			return filteredRenderedMetadataKeys;
		},
		_isMetadataItemHasPropertyAttribute: function (dataItem, propAttribute) {
			return dataItem && dataItem.hasOwnProperty(propAttribute);
		},
		/**
		 * Loads a {JSON} file containing meta data in an {object} for later runtime use.
		 *
		 * @method load
		 * @param {String} file Path of file containing meta data.
		 * @return {void} no return value
		 */
		load: function (file) {
			_metadata = {};
			_metadata = JSON.parse(app.cache.get(file));
			if (!_metadata) {
				$.ajax({
					url: file,
					dataType: 'json',
					success: function (json) {
						_metadata = json;
					},
					error: function (jqXHR, textStatus) {
						var err = {
							current: 'Error loading media repository metadata file \'' + file + '\': ' + textStatus
						};
						throw err.current;
					},
					async: false
				});
			}
		},
		/**
		 * Returns the meta data object representing the meta database at runtime.
		 *
		 * @method metadata
		 * @return {Object} Reference to the meta data object.
		 */
		metadata: function () {
			return _metadata;
		},
		/**
		 * Searches through the meta data object for given search terms in given attributes.
		 *
		 * @method find
		 * @param {String} [searchTerms] Search terms separated by whitespaces. If not specified, all entries will be returned.
		 * @param {String|Array} [attributesToBeSearched] Meta attribute keys whose values should be searched for searchTerms. If not specified, all attributes are being searched.
		 * @return {Object} Hash containing found media entries or null if nothing was found.
		 */
		find: function (searchTerms, attributesToBeSearched) {
			var defaultSearchTerms = searchTerms || '';
			var currentSearchTerms = defaultSearchTerms.toLowerCase().split(/\s+/g);
			var localAttributesToBeSearched = attributesToBeSearched;
			if (typeof localAttributesToBeSearched === 'string') {
				localAttributesToBeSearched = [localAttributesToBeSearched];
			}
			var results = null;
			$.each(_metadata, function (file, meta) {
				var haystack = '';

				if (localAttributesToBeSearched) { // search only specific attributes
					$.each(localAttributesToBeSearched, function (index, attribute) {
						var attrVal = meta[attribute];
						if (attrVal && (typeof attrVal === 'string'
								|| typeof attrVal === 'number'
								|| typeof attrVal === 'boolean')) {
							haystack += attrVal.toString() + ' ';
						}
					});
				} else { // search all
					$.each(meta, function (attrKey, attrVal) {
						if (typeof attrVal === 'string' || typeof attrVal === 'number' || typeof attrVal === 'boolean') {
							haystack += attrVal.toString() + ' ';
						}
					});
				}
				if (haystack.length > 0) {
					haystack = haystack.toLowerCase();
					var found = currentSearchTerms.reduce(function (foundEl, searchTerm) {
						return foundEl && (haystack.indexOf(searchTerm) !== -1);
					}, true);
					if (found) {
						results = results || {};
						results[file] = meta;
					}
				}
			});
			return results;
		},
		/**
		 * Set new content for an attribute of the metadata object
		 *
		 * @method setMetada
		 * @param {String} attribute Attribute of the metadata object.
		 * @param {String} key key to set a new value.
		 * @param {String} content New content for that attribute.
		 * @return {Object} The new metadata object
		 */
		setMetada: function (attribute, key, content) {
			_metadata[attribute][key] = content;
			return _metadata;
		},
		/**
		 * Get attribute content
		 *
		 * @method getMetada
		 * @param {String} attribute Attribute to get value.
		 * @return {Object} The new metadata object
		 */
		getMetada: function (attribute) {
			return _metadata[attribute];
		},
		/**
		 * Adds a {renderer} to the {renderer} chain.
		 *
		 * @method addRenderer
		 * @param {Object} renderer to be added.
		 * @return {void} no return value
		 */
		addRenderer: function (renderer) {
			_renderers = _renderers || [];
			_renderers.push(renderer);
		},
		/**
		 * Renders a media entry. Uses the first matching renderer in the renderer chain.
		 *
		 * @method render
		 * @param {String} file Media entry key.
		 * @param {Object} meta Media entry meta data.
		 * @param {Object} options Attributes passed to the designated renderer.
		 * @return {jQuery} Rendered {jQuery} Element ready to be inserted into the DOM.
		 */
		render: function (file, meta, options) {
			var renderer = _renderers.reduce(function (bestRenderer, currentRenderer) {
				return bestRenderer || (currentRenderer.regex.test(file) && currentRenderer);
			}, void 0);
			if (renderer) {
				return renderer.render(file, meta, options);
			}
			// eslint-disable-next-line
			console.log('Warning: no renderer found for media resource ', file);
			return void 0;
		}
	};
});

app.register('ap-notepad', function () {
	/**
	 * This class enables the user to draw over other content via touch/mouse events.
	 *
	 * The module is loaded within index.html
	 *
	 * @class ap-notepad
	 * @constructor
	 *
	 */

	var BRUSH_RED = "modules/ap-notepad/assets/brush_red.png";
	var BRUSH_GREEN = "modules/ap-notepad/assets/brush_green.png";
	var brush = new Image();
	var sketcher;
	var head;
	var self;

	return {
		publish: {},
		events: {
			'tap .exit': 'toggleNotepad',
			'tap .green': function () {
				brush.src = BRUSH_GREEN;
			},
			'tap .red': function () {
				brush.src = BRUSH_RED;
			},
			'tap .clear': function () {
				sketcher.clear();
			}
		},
		states: [
			{
				id: 'active',
				onEnter: function () {
					app.lock();
					var pencilPos = $('.bar .button.notepad').offset();
					$('.palette').css({
						top: pencilPos.top + 3,
						left: pencilPos.left + 9
					});
				}
			},
			{
				id: 'draw1',
				onEnter: function (prev, data) {
					this.copyDrawing(data);
				}
			},
			{
				id: 'draw2',
				onEnter: function (prev, data) {
					this.copyDrawing(data);
				}
			}
		],
		toggleNotepad: function () {
			var state = this.getState();
			if (state) {
				this.reset();
			} else {
				this.goTo('active');
			}
			sketcher.clear();
		},
		// Methods for copying drawing on remote screen
		copyDrawing: function (data) {
			sketcher.context.putImageData(data, 0, 0);
		},
		updateDrawing: function () {
			var wrapper = app.dom.get('wrapper');
			var imageData = sketcher.context.getImageData(0, 0, wrapper.offsetWidth, wrapper.offsetHeight);
			if (this.stateIs('draw1')) {
				this.goTo('draw2', imageData);
			} else {
				this.goTo('draw1', imageData);
			}
		},
		init: function () {
			brush.src = BRUSH_RED;
			brush.onload = function () {
				sketcher = new window.Sketcher($('#notepad-canvas'), brush);
			};
		},
		onRender: function (el) {
			self = this;
			app.$.notepad = this;
			this.on('reset', app.unlock);
			app.on('updateNotepad', function () {
				self.updateDrawing();
			});
			if (window.Sketcher) {
				this.init();
			} else {
				head.load('modules/ap-notepad/cdm.sketcher.min.js', function () {
					this.init();
				});
			}
			$(el).find('.button:not(.clear,.exit)').on('tap', function (event) {
				$(self.el).find('.button').removeClass('active');
				$(event.target).addClass('active');
			});
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-notepad',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		}
	};
});

var Trig={distanceBetween2Points:function(t,e){return Math.sqrt(Math.pow(e.x-t.x,2)+Math.pow(e.y-t.y,2))},angleBetween2Points:function(t,e){var o=e.x-t.x,n=e.y-t.y;return Math.atan2(o,n)}};function Sketcher(t,e,o){var n=this;n.brush=e,n.touchSupported="ontouchstart"in window,n.canvas=t,n.context=n.canvas.get(0).getContext("2d"),n.lastMousePoint={x:0,y:0},n.angle_old=0,n.touchSupported?(n.mouseDownEvent="touchstart",n.mouseMoveEvent="touchmove",n.mouseUpEvent="touchend"):(n.mouseDownEvent="mousedown",n.mouseMoveEvent="mousemove",n.mouseUpEvent="mouseup"),o&&(n.callback=o),n.canvas.on(n.mouseDownEvent,n.onCanvasMouseDown())}Sketcher.prototype.onCanvasMouseDown=function(){var t=this;return function(e){t.mouseMoveHandler=t.onCanvasMouseMove(),t.mouseUpHandler=t.onCanvasMouseUp(),t.canvas.on(t.mouseMoveEvent,t.mouseMoveHandler),t.canvas.on(t.mouseUpEvent,t.mouseUpHandler),t.updateMousePosition(e),t.updateCanvas(e)}},Sketcher.prototype.onCanvasMouseMove=function(){var t=this;return function(e){return t.updateCanvas(e),this.touchSupported?(e.originalEvent.targetTouches[0].preventDefault(),e.originalEvent.targetTouches[0].stopPropagation()):(e.preventDefault(),e.stopPropagation()),!1}},Sketcher.prototype.onCanvasMouseUp=function(){var t=this;return function(e){app.trigger("updateNotepad"),t.canvas.off(t.mouseMoveEvent,t.mouseMoveHandler),t.canvas.off(t.mouseUpEvent,t.mouseUpHandler),t.mouseMoveHandler=null,t.mouseUpHandler=null}},Sketcher.prototype.updateMousePosition=function(t){var e;e=this.touchSupported?t.originalEvent.targetTouches[0]:t;var o=this.canvas.offset();this.lastMousePoint.x=e.pageX-o.left,this.lastMousePoint.y=e.pageY-o.top},Sketcher.prototype.updateCanvas=function(t){var e=this.brush.width/2,o=this.brush.height/2,n={x:this.lastMousePoint.x,y:this.lastMousePoint.y};this.updateMousePosition(t);var a={x:this.lastMousePoint.x,y:this.lastMousePoint.y},s=parseInt(Trig.distanceBetween2Points(n,a)),u=Trig.angleBetween2Points(n,a),i=n.x,r=n.y;0==this.angle_old&&(this.angle_old=u);var h=u-this.angle_old;h>Math.PI&&(h-=2*Math.PI),h<-Math.PI&&(h+=2*Math.PI);for(var v=0;v<=s||0==v;v++)s>0&&(this.angle_old+=h/s,i+=Math.sin(this.angle_old),r+=Math.cos(this.angle_old)),this.context.drawImage(this.brush,i-e,r-o);this.lastMousePoint.x=i,this.lastMousePoint.y=r,this.angle_old=u},Sketcher.prototype.clear=function(){var t=this.canvas[0];this.context.clearRect(0,0,t.width,t.height)};
/**
 * Implements the Collections Overview.
 * ------------------------------------
 * This module Presents an overview of the default collections set in presentation.json and allows loading slides by tapping on their thumbnails.
 *
 *
 * @module ap-overview.js
 * @constructor
 * @param {object} options Valid properties are:
 * 	- slideTapAction: one of the following strings representing what to do when a thumbnail receives the tap event
 * 		- 'goto' => goto slide on tap
 * 		- 'noop' => do nothing
 * 		- 'showPreview' => (NOT IMPLEMENTED) shows a bigger thumbnail of the slide
 * 	- disableScrollOnSlideDrag: {boolean}
 */
app.register('ap-overview', function () {
	/**
	 * Implements the Collections Overview.
	 * ------------------------------------
	 * This module Presents an overview of the default collections set in presentation.json and allows loading slides by tapping on their thumbnails.
	 *
	 * @class ap-overview
	 * @constructor
	 * @param {object} options Valid properties are:
	 * 	- $container: jQuery DOM object inside which to load the module
	 * 	- slideTapAction: one of the following strings representing what to do when a thumbnail receives the tap event
	 * 		- 'goto' => goto slide on tap
	 * 		- 'noop' => do nothing
	 * 		- 'showPreview' => (NOT IMPLEMENTED) shows a bigger thumbnail of the slide
	 * 	- disableScrollOnSlideDrag: {boolean}
	 */
	var self;
	var eventNamespace = '.overview';
	var $collectionsList;
	var $collectionOverview;
	var $collectionContainer;
	var _mouseDownEvent;
	var _mouseUpEvent;
	var daDefaultDataPath = './da-default-data.json';
	var daDefaultData = {};
	return {
		slideTapAction: ['goto', 'noop', 'showPreview'][0],
		disableScrollOnSlideDrag: false,
		publish: {
			iscustomoverview: false,
			highlightCurrentSlide: true
		},
		events: {
			'tap .overview .collection-name': 'collectionsListHandler',
			'tap .overview .o-slide': 'slideHandler',
			'longTouch .overview .o-slide': 'slidePreview'
		},
		states: [
			{
				id: 'visible'
			},
			{
				id: 'customOverview'
			}
		],
		onRender: function (el) {
			self = this;
			$.ajax(daDefaultDataPath)
				.done(function (data) {
					var parsedData = typeof data === 'string' ? JSON.parse(data) : data;
					generateDefaultStructure(parsedData);
				});
			function generateDefaultStructure (data) {
				daDefaultData = data;
				var structures = app.model.get().structures;
				var slideshows = daDefaultData.slideshows.map(function (slideshowId) {
					return structures[slideshowId] || false;
				});
				daDefaultData.slideshows = slideshows;
			}
			app.$.overview = this;
			this.normalizeCollections();
			if (this.props.iscustomoverview) {
				this.goTo('customOverview');
			}
			_mouseDownEvent = touchy.events.start + eventNamespace;
			_mouseUpEvent = touchy.events.end + eventNamespace;
			$collectionsList = $(el).find('.collections'); // available collections container
			$collectionOverview = $(el).find('.collection-overview'); // selected collection overview container
			$collectionContainer = $collectionOverview.find('.o-collection'); // actual collection thumbnail
			$collectionsList.empty();
			app.$.on('open:ap-overview', function () {
				if (this.stateIsnt('visible')) {
					this.show();
				}
			}.bind(this));
			app.$.on('close:ap-overview', function () {
				this.hide();
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				this.hide();
			}.bind(this));
			app.$.on('insert:ap-overview', function () {
				this.goTo('custom-overview');
				this.updateOverview();
			}.bind(this));
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-overview',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		hide: function () {
			app.unlock();
			this.reset();
		},
		show: function () {
			this.goTo('visible');
			app.lock();
			self.slideTapAction = 'goto';
			app.$.customCollections.hide();
			this.updateOverview();
		},
		showCustomOverview: function () {
			this.goTo('customOverview');
			self.slideTapAction = 'noop';
			self.disableScrollOnSlideDrag = true;
			this.updateOverview();
		},
		unload: function () {
			// $collectionsList.empty();
		},
		updateOverview: function () {
			var storyboards = app.model.get().storyboards;
			$collectionsList = $('.overview .collections'); // available collections container
			$collectionContainer = $collectionOverview.find('.o-collection'); // actual collection thumbnail
			$collectionsList.empty();
			// traverse app.json and get the collections list (ignoring custom collections)
			$.each(storyboards, function (index, collection) {
				if (collection.type !== 'collection') return;
				if (collection.id.indexOf('custom') === -1) {
					var $collectionName = $('<li class="collection-name" ></li>');
					$.extend({id: collection.id}, collection);
					$collectionName.data('collection', collection);
					$collectionName.attr('data-collectionId', collection.id);
					$collectionName.text(collection.name);
					$collectionsList.append($collectionName);
				}
			});
			// get custom collections
			$.each(app.$.customCollectionsStorage.getAll(), function (name, content) {
				if (content.slideshows.length !== 0) {
					var $collectionName = $('<li class="collection-name"></li>');
					$collectionName.data('collection', content);
					$collectionName.attr('data-collectionId', content.id);
					$collectionName.text(content.name);
					$collectionsList.append($collectionName);
				}
			});
			/* very strange fix, which currently works */
			var $currentCollection = $collectionsList.find('[data-collectionid=\'' + app.getPath().split('/')[0] + '\']');
			var tempEvent = {
				target: $currentCollection
			};
			self.collectionsListHandler(tempEvent);
			if (this.props.iscustomoverview) {
				$collectionOverview = $('.custom-collections .overview .collection-overview'); // selected collection overview container
			} else {
				$collectionOverview = $('.overview .collection-overview'); // selected collection overview container
			}
			if (self.scroll) self.scroll.destroy();
			self.scroll = new IScroll( $('.overview .collection-overview').get(0), {
				scrollbars: true,
				mouseWheel: true,
				scrollX: true,
				directionLockThreshold: Number.MAX_VALUE
			});
			if (self.scrollCustom) self.scrollCustom.destroy();
			self.scrollCustom = new IScroll( $('.custom-collections .overview .collection-overview').get(0), {
				scrollbars: true,
				mouseWheel: true,
				scrollX: true,
				directionLockThreshold: Number.MAX_VALUE
			});
			if (self.disableScrollOnSlideDrag) {
				$collectionContainer.on(_mouseDownEvent, '.o-slide', function () {
					self.scrollCustom.disable();
					$('body').one(_mouseUpEvent, function () {
						self.scrollCustom.enable();
					});
				});
			}
		},
		normalizeCollections: function () {
			var allCollections = app.model.get().storyboards;
			$.each(allCollections, function (i, collection) {
				if (!collection.id && !collection.type) {
					var newContent = [];
					var slideShowName;
					var slideShowId;
					$.each(collection.content, function (index, content) {
						var defaultSlideshow = daDefaultData.slideshows[index];
						slideShowName = defaultSlideshow ? defaultSlideshow.name : 'Chapter ' + (index);

						if (typeof content === 'string') {
							slideShowId = content + '-chapter-' + Math.floor((Math.random() * 10000) + 1);
						} else {
							slideShowId = slideShowName + '-' + Math.floor((Math.random() * 10000) + 1);
						}
						var temp = {
							'id': slideShowId,
							'name': slideShowName,
							'type': 'slideshow',
							'content': content
						};
						app.model.addStructure(slideShowId, temp);
						newContent.push(slideShowId);
					});
					app.model.deleteStoryboard(i);
					var collectionId = collection.name + '_' + Math.floor((Math.random() * 10000) + 1);
					var collectionObj = {
						'id': collectionId,
						'name': collection.name,
						'linear': false,
						'type': 'collection',
						'content': newContent
					};
					app.model.addStoryboard(collectionId, collectionObj);
				}
			});
		},
		collectionsListHandler: function (event) {
			if (self.scroll) {
				setTimeout(function () {
					self.scroll.refresh();
					self.scroll.scrollTo(0, 0);
				}, 0);
			}
			if (self.scrollCustom) {
				setTimeout(function () {
					self.scrollCustom.refresh();
					self.scrollCustom.scrollTo(0, 0);
				}, 0);
			}
			$collectionsList.find('.selected').removeClass('selected');
			$(event.target).addClass('selected');
			// load the selected collection
			var collection = $(event.target).data('collection');
			var collectionId = $(event.target).attr('data-collectionId');
			function checkImageAddText(src, ele, slide) {
				var image = new Image();
				image.onload = function () {
					if ('naturalHeight' in this) {
						if (this.naturalHeight + this.naturalWidth === 0) {
							this.onerror();
							return false;
						}
					} else if (this.width + this.height === 0) {
						this.onerror();
						return false;
					}
					ele.text('');
					return true;
				};
				image.onerror = function () {
					ele.text(app.model.get().slides[slide].name);
					// eslint-disable-next-line
					console.log('thumbnail not loaded');
					return false;
				};
				image.src = src;
			}
			$collectionContainer
				.empty()
				.attr('data-id', collectionId)
				.data('contentGroups', app.model.getStoryboard(collectionId).contentGroups);
			// TODO add Class and Data
			if (collection.isCustomPresentation) {
				$collectionContainer.data('contentGroups', collection.savedContentGroups);
				$.each(collection.slideshows, function (index, slideshow) {
					var $slideshow = $('<figure class="o-slideshow"></figure>')
						.appendTo($collectionContainer)
						.attr('data-id', slideshow.id)
						.append('<figcaption class="o-slideshow-name">' + slideshow.name + '</figcaption>');
					$.each(slideshow.content, function (ind, slideId) {
						var $slide = $('<div class="o-slide"></div>')
							.appendTo($slideshow)
							.css({'background-image': 'url(slides/' + slideId + '/' + slideId + '.png)'})
							.attr('data-id', slideId);
						var $thumbText = $('<p class="o-text"></p>')
							.appendTo($slide);
						if (collection.savedContentGroups) {
							$.each(collection.savedContentGroups, function (i, group) {
								if (~group.slides.indexOf(slideId)) {
									$slide.data('contentGroup', group);
									$slide.addClass('grouped');
									if(group.slides[0] === slideId){
										$slide.addClass('first-group-slide');
									}
									if (group.orientation === 'horizontal') {
										$slide.parents('.o-slideshow').addClass('grouped');
									}
									if (group.orientation === 'vertical') {
										$slide.addClass('vertical-group');
									}
								}
								if (group.slides.indexOf(slideId) === 0) {
									$slide.addClass('first-in-group');
								}
							});
						}
						checkImageAddText('slides/' + slideId + '/' + slideId + '.png', $thumbText, slideId);
					});
				});
				if (collection.slideshows.length > 0 && app.model.hasStoryboard(collection.id) === false) {
					var slideshowIdArray = [];
					$.each(collection.slideshows, function (ind, slideshow) {
						slideshowIdArray.push(slideshow.id);
						var slidesArrary = [];
						$.each(slideshow.content, function (i, slide) {
							slidesArrary.push(slide);
						});
						app.model.addStructure(slideshow.id, slideshow);
					});
					var collectionObj = {
						'content': slideshowIdArray,
						'name': collection.name,
						'linear': false,
						'id': collection.id,
						'type': 'collection'
					};
					app.model.addStoryboard(collection.id, collectionObj);
				}
			} else {
				collection = app.model.getStoryboard(collectionId);
				$.each(collection.content, function (index, slideshowId) {
					var slideshow = app.model.getStructure(slideshowId);
					slideshow = $.extend({id: slideshowId}, slideshow);
					var $slideshow = $('<figure class=\'o-slideshow\'></figure>')
						.appendTo($collectionContainer)
						.attr('data-id', slideshow.id)
						.append('<figcaption class=\'o-slideshow-name\'>' + slideshow.name + '</figcaption>');
					if (typeof slideshow.content === 'string') {
						var slideId = slideshow.content;
						var $slide = $('<div class=\'o-slide\' ></div>')
							.appendTo($slideshow)
							.css({'background-image': 'url(slides/' + slideId + '/' + slideId + '.png)'})
							.attr('data-id', slideId);
						var $thumbText = $('<p class=\'o-text\'></p>')
							.appendTo($slide);
						if (collection.contentGroups) {
							$.each(collection.contentGroups, function (i, group) {
								if (~group.slides.indexOf(slideId)) {
									$slide.data('contentGroup', group);
									$slide.addClass('grouped');
									if(group.slides[0] === slideId){
										$slide.addClass('first-group-slide');
									}
									if (group.orientation === 'horizontal') {
										$slide.parents('.o-slideshow').addClass('grouped');
									}
								}
								if (group.slides.indexOf(slideId) === 0) {
									$slide.addClass('first-in-group');
								}
							});
						}
						checkImageAddText('slides/' + slideId + '/' + slideId + '.png', $thumbText, slideId);
					} else {
						$.each(slideshow.content, function (ind, $slideId) {
							var $slide_ = $('<div class=\'o-slide\' ></div>')
								.appendTo($slideshow)
								.css({'background-image': 'url(slides/' + $slideId + '/' + $slideId + '.png)'})
								.attr('data-id', $slideId);
							var thumbText = $('<p class=\'o-text\'></p>')
								.appendTo($slide_);
							if (collection.contentGroups) {
								$.each(collection.contentGroups, function (i, group) {
									if (~group.slides.indexOf($slideId)) {
										$slide_.data('contentGroup', group);
										$slide_.addClass('grouped');
										if(group.slides[0] === $slideId){
											$slide_.addClass('first-group-slide');
										}
										if (group.orientation === 'horizontal') {
											$slide_.parents('.o-slideshow').addClass('grouped');
										}
									}
								});
							}
							checkImageAddText('slides/' + $slideId + '/' + $slideId + '.png', thumbText, $slideId);
						});
					}
				});
			}
			var slideThumbSelector = '';
			$.each(app.getPath().substr().split('/'), function (i, id) {
				slideThumbSelector += '[data-id=\'' + id + '\'] ';
			});
			$collectionOverview.find('.highlighted').removeClass('highlighted');
			$collectionOverview.find(slideThumbSelector).addClass('highlighted');
		},
		slideHandler: function (event) {
			if (self.slideTapAction === 'goto') {
				var slideId = $(event.target).attr('data-id');
				var slideshowId = $(event.target).parent().attr('data-id');
				var collectionId = $(event.target).parent().parent().attr('data-id');
				var path = collectionId + '/' + slideshowId + '/' + slideId;
				app.goTo(path);
				app.$.menu.updateCurrent();
				app.$.toolbar.hide();
			} else if (self.slideTapAction === 'noop') {
				// do nothing
			}
		},
		slidePreview: function (event) {
			function addClick(elem) {
				var slideId = $(elem).attr('data-id');
				var slideshowId = $(elem).parent().attr('data-id');
				var collectionId = $(elem).parent().parent().attr('data-id');
				var path = collectionId + '/' + slideshowId + '/' + slideId;
				app.goTo(path);
				app.$.menu.updateCurrent();
				app.$.toolbar.hide();
				eleInPopup.removeEventListener('tap', function () {
					addClick(event.target);
				}, false);
				app.view.get('ag-overlay').reset();
			}
			if (self.stateIsnt('customOverview')) {
				event.preventDefault();
				var slide = $(event.target).attr('data-id');
				if (slide) {
					var ele = event.target.outerHTML;
					var eleInPopup;
					app.view.get('ag-overlay').open(ele);
					eleInPopup = document.querySelector('#ag-overlay .o-slide');
					eleInPopup.addEventListener('tap', function () {
						addClick(event.target);
					}, false);
				}
			}
		}
	};
});

/**
 * Provides a reference library section.
 * -------------------------------------
 * Implements a user interface for viewing media entries that have a "referenceId" attribute.
 *
 * @module ap-reference-library.js
 * @requires module.js, jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz antwerpes ag
 */
app.register('ap-reference-library', function () {
	var self;
	return {
		publish: {},
		events: {},
		states: [
			{
				id: 'visible'
			}
		],
		onRender: function (el) {
			self = this;
			app.$.on('open:ap-reference-library', function () {
				if (this.stateIsnt('visible')) {
					this.show();
				}
			}.bind(this));
			app.$.on('close:ap-reference-library', function () {
				this.hide();
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				this.hide();
				this.unload();
			}.bind(this));
			this.load(el);
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-reference-library',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		hide: function () {
			app.unlock();
			this.reset();
		},
		show: function () {
			app.lock();
			this.goTo('visible');
			this.load(this.el);
		},
		unload: function () {
			if (this.stateIs('visible')) {if (self.scroll) self.scroll.destroy();}
		},
		load: function (el) {
			var $list = $(el).find('ul');
			$list.empty();
			// Initialize scrolling:
			if (self.scroll) self.scroll.destroy();
			self.scroll = new IScroll(self.$('.scroll')[0], {scrollbars: true});
			// Fill the list with all media entries that have a reference id:
			var references = window.mediaRepository.find('', 'referenceId');
			if (references) {
				references = window.mediaRepository.updateAndGetRenderedMetadata(references, 'filterbytrack');
				$.each(references, function (file, meta) {
					$list.append(window.mediaRepository.render(file, meta));
				});
				self.scroll.refresh();
			}
		}
	};
});

app.register("ap-slide-indicator", function () {

    return {
        publish: {},
        events: {},
        states: [],
        onRender: function () {
            app.listenToOnce(app.slide, 'slide:enter', this.updateIndicators.bind(this));
            app.listenToOnce(app.slideshow, 'load', this.updateIndicators.bind(this));
            app.listenTo(app.slideshow, 'update:current', this.updateIndicators.bind(this));
            this.monitorUsage();
        },
        onRemove: function () {

        },
        monitorUsage: function () {
          if (window.ag) {
            window.ag.submit.data({
              label: "Registered Module",
              value: "ap-slide-indicator",
              category: "BHC Template Modules",
              isUnique: true,
              labelId: "bhc_registered_module",
              categoryId: "bhc_template_modules"
            });
          }
        },

        /**
         * Updates the navigation joystick
         * @method updateIndicators
         */

        updateIndicators: function () {


            var slideList = app.slideshow.inspect();
            var $joystick = $('.joystick');

            if(slideList.getLeft()){
                $joystick.find('.left').css('display', 'block');
            }
            else{
                $joystick.find('.left').css('display', 'none');
            }
            if(slideList.getRight()){
                $joystick.find('.right').css('display', 'block');
            }
            else{
                $joystick.find('.right').css('display', 'none');
            }
            if(slideList.getUp()){
                $joystick.find('.up').css('display', 'block');
            }
            else {
                $joystick.find('.up').css('display', 'none');
            }
            if(slideList.getDown()){
                $joystick.find('.down').css('display', 'block');
            }
            else{
                $joystick.find('.down').css('display', 'none');
            }
        }
    }

});

/**
 * Implements a function that will open the specific-product-characteristics.pdf file with the in-app PDF viewer.
 * -----------------------------------------------------------------
 * @module ap-specific-product-characteristics.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register('ap-specific-product-characteristics', function () {
	/**
	 * Implements a function that will open the specific-product-characteristics.pdf file with the in-app PDF viewer.
	 * ----------------------------------------------------------------------------------------------------------------------
	 *
	 * @class ap-specific-product-characteristics.js
	 * @constructor
	 */
	return {
		publish: {},
		events: {},
		states: [],
		onRender: function () {
			app.$.specificProductCharacteristics = this;
			this.monitorUsage();
		},
		onRemove: function () {},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-specific-product-characteristics',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},

		openPdf: function () {
			// eslint-disable-next-line
			console.log('openPDF(\'shared/references/specific-product-characteristics.pdf\', \'Specific Product Characteristics\'');
			ag.openPDF('shared/references/specific-product-characteristics.pdf', 'Specific Product Characteristics');
			var $joystick = $('.joystick');
			var $addSlideButton = $('[data-module=\'ap-add-slide-button\']');
			$joystick.fadeIn();
			$addSlideButton.fadeIn();
		},

		openPopup: function (overlay, popup) {
			if (app.module.get(overlay) && app.module.get(overlay).load) {
				app.module.get(overlay).load(popup);
			}
		}
	};
});

app.register('ap-toolbar', function () {
	var self;
	return {
		publish: {
			hide: false,
			microsite: false
		},
		events: {
			'tap': 'handleEvent',
			'tap .button[data-module-to-load=\'ap-specific-product-characteristics\']': function (event) {
				app.$.toolbar.hide();
				if (event.target.dataset.hasOwnProperty('ispopup')) {
					app.$.specificProductCharacteristics.openPopup('ag-overlay', 'pi_popup_slide');
				} else {
					app.$.specificProductCharacteristics.openPdf();
				}
				self.handleEvent(event);
			},
			'tap .bar .button.notepad': function () {
				app.$.notepad.toggleNotepad();
			},
			'tap .bar .button.jumpToLastSlide': function () {
				self.jumpToLastSlide();
				app.$.menu.updateCurrent();
				app.$.toolbar.hide();
			},
			'swipeleft': function (event) {
				event.stopPropagation();
			},
			'swiperight': function (event) {
				event.stopPropagation();
			},
			'swipeup': function (event) {
				event.stopPropagation();
			},
			'swipedown': function (event) {
				event.stopPropagation();
			}
		},
		states: [
			{
				id: 'minimized',
				onEnter: function () {
					// app.util.transformElement(this.$el, '-webkit-translate3d(0,100%,0)');
				}
			},
			{
				id: 'hidden',
				onEnter: function () {
					// app.util.transformElement(this.$el, '-webkit-translate3d(0,667px,0)');
				}
			},
			{
				id: 'maximized',
				onEnter: function () {
					// app.util.transformElement(this.$el, '-webkit-translate3d(0,0,0)');
				}
			}
		],
		setVisibility: function (button, value) {
			if (button) {
				button.addClass(value)
			}
		},
		onRender: function () {
			self = this;
			app.$.toolbar = this;
			var buttons = $(this.el).find('.bar');
			const visibilityConfig = {};
			for (var key in visibilityConfig) {
				this.setVisibility(buttons.find(key), visibilityConfig[key]);
			}
			if (this.props.hide) {
				this.hide();
			}
			$('.ap-toolbar').attr('data-state', 'hidden');
			this.goTo('hidden');
		},
		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {},
		setMicrosite: function () {
			self.microsite = true;
			$(this.$el).addClass('microsite');
		},
		hide: function () {
			app.$.trigger('toolbar:hidden');
			var $joystick = $('.joystick');
			$('.ap-toolbar').attr('data-state', 'hidden');
			$joystick.fadeIn();
			this.goTo('hidden');
		},
		open: function () {},
		jumpToLastSlide: function () {
			var collectionLength = app.slideshow.getLength();
			var lastSlide = app.model.getStoryboard(app.slideshow.getId()).content[collectionLength - 2];
			app.$.BackNavigation.setPrevCollection(app.model.getStoryboard(app.slideshow.getId()).id);
			app.slideshow.goTo(lastSlide);
		},
		handleEvent: function (e) {
			if ($(e.target).hasClass('active')) return;
			var $allButtons = $('.button[data-module-to-load]');
			var $joystick = $('.joystick');
			var $addSlideButton = $('[data-module="ap-add-slide-button"]');
			var target = e.target;
			if ($(target).hasClass(this.props.dataModule)) {
				var state = $(target).attr('data-state');
				var map = {
					hidden: 'minimized',
					maximized: 'hidden',
					minimized: 'hidden'
				};
				self.goTo(map[state]);
				$allButtons.removeClass('active');
				$('input').blur();
				$joystick.fadeIn();
				if (app.env !== 'ag-microsites' && app.env !== 'ag-remote') {
					$addSlideButton.fadeIn();
				}
				$(target).attr('data-state', map[state]);
				app.$.menu.hide();
				app.$.trigger('toolbar:hidden');
			}
			var moduleToLoad = $(target).attr('data-module-to-load');
			var currentModule = '';
			if (moduleToLoad) {
				var $state = $(target).attr('data-toolbar-state');
				setTimeout(function () {
					// avoid same touch event triggering input elements focus
				}, 0);
				if ($state) {
					app.$.toolbar.goTo($state);
					app.$.menu.hide();
				}
				if (currentModule === moduleToLoad) return;
				var trigger = 'open:' + moduleToLoad;
				app.$.trigger(trigger);
				$joystick.fadeOut();
				$addSlideButton.fadeOut();
				$allButtons.not($(target)).removeClass('active');
				$allButtons.each(function (index, item) {
					var otherModule = $(item).attr('data-module-to-load');
					if (moduleToLoad !== otherModule) {
						var $trigger = 'close:' + otherModule;
						app.$.trigger($trigger);
					}
				});
				$(target).addClass('active');
			}
		}
	};
});

/**
 * Provides a video library section.
 * ---------------------------------
 * Implements a user interface for viewing media entries that have the "video" tag.
 *
 * @module ap-video-library.js
 * @requires module.js, jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz antwerpes ag
 */
app.register('ap-video-library', function () {
	/**
	* Implements a video library section.
	* -----------------------------------
	* Implements a user interface for viewing media entries that have the "video" tag.
	*
	* @class ap-video-library
	* @constructor
	*
	*/
	var self;
	return {
		publish: {},
		events: {},
		states: [
			{
				id: 'visible'
			}
		],
		onRender: function (el) {
			self = this;
			app.$.on('open:ap-video-library', function () {
				if (this.stateIsnt('visible')) {
					this.show();
				}
			}.bind(this));
			app.$.on('close:ap-video-library', function () {
				this.hide();
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				this.hide();
				this.unload();
			}.bind(this));
			var $list = $(el).find('ul');
			// Initialize scrolling:
			self.scroll = new IScroll(self.$('.scroll')[0], {scrollbars: true});
			// Fill the list with all media entries that have the "video" tag:
			var videos = window.mediaRepository.find('video');
			if (videos) {
				videos = window.mediaRepository.updateAndGetRenderedMetadata(videos);
				$.each(videos, function (file, meta) {
					$list.append(window.mediaRepository.render(file, meta));
				});
				self.scroll.refresh();
			}
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-video-library',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		hide: function () {
			app.unlock();
			this.reset();
		},
		show: function () {
			app.lock();
			this.goTo('visible');
		},
		unload: function () {
			if (this.stateIs('visible')) {if (self.scroll) self.scroll.destroy();}
		}
	};
});

app.register('bhc-gpc-number', function () {
	return {
		template: false,
		publish: {},
		events: {},
		states: [],
		onRender: function () {
			var gpc = app.config.get('gpc');
			var existingText = this.el.innerHTML;
			if (!gpc || gpc === '999-999-999') {
				gpc = 'Not Applied';
			}
			if (gpc) {
				this.el.innerHTML = existingText + ' ' + gpc;
			}
			this.monitorUsage();
		},
		onRemove: function () {},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'bhc-gpc-number',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		}
	};
});

app.register("folic_acid_page_01", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("folic_acid_page_00", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("folic_acid_page_02", function() {

    var mainClass = "#folic_acid_page_02 ";
    
  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        var elms = document.querySelectorAll('img.zoom');
          if(elms.length > 0) {
           for(var elmKey = 0; elmKey < elms.length; elmKey++) {
             new PinchZoom.default(elms[elmKey]);
           }
        } 
              
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(600).animate({
        //     width:'454px', 
        //     opacity:"show"
        // }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     width:'0px'
        // }, 650);

    }
  }

});
app.register("folic_acid_page_03", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("folic_acid_page_04", function() {

    var mainClass = "#folic_acid_page_04 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        var elms = document.querySelectorAll('img.zoom');
          if(elms.length > 0) {
           for(var elmKey = 0; elmKey < elms.length; elmKey++) {
             new PinchZoom.default(elms[elmKey]);
           }
        } 
              
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(600).animate({
        //     width:'454px', 
        //     opacity:"show"
        // }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     width:'0px'
        // }, 650);

    }
  }

});
app.register("formulation_page_00", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("formulation_page_01", function() {

    var mainClass = "#formulation_page_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {
    // Set times for each animation
    var time =0;
    var setTime =450;

        // $(mainClass + ".ani-1").delay(0).fadeIn(1000); 
        $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-1").delay(0).show( "slide",{direction: "down"}, 650);
        $(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "left"}, 650);
        $(mainClass + ".ani-3").delay(time+=setTime).fadeIn(450);
        // $(mainClass + ".ani-3").delay(time+=setTime).show( "slide",{direction: "right"}, 1000);
        // $(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);
        // $(mainClass + ".ani-1").delay(0).animate({
        //     bottom:'40px', 
        //     opacity:"show"
        // }, 1000);                 
        // $(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "left"}, 1000);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 
        // $(mainClass + ".ani-2").delay(0).animate({
        //     bottom:'0px'
        // }, 1000);  
    }
  }

});
app.register("formulation_page_03", function() {

    var mainClass = "#formulation_page_03 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
        var elms = document.querySelectorAll('img.zoom');
          if(elms.length > 0) {
           for(var elmKey = 0; elmKey < elms.length; elmKey++) {
             new PinchZoom.default(elms[elmKey]);
           }
        } 

		// $('p.zoom').zoom({ magnify: 1 });
        // $('p.zoom').zoom({
        //     magnify: 1,
        //     on: 'grab',
        //     onZoomIn: function() {
        //         $('.icon-zoom').hide();
        //     },
        //     onZoomOut: function() {
        //         $('.icon-zoom').show();
        //     }
        // });        
	},
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =0;
    var setTime =0;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);        
        // $(mainClass + ".ani-1").delay(time+=setTime).show( "slide",{direction: "up"}, 850);

    },
    onExit: function(el) {

        // $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.register("home_slide", function () {
	return {
		events: {
			'tap [data-popup]': 'openPopup'
		},
		states: [],
		onRender: function (el) {
			this.ahPopup = app.module.get('ah-popup-management');
		},
		openPopup: function (el) {
			this.ahPopup.openPopup(el.target.attributes["data-popup"].value);
		},
		onRemove: function (el) {

		},
		onEnter: function (el) {


		},
		onExit: function (el) {
		}
	}

});

app.register("formulation_page_02", function() {

    var mainClass = "#formulation_page_02 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
        var elms = document.querySelectorAll('img.zoom');
          if(elms.length > 0) {
           for(var elmKey = 0; elmKey < elms.length; elmKey++) {
             new PinchZoom.default(elms[elmKey]);
           }
        }        
		// $('div.zoom').zoom({
  //           magnify: 1,
  //           on: 'grab',
  //           onZoomIn: function() {
  //               $('.icon-zoom').hide();
  //           },
  //           onZoomOut: function() {
  //               $('.icon-zoom').show();
  //           }
  //       });
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =0;
    var setTime =0;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);        
        // $(mainClass + ".ani-1").delay(time+=setTime).show( "slide",{direction: "up"}, 850);

    },
    onExit: function(el) {

        //$(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.register("gpc_slide", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

    },
    onRemove: function(el) {

    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});

app.register("iodine_page_00", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("iodine_page_01", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("iodine_page_02", function() {

    var mainClass = "#iodine_page_02 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {
    // Set times for each animation
    var time =0;
    var setTime =0;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450); 
    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.register("iron_page_00", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("iodine_page_03", function() {

    var mainClass = "#iodine_page_03 ";
    
  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        var elms = document.querySelectorAll('img.zoom');
          if(elms.length > 0) {
           for(var elmKey = 0; elmKey < elms.length; elmKey++) {
             new PinchZoom.default(elms[elmKey]);
           }
        } 
              
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {
    // Set times for each animation
    var time =0;
    var setTime =0;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450); 
    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.register("iron_page_02", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("iron_page_01", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("iron_page_04", function() {

    var mainClass = "#iron_page_04 ";
    
  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(600).animate({
        //     width:'454px', 
        //     opacity:"show"
        // }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     width:'0px'
        // }, 650);

    }
  }

});
app.register("iron_page_03", function() {

    var mainClass = "#iron_page_03 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        var elms = document.querySelectorAll('img.zoom');
          if(elms.length > 0) {
           for(var elmKey = 0; elmKey < elms.length; elmKey++) {
             new PinchZoom.default(elms[elmKey]);
           }
        } 
              
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(600).animate({
        //     width:'454px', 
        //     opacity:"show"
        // }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     width:'0px'
        // }, 650);

    }
  }

});
app.register("iron_page_05", function() {

    var mainClass = "#iron_page_05 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        var elms = document.querySelectorAll('img.zoom');
          if(elms.length > 0) {
           for(var elmKey = 0; elmKey < elms.length; elmKey++) {
             new PinchZoom.default(elms[elmKey]);
           }
        }       
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(600).animate({
        //     width:'454px', 
        //     opacity:"show"
        // }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     width:'0px'
        // }, 650);

    }
  }

});
app.register("nutrition_page_00", function() {

    var mainClass = "#nutrition_page_00 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =0;
    var setTime =0;

        // $(mainClass + ".ani-1").delay(0).fadeIn(150);        
        // $(mainClass + ".ani-3").delay(time+=setTime).show( "slide",{direction: "left"}, 1000);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.register("nutrition_page_01", function() {

    var mainClass = "#nutrition_page_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =0;
    var setTime =0;

        // $(mainClass + ".ani-1").delay(0).fadeIn(150);
        $(mainClass + ".ani-2").delay(450).fadeIn(150);
        // $(mainClass + ".ani-2").delay(1000).animate({
        //     height:'166px',
        //     opacity:"show"
        // }, 650);   
        $(mainClass + ".ani-3").delay(time+=setTime).animate({
            height:'224px',
            opacity:"show"
        }, 650); 

        $(mainClass + ".g-s01 > img").click(function(){
            $(mainClass + ".ani-4").delay(0).animate({
                height:'280px',
                opacity:"show"
            }, 650);                
        });
        //$("#side-tabs").fadeIn(500);
        //$(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);
        //$(mainClass + ".ani-1").delay(time+=setTime).show( "slide",{direction: "left"}, 1000);
        //$(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "right"}, 1000);
        //$(mainClass + ".ani-1").delay(time+=setTime).show( "slide",{direction: "right"}, 1000);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     height:'0px',
        // }, 350);

        $(mainClass + ".ani-3").delay(0).animate({
            height:'0px',
        }, 350);

        $(mainClass + ".ani-4").delay(0).animate({
            height:'0px',
        }, 350);
 

    }
  }

});
app.register("nutrition_page_02", function() {

    var mainClass = "#nutrition_page_02 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        $(mainClass + ".ani-2").delay(0).animate({
            width:'720px', 
            opacity:"show"
        }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        $(mainClass + ".ani-2").delay(0).animate({
            width:'0px'
        }, 650);

    }
  }

});
app.register("nutrition_page_03", function() {

    var mainClass = "#nutrition_page_03 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        $(mainClass + ".ani-2").delay(0).animate({
            width:'491px', 
            opacity:"show"
        }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        $(mainClass + ".ani-2").delay(0).animate({
            width:'0px'
        }, 650);

    }
  }

});
app.register("nutrition_page_04", function() {

    var mainClass = "#nutrition_page_04 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        $(mainClass + ".ani-2").delay(0).animate({
            width:'454px', 
            opacity:"show"
        }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        $(mainClass + ".ani-2").delay(0).animate({
            width:'0px'
        }, 650);

    }
  }

});
app.register("nutrition_page_05", function() {

    var mainClass = "#nutrition_page_05 ";
    
  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(600).animate({
        //     width:'454px', 
        //     opacity:"show"
        // }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     width:'0px'
        // }, 650);

    }
  }

});
app.register("nutrition_page_06", function() {

    var mainClass = "#nutrition_page_06 ";
    
  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(600).animate({
        //     width:'454px', 
        //     opacity:"show"
        // }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     width:'0px'
        // }, 650);

    }
  }

});
app.register("nutrition_page_08", function() {

    var mainClass = "#nutrition_page_08 ";
    
  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(600).animate({
        //     width:'454px', 
        //     opacity:"show"
        // }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     width:'0px'
        // }, 650);

    }
  }

});
app.register("nutrition_page_07", function() {

    var mainClass = "#nutrition_page_07 ";
    
  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(600).animate({
        //     width:'454px', 
        //     opacity:"show"
        // }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     width:'0px'
        // }, 650);

    }
  }

});
app.register("pi_popup_slide", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

    },
    onRemove: function(el) {

    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});

app.register("resources_page_01", function() {

    var mainClass = "#resources_page_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {
        // Set times for each animation
        var time = 150;
        var setTime = 500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);
        // $(mainClass + ".ani-3").delay(time+=setTime).fadeIn(1000);
        // $(mainClass + ".ani-4").delay(time+=setTime).fadeIn(1000);
        // $(mainClass + ".ani-5").delay(time+=setTime).fadeIn(1000);
        // $(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);

        // openPDF   
        $(mainClass + ".popup-pdf").click(function() {
            var file = $(this).data("file");
            var title = $(this).text().trim();
            ag.openPDF(file, title);
            return false;
        });          

        // $("#frame").attr("src", "shared/references/BA3929_YAZ_Patient_Booklet_Final.pdf");

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.register("summary_slide", function () {
	return {
		events: {
			'tap [data-popup]': 'openPopup'
		},
		states: [],
		onRender: function (el) {
			this.ahPopup = app.module.get('ah-popup-management');
		},
		openPopup: function (el) {
			this.ahPopup.openPopup(el.target.attributes["data-popup"].value);
		},
		onRemove: function (el) {

		},
		onEnter: function (el) {

		},
		onExit: function (el) {

		}
	}

});

app.register("start_slide", function() {

    var mainClass = "#start_slide ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =0;
    var setTime =0;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(0).animate({
        //     bottom:'40px', 
        //     opacity:"show"
        // }, 1000);                 
        $(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "down"}, 550);
        $(mainClass + ".ani-3").delay(time+=setTime).show( "slide",{direction: "left"}, 550);
        $(mainClass + ".ani-4").delay(time+=setTime).show( "slide",{direction: "right"}, 550);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 
        // $(mainClass + ".ani-2").delay(0).animate({
        //     bottom:'0px'
        // }, 1000);        
    }
  }

});
app.register("tablet_comparison_page_01", function() {

    var mainClass = "#tablet_comparison_page_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        var elms = document.querySelectorAll('img.zoom');
          if(elms.length > 0) {
           for(var elmKey = 0; elmKey < elms.length; elmKey++) {
             new PinchZoom.default(elms[elmKey]);
           }
        } 

    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =0;
    var setTime =450;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450); 
        //$(mainClass + ".ani-1").delay(0).show( "slide",{direction: "down"}, 650);
        //$(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "up"}, 850);

    },
    onExit: function(el) {

        //$(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.register("test_popup_1_slide", function () {
	return {
		events: {
			'tap [data-popup]': 'openPopup'
		},
		states: [],
		onRender: function (el) {
			this.ahPopup = app.module.get('ah-popup-management');
		},
		openPopup: function (el) {
			this.ahPopup.openPopup(el.target.attributes["data-popup"].value);
		},
		onRemove: function (el) {

		},
		onEnter: function (el) {


		},
		onExit: function (el) {
		}
	}
});

app.register("test_popup_2_slide", function () {
	return {
		events: {
			'tap [data-popup]': 'openPopup'
		},
		states: [],
		onRender: function (el) {
			this.ahPopup = app.module.get('ah-popup-management');
		},
		openPopup: function (el) {
			this.ahPopup.openPopup(el.target.attributes["data-popup"].value);
		},
		onRemove: function (el) {

		},
		onEnter: function (el) {


		},
		onExit: function (el) {
		}
	}
});

app.register("test_popup_3_slide", function () {

	return {
		events: {},
		states: [],
		onRender: function (el) {

		},
		onRemove: function (el) {

		},
		onEnter: function (el) {

		},
		onExit: function (el) {

		}
	}

});

app.register("test_slide_1", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("test_slide_3", function() {

  return {
    events: {
        "tap .custom-attachment-button": "addCustomAttachment"
    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    },
    addCustomAttachment: function() {
        // In this demo we get the input from the textarea but it can be anything
        var textarea = this.$(".custom-attachment-text");
        if (textarea.length) {
            // Update the media repository entry (media.json)
            window.mediaRepository.setMetada("content://Calculator result", "content", textarea[0].value);
            // Tell the Follow Up Mail module to attach the media entry
            app.$.trigger("attach:ap-follow-up-mail", {"key":"content://Calculator result"});
        }
    }
  };

});

app.register("test_slide_2", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

    },
    onRemove: function(el) {

    },
    onEnter: function(el) {
      
    },
    onExit: function(el) {

    }
  }

});

app.register("test_slide_4", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("test_slide_5", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("test_slide_6", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("test_slide_8", function () {
	return {
		events: {
			'tap [data-popup]': 'openPopup'
		},
		states: [],
		onRender: function (el) {
			this.ahPopup = app.module.get('ah-popup-management');
		},
		openPopup: function (el) {
			this.ahPopup.openPopup(el.target.attributes["data-popup"].value);
		},
		onRemove: function (el) {

		},
		onEnter: function (el) {

		},
		onExit: function (el) {

		}
	}

});

app.register("test_slide_7", function() {

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});
app.register("the_elevit_range_page_00", function() {

    var mainClass = "#the_elevit_range_page_00 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {
    // Set times for each animation
    var time =0;
    var setTime =0;

        // $(mainClass + ".ani-1").delay(0).fadeIn(0);  
        // $(mainClass + ".ani-1").delay(time+=setTime).show( "slide",{direction: "left"}, 1000);
    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();
    }
  }

});
app.register("the_elevit_range_page_01", function() {

    var mainClass = "#the_elevit_range_page_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {
        // Set times for each animation
        var time =0;
        var setTime =150;

            $(mainClass + ".ani-1").delay(0).fadeIn(1000); 
            $(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);
            $(mainClass + ".ani-3").delay(time+=setTime).fadeIn(1000);
            $(mainClass + ".ani-4").delay(time+=setTime).fadeIn(1000);
            $(mainClass + ".ani-5").delay(time+=setTime).fadeIn(1000);
            $(mainClass + ".ani-6").delay(time+=setTime).fadeIn(1000);
            // $(mainClass + ".ani-1").delay(0).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-3").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-4").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-5").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-1").delay(0).fadeIn(1000); 
            // $(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);
            // $(mainClass + ".ani-3").delay(time+=setTime).fadeIn(1000);
            // $(mainClass + ".ani-4").delay(time+=setTime).fadeIn(1000);
            // $(mainClass + ".ani-5").delay(time+=setTime).fadeIn(1000);
            // $(mainClass + ".ani-6").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-6").delay(time+=setTime).fadeIn(1000);
    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.register("the_elevit_range_page_03", function() {

    var mainClass = "#the_elevit_range_page_03 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

        $(mainClass + ".ani-1").delay(0).show( "slide",{direction: "right"}, 650);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.register("the_elevit_range_page_02", function() {

    var mainClass = "#the_elevit_range_page_02 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

        $(mainClass + ".ani-1").delay(0).show( "slide",{direction: "right"}, 650);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.register("the_elevit_range_page_04", function() {

    var mainClass = "#the_elevit_range_page_04 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

        $(mainClass + ".ani-1").delay(0).show( "slide",{direction: "right"}, 650);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});
app.cache.put("modules/ag-auto-menu/CHANGELOG.html","<h3 id=\"version-0-9-1-2018-03-29\">Version 0.9.1 2018-03-29</h3>\n<ul>\n<li>update styles according to csslint</li>\n</ul>\n<h3 id=\"version-0-9-0-2015-09-22\">Version 0.9.0 2015-09-22</h3>\n<ul>\n<li>Fixed link for nested array</li>\n<li>Added trigger property</li>\n</ul>\n<h3 id=\"version-0-8-5-2015-08-12\">Version 0.8.5 2015-08-12</h3>\n<ul>\n<li>Fixed placement settings</li>\n<li>Small fix to &#39;slideshows&#39; setting</li>\n</ul>\n<h3 id=\"version-0-8-0-2015-08-11\">Version 0.8.0 2015-08-11</h3>\n<ul>\n<li>Added &#39;slideshows&#39; setting</li>\n</ul>\n");
app.cache.put("modules/ag-overlay/overlay.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Overlay</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"overlay\" class=\"slide\">\r\n      <div class=\"splash-container\">\r\n        <div class=\"module-content splash-container\">\r\n          <div class=\"pure-g\">\r\n              <div class=\"pure-u-1 pure-u-lg-2-3\">\r\n                <div class=\"l-box\"><h1>ag-overlay <span class=\"mversion\" id=\"overlay-version\">v0.5.0</span></h1>\r\n\r\n<h2>CLI Import</h2>\r\n\r\n<pre>\r\n<code class=\"bash hljs state-default\" data-module=\"ag-highlight-js\" id=\"overlay-import\">agnitio use ag-overlay</code></pre>\r\n\r\n<h2>Usage</h2>\r\n\r\n<p>In index.html or inside slide html, add:</p>\r\n\r\n<pre>\r\n<code class=\"html hljs xml state-default\" data-module=\"ag-highlight-js\" id=\"overlay-use\"><span class=\"hljs-tag\">&lt;<span class=\"hljs-title\">div</span> <span class=\"hljs-attribute\">id</span>=<span class=\"hljs-value\">&quot;overlay-demo&quot;</span> <span class=\"hljs-attribute\">data-module</span>=<span class=\"hljs-value\">&quot;ag-overlay&quot;</span>&gt;</span><span class=\"hljs-tag\">&lt;/<span class=\"hljs-title\">div</span>&gt;</span></code></pre>\r\n\r\n<p>Then from any JavaScript you can call:</p>\r\n\r\n<pre>\r\n<code class=\"javascript hljs state-default\" data-module=\"ag-highlight-js\" id=\"overlay-use2\"><span class=\"hljs-keyword\">var</span> overlay = app.view.get(<span class=\"hljs-string\">&apos;overlay-demo&apos;</span>);\r\n<span class=\"hljs-keyword\">if</span> (overlay) {\r\n    overlay.open(<span class=\"hljs-string\">&apos;&lt;h1&gt;Overlay Demo&lt;/h1&gt;&apos;</span>);\r\n}</code></pre>\r\n\r\n<p>Or to load a slide into the overlay:</p>\r\n\r\n<pre>\r\n<code class=\"javascript hljs state-default\" data-module=\"ag-highlight-js\" id=\"overlay-use3\"><span class=\"hljs-keyword\">var</span> overlay = app.view.get(<span class=\"hljs-string\">&apos;overlay-demo&apos;</span>);\r\n<span class=\"hljs-keyword\">if</span> (overlay) {\r\n    overlay.load(\'some_slide_id\');\r\n}</code></pre>\r\n\r\n<h2>Options</h2>\r\n\r\n<p>The following attributes can be set on the element.</p>\r\n\r\n<table class=\"pure-table\">\r\n	<thead>\r\n		<tr>\r\n			<th>Attribute</th>\r\n			<th>Type</th>\r\n			<th>Default</th>\r\n			<th>Notes</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>width</td>\r\n			<td>STRING</td>\r\n			<td>&quot;80%&quot;</td>\r\n			<td>Width of the overlay content area. Only supports percent at the moment.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>height</td>\r\n			<td>STRING</td>\r\n			<td>&quot;80%&quot;</td>\r\n			<td>Height of the overlay content area. Only supports percent at the moment.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>no-background</td>\r\n			<td>BOOLEAN</td>\r\n			<td>false</td>\r\n			<td>If the darker background behind the content should not be there.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>content</td>\r\n			<td>STRING</td>\r\n			<td>&quot;No content available&quot;</td>\r\n			<td>Default content to be used if nothing is provided in call to &quot;open&quot;.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>API</h2>\r\n\r\n<p>The following JavaScript API can be used with a module instance:</p>\r\n\r\n<table class=\"pure-table\">\r\n	<thead>\r\n		<tr>\r\n			<th>API</th>\r\n			<th>Parameters</th>\r\n			<th>Notes</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>open</td>\r\n			<td>content [STRING]</td>\r\n			<td>Opens the overlay with the provided content.</td>\r\n		</tr>\r\n    <tr>\r\n      <td>load</td>\r\n      <td>slideId [STRING]</td>\r\n      <td>Loads a slide into the overlay and opens it.</td>\r\n    </tr>\r\n		<tr>\r\n			<td>close</td>\r\n			<td></td>\r\n			<td>Closes the overlay.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>setDimensions</td>\r\n			<td>width [INTEGER], height [INTEGER]</td>\r\n			<td>Update the width and height of the content container. Should be between 0 and 100.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Styling</h2>\r\n\r\n<p>The overlay can be styled as desired. The following classes are set on the elements:</p>\r\n\r\n<table class=\"pure-table\">\r\n	<thead>\r\n		<tr>\r\n			<th>Class name</th>\r\n			<th>Notes</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>ag-overlay-background</td>\r\n			<td>Use this to style the semi-transparent background.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ag-overlay-content</td>\r\n			<td>Use this to style the container holding the content of the overlay.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>ag-overlay-x</td>\r\n			<td>This class can be used to style the default close button.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Custom Close Element</h2>\r\n\r\n<p>Adding an element anywhere within the module with the class &quot;ag-overlay-close&quot; will close the overlay when tapped. You can include this within the content you provide for the overlay or as a separate element inside the module itself, e.g.:</p>\r\n\r\n<pre>\r\n<code class=\"html hljs xml state-default\" data-module=\"ag-highlight-js\" id=\"overlay-custom-close\"><span class=\"hljs-tag\">&lt;<span class=\"hljs-title\">div</span> <span class=\"hljs-attribute\">id</span>=<span class=\"hljs-value\">&quot;overlay-demo&quot;</span> <span class=\"hljs-attribute\">data-module</span>=<span class=\"hljs-value\">&quot;ag-overlay&quot;</span> <span class=\"hljs-attribute\">no-close-btn</span>&gt;</span>\r\n    <span class=\"hljs-tag\">&lt;<span class=\"hljs-title\">div</span> <span class=\"hljs-attribute\">class</span>=<span class=\"hljs-value\">&quot;ag-overlay-close overlay-custom-close-btn&quot;</span>&gt;</span>Close<span class=\"hljs-tag\">&lt;/<span class=\"hljs-title\">div</span>&gt;</span>\r\n<span class=\"hljs-tag\">&lt;/<span class=\"hljs-title\">div</span>&gt;</span></code></pre>\r\n</div>\r\n              </div>\r\n              <div class=\"pure-u-1 pure-u-lg-1-3\">\r\n                  <h2>Demo</h2>\r\n                  <button id=\"overlay_test\" class=\"pure-button\">Click to open basic overlay</button>\r\n                  <button id=\"overlay_slide\" class=\"pure-button\">Click to open overlay with slide</button>\r\n                  <div id=\"overlay-demo\" data-module=\"ag-overlay\" width=\"80%\" height=\"40%\"></div>\r\n                  <h2>Description</h2>\r\n                  <p>This module will display provided content in an overlay. The overlay will be displayed in the center of the screen. Clicking outside the content will close the overlay.<br>\r\n<br>\r\n<span style=\"color:#FF0000;\"><strong>NOTE:</strong></span> If loading a slide that already have been loaded, the previous version will first be removed from DOM in order to load it in the overlay. This mean that any DOM states will be reset.</p>\r\n              </div>\r\n          </div>\r\n      </div>\r\n      </div>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("modules/ag-overlay/CHANGELOG.html","<h3 id=\"version-0-6-0-2018-03-21\">Version 0.6.0 2018-03-21</h3>\n<ul>\n<li>Fixed wrong behaviour on exit state when unlock presentation.</li>\n</ul>\n<h3 id=\"version-0-6-1-2018-04-17\">Version 0.6.1 2018-04-17</h3>\n<ul>\n<li>Add functionality for set &#39;state-default&#39; for popup (The same as slide). Variable for this class is defaultClassSlide and set this class if slide hasn&#39;t active state.</li>\n</ul>\n");
app.cache.put("modules/ag-slide-analytics/CHANGELOG.html","<h3 id=\"version-0-7-1-2018-06-28\">Version 0.7.1 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n");
app.cache.put("modules/ag-viewer/CHANGELOG.html","<h3 id=\"version-1-2-1-2018-03-26\">Version 1.2.1 2018-03-26</h3>\n<ul>\n<li>Add functionality adding custom class for slideshow in ag-viewer. You need to add attribute &#39;data-custom-class=className&#39; to open button.</li>\n<li>Change event &quot;click&quot; to &quot;tap&quot;</li>\n</ul>\n");
app.cache.put("modules/ag-video/ag-video.html","<div class=\"ag-video-container\">\r\n	<video src=\"\" type=\"\" poster=\"\" class=\"ag-video-element\"></video>\r\n	<div class=\"ag-video-overlay\">\r\n		<div class=\"ag-video-big-play-button\"><span></span></div>\r\n		<div class=\"ag-upper-click-layer\"></div>\r\n		<div class=\"ag-video-controls\">\r\n			<div class=\"ag-video-play-toggle\"><div></div></div>\r\n            <div class=\"ag-video-progress-container ag-video-progress-controller\">\r\n                <div class=\"ag-video-timer ag-video-current-time\">0:00</div>\r\n                <div class=\"ag-video-timer ag-video-total-time\"></div>\r\n                <div class=\"ag-video-progress-bar\"></div>\r\n                <div class=\"ag-video-seek-ele\"></div>\r\n            </div>\r\n            <div class=\"ag-video-fullscreen-button\"><div></div></div>\r\n		</div>\r\n	</div>\r\n</div>");
app.cache.put("modules/ag-video/CHANGELOG.html","<h3 id=\"version-0-10-1-2018-06-28\">Version 0.10.1 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n<h3 id=\"version-0-10-0-2016-05-06\">Version 0.10.0 2016-05-06</h3>\n<ul>\n<li>Fixed a bug that prevented using more than one instance of the video-module.</li>\n<li>Fixed positionting of the video-controls.</li>\n</ul>\n<h3 id=\"version-0-9-7-2015-11-02\">Version 0.9.7 2015-11-02</h3>\n<ul>\n<li>Fixed minor bug that could result in loop if removing element</li>\n</ul>\n<h3 id=\"version-0-9-5-2015-09-22\">Version 0.9.5 2015-09-22</h3>\n<ul>\n<li>Added monitoring</li>\n</ul>\n");
app.cache.put("modules/ah-auto-references/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<ul>\n<li>Initial version</li>\n</ul>\n");
app.cache.put("modules/ah-cleanup/CHANGELOG.html","<h3 id=\"version-1-0-1\">Version 1.0.1</h3>\n<h3 id=\"2018-03-06-a-shapovalova\">2018-03-06 - A.Shapovalova</h3>\n<ul>\n<li>slide:enter event replaced with update:anthill-current to fix appearing slide without background, which then appears some time later</li>\n</ul>\n<h3 id=\"version-1-0-0\">Version 1.0.0</h3>\n<h3 id=\"2018-02-27-nina-koval\">2018-02-27 - Nina Koval</h3>\n<ul>\n<li>module was updated according to jslint rules</li>\n</ul>\n<h3 id=\"version-0-5-0\">Version 0.5.0</h3>\n<h3 id=\"2016-06-22-roman-kovalenko\">2016-06-22 - Roman Kovalenko</h3>\n<ul>\n<li>version of module ag-cleanup, on which ah-cleanup is based</li>\n</ul>\n");
app.cache.put("modules/ah-popup-management/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-10-02\">Version 0.0.1 2018-10-02</h3>\n<ul>\n<li>add management for popups in BHC</li>\n</ul>\n");
app.cache.put("modules/ah-tutorial/tutorial.html","<div data-bind=\"css: tutorialStep\">\r\n	<div class=\"info-popup\">\r\n		<button class=\"close-tutorial\" data-bind=\"event: {\'tap\': $root.fastCloseTour}\"></button>\r\n		<h1 data-bind=\"html: data.info.header\"></h1>\r\n		<div class=\"tab-group\" id=\"tutorialTabGroup\" data-module=\"ah-tab-group\">\r\n			<nav class=\"tg-nav\"><!-- ko foreach: data.info.tab_items -->\r\n				<button class=\"tg-tab tg-general-button\"\r\n						data-bind=\"attr: { \'data-tab\': $index }, html: $data.tab_header\"></button><!-- /ko -->\r\n				<!-- ko foreach: data.info.placeholders -->\r\n				<button class=\"tg-general-button\" data-bind=\"html: $data.header\"></button><!-- /ko -->\r\n				<button class=\"tg-general-button start-button-training\"\r\n						data-bind=\"event: { \'tap\': $root.goTour }, text: data.info.button\"></button>\r\n			</nav><!-- ko foreach: data.info.tab_items -->\r\n			<div class=\"tg-content\" data-bind=\"attr: { \'data-info-item\': $index, \'data-tab\': $index }\">\r\n				<!-- ko if: $data.content_header -->\r\n				<h2 data-bind=\"html: $data.content_header\"></h2><!-- /ko -->\r\n				<div class=\"content-wrap\">\r\n					<div class=\"scroll-wrapper-container\"><!-- ko if: $data.text_items -->\r\n						<div class=\"text-items\" data-bind=\"foreach: { data: $data.text_items, as: \'item\' }\">\r\n							<div class=\"item\" data-bind=\"foreach: { data: $data.texts, as: \'textsItem\' }\">\r\n								<p data-bind=\"html: textsItem.text\"></p>\r\n							</div>\r\n						</div><!-- /ko --><!-- ko if: $data.unordered_list -->\r\n						<ul data-bind=\"foreach: { data: $data.unordered_list, as: \'li\' }\">\r\n							<li data-bind=\"html: li.text\"></li><!-- ko if: li.sublist -->\r\n							<ol data-bind=\"foreach: { data: li.sublist, as: \'subLi\' }\">\r\n								<li data-bind=\"html: subLi.subtext\"></li>\r\n							</ol><!-- /ko -->\r\n						</ul><!-- /ko --><!-- ko if: $data.steps_list -->\r\n						<ol data-bind=\"foreach: { data: $data.steps_list, as: \'li\' }\">\r\n							<li data-bind=\"html: li.text\"></li><!-- ko if: li.sublist -->\r\n							<ul data-bind=\"foreach: { data: li.sublist, as: \'subLi\' }\">\r\n								<li data-bind=\"html: subLi.subitem\"></li>\r\n							</ul><!-- /ko -->\r\n							<!-- ko if: li.info -->\r\n							<p data-bind=\"html: li.info\"></p><!-- /ko -->\r\n							<!-- ko if: li.thumbnails -->\r\n							<ul class=\"thumbnail-list\"\r\n								data-bind=\"foreach: { data: li.thumbnails, as: \'thumbnailItem\' }\">\r\n								<li class=\"thumbnail-item-wrap\">\r\n									<div class=\"thumbnail-item\"></div>\r\n									<p class=\"thumbnail-name\" data-bind=\"html: thumbnailItem.name\"></p>\r\n									<!-- ko if: thumbnailItem.thumbnailChildren -->\r\n									<ul class=\"thumbnail-children\"\r\n										data-bind=\"foreach: { data: thumbnailItem.thumbnailChildren, as: \'child\' }\">\r\n										<li class=\"thumbnail-item-wrap\">\r\n											<div class=\"thumbnail-item\"></div>\r\n											<p class=\"thumbnail-name\" data-bind=\"html: child.name\"></p>\r\n										</li>\r\n									</ul><!-- /ko -->\r\n								</li>\r\n							</ul><!-- /ko -->\r\n						</ol><!-- /ko -->\r\n					</div>\r\n				</div>\r\n			</div><!-- /ko -->\r\n		</div>\r\n	</div>\r\n</div>");
app.cache.put("modules/ah-tour/custom-popover.html","<div class=\"custom-popover-wrapper\">\r\n	<div class=\"scroll-wrapper-container\" data-bind=\"foreach: { data: content, as: \'item\'}\">\r\n		<div data-bind=\"attr: { class: item.classes ? item.classes + \' item\' : \'item\'}\">\r\n			<!-- ko if: item.title -->\r\n			<h4 data-bind=\"html: item.title\"></h4>\r\n			<!-- /ko -->\r\n			<!-- ko if: item.list -->\r\n			<ul class=\"dotted-list\" data-bind=\"foreach: { data: item.list, as: \'li\' }\">\r\n				<li data-bind=\"html: li\"></li>\r\n			</ul>\r\n			<!-- /ko -->\r\n			<!-- ko if: item.numberList -->\r\n			<!-- ko if: item.numberList.title -->\r\n			<p class=\"numeric-title\" data-bind=\"html: item.numberList.title\"></p>\r\n			<!-- /ko -->\r\n			<ul class=\"numeric-list\" data-bind=\"foreach: { data: item.numberList.list, as: \'li\' }\">\r\n				<li data-bind=\"html: li\"></li>\r\n			</ul>\r\n			<!-- /ko -->\r\n			<!-- ko if: item.text_list -->\r\n			<ul class=\"text-list\" data-bind=\"foreach: { data: item.text_list, as: \'li\' }\">\r\n				<li data-bind=\"html: li\"></li>\r\n			</ul>\r\n			<!-- /ko -->\r\n			<!-- ko if: item.references -->\r\n			<p class=\"overlay-references\">\r\n				<strong data-bind=\"if : item.references.length === 1\">Reference:</strong>\r\n				<strong data-bind=\"if : item.references.length > 1\">References:</strong>\r\n				<!-- ko foreach: { data: item.references, as: \'dd\' } -->\r\n				<strong data-bind=\"html: $index() + 1 + \'.\'\"></strong>\r\n				<span data-bind=\"attr: { \'data-overlay-reference\': dd.item }\"></span>\r\n				<!-- /ko -->\r\n			</p>\r\n			<!-- /ko -->\r\n			<!-- ko if: item.table -->\r\n			<table>\r\n				<colgroup data-bind=\"foreach: { data: item.table.thead, as: \'th\' }\">\r\n					<col>\r\n				</colgroup>\r\n				<thead>\r\n					<tr data-bind=\"foreach: { data: item.table.thead, as: \'th\' }\">\r\n						<th data-bind=\"html: th\"></th>\r\n					</tr>\r\n				</thead>\r\n				<tbody data-bind=\"foreach: { data: item.table.tbody, as: \'tr\' }\">\r\n					<tr data-bind=\"foreach: { data: tr, as: \'td\' }\">\r\n						<td data-bind=\"html: td\"></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<!-- /ko -->\r\n			<!-- ko if: item.note -->\r\n			<p class=\"overlay-note\" data-bind=\"html: item.note\"></p>\r\n			<!-- /ko -->\r\n		</div>\r\n	</div>\r\n</div>\r\n\r\n");
app.cache.put("modules/ah-tour/template.html","<div class=\"popover\" role=\"tooltip\">\r\n	<div class=\"arrow\"></div>\r\n	<h3 class=\"popover-title\"></h3>\r\n	<div class=\"popover-content\"></div>\r\n</div>");
app.cache.put("modules/ah-tutorial-controls/ah-tutorial-controls.html","<button class=\"tutorial\"></button>");
app.cache.put("modules/ap-auto-menu-handle/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<ul>\n<li>Initial version</li>\n</ul>\n");
app.cache.put("modules/ap-auto-references/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<ul>\n<li>Initial version</li>\n</ul>\n<h3 id=\"version-1-0-2-2018-10-09\">Version 1.0.2 2018-10-09</h3>\n<ul>\n<li>Addition of abilities to:<ul>\n<li>Render references to a referencing element</li>\n<li>Multiple references addition specifying references or groups of references using comma (default reference separator)</li>\n<li>Specify sequential group of references using dash (default reference group separator)</li>\n</ul>\n</li>\n</ul>\n");
app.cache.put("modules/ap-auto-references-popup/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<ul>\n<li>Initial version</li>\n</ul>\n<h3 id=\"version-1-0-2-2018-10-09\">Version 1.0.2 2018-10-09</h3>\n<ul>\n<li>Removing code duplication with ap-auto-references module</li>\n<li>Creating popup on render and populating it with content on slide enter</li>\n<li>Receiving reference indexes from ap-auto-references module on &#39;refindexes:created&#39; event</li>\n<li>Refactoring</li>\n</ul>\n");
app.cache.put("modules/ap-auto-side-clip/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<ul>\n<li>Initial version</li>\n</ul>\n");
app.cache.put("modules/ap-back-navigation/CHANGELOG.html","<h3 id=\"version-0-0-2-2018-06-28\">Version 0.0.2 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n<h3 id=\"version-0-0-1-01-06-2018\">Version 0.0.1 01.06.2018</h3>\n<ul>\n<li>Fix of the bug to incorrect writes to history, when we very fast swipe to  slides.</li>\n</ul>\n");
app.cache.put("modules/ap-back-navigation/readme.html","<h2 id=\"ap-back-navigation\">ap-back-navigation</h2>\n<h3 id=\"this-module-writes-to-slides-history-and-provides-a-function-to-navigate-backwards-\">This module writes to slides history and provides a function to navigate backwards.</h3>\n<h3 id=\"navigates-backwards-through-the-sequence-of-app-goto-calls-\">Navigates backwards through the sequence of app.goTo calls.</h3>\n<h2 id=\"how-to-use\">How to use</h2>\n<h5 id=\"include-module-to-presentation-json\">Include module to presentation.json</h5>\n<pre><code>&quot;ap-back-navigation&quot;: {\n    &quot;id&quot;: &quot;ap-back-navigation&quot;,\n    &quot;files&quot;: {\n        &quot;templates&quot;: [],\n        &quot;scripts&quot;: [\n            &quot;modules/ap-back-navigation/ap-back-navigation.js&quot;\n        ],\n        &quot;styles&quot;: []\n    }\n}\n</code></pre><h3 id=\"include-module-to-jade-pug-html-file-or-template\">Include module to jade/pug/html file or template</h3>\n<pre><code>&lt;div data-module=&quot;ap-back-navigation&quot;&gt;&lt;/div&gt;\n</code></pre>");
app.cache.put("modules/ap-content-groups/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n");
app.cache.put("modules/ap-custom-collections-menu/ap-custom-collections-menu.html","<div class=\"custom-collections-menu\">\r\n  <div class=\"presentationsContainer\">\r\n    <div class=\"row template\">\r\n      <div class=\"button favorite\"> </div>\r\n      <div class=\"button presentation\">\r\n        <div class=\"name\">\r\n          to be replaced by javascript\r\n        </div>\r\n      </div>\r\n      <div class=\"button edit\"> </div>\r\n      <div class=\"button rename\"> </div>\r\n      <div class=\"button trash\"> </div>\r\n    </div>\r\n    <div class=\"row newPresentationButtonContainer\">\r\n      <div class=\"button newPresentation\">\r\n        ...\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div id=\"maxFavoritesPopUp\"><span>You can only tag 3 presentations simultaneously as favorite!</span></div>\r\n</div>\r\n");
app.cache.put("modules/ap-custom-collections-menu/CHANGELOG.html","<h3 id=\"version-0-0-2-26-06-2018\">Version 0.0.2 26.06.2018</h3>\n<ul>\n<li>fix issue with getting data in ajax response</li>\n</ul>\n<h3 id=\"version-0-0-1-31-05-2018\">Version 0.0.1 31.05.2018</h3>\n<ul>\n<li>Update getting data for default custom collection</li>\n<li>Fix Linters warnings (css, js)</li>\n</ul>\n");
app.cache.put("modules/ap-custom-collections/ap-custom-collections.html","<div class=\"custom-collections\">\r\n	<div data-module=\"ap-overview\" iscustomoverview></div>\r\n	<div class=\"edit-zone\"></div>\r\n	<div class=\"divisor\">\r\n		<p class=\"presentation-name\">e.g. name</p>\r\n		<div class=\"arrow\"></div>\r\n		<p class=\"instructions\">Build your presentation by dragging slides</p>\r\n		<div class=\"arrow\"></div>\r\n		<button class=\"save\">Save</button>\r\n		<button class=\"cancel\">Cancel</button>\r\n	</div>\r\n</div>\r\n");
app.cache.put("modules/ap-custom-collections/CHANGELOG.html","<h3 id=\"version-0-0-8-2018-09-27\">Version 0.0.8 2018-09-27</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n<h3 id=\"version-0-0-7-16-07-18-oleksandr-melnyk\">Version  0.0.7 16.07.18 Oleksandr Melnyk</h3>\n<ul>\n<li>fix css linter warnings</li>\n</ul>\n<h3 id=\"version-0-0-6-26-06-2018\">Version 0.0.6 26.06.2018</h3>\n<ul>\n<li>fix issue with getting data in ajax response</li>\n<li>fix issue with wrong slides naming</li>\n</ul>\n<h3 id=\"version-0-0-5-06-06-2018\">Version 0.0.5 06.06.2018</h3>\n<ul>\n<li>Fixed issue with adding linked slides</li>\n<li>Added mandatory parameter</li>\n</ul>\n<h3 id=\"version-0-0-4-06-06-2018\">Version 0.0.4 06.06.2018</h3>\n<ul>\n<li>Fix of the bug with wrong location slides to collection when moving the slide fast</li>\n</ul>\n<h3 id=\"version-0-0-3-05-06-2018\">Version 0.0.3 05.06.2018</h3>\n<ul>\n<li>Fix semantic</li>\n</ul>\n<h3 id=\"version-0-0-2-31-05-2018\">Version 0.0.2 31.05.2018</h3>\n<ul>\n<li>Update getting data for default custom collection</li>\n<li>Fix Linters warnings (css, js)</li>\n<li>Update collection editing flow (first and last slides could be deleted, but not last slide or last group in collection)</li>\n</ul>\n<h3 id=\"version-0-0-1-16-04-2018\">Version 0.0.1 16.04.2018</h3>\n<ul>\n<li>Fix of the bug that led to a slide image freezing while being dragged with another slide grabbed, the bug that led to a discarded result of a movement because of an another mouseUp event detection, the bug that led to a state change of the .o_slide because of a click on the .edit button from the &quot;ap-custom-collections-menu&quot;, the bug that led to a wrong touch being used</li>\n</ul>\n");
app.cache.put("modules/ap-custom-collections-storage/CHANGELOG.html","<h3 id=\"verson-0-0-1-02-05-2018\">Verson 0.0.1 02.05.2018</h3>\n<ul>\n<li>Fix of the bug with custom presentations storageNamespace that is available for all presentations that have one product name</li>\n</ul>\n");
app.cache.put("modules/ap-favorite-presentations-buttons/ap-favorite-presentations-buttons.html","<div class=\"favoritePresentationsContainer\">\r\n\r\n</div>");
app.cache.put("modules/ap-favorite-presentations-buttons/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<ul>\n<li>Initial version</li>\n</ul>\n");
app.cache.put("modules/ap-follow-up-mail/ap-follow-up-mail.html","<div class=\"FollowUpMail\">\r\n    <div class=\"mail\">\r\n        <h1>Follow-Up Mail Attachments</h1>\r\n\r\n        <div class=\"attachments\">\r\n            <div data-module=\"ap-media-library\" attachment prefilterattributestobesearched=\"allowDistribution\"\r\n                 prefiltersearchterms=\"true\" id=\"ap-media-library_471147114711\"></div>\r\n        </div>\r\n        <div class=\"sendButton\">New Follow-Up Mail</div>\r\n        <div class=\"clearButton\">Clear</div>\r\n    </div>\r\n    <div class=\"library\">\r\n        <div data-module=\"ap-media-library\" followupmail prefilterattributestobesearched=\"allowDistribution\"\r\n             prefiltersearchterms=\"true\"></div>\r\n    </div>\r\n</div>\r\n");
app.cache.put("modules/ap-follow-up-mail/CHANGELOG.html","<h3 id=\"version-1-0-6-2018-09-27\">Version 1.0.6 2018-09-27</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n<h3 id=\"version-1-0-5-2018-07-16\">Version 1.0.5 2018-07-16</h3>\n<h3 id=\"oleksandr-melnyk\">Oleksandr Melnyk</h3>\n<ul>\n<li>fix coffee linter errors.</li>\n</ul>\n<h3 id=\"version-1-0-4-2018-03-16\">Version 1.0.4 2018-03-16</h3>\n<h3 id=\"oleksandr-melnyk\">Oleksandr Melnyk</h3>\n<ul>\n<li>fix coffee linter errors.</li>\n</ul>\n<h3 id=\"version-1-0-3-2018-05-31\">Version 1.0.3 2018-05-31</h3>\n<h3 id=\"oleksandr-tkachov\">Oleksandr Tkachov</h3>\n<ul>\n<li>Update code to use current ap-media-repository module functionality.</li>\n</ul>\n<h3 id=\"version-1-0-2-2018-04-16\">Version 1.0.2 2018-04-16</h3>\n<h3 id=\"vladyslav-voit\">Vladyslav Voit</h3>\n<ul>\n<li>Fix styles for input search field.</li>\n</ul>\n<h3 id=\"version-1-0-1-2018-04-13\">Version 1.0.1 2018-04-13</h3>\n<h3 id=\"vlad-kodenko\">Vlad Kodenko</h3>\n<ul>\n<li>Fixing an issues with Helvetica font</li>\n<li>Changing the positioning property of the .mail section from top to bottom.</li>\n</ul>\n");
app.cache.put("modules/ap-frequently-asked-questions/ap-frequently-asked-questions.html","<div class=\"FrequentlyAskedQuestions\">\r\n  <h1>FAQ</h1>\r\n  <div class=\"scroll\">\r\n    <ol>\r\n      <li>\r\n        <h2 class=\"q\">This is the first question?</h2>\r\n        <p class=\"a\">Here is the answer to the first question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the second question?</h2>\r\n        <p class=\"a\">Here is the answer to the second question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the third question?</h2>\r\n        <p class=\"a\">Here is the answer to the third question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the fourth question?</h2>\r\n        <p class=\"a\">Here is the answer to the fourth question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the fifth question?</h2>\r\n        <p class=\"a\">Here is the answer to the fifth question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the sixth question?</h2>\r\n        <p class=\"a\">Here is the answer to the sixth question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the seventh question?</h2>\r\n        <p class=\"a\">Here is the answer to the seventh question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the eighth question?</h2>\r\n        <p class=\"a\">Here is the answer to the eighth question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the ninth question?</h2>\r\n        <p class=\"a\">Here is the answer to the ninth question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the tenth question?</h2>\r\n        <p class=\"a\">Here is the answer to the tenth question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the eleventh question?</h2>\r\n        <p class=\"a\">Here is the answer to the eleventh question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the twelfth question?</h2>\r\n        <p class=\"a\">Here is the answer to the twelfth question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the thirteenth question?</h2>\r\n        <p class=\"a\">Here is the answer to the thirteenth question.</p>\r\n      </li>\r\n      <li>\r\n        <h2 class=\"q\">This is the fourteenth question?</h2>\r\n        <p class=\"a\">Here is the answer to the fourteenth question.</p>\r\n      </li>\r\n    </ol>\r\n  </div>\r\n</div>\r\n");
app.cache.put("modules/ap-frequently-asked-questions/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n");
app.cache.put("modules/ap-media-library/ap-media-library.html","<div class=\"MediaLibrary\">\r\n  <h1>Media Library</h1>\r\n  <input type=\"text\" placeholder=\"Search (e.g. pdf reference 2009)\"/>\r\n  <div class=\"scroll\">\r\n      <ul></ul>\r\n  </div>\r\n</div>\r\n");
app.cache.put("modules/ap-media-library/CHANGELOG.html","<h3 id=\"version-1-0-3-2018-06-28\">Version 1.0.3 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n<h3 id=\"version-1-0-2-2018-05-31\">Version 1.0.2 2018-05-31</h3>\n<h3 id=\"oleksandr-tkachov\">Oleksandr Tkachov</h3>\n<ul>\n<li>Update code to use current ap-media-repository module functionality, add &quot;metadatafiltrationopts&quot; property which accepts options to perform metadata filtration before it will be rendered.</li>\n</ul>\n<h3 id=\"version-1-0-1-2018-04-13\">Version 1.0.1 2018-04-13</h3>\n<h3 id=\"vlad-kodenko\">Vlad Kodenko</h3>\n<ul>\n<li>Added span tag to referenceId number wrapper.</li>\n</ul>\n");
app.cache.put("modules/ap-media-repository/CHANGELOG.html","<h3 id=\"version-1-0-5-2018-10-10\">Version 1.0.5 2018-10-10</h3>\n<h3 id=\"andrii-romanyshyn\">Andrii Romanyshyn</h3>\n<ul>\n<li>Added usereferenceslistnumeration property. If set to usereferenceslistnumeration property references numeration will be rendered according to the reference position in the list instead of using &quot;referenceId&quot; property.</li>\n</ul>\n<h3 id=\"version-1-0-3-2018-07-16\">Version 1.0.3 2018-07-16</h3>\n<h3 id=\"oleksandr-melnyk\">Oleksandr Melnyk</h3>\n<ul>\n<li>fix js linter errors.</li>\n</ul>\n<h3 id=\"version-1-0-2-2018-06-04\">Version 1.0.2 2018-06-04</h3>\n<h3 id=\"oleksandr-tkachov\">Oleksandr Tkachov</h3>\n<ul>\n<li>add functionality for rendering references numeration according to their position in the list instead of using &quot;referenceId&quot; property.</li>\n</ul>\n<h3 id=\"version-1-0-1-2018-05-31\">Version 1.0.1 2018-05-31</h3>\n<h3 id=\"oleksandr-tkachov\">Oleksandr Tkachov</h3>\n<ul>\n<li>add functionality for filtering metadata by current active presentation flow, getting list of rendered metadata filtered with filtration handlers.</li>\n</ul>\n");
app.cache.put("modules/ap-notepad/ap-notepad.html","<div class=\"Notepad\">\r\n  <canvas id=\"notepad-canvas\" width=\"1920\" height=\"1080\"></canvas>\r\n  <div class=\"palette\">\r\n    <div class=\"button exit\"></div>\r\n    <div class=\"button clear\"></div>\r\n    <div class=\"button red\"></div>\r\n    <div class=\"button green\"></div>\r\n  </div>\r\n</div>\r\n");
app.cache.put("modules/ap-notepad/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n");
app.cache.put("modules/ap-overview/ap-overview.html","<div class=\"overview\">\r\n	<ul class=\"collections\"></ul>\r\n	<div class=\"collection-overview\">\r\n		<div class=\"o-collection\"></div>\r\n	</div>\r\n</div>\r\n");
app.cache.put("modules/ap-overview/CHANGELOG.html","<h3 id=\"version-0-0-3-2018-06-28\">Version 0.0.3 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n<h3 id=\"version-0-0-2-26-06-2018\">Version 0.0.2 26.06.2018</h3>\n<ul>\n<li>fix issue with getting data in ajax response</li>\n</ul>\n<h3 id=\"verson-0-0-1-31-05-2018\">Verson 0.0.1 31.05.2018</h3>\n<ul>\n<li>Update getting data for default custom collection</li>\n<li>Fix Linters warnings (css, js)</li>\n</ul>\n");
app.cache.put("modules/ap-overview/readme.html","<h1 id=\"ap-overview\">ap-overview</h1>\n<p>This module Presents an overview of the default collections.</p>\n<h2 id=\"usage\">Usage</h2>\n<p><strong>This module is working in conjunction with &quot;ap-custom-collections&quot;, &quot;ap-custom-collections-menu&quot;, &quot;ap-load&quot; and da-default-data.json in app folder</strong></p>\n<ul>\n<li>Add the following code to the presentation json file<pre><code>  &quot;ap-overview&quot;: {\n      &quot;id&quot;: &quot;ap-overview&quot;,\n      &quot;ignoreVeeva&quot;: true,\n      &quot;files&quot;: {\n          &quot;templates&quot;: [\n              &quot;modules/rainmaker_modules/ap-overview/ap-overview.html&quot;\n          ],\n          &quot;scripts&quot;: [\n              &quot;modules/rainmaker_modules/ap-overview/ap-overview.js&quot;\n          ],\n          &quot;styles&quot;: [\n              &quot;modules/rainmaker_modules/ap-overview/ap-overview.css&quot;\n          ]\n      }\n  }\n</code></pre></li>\n</ul>\n");
app.cache.put("modules/ap-reference-library/ap-reference-library.html","<div class=\"ReferenceLibrary\">\r\n  <h1>Reference Library</h1>\r\n  <div class=\"scroll\">\r\n    <ul></ul>\r\n  </div>\r\n</div>\r\n");
app.cache.put("modules/ap-reference-library/CHANGELOG.html","<h3 id=\"version-1-0-5-2018-10-10\">Version 1.0.5 2018-10-10</h3>\n<h3 id=\"andrii-romanyshyn\">Andrii Romanyshyn</h3>\n<ul>\n<li>Remove &quot;usereferenceslistnumeration&quot; property functionality</li>\n</ul>\n<h3 id=\"version-1-0-4-2018-09-27\">Version 1.0.4 2018-09-27</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n<h3 id=\"version-1-0-3-2018-07-16\">Version 1.0.3 2018-07-16</h3>\n<h3 id=\"oleksandr-melnyk\">Oleksandr Melnyk</h3>\n<ul>\n<li>fix css linter warnings</li>\n</ul>\n<h3 id=\"version-1-0-2-2018-06-04\">Version 1.0.2 2018-06-04</h3>\n<h3 id=\"oleksandr-tkachov\">Oleksandr Tkachov</h3>\n<ul>\n<li>Update code to use current ap-media-repository module functionality, add &quot;usereferenceslistnumeration&quot; property for rendering references numeration according to their</li>\n</ul>\n<h3 id=\"verson-1-0-1-2018-05-31\">Verson 1.0.1 2018-05-31</h3>\n<h3 id=\"oleksandr-tkachov\">Oleksandr Tkachov</h3>\n<ul>\n<li>Update code to use current ap-media-repository module functionality.</li>\n</ul>\n");
app.cache.put("modules/ap-slide-indicator/ap-slide-indicator.html","<div class=\"joystick\">\r\n    <div class=\"up\" ></div>\r\n    <div class=\"down\" ></div>\r\n    <div class=\"left\" ></div>\r\n    <div class=\"right\"></div>\r\n</div>");
app.cache.put("modules/ap-slide-indicator/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n");
app.cache.put("modules/ap-specific-product-characteristics/CHANGELOG.html","<h3 id=\"version-0-0-2-2018-09-27\">Version 0.0.2 2018-09-27</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>add possibility to open popup</li>\n</ul>\n<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n");
app.cache.put("modules/ap-toolbar/ap-toolbar.html","<div class=\"ap-toolbar\">\r\n  <div class=\"bar\">\r\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-overview\"></div>\r\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-custom-collections-menu\"></div>\r\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-video-library\"></div>\r\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-follow-up-mail\"></div>\r\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-media-library\"></div>\r\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-frequently-asked-questions\"></div>\r\n    <!-- <div class=\"button\" data-toolbar-state=\"minimized\" data-module-to-load=\"ap-specific-product-characteristics\" data-ispopup></div> -->\r\n    <div class=\"button\" data-toolbar-state=\"maximized\" data-module-to-load=\"ap-reference-library\"></div>\r\n    <div class=\"button notepad\"></div>\r\n    <div class=\"button\" data-module=\'ah-tutorial-controls\'></div>\r\n    <div class=\"button jumpToLastSlide\"></div>\r\n    <div class=\"button back\" data-module=\"ap-back-navigation\"></div>\r\n  </div>\r\n  <div class=\"content\" data-module-container>\r\n    <!-- occupied by currently loaded module -->\r\n    <div data-module=\"ap-overview\" hide></div>\r\n    <div data-module=\"ap-custom-collections-menu\" hide></div>\r\n    <div data-module=\"ap-custom-collections\" hide ></div>\r\n    <div data-module=\"ap-media-library\" hide> </div>\r\n    <div data-module=\"ap-reference-library\" hide> </div>\r\n    <div data-module=\"ap-video-library\" hide> </div>\r\n    <div data-module=\"ap-follow-up-mail\" hide allow-custom-text> </div>\r\n    <div data-module=\"ap-frequently-asked-questions\" hide> </div>\r\n    <!-- <div data-module=\"ap-specific-product-characteristics\"> </div> -->\r\n  </div>\r\n</div>\r\n");
app.cache.put("modules/ap-toolbar/CHANGELOG.html","<h3 id=\"version-1-0-3-2018-09-28\">Version 1.0.3 2018-09-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>add possibility to fast hide module icons for presentation.</li>\n</ul>\n<h3 id=\"version-1-0-2-2018-09-27\">Version 1.0.2 2018-09-27</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>add possibility to add pdfs to smpc or popup</li>\n</ul>\n<h3 id=\"version-1-0-1-2018-07-17\">Version 1.0.1 2018-07-17</h3>\n<h3 id=\"oleksandr-melnyk\">Oleksandr Melnyk</h3>\n<ul>\n<li>fix css linter error</li>\n</ul>\n");
app.cache.put("modules/ap-video-library/ap-video-library.html","<div class=\"VideoLibrary\">\r\n  <h1>Video Library</h1>\r\n  <div class=\"scroll\">\r\n    <ul></ul>\r\n  </div>\r\n</div>\r\n");
app.cache.put("modules/ap-video-library/CHANGELOG.html","<h3 id=\"version-1-0-3-2018-09-27\">Version 1.0.3 2018-09-27</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n<h3 id=\"version-1-0-2-2018-07-16\">Version 1.0.2 2018-07-16</h3>\n<h3 id=\"oleksandr-melnyk\">Oleksandr Melnyk</h3>\n<ul>\n<li>fix css linter errors</li>\n</ul>\n<h3 id=\"version-1-0-1-2018-05-31\">Version 1.0.1 2018-05-31</h3>\n<h3 id=\"oleksandr-tkachov\">Oleksandr Tkachov</h3>\n<ul>\n<li>Update code to use current ap-media-repository module functionality.</li>\n</ul>\n");
app.cache.put("modules/bhc-gpc-number/CHANGELOG.html","<h3 id=\"version-0-0-1-2018-06-28\">Version 0.0.1 2018-06-28</h3>\n<h3 id=\"dryzhuk-andrii\">Dryzhuk Andrii</h3>\n<ul>\n<li>change ag to window.ag in monitor usage function</li>\n</ul>\n");
app.cache.put("slides/folic_acid_page_01/folic_acid_page_01.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Folic_acid_page_01</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"folic_acid_page_01\" class=\"slide\">\r\n    	<header><a href=\"#elevit/folic_acid_page/folic_acid_page_00\"><div class=\"icon-folic-acid\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Folic acid is essential in the preconception period<br/> and throughout pregnancy<sup data-reference-id=\'ref_1\'></sup></h1>\r\n			<ul>\r\n				<li>Only around 50% of women planning a pregnancy take supplemental folate<sup data-reference-id=\'ref_6\'></sup></li>\r\n				<li>Folic acid is clinically proven to reduce the risk of neural tube defects like spina bifida<sup data-reference-id=\'ref_7\'></sup></li>\r\n				<li>As the neural tube closes by the fourth week of pregnancy (28 days after conception), the risk of neural tube defects is highest during the first weeks of pregnancy<sup data-reference-id=\'ref_16\'></sup></li>\r\n				<li><strong>Recommended Daily Intake (RDI) for folate: 600 micrograms per day</strong><sup data-reference-id=\'ref_1\'></sup></li>\r\n				<li><strong>Elevit contains 800 micrograms folic acid – meeting the Australian RDI for folic acid</strong><sup data-reference-id=\'ref_1\'></sup></li>\r\n			</ul>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/folic_acid_page_00/folic_acid_page_00.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Folic acid</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"folic_acid_page_00\" class=\"slide\">\r\n    	<header><a href=\"#elevit/start\"><div class=\"icon-folic-acid\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n			<div class=\"landing-row landing-2-items\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/folic_acid_page/folic_acid_page_01\">\r\n						<img src=\"./contents/folic-acid/Elevit-Folic-acid-page-C-01.png\"/>\r\n						PAGE 1\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/folic_acid_page/folic_acid_page_02\">\r\n						<img src=\"./contents/folic-acid/Elevit-Folic-acid-page-C-02.png\"/>\r\n						PAGE 2\r\n					</a>\r\n				</div>\r\n			</div>\r\n			<div class=\"landing-row landing-2-items\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/folic_acid_page/folic_acid_page_03\">\r\n						<img src=\"./contents/folic-acid/Elevit-Folic-acid-page-C-03.png\"/>\r\n						PAGE 3\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/folic_acid_page/folic_acid_page_04\">\r\n						<img src=\"./contents/folic-acid/Elevit-Folic-acid-page-C-04.png\"/>\r\n						PAGE 4\r\n					</a>\r\n				</div>\r\n			</div>\r\n\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/folic_acid_page_02/folic_acid_page_02.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Folic_acid_page_02</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"folic_acid_page_02\" class=\"slide\">\r\n    	<!-- <header><a href=\"#elevit/folic_acid_page\"><div class=\"icon-folic-acid\"></div></a></header> -->\r\n      <div class=\"hd-graph\">\r\n        <h4 class=\"h4\">CLINICAL DATA SHOWS THAT OPTIMAL TARGET RED BLOOD CELL FOLATE LEVELS CAN BE ACHIEVED</h4>\r\n        <h5 class=\"h5\">IN JUST 4 WEEKS OF SUPPLEMENTATION WITH ELEVIT AT 800 &#x338D; OF FOLIC ACID.<sup data-reference-id=\'ref_16,ref_17\'></sup></h5>\r\n      </div>\r\n  		<div class=\"container container-small\" >\r\n  			<div class=\"graph-z\">\r\n          <img src=\"./contents/folic-acid/folic-acid-table-1sm.png\" class=\"img-full zoom\" />\r\n          <div class=\"icon-zoom\"></div>\r\n        </div>\r\n  			<p class=\"text-ref\">‡This level has been shown to deliver maximal and optimal protection against NTDs.<sup data-reference-id=\'ref_19\'></sup></p>\r\n  		</div>\r\n\r\n      	<!-- <div class=\"bayerLogoWhite\"></div> -->\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\" ></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/folic_acid_page_03/folic_acid_page_03.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Folic_acid_page_03</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"folic_acid_page_03\" class=\"slide\">\r\n    	<header><a href=\"#elevit/folic_acid_page/folic_acid_page_00\"><div class=\"icon-folic-acid\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Folic acid levels in Elevit provide benefits<br/>beyond the first trimester</h1>\r\n			<ul>\r\n				<li>While the risk of neural tube defects is greatest during early pregnancy<sup data-reference-id=\'ref_16\'></sup>, the RDI for folic acid in pregnancy remains the same throughout the trimesters<sup data-reference-id=\'ref_1\'></sup></li>\r\n				<li>Demand for folic acid increases during pregnancy, especially during the third trimester<sup data-reference-id=\'ref_22\'></sup></li>\r\n				<li>Increased folic acid is needed for DNA synthesis related to the growth of the foetus, placenta and maternal tissues, as well as blood volume expansion<sup data-reference-id=\'ref_23\'></sup></li>\r\n			</ul>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>\r\n");
app.cache.put("slides/folic_acid_page_04/folic_acid_page_04.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Folic_acid_page_04</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"folic_acid_page_04\" class=\"slide\">\r\n    	<!-- <header><a href=\"#elevit/folic_acid_page\"><div class=\"icon-folic-acid\"></div></a></header> -->\r\n\r\n      <div class=\"hd-graph\">\r\n        <h4 class=\"h4\">INCREASED FOLATE BREAKDOWN DURING PREGNANCY.<sup data-reference-id=\'ref_22\'></sup></h4>\r\n      </div>\r\n\r\n  		<div class=\"container container-small\">\r\n        <div class=\"graph-z\">\r\n          <img src=\"./contents/folic-acid/folic-acid-table-2sm.png\" class=\"img-full zoom\" />\r\n          <div class=\"icon-zoom\"></div>\r\n        </div>        \r\n  			<div class=\"text-ref\"><p>§p-aminobenzoylglutamate (pABPG) is a breakdown product of folate metabolism. The rate of folate breakdown during pregnancy has been shown to peak during the third trimester. This accelerated breakdown suggests that there is an increased demand for folate (due to the demands of pregnancy).<sup data-reference-id=\'ref_22\'></sup></p>\r\n        </div>\r\n  		</div>\r\n\r\n      	<!-- <div class=\"bayerLogoWhite\"></div> -->\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/formulation_page_00/formulation_page_00.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Elevit Formulation</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"formulation_page_00\" class=\"slide\">\r\n    	<header><a href=\"#elevit/start\"><div class=\"icon-formulation\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n			<div class=\"landing-row landing-2-items\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/formulation_page/formulation_page_01\">\r\n						<img src=\"./contents/formulation/Elevit-Formulation-page-C-01-v3.png\"/>\r\n						PAGE 1\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/formulation_page/formulation_page_02\">\r\n						<img src=\"./contents/formulation/Elevit-Formulation-page-C-02-v2.png\"/>\r\n						PAGE 2\r\n					</a>\r\n				</div>\r\n			</div>\r\n			<div class=\"landing-row\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/formulation_page/formulation_page_03\">\r\n						<img src=\"./contents/formulation/Elevit-Formulation-page-C-03.png\"/>\r\n						PAGE 3\r\n					</a>\r\n				</div>\r\n			</div>\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/formulation_page_01/formulation_page_01.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Formulation_page_01</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"formulation_page_01\" class=\"slide\">\r\n    	<header><a href=\"#elevit/formulation_page/formulation_page_00\"><div class=\"icon-formulation\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n			<h1>Elevit is specially formulated with <span class=\"color-1\">19 vitamins</span>, <span class=\"color-2\">minerals</span> and<br/><span class=\"color-3\">trace elements</span> tailored for growth and development</h1>\r\n      <div class=\"contains d-n ani-3\">\r\n          <p><img src=\"./contents/icon-check.png\" class=\"\"/>Contains no gluten</p>\r\n          <p><img src=\"./contents/icon-check.png\" class=\"\"/>Contains no porcine or bovine derived ingredients</p>\r\n          <p><img src=\"./contents/icon-check.png\" class=\"\"/>Halal certified</p>\r\n      </div>\r\n      <div class=\"prod-packshot d-n ani-1\"><img src=\"./contents/formulation/Elevit_100_Tablets_3D_BUILD.png\" class=\"product-mainx\"/></div>\r\n      <div class=\"product-badge d-n ani-2\"><img src=\"./contents/formulation/tablet-daily.png\" class=\"product-badgex\"/></div>\r\n<!-- 			<div class=\"product-box\">\r\n				<div class=\"d-n ani-1\"><img src=\"./contents/formulation/Elevit_100_Tablets_3D_BUILD.png\" class=\"product-main\"/></div>\r\n				<div class=\"d-n ani-2\"><img src=\"./contents/formulation/tablet-daily.png\" class=\"product-badge\"/></div>\r\n			</div> -->\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/formulation_page_03/formulation_page_03.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Formulation_page_03</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"formulation_page_03\" class=\"slide\">\r\n    	<!-- <header><a href=\"#elevit/formulation_page\"><div class=\"icon-formulation\"></div></a></header> -->\r\n      <h1>Elevit is the only pregnancy multivitamin that<br/>meets the Australian RDI for folate, iron and iodine<sup data-reference-id=\'ref_1\'>1</sup>*</h1>\r\n  		<div class=\"container container-small\">\r\n\r\n        <div class=\"graph-z\">\r\n          <img src=\"./contents/formulation/table-Formulation-sm.png\" class=\"img-full zoom\" />\r\n          <div class=\"icon-zoom\"></div>\r\n        </div>\r\n\r\n			<p class=\"text-ref\">*As at November 2019</p>\r\n  		</div>\r\n\r\n      	<!-- <div class=\"bayerLogoWhite\"></div> -->\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/home_slide/home_slide.html","<!doctype html>\r\n<html>\r\n<head>\r\n    <title>home_slide</title>\r\n</head>\r\n<body>\r\n    <article id=\"home_slide\" class=\"slide\" data-ag-name=\"Home Slide\">\r\n\r\n        <h1 data-ag-editable=\"header\">Home Slide</h1>\r\n        <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n        <button data-popover=\'openPopup/bottomCenterCustom1/bottom\' data-popup=\"test_popup_1_slide\">TEST_POPUP_1_SLIDE</button>\r\n\r\n        <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n        <p data-ag-editable=\"paragraph\">Introduction to the presentation</p>\r\n        <p class=\"set-custom-popover\" data-popover=\"Home/slide\"></p>\r\n        <div class=\"bayerLogo\"></div>\r\n        <div class=\"brandLogo\"></div>\r\n    </article>\r\n</body>\r\n</html>\r\n");
app.cache.put("slides/formulation_page_02/formulation_page_02.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Formulation_page_02</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"formulation_page_02\" class=\"slide\">\r\n    	<header><a href=\"#elevit/formulation_page/formulation_page_00\"><div class=\"icon-formulation\"></div></a></header>\r\n\r\n  		<div class=\"container container-small\">\r\n			<!-- <h1 class=\"color-2\">TBC – THIS TABLE WILL BE TURNED INTO A GRAPH</h1> -->\r\n\r\n			<div class=\"table-vit\"><img src=\"./contents/formulation/Table-Vitamins.png\" class=\"img-full zoom\"/>\r\n        <div class=\"icon-zoom\"></div>\r\n      </div>\r\n      <div class=\"table-min\"><img src=\"./contents/formulation/Table-Minerals.png\" class=\"img-full zoom\"/>\r\n        <div class=\"icon-zoom\"></div>\r\n      </div>\r\n\r\n  		</div>\r\n      <div class=\"text-ref\">\r\n        <p><sup>‡‡</sup>Elevit contains 183 mg ferrous fumarate which corresponds to 60 mg elemental iron.</p>\r\n        <p><sup>§§</sup>No RDI for pregnancy, therefore adequate intake (AI) recommendation shown.</p>\r\n      </div>\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/gpc_slide/gpc_slide.html","<article id=\"gpc_slide\" class=\"slide\">\r\n  <h1>Science For A Better Life</h1>\r\n  <div class=\"bayerLogo huge\"></div>\r\n  <div data-module=\"bhc-gpc-number\" class=\"gpc-number\">GPC:</div>\r\n  <p class=\"set-custom-popover\" data-popover=\"gpc/slide\"></p>\r\n</article>\r\n");
app.cache.put("slides/iodine_page_00/iodine_page_00.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Iodine</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"iodine_page_00\" class=\"slide\">\r\n    	<header><a href=\"#elevit/start\"><div class=\"icon-iodine\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n			<div class=\"landing-row landing-2-items\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/iodine_page/iodine_page_01\">\r\n						<img src=\"./contents/iodine/Elevit-Iodine-page-C-01.png\"/>\r\n						PAGE 1\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/iodine_page/iodine_page_02\">\r\n						<img src=\"./contents/iodine/Elevit-Iodine-page-C-02.png\"/>\r\n						PAGE 2\r\n					</a>\r\n				</div>\r\n			</div>\r\n			<div class=\"landing-row\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/iodine_page/iodine_page_03\">\r\n						<img src=\"./contents/iodine/Elevit-Iodine-page-C-03.png\"/>\r\n						PAGE 3\r\n					</a>\r\n				</div>\r\n			</div>\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/iodine_page_01/iodine_page_01.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Iodine_page_01</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"iodine_page_01\" class=\"slide\">\r\n    	<header><a href=\"#elevit/iodine_page/iodine_page_00\"><div class=\"icon-iodine\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>With you during foetal brain development</h1>\r\n			<ul>\r\n				<li>Iodine deficiency is the most common <strong>preventable</strong> cause of intellectual impairment<sup data-reference-id=\'ref_8,ref_9\'></sup></li>\r\n				<li>Mandatory iodisation of salt used in commercial bread-baking has not corrected iodine deficiency in pregnant women in Australia<sup data-reference-id=\'ref_10\'></sup></li>\r\n				<li>Post fortification studies show that pregnant women using pregnancy supplements containing iodine are sufficient<sup data-reference-id=\'ref_11\'></sup></li>\r\n				<li>Recommended Daily Intake (RDI) for iodine: 220 μg/day<sup data-reference-id=\'ref_1\'></sup></li>\r\n				<li>Elevit contains 220 μg of iodine – meeting the Australian RDI for iodine<sup data-reference-id=\'ref_1\'></sup></li>\r\n			</ul>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>\r\n");
app.cache.put("slides/iodine_page_02/iodine_page_02.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Iodine_page_02</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"iodine_page_02\" class=\"slide\">\r\n    	<header><a href=\"#elevit/iodine_page/iodine_page_00\"><div class=\"icon-iodine\"></div></a></header>\r\n\r\n  		<div class=\"container container-small\">\r\n			<h1>Studies of iodine status of pregnant women<br/>consistently suggest iodine intake is inadequate</h1>\r\n			<div class=\"\"><a href=\"#\"><img src=\"./contents/iodine/graph-Iodine-01-001.png\" class=\"img-full\" data-reference-id=\'ref_27\' /></a></div>\r\n			<p class=\"text-ref\">EAR = estimated average requirement</p>\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/iron_page_00/iron_page_00.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Iron</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"iron_page_00\" class=\"slide\">\r\n    	<header><a href=\"#elevit/start\"><div class=\"icon-iron\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n			<div class=\"landing-row\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/iron_page/iron_page_01\">\r\n						<img src=\"./contents/iron/Elevit-Iron-page-C-01.png\"/>\r\n						PAGE 1\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/iron_page/iron_page_02\">\r\n						<img src=\"./contents/iron/Elevit-Iron-page-C-02.png\"/>\r\n						PAGE 2\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/iron_page/iron_page_03\">\r\n						<img src=\"./contents/iron/Elevit-Iron-page-C-03.png\"/>\r\n						PAGE 3\r\n					</a>\r\n				</div>\r\n			</div>\r\n			<div class=\"landing-row landing-2-items\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/iron_page/iron_page_04\">\r\n						<img src=\"./contents/iron/Elevit-Iron-page-C-04.png\"/>\r\n						PAGE 4\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/iron_page/iron_page_05\">\r\n						<img src=\"./contents/iron/Elevit-Iron-page-C-05.png\"/>\r\n						PAGE 5\r\n					</a>\r\n				</div>\r\n			</div>\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/iodine_page_03/iodine_page_03.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Iodine_page_03</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"iodine_page_03\" class=\"slide\">\r\n    	<!-- <header><a href=\"#elevit/iodine_page\"><div class=\"icon-iodine\"></div></a></header> -->\r\n\r\n\r\n\r\n  		<div class=\"container container-small\">\r\n			<h1>The developing foetus is solely dependent on the<br/>maternal supply of thyroid hormone and iodine<sup data-reference-id=\'ref_28\'></sup></h1>\r\n\r\n      <div class=\"hd-graph\">\r\n        <h4 class=\"h4\">MAJOR DEVELOPMENT PERIOD OF THE CENTRAL NERVOUS SYSTEM.<sup data-reference-id=\'ref_28\'></sup></h4>\r\n      </div>\r\n        <div class=\"graph-z\">\r\n          <img src=\"./contents/iodine/iodine-table-2sm.png\" class=\"img-full zoom\" />\r\n          <div class=\"icon-zoom\"></div>\r\n        </div>\r\n\r\n			<p class=\"text-ref\">Adapted from Gallego et al. 2010.<sup data-reference-id=\'ref_28\'></sup></p>\r\n  		</div>\r\n\r\n      	<!-- <div class=\"bayerLogoWhite\"></div> -->\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/iron_page_02/iron_page_02.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Iron_page_02</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"iron_page_02\" class=\"slide\">\r\n    	<header><a href=\"#elevit/iron_page/iron_page_00\"><div class=\"icon-iron\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>The iron in Elevit benefits mother and baby<sup data-reference-id=\'ref_34\'></sup></h1>\r\n			<ul>\r\n				<li>Iron deficiency anaemia in pregnancy has been linked to pre-term delivery, low birth weight and reduction of iron stores in both mother and baby<sup data-reference-id=\'ref_33,ref_35\'></sup></li>\r\n				<li>Iron supplementation up to 28 weeks gestation had a strong effect on birth weight, even in the absence of maternal iron deficiency anaemia<sup data-reference-id=\'ref_34\'></sup></li>\r\n				<li>79% of initially iron-replete women receiving 30 mg iron supplementation had depleted or absent iron stores by 28 weeks gestation<sup data-reference-id=\'ref_34\'></sup></li>\r\n			</ul>\r\n			<div class=\"quote-box\">\r\n				<div class=\"quote-desc\">…study provides evidence that infant may benefit substantially<br/>from maternal iron supplementation beginning early in gestation</div>\r\n				<div class=\"quote-name\">Cogswell ME, <em>et al.</em> 2003<sup data-reference-id=\'ref_34\'></sup></div>\r\n			</div>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>\r\n\r\n");
app.cache.put("slides/iron_page_01/iron_page_01.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Iron_page_01</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"iron_page_01\" class=\"slide\">\r\n    	<header><a href=\"#elevit/iron_page/iron_page_00\"><div class=\"icon-iron\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Elevit assists in meeting maternal iron needs</h1>\r\n			<ul>\r\n				<li>Iron demand increases during pregnancy due to rise in maternal blood volume, growth of the foetus and placenta as well as blood loss at delivery<sup data-reference-id=\'ref_12\'></sup></li>\r\n				<li>As iron stores build slowly (usually 3–6 months)13, early supplementation will help ensure iron stores are there when needed</li>\r\n				<li>The World Health Organization recommends 30–60 mg of elemental iron during pregnancy<sup data-reference-id=\'ref_14\'></sup></li>\r\n				<li>Recommended Daily Intake (RDI) for iron: 27 mg/day<sup data-reference-id=\'ref_1\'></sup></li>\r\n				<li>Elevit contains 60 mg of elemental iron – meeting both WHO guidelines and Australian RDI for iron<sup data-reference-id=\'ref_1,ref_14\'></sup></li>\r\n			</ul>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>\r\n\r\n");
app.cache.put("slides/iron_page_04/iron_page_04.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Iron_page_04</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"iron_page_04\" class=\"slide\">\r\n    	<header><a href=\"#elevit/iron_page/iron_page_00\"><div class=\"icon-iron\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Iron prophylaxis: reaching optimal levels<br>and avoiding side effects</h1>\r\n			<ul>\r\n				<li>Supplemental iron in high doses (over 100 mg) is associated with gastrointestinal side effects such as constipation, nausea, and diarrhoea<sup data-reference-id=\'ref_36,ref_37\'></sup></li>\r\n				<li>A study showed 20 mg ferrous iron to be inadequate as prophylaxis against iron deficiency, with a significantly higher prevalence of iron deficiency (50%) and iron deficient anaemia (10%) compared to groups treated with 40 mg, 60 mg or 80 mg iron<sup data-reference-id=\'ref_37\'></sup></li>\r\n				<li>The original formulation of Elevit, which also contained 60 mg iron, was included in a large randomised controlled trial and showed no significant difference in constipation or diarrhoea compared to placebo<sup data-reference-id=\'ref_38\'></sup></li>\r\n			</ul>\r\n			<div class=\"quote-box\">\r\n				<div class=\"quote-desc\">…we could not demonstrate any specific side effects of<br>iron prophylaxis, not even in the 80 mg group</div>\r\n				<div class=\"quote-name\">Millman N. <em>et al.</em> 2005<sup data-reference-id=\'ref_37\'></sup></div>\r\n			</div>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/iron_page_03/iron_page_03.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Iron_page_03</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"iron_page_03\" class=\"slide\">\r\n    	<!-- <header><a href=\"#elevit/iron_page\"><div class=\"icon-iron\"></div></a></header> -->\r\n      <div class=\"hd-graph\">\r\n        <h4 class=\"h4\">BIRTH OUTCOMES FOR INITIALLY IRON-REPLETE, NON-ANAEMIC PREGNANT WOMEN.<sup data-reference-id=\'ref_34\'></sup></h4>\r\n      </div>\r\n  		<div class=\"container container-small\">\r\n        <div class=\"graph-z\">\r\n          <img src=\"./contents/iron/iron-table-1sm-v2.png\" class=\"img-full zoom\" />\r\n          <div class=\"icon-zoom\"></div>\r\n        </div>        \r\n\r\n			<p class=\"text-ref\">*Iron supplementation from enrolment up to 28 weeks gestation.</p>\r\n  		</div>\r\n\r\n      	<!-- <div class=\"bayerLogoWhite\"></div> -->\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/iron_page_05/iron_page_05.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Iron_page_05</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"iron_page_05\" class=\"slide\">\r\n    	<!-- <header><a href=\"#elevit/iron_page\"><div class=\"icon-iron\"></div></a></header> -->\r\n      <div class=\"hd-graph\">\r\n        <h4 class=\"h4\">IRON LEVELS** IN WOMEN THROUGHOUT PREGNANCY AND<br>POSTPARTUM AT DIFFERENT DOSES OF IRON SUPPLEMENTATION.<sup data-reference-id=\'ref_37\'></sup></h4>\r\n      </div>\r\n  		<div class=\"container container-small\">\r\n        <div class=\"graph-z\">\r\n          <img src=\"./contents/iron/iron-table-2sm.png\" class=\"img-full zoom\" />\r\n          <div class=\"icon-zoom\"></div>\r\n        </div>         \r\n			  <p class=\"text-ref\">**Measured as serum ferritin.</p>\r\n  		</div>\r\n\r\n      	<!-- <div class=\"bayerLogoWhite\"></div> -->\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/nutrition_page_00/nutrition_page_00.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Nutrition</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"nutrition_page_00\" class=\"slide\">\r\n    	<header><a href=\"#elevit/start\"><div class=\"icon-nutrition\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n			<div class=\"landing-row\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/nutrition_page/nutrition_page_01\">\r\n						<img src=\"./contents/nutrition/Elevit-Nutrition-page-C-01.png\"/>\r\n						PAGE 1\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/nutrition_page/nutrition_page_02\">\r\n						<img src=\"./contents/nutrition/Elevit-Nutrition-page-C-02.png\"/>\r\n						PAGE 2\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/nutrition_page/nutrition_page_03\">\r\n						<img src=\"./contents/nutrition/Elevit-Nutrition-page-C-03.png\"/>\r\n						PAGE 3\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/nutrition_page/nutrition_page_04\">\r\n						<img src=\"./contents/nutrition/Elevit-Nutrition-page-C-04.png\"/>\r\n						PAGE 4\r\n					</a>\r\n				</div>\r\n			</div>\r\n			<div class=\"landing-row\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/nutrition_page/nutrition_page_05\">\r\n						<img src=\"./contents/nutrition/Elevit-Nutrition-page-C-05.png\"/>\r\n						PAGE 5\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/nutrition_page/nutrition_page_06\">\r\n						<img src=\"./contents/nutrition/Elevit-Nutrition-page-C-06.png\"/>\r\n						PAGE 6\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/nutrition_page/nutrition_page_07\">\r\n						<img src=\"./contents/nutrition/Elevit-Nutrition-page-C-07.png\"/>\r\n						PAGE 7\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/nutrition_page/nutrition_page_08\">\r\n						<img src=\"./contents/nutrition/Elevit-Nutrition-page-C-08.png\"/>\r\n						PAGE 8\r\n					</a>\r\n				</div>\r\n			</div>\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/nutrition_page_01/nutrition_page_01.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>nutrition_page_01</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"nutrition_page_01\" class=\"slide\">\r\n      <header><a href=\"#elevit/nutrition_page/nutrition_page_00\"><div class=\"icon-nutrition\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>During pregnancy, the recommended dietary intake of certain nutrients increases by up to 50%<sup data-reference-id=\'ref_1\'></sup>.</h1>\r\n			<div class=\"graph-nutrition-01\">\r\n        <div class=\"g-s02 d-n ani-4\"><img src=\"./contents/nutrition/graph-nutrition-p1-03-breas-sm.png\" alt=\"\" class=\"img-no-border\"></div>\r\n        <div class=\"g-s01 d-n ani-3\"><img src=\"./contents/nutrition/graph-nutrition-p1-03-pregnant-sm.png\" alt=\"\" class=\"img-no-border\"></div>\r\n        <div class=\"g-s00 d-n ani-2\"><img src=\"./contents/nutrition/graph-nutrition-p1-02-dotted-sm.png\" alt=\"\" class=\"img-no-border\"></div>\r\n        <img src=\"./contents/nutrition/graph-nutrition-p1-01sm.png\" class=\"g-bg-s00 img-full img-no-border\"/>\r\n      </div>\r\n			<p class=\"text-ref\">The use of multinutrients during preconception and pregnancy has been positively linked with healthy pregnancy outcomes.</p>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n        \r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/nutrition_page_02/nutrition_page_02.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>nutrition_page_02</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"nutrition_page_02\" class=\"slide\">\r\n      <header><a href=\"#elevit/nutrition_page/nutrition_page_00\"><div class=\"icon-nutrition\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Many Australian women may not be getting all the<br/>nutrients they need during preconception & pregnancy<sup data-reference-id=\'ref_4\'></sup>.</h1>\r\n			<div class=\"graph\">\r\n        <div class=\"g-s01 d-n ani-2\"><img src=\"./contents/nutrition/Woman-96new.png\" alt=\"\" class=\"\"></div>\r\n        <img src=\"./contents/nutrition/Woman-STK.png\" class=\"img-full img-no-border\"/>\r\n      </div>\r\n			<p class=\"text-big\"><span>96<sup>%</sup></span> of women of reproductive age report inadequate intake of fruit and vegetables</p>\r\n			<p class=\"text-ref\">Adapted from Malek L. <em>et al.</em> 2015<sup data-reference-id=\'ref_4\'></sup></p>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/nutrition_page_03/nutrition_page_03.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>nutrition_page_03</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"nutrition_page_03\" class=\"slide\">\r\n      <header><a href=\"#elevit/nutrition_page/nutrition_page_00\"><div class=\"icon-nutrition\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Many Australian women may not be getting all the<br/>nutrients they need during preconception & pregnancy<sup data-reference-id=\'ref_4\'></sup>.</h1>\r\n      <div class=\"graph\">\r\n        <div class=\"g-s01 d-n ani-2\"><img src=\"./contents/nutrition/Woman-63.png\" alt=\"\" class=\"\"></div>\r\n        <img src=\"./contents/nutrition/Woman-STK.png\" class=\"img-full img-no-border\"/>\r\n      </div>\r\n			<p class=\"text-big\"><span>63<sup>%</sup></span> of women made changes to their diet for pregnancy</p>\r\n			<p class=\"text-ref\">Adapted from Malek L. <em>et al.</em> 2015<sup data-reference-id=\'ref_4\'></sup></p>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/nutrition_page_04/nutrition_page_04.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>nutrition_page_04</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"nutrition_page_04\" class=\"slide\">\r\n      <header><a href=\"#elevit/nutrition_page/nutrition_page_00\"><div class=\"icon-nutrition\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Many Australian women may not be getting all the<br/>nutrients they need during preconception & pregnancy<sup data-reference-id=\'ref_4\'></sup>.</h1>\r\n      <div class=\"graph\">\r\n        <div class=\"g-s01 d-n ani-2\"><img src=\"./contents/nutrition/Woman-61.png\" alt=\"\" class=\"\"></div>\r\n        <img src=\"./contents/nutrition/Woman-STK.png\" class=\"img-full img-no-border\"/>\r\n      </div>\r\n			<p class=\"text-big\"><span>61<sup>%</sup></span> of women believed their diet was healthy during pregnancy</p>\r\n			<p class=\"text-ref\">Adapted from Malek L. <em>et al.</em> 2015<sup data-reference-id=\'ref_4\'></sup></p>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/nutrition_page_05/nutrition_page_05.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>nutrition_page_05</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"nutrition_page_05\" class=\"slide\">\r\n      <header><a href=\"#elevit/nutrition_page/nutrition_page_00\"><div class=\"icon-nutrition\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Many Australian women may not be getting all the<br/>nutrients they need during preconception & pregnancy<sup data-reference-id=\'ref_4\'></sup>.</h1>\r\n      <div class=\"graph\">\r\n        <img src=\"./contents/nutrition/Woman-STK.png\" class=\"img-full img-no-border\"/>\r\n      </div>			\r\n			<p class=\"text-big\"><span>NONE</span> of the women reported meeting the recommendations for all 5 food groups</p>\r\n			<p class=\"text-ref\">Adapted from Malek L. <em>et al.</em> 2015<sup data-reference-id=\'ref_4\'></sup></p>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/nutrition_page_06/nutrition_page_06.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>nutrition_page_06</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"nutrition_page_06\" class=\"slide\">\r\n      <header><a href=\"#elevit/nutrition_page/nutrition_page_00\"><div class=\"icon-nutrition\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Meeting the increased nutritional needs of pregnancy<br/>can be difficult with diet alone</h1>\r\n			<h2><span class=\"image-box color-2\">What a woman would need to <span class=\"color-4\">consume</span> daily to meet the RDI for <span class=\"color-4\">folate</span><sup data-reference-id=\'ref_15\'></sup></span></h2>\r\n			<h2 class=\"folate\">Folate – RDI <span class=\"color-2\">600</span> micrograms per day</h2>\r\n			<div class=\"landing-row circle-row\">\r\n				<div class=\"landing-col\">\r\n					<img src=\"./contents/nutrition/circle-6-1n.png\" class=\"img-no-border\"/>\r\n					3 cups of <br/>cooked spinach\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<img src=\"./contents/nutrition/circle-6-2n.png\" class=\"img-no-border\"/>\r\n					15 slices of <br/>bread\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<img src=\"./contents/nutrition/circle-6-3n.png\" class=\"img-no-border\"/>\r\n					150g beans\r\n				</div>\r\n			</div>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/nutrition_page_08/nutrition_page_08.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>nutrition_page_08</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"nutrition_page_08\" class=\"slide\">\r\n      <header><a href=\"#elevit/nutrition_page/nutrition_page_00\"><div class=\"icon-nutrition\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Meeting the increased nutritional needs of pregnancy<br/>can be difficult with diet alone</h1>\r\n			<h2><span class=\"image-box color-2\">What a woman would need to <span class=\"color-4\">consume</span> daily to meet the RDI for <span class=\"color-4\">iodine</span><sup data-reference-id=\'ref_15\'></sup></span></h2>\r\n			<h2 class=\"folate\">Iodine – RDI <span class=\"color-2\">220</span> micrograms per day</h2>\r\n			<div class=\"landing-row circle-row\">\r\n				<div class=\"landing-col\">\r\n					<img src=\"./contents/nutrition/circle-8-1n.png\" class=\"img-no-border\"/>\r\n					6 eggs\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<img src=\"./contents/nutrition/circle-8-2n.png\" class=\"img-no-border\"/>\r\n					31 tins of <br/>tuna\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<img src=\"./contents/nutrition/circle-6-2n.png\" class=\"img-no-border\"/>\r\n					15 slices of <br/>bread\r\n				</div>\r\n			</div>\r\n		</div>\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/nutrition_page_07/nutrition_page_07.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>nutrition_page_07</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"nutrition_page_07\" class=\"slide\">\r\n      <header><a href=\"#elevit/nutrition_page/nutrition_page_00\"><div class=\"icon-nutrition\"></div></a></header>\r\n\r\n		<div class=\"container\">\r\n			<h1>Meeting the increased nutritional needs of pregnancy<br/>can be difficult with diet alone</h1>\r\n			<h2><span class=\"image-box color-2\">What a woman would need to <span class=\"color-4\">consume</span> daily to meet the RDI for <span class=\"color-4\">iron</span><sup data-reference-id=\'ref_15\'></sup></span></h2>\r\n			<h2 class=\"folate\">Iron – RDI <span class=\"color-2\">27</span> mg per day</h2>\r\n			<div class=\"landing-row circle-row\">\r\n				<div class=\"landing-col\">\r\n					<img src=\"./contents/nutrition/circle-6-1n.png\" class=\"img-no-border\"/>\r\n					700 g cooked <br/>spinach\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<img src=\"./contents/nutrition/circle-7-2n.png\" class=\"img-no-border\"/>\r\n					900 g beef <br/>steak\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<img src=\"./contents/nutrition/circle-7-3n.png\" class=\"img-no-border\"/>\r\n					4 cans of <br/>chickpeas\r\n				</div>\r\n			</div>\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/pi_popup_slide/pi_popup_slide.html","<article id=\"pi_popup_slide\" class=\"slide\">\r\n\r\n  <h1 data-ag-editable=\"header\">Headline PI Popup Slide</h1>\r\n\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n\r\n</article>\r\n");
app.cache.put("slides/resources_page_01/resources_page_01.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Resources</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"resources_page_01\" class=\"slide\">\r\n    	<header><a href=\"#elevit/\"><div class=\"icon-resources\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n			<div class=\"landing-row landing-2-items\">\r\n				<div class=\"landing-col a-1\">\r\n					<a href=\"#\" class=\"popup-pdf\" data-file=\"./shared/references/Nutrition-Pregnancy-Brochure.pdf\">\r\n						<img src=\"./contents/resources/Nutrition-Pregnancy-Brochure-1.png\"/>\r\n						Nutrition & Pregnancy<br/>Booklet\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#\" class=\"popup-pdf\" data-file=\"./shared/references/Menevit-leave-behind.pdf\">\r\n						<img src=\"./contents/resources/Menevit-leave-behind-1.png\"/>\r\n						Menevit Summary\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#\" class=\"popup-pdf\" data-file=\"./shared/references/Elevit-Breastfeeding-leave-behind.pdf\">\r\n						<img src=\"./contents/resources/Elevit-Breastfeeding-leave-behind-1.png\"/>\r\n						Breastfeeding Summary\r\n					</a>\r\n				</div>\r\n			</div>\r\n			<div class=\"landing-row landing-2-items\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#\" class=\"popup-pdf\" data-file=\"./shared/references/Elevit-Leave-Behind.pdf\">\r\n						<img src=\"./contents/resources/Elevit-Leave-Behind-1.png\"/>\r\n						Elevit Summary \r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#\" class=\"popup-pdf\" data-file=\"./shared/references/Elevit-HCP-Range-Brochure.pdf\">\r\n						<img src=\"./contents/resources/Elevit-HCP-Range-Brochure-1.png\"/>\r\n						Elevit Range Brochure\r\n					</a>\r\n				</div>\r\n			</div>\r\n\r\n		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/summary_slide/summary_slide.html","<article id=\"summary_slide\" class=\"slide\" data-ag-name=\"Summary Slide\">\r\n\r\n  <h1 data-ag-editable=\"header\">Summary</h1>\r\n  <button data-popover=\'openPopup/bottomCenterCustom1/bottom\' data-popup=\"test_popup_1_slide\">TEST_POPUP_1_SLIDE</button>\r\n  <ul>\r\n    <li data-ag-editable=\"list-entry-1\">Donec in sagittis erat.</li>\r\n    <li data-ag-editable=\"list-entry-2\">Duis blandit tincidunt hendrerit.</li>\r\n    <li data-ag-editable=\"list-entry-3\">Phasellus sed sem lacus. Vestibulum ante<br/>ipsum primis in faucibus orci luctus et ultrices.</li>\r\n  </ul>\r\n  <p class=\"set-custom-popover\" data-popover=\"summary/slide\"></p>\r\n  <div class=\"bayerLogo\"></div>\r\n  <div class=\"brandLogo\"></div>\r\n</article>\r\n");
app.cache.put("slides/start_slide/start_slide.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Elevit - Homepage</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"start_slide\" class=\"slide homepage\">\r\n    	<div class=\"container\">\r\n        \r\n    		<div class=\"contain-inside d-n ani-3\">\r\n				<div class=\"landing-row\">\r\n					<div class=\"landing-col\">\r\n						<a href=\"#elevit/nutrition_page/nutrition_page_01\">\r\n							<img src=\"./contents/n-icon-home-nutrition.png\"/>\r\n							Nutrition\r\n						</a>\r\n					</div>\r\n					<div class=\"landing-col\">\r\n						<a href=\"#elevit/folic_acid_page/folic_acid_page_01\">\r\n							<img src=\"./contents/n-icon-home-folic-acid.png\"/>\r\n							Folic Acid\r\n						</a>\r\n					</div>\r\n					<div class=\"landing-col\">\r\n						<a href=\"#elevit/iron_page/iron_page_01\">\r\n							<img src=\"./contents/n-icon-home-iron.png\"/>\r\n							Iron\r\n						</a>\r\n					</div>\r\n					<div class=\"landing-col\">\r\n						<a href=\"#elevit/iodine_page/iodine_page_01\">\r\n							<img src=\"./contents/n-icon-home-iodine.png\"/>\r\n							Iodine\r\n						</a>\r\n					</div>\r\n				</div>\r\n				<div class=\"landing-row\">\r\n					<div class=\"landing-col\">\r\n						<a href=\"#elevit/formulation_page/formulation_page_01\">\r\n							<img src=\"./contents/n-icon-home-formulation.png\"/>\r\n							Elevit formulation\r\n						</a>\r\n					</div>\r\n					<div class=\"landing-col\">\r\n						<a href=\"#elevit/tablet_comparison_page/tablet_comparison_page_01\">\r\n							<img src=\"./contents/n-icon-home-tablet-comparison.png\"/>\r\n							Tablet comparison\r\n						</a>\r\n					</div>\r\n					<div class=\"landing-col\">\r\n						<a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_01\">\r\n							<img src=\"./contents/n-icon-home-elevit-range.png\"/>\r\n							The Elevit Range\r\n						</a>\r\n					</div>\r\n					<div class=\"landing-col\">\r\n						<a href=\"#elevit/resources_page/resources_page_01\">\r\n							<img src=\"./contents/n-icon-home-resources.png\"/>\r\n							Resources\r\n						</a>\r\n					</div>\r\n				</div>				\r\n    		</div>        \r\n\r\n    		<div class=\"slogan d-n ani-4\"><h2>The ONLY pregnancy multivitamin to meet the RDI for folic acid, iron & iodine*</h2></div>\r\n    		<div class=\"ref\">*Compared to leading Australian pregnancy multivitamins at January 2020.</div>\r\n    		<div class=\"la-number\"><p>L.AU.MKTG.02.2020.04390</p></div>\r\n    		<div class=\"prod-packshot d-n ani-2\"><img src=\"./contents/Elevit-packshot-home-new.png\" /></div>\r\n        	<div class=\"brandLogo-elevit\"></div>\r\n        	<div class=\"bayerLogo\"></div>\r\n        </div>\r\n    </article>\r\n  </body>\r\n</html>\r\n");
app.cache.put("slides/tablet_comparison_page_01/tablet_comparison_page_01.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>Tablet Comparison</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"tablet_comparison_page_01\" class=\"slide\">\r\n    	<header><a href=\"#elevit/start\"><div class=\"icon-tablet-comparison\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n			<h1>PREGNANCY MULTIVITAMINS<sup>†</sup></h1>\r\n			<h2>Daily Dosage and Size Comparison</h2>\r\n      <div class=\"image-box\">\r\n        <div class=\"graph-z\">\r\n          <img src=\"./contents/tablet-comparison/Tablet-comparison-updated.png\" class=\"img-full zoom\" />\r\n          <div class=\"icon-zoom\"></div>\r\n        </div>\r\n      </div>\r\n			<!-- <div class=\"image-box d-n ani-1\"><img src=\"./contents/tablet-comparison/Tablet-comparison-updated.png\"/></div> -->\r\n			<p class=\"text-refx text-center d-nx ani-2x\">\r\n				The tablets / capsules shown above are representative of the actual tablet /capsule size.<sup>†</sup><br/>\r\n				Based on Leading Australian Pregnancy Multivitamins as at November 2019.\r\n			</p>\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/test_popup_1_slide/test_popup_1_slide.html","<article id=\"test_popup_1_slide\" class=\"slide\">\r\n\r\n  <h1 data-ag-editable=\"header\">Headline Popup Slide 1</h1>\r\n  <button  data-popover=\'openPopup/bottomCenterCustom1/bottom\' data-popup=\"test_popup_2_slide\">TEST_POPUP_2_SLIDE</button>\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline<sup data-reference-id=\'wall_r_2001,esccap_2011,doe_j_01_2005,doe_j_04_2007\'></sup></h2>\r\n\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n\r\n</article>\r\n");
app.cache.put("slides/test_popup_2_slide/test_popup_2_slide.html","<article id=\"test_popup_2_slide\" class=\"slide\">\r\n\r\n  <h1 data-ag-editable=\"header\">Headline Popup Slide 2</h1>\r\n  <button  data-popover=\'openPopup/bottomCenterCustom1/bottom\' data-popup=\"test_popup_3_slide\">TEST_POPUP_3_SLIDE</button>\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n\r\n</article>\r\n");
app.cache.put("slides/test_popup_3_slide/test_popup_3_slide.html","<article id=\"test_popup_3_slide\" class=\"slide\">\r\n\r\n  <h1 data-ag-editable=\"header\">Headline Popup Slide 3</h1>\r\n\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n\r\n</article>\r\n");
app.cache.put("slides/test_slide_1/test_slide_1.html","<!doctype html>\r\n<html>\r\n<head>\r\n    <title>test_slide_1</title>\r\n</head>\r\n<body>\r\n<article id=\"test_slide_1\" class=\"slide\" data-ag-name=\"Test Slide 1\">\r\n\r\n    <h1 data-ag-editable=\"header\">Headline Slide 1</h1>\r\n\r\n    <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n\r\n    <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n    <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n    <ul>\r\n        <li data-ag-editable=\"list-entry-1\">Donec in sagittis erat.</li>\r\n        <li data-ag-editable=\"list-entry-2\">Duis blandit tincidunt hendrerit.</li>\r\n        <li data-ag-editable=\"list-entry-3\">Phasellus<sup data-reference-id=\'wall_r_2001,esccap_2011,doe_j_01_2005,doe_j_04_2007,doe_j_05_2007,doe_j_06_2007,doe_j_07_2007\'></sup> sed sem lacus. Vestibulum ante<br />ipsum primis in faucibus orci luctus et<br />ultrices.</li>\r\n    </ul>\r\n    <p class=\"set-custom-popover\" data-popover=\"Slide1/slide\"></p>\r\n    <div class=\"bayerLogo\"></div>\r\n    <div class=\"brandLogo\"></div>\r\n</article>\r\n</body>\r\n</html>\r\n");
app.cache.put("slides/test_slide_3/test_slide_3.html","<article id=\"test_slide_3\" class=\"slide\" data-ag-name=\"Test Slide 3\">\r\n  <h1 data-ag-editable=\"header\">Headline Slide 3</h1>\r\n\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">This slide demonstrates how to add custom email attachment</p>\r\n  <textarea class=\"custom-attachment-text\">\r\nThis text will be attached.<br />\r\nIt can contain <b>HTML</b> as supported in emails.<br /><br />\r\n<table>\r\n    <tr>\r\n        <th>Product</th>\r\n        <th>Recommended Dilution (%)</th>\r\n        <th>Consumption/Year</th>\r\n        <th>Price ($/lt)</th>\r\n    </tr>\r\n    <tr>\r\n        <td>Virkon S</td>\r\n        <td>1.00</td>\r\n        <td>30</td>\r\n        <td>20.00</td>\r\n    </tr>\r\n</table>\r\n  </textarea>\r\n  <div>\r\n      <button class=\"pure-button custom-attachment-button\" data-ag-editable=\"button\">Add attachment</button>\r\n  </div>\r\n  <p class=\"set-custom-popover\" data-popover=\"Slide3/slide\"></p>\r\n  <div class=\"bayerLogo\"></div>\r\n  <div class=\"brandLogo\"></div>\r\n</article>\r\n");
app.cache.put("slides/test_slide_2/test_slide_2.html","<article id=\"test_slide_2\" class=\"slide\" data-ag-name=\"Test Slide 2\">\r\n\r\n  <h1 data-ag-editable=\"header\">Headline Slide 2</h1>\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n  <ul>\r\n    <li data-ag-editable=\"list-entry-1\">Donec in sagittis erat.<sup data-reference-id=\'esccap_2011\'></sup></li>\r\n    <li data-ag-editable=\"list-entry-2\">Duis blandit tincidunt hendrerit.<sup data-reference-id=\'doe_j_05_2007,doe_j_01_2005,doe_j_13_2007,doe_j_14_2007,doe_j_15_2007,doe_j_16_2007,doe_j_03_2009\'></sup></li>\r\n    <li data-ag-editable=\"list-entry-3\">Phasellus sed sem lacus. Vestibulum ante<br/>ipsum primis in faucibus<sup data-reference-id=\'doe_j_04_2007\'></sup> orci luctus et ultrices.</li>\r\n  </ul>\r\n  <p class=\"set-custom-popover\" data-popover=\"Slide2/slide\"></p>\r\n  <div class=\"bayerLogo\"></div>\r\n  <div class=\"brandLogo\"></div>\r\n</article>\r\n");
app.cache.put("slides/test_slide_4/test_slide_4.html","<article id=\"test_slide_4\" class=\"slide\" data-ag-name=\"Test Slide 4\">\r\n  <h1 data-ag-editable=\"header\">Headline Slide 4</h1>\r\n\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n  <ul>\r\n    <li data-ag-editable=\"list-entry-1\">Donec in sagittis erat.</li>\r\n    <li data-ag-editable=\"list-entry-2\">Duis blandit tincidunt hendrerit.</li>\r\n    <li data-ag-editable=\"list-entry-3\">Phasellus sed sem lacus. Vestibulum ante<br/>ipsum primis in faucibus orci luctus et ultrices.</li>\r\n  </ul>\r\n  <p class=\"set-custom-popover\" data-popover=\"Slide4/slide\"></p>\r\n  <div class=\"bayerLogo\"></div>\r\n  <div class=\"brandLogo\"></div>\r\n</article>\r\n");
app.cache.put("slides/test_slide_5/test_slide_5.html","<article id=\"test_slide_5\" class=\"slide black\" data-ag-name=\"Test Slide 5\">\r\n  <h1 data-ag-editable=\"header\">Headline Slide 5</h1>\r\n\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n  <ul>\r\n    <li data-ag-editable=\"list-entry-1\">Donec in sagittis erat.</li>\r\n    <li data-ag-editable=\"list-entry-2\">Duis blandit tincidunt hendrerit.<sup data-reference-id=\'wall_r_2001,esccap_2011,doe_j_05_2007\'></sup></li>\r\n    <li data-ag-editable=\"list-entry-3\">Phasellus sed sem lacus. Vestibulum ante<br/>ipsum primis in faucibus orci luctus et ultrices.</li>\r\n  </ul>\r\n  <p class=\"set-custom-popover\" data-popover=\"Slide5/slide\"></p>\r\n  <div class=\"bayerLogoWhite\"></div>\r\n  <div class=\"brandLogo\"></div>\r\n</article>\r\n");
app.cache.put("slides/test_slide_6/test_slide_6.html","<article id=\"test_slide_6\" class=\"slide\" data-ag-name=\"Test Slide 6\">\r\n  <h1 data-ag-editable=\"header\">Headline Slide 6</h1>\r\n\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n  <ul>\r\n    <li data-ag-editable=\"list-entry-1\">Donec in sagittis erat.</li>\r\n    <li data-ag-editable=\"list-entry-2\">Duis blandit tincidunt hendrerit.</li>\r\n    <li data-ag-editable=\"list-entry-3\">Phasellus sed sem lacus. Vestibulum ante<br/>ipsum primis in faucibus orci luctus et ultrices.</li>\r\n  </ul>\r\n  <p class=\"set-custom-popover\" data-popover=\"Slide6/slide\"></p>\r\n  <div class=\"bayerLogo\"></div>\r\n  <div class=\"brandLogo\"></div>\r\n</article>\r\n");
app.cache.put("slides/test_slide_8/test_slide_8.html","<article id=\"test_slide_8\" class=\"slide\" data-ag-name=\"Test Slide 8\">\r\n\r\n  <h1 data-ag-editable=\"header\">Headline Slide 8</h1>\r\n  <button data-popover=\'openPopup/bottomCenterCustom1/bottom\' data-popup=\"test_popup_1_slide\">TEST_POPUP_1_SLIDE</button>\r\n\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n  <ul>\r\n    <li data-ag-editable=\"list-entry-1\">Donec in sagittis erat.</li>\r\n    <li data-ag-editable=\"list-entry-2\">Duis blandit tincidunt hendrerit.</li>\r\n    <li data-ag-editable=\"list-entry-3\">Phasellus sed sem lacus. Vestibulum ante\r\nipsum primis in faucibus orci luctus et ultrices.<br>ipsum primis in faucibus orci luctus et ultrices.</li>\r\n  </ul>\r\n  <p class=\"set-custom-popover\" data-popover=\"Slide8/slide\"></p>\r\n  <div class=\"bayerLogo\"></div>\r\n  <div class=\"brandLogo\"></div>\r\n</article>\r\n");
app.cache.put("slides/test_slide_7/test_slide_7.html","<article id=\"test_slide_7\" class=\"slide\" data-ag-name=\"Test Slide 7\">\r\n\r\n  <h1 data-ag-editable=\"header\">Headline Slide 7</h1>\r\n\r\n  <h2 data-ag-editable=\"sub-headline\">Sub-Headline</h2>\r\n\r\n  <h3 data-ag-editable=\"sub-sub-headline\">Sub-Sub-Headline</h3>\r\n\r\n  <p data-ag-editable=\"paragraph\">Paragraph</p>\r\n  <ul>\r\n    <li data-ag-editable=\"list-entry-1\">Donec in sagittis erat.</li>\r\n    <li data-ag-editable=\"list-entry-2\">Duis blandit tincidunt hendrerit.</li>\r\n    <li data-ag-editable=\"list-entry-3\">Phasellus sed sem lacus. Vestibulum ante<br/>ipsum primis in faucibus orci luctus et ultrices.</li>\r\n  </ul>\r\n  <p class=\"set-custom-popover\" data-popover=\"Slide7/slide\"></p>\r\n  <div class=\"bayerLogo\"></div>\r\n  <div class=\"brandLogo\"></div>\r\n</article>\r\n");
app.cache.put("slides/the_elevit_range_page_00/the_elevit_range_page_00.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>The Elevit Range</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"the_elevit_range_page_00\" class=\"slide\">\r\n    	<header><a href=\"#elevit/start\"><div class=\"icon-the-elevit-range\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n			<div class=\"landing-row landing-2-items\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_01\">\r\n						<img src=\"./contents/the-elevit-range/Elevit-The-Elevit-Range-page-C-01.png\"/>\r\n						THE ELEVIT RANGE\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_02\">\r\n						<img src=\"./contents/the-elevit-range/Elevit-The-Elevit-Range-page-C-02.png\"/>\r\n						MENEVIT\r\n					</a>\r\n				</div>\r\n			</div>\r\n			<div class=\"landing-row landing-2-items\">\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_03\">\r\n						<img src=\"./contents/the-elevit-range/Elevit-The-Elevit-Range-page-C-03.png\"/>\r\n						MORNING SICKNESS\r\n					</a>\r\n				</div>\r\n				<div class=\"landing-col\">\r\n					<a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_04\">\r\n						<img src=\"./contents/the-elevit-range/Elevit-The-Elevit-Range-page-C-04.png\"/>\r\n						ELEVIT BREASTFEEDING\r\n					</a>\r\n				</div>\r\n			</div>\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/the_elevit_range_page_01/the_elevit_range_page_01.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>The_Elevit_Range_page_01</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"the_elevit_range_page_01\" class=\"slide\">\r\n    	<header><a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_00\"><div class=\"icon-the-elevit-range\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n        <div class=\"brandLogo-elevit-range\"></div>\r\n\r\n        <div class=\"woman-range-1\"></div>\r\n        <div class=\"woman-range-2\"></div>\r\n        <div class=\"bg-curve\"><img src=\"./contents/the-elevit-range/Crv-1.png\" class=\"img-full img-no-border\"/></div>\r\n        <h2 class=\"header-range\">Elevit supports baby’s healthy development at every step</h2>\r\n\r\n        <div class=\"bg-line-range\">\r\n          <div class=\"label\">\r\n            <div class=\"btn label-range\">Planning</div>\r\n            <div class=\"btn label-range\">Pregnancy</div>\r\n            <div class=\"btn label-range\">Breastfeeding</div>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"packshot-menevit d-n ani-1\"><img src=\"./contents/the-elevit-range/Menevit-pack-shot-new.png\" /></div>\r\n        <div class=\"packshot-elevit-30-tablets d-n ani-2\"><img src=\"./contents/the-elevit-range/Elevit_30_Tablets.png\" /></div>\r\n        \r\n        <div class=\"packshot-elevit-100-tablets d-n ani-3\"><img src=\"./contents/the-elevit-range/Elevit_100_Tablets_3D_BUILD.png\" /></div>\r\n        <div class=\"packshot-morning-sickness d-n ani-4\"><img src=\"./contents/the-elevit-range/Elevit-Morning-Sickness-pack-shot-new.png\" /></div>\r\n\r\n        <div class=\"packshot-breastfeedings d-n ani-5\"><img src=\"./contents/the-elevit-range/Elevit-Breastfeeding-pack-shot-new.png\" /></div>\r\n\r\n        <div class=\"button-range d-n ani-6\">\r\n          <div class=\"label\">\r\n            <a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_02\"><div class=\"btn-range blue\">Menevit</div></a>\r\n            <a href=\"#elevit/formulation_page/formulation_page_01\"><div class=\"btn-range\">Elevit</div></a>\r\n            <a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_03\"><div class=\"btn-range vilolet\">Morning Sickness</div></a>\r\n            <a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_04\"><div class=\"btn-range navy\">Elevit Breastfeeding</div></a>\r\n          </div>\r\n        </div>        \r\n\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/the_elevit_range_page_03/the_elevit_range_page_03.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>The_Elevit_Range_page_03</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"the_elevit_range_page_03\" class=\"slide\">\r\n    	<header><a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_00\"><div class=\"icon-the-elevit-range\"></div></a></header>\r\n\r\n      <div class=\"container\">\r\n        <h1>Elevit Morning Sickness</h1>\r\n        <p>A unique bi-layer sustained release tablet which is specially formulated with <strong>ginger</strong> and <strong>vitamin B6</strong> to provide relief from symptoms including nausea and vomiting. \r\n          <br><br><strong>Ginger</strong> and <strong>vitamin B6</strong> have been shown to help reduce the frequency and severity of morning sickness<sup data-reference-id=\'ref_43,ref_46\'></sup></p>\r\n\r\n        <div class=\"packshot-morning-sickness d-n ani-1\"><img src=\"./contents/the-elevit-range/Elevit-Morning-Sickness-pack-shot-new.png\" /></div>\r\n\r\n        <div class=\"contain-inside\">\r\n          <p><strong>Dosage:</strong> <span>One tablet in the morning and if required, one tablet 12 hours later to help maintain relief</span></p>\r\n          <p><strong>When to take:</strong> <span>Elevit Morning Sickness Relief should be taken in conjunction with Elevit Pregnancy Multivitamin</span></p>\r\n        </div>\r\n\r\n      </div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/the_elevit_range_page_02/the_elevit_range_page_02.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>The_Elevit_Range_page_02</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"the_elevit_range_page_02\" class=\"slide\">\r\n    	<header><a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_00\"><div class=\"icon-the-elevit-range\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n  		  <h1>Menevit</h1>\r\n  		  <p>A unique combination of antioxidants to help promote sperm health for couples planning pregnancy. The antioxidants in <strong>Menevit</strong> have been independently associated with supporting sperm health and production, as well as protecting against DNA damage<sup data-reference-id=\'ref_40,ref_41\'></sup></p>\r\n\r\n        <div class=\"packshot-menevit d-n ani-1\"><img src=\"./contents/the-elevit-range/Menevit-pack-shot-new.png\" /></div>\r\n\r\n        <div class=\"contain-inside\">\r\n          <p><strong>Dosage:</strong> <span>1 tablet per day with food</span></p>\r\n          <p><strong>When to take:</strong> <span>Men should preferably take Menevit from at least 90 days before trying for pregnancy as the sperm production cycle takes 74 days<sup data-reference-id=\'ref_42\'></sup></span></p>\r\n        </div>\r\n\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("slides/the_elevit_range_page_04/the_elevit_range_page_04.html","<!doctype html>\r\n<html>\r\n  <head>\r\n    <title>The_Elevit_Range_page_04</title>\r\n  </head>\r\n  <body>\r\n    <article id=\"the_elevit_range_page_04\" class=\"slide\">\r\n    	<header><a href=\"#elevit/the_elevit_range_page/the_elevit_range_page_00\"><div class=\"icon-the-elevit-range\"></div></a></header>\r\n\r\n  		<div class=\"container\">\r\n        <h1>Elevit Breastfeeding</h1>\r\n        <p>Nutrient requirements increase significantly again during breastfeeding and are unique to this stage. Even when dietary changes are made for pregnancy, most women report having difficulty adhering to dietary guidelines during lactation<sup data-reference-id=\'ref_4\'></sup>\r\n        </p>\r\n        <p><strong>Elevit Breastfeeding contains:</strong></p>\r\n\r\n        <div class=\"packshot-morning-sickness d-n ani-1\"><img src=\"./contents/the-elevit-range/Elevit-Breastfeeding-pack-shot-new.png\" /></div>\r\n\r\n        <div class=\"contain-inside\">\r\n          <ul>\r\n            <li>Lutein & Betacarotene, to support baby’s development of vision and eye health</li>\r\n            <li>Omega 3 & Iodine, to support healthy development of baby’s brain</li>\r\n            <li>B Group Vitamins & Iron, to help support energy levels</li>\r\n            <li>Vitamin C & Zinc, to support immunity</li>\r\n          </ul>          \r\n          <p><strong>Dosage:</strong> <span>One capsule per day with food</span></p>\r\n          <p><strong>When to take:</strong> <span>Women should switch from Elevit Preconception & Pregnancy to Elevit Breastfeeding once their baby is born, to support their new nutritional needs.</span></p>\r\n        </div>\r\n  		</div>\r\n\r\n      	<div class=\"bayerLogoWhite\"></div>\r\n      	<a href=\"#elevit/start\">\r\n      		<div class=\"brandLogo-elevit-sm\"></div>\r\n      	</a>\r\n    </article>\r\n  </body>\r\n</html>");
app.cache.put("config.json","{\r\n  \"name\": \"Elevit eDetailer\",\r\n  \"gpc\": \"999-999-999\",\r\n  \"startPath\": \"elevit\",\r\n  \"paths\": {\r\n    \"thumbs\": \"slides/<id>/<id>.png\",\r\n    \"references\": \"shared/references/<id>.pdf\"\r\n  },\r\n  \"lang\": \"en\",\r\n  \"transitionSpeed\": \"fast\",\r\n  \"dependencies\": [\r\n    { \"src\": \"accelerator/lib/draggy.js\" },\r\n    { \"src\": \"accelerator/lib/jquery.min.js\"},\r\n    { \"src\": \"_vendor/jquery-ui.min.js\"},\r\n    { \"src\": \"_vendor/jquery.ui.touch-punch.js\"},\r\n    { \"src\": \"_vendor/jquery.requestAnimationFrame.js\"},\r\n    { \"src\": \"_vendor/iscroll.js\"},\r\n    { \"src\": \"_vendor/apprise-1.5.full.js\"},\r\n    { \"src\": \"_vendor/apprise.css\"},\r\n    { \"src\": \"_vendor/knockout.js\"},\r\n    { \"src\": \"_vendor/bootstrap.min.js\"},\r\n    { \"src\": \"_vendor/bootstrap.css\"},\r\n    { \"src\": \"_vendor/polyfill.js\"},\r\n    { \"src\": \"node_modules/pinch-zoom-js/dist/pinch-zoom.umd.js\"}\r\n  ],\r\n  \"iPlanner\": {\r\n    \"bounce\": false\r\n  },\r\n  \"ag-engager\": {\r\n    \"width\": 1024,\r\n    \"height\": 760\r\n  },\r\n  \"width\": \"100%\",\r\n  \"height\": \"100%\",\r\n  \"margin\": 0,\r\n  \"padding\": 0,\r\n  \"bundle\": {\r\n    \"presentation\": {\r\n      \"styles\": [\"accelerator/css/styles.css\", \"templates/master/**/*.{css,styl}\", \"modules/**/*.{css,styl}\", \"slides/**/*.{css,styl}\"],\r\n      \"scripts\": [\r\n        \"accelerator/js/init.js\",\r\n        \"accelerator/lib/head.min.js\",\r\n        \"templates/master/**/*.js\",\r\n        \"modules/**/*.{js,html,jade,md}\",\r\n        \"slides/**/*.{js,html,jade,md}\",\r\n        \"config.json\",\r\n        \"presentation.json\",\r\n        \"references.json\",\r\n        \"media.json\"\r\n      ]\r\n    },\r\n\r\n    \"translations\": {\r\n      \"templates\": [\"slides/**/translations/*.json\"]\r\n    }\r\n  }\r\n}\r\n");
app.cache.put("presentation.json","{\r\n    \"slides\": {\r\n        \"start_slide\": {\r\n            \"id\": \"start_slide\",\r\n            \"name\": \"Start slide\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/start_slide/start_slide.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/start_slide/start_slide.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/start_slide/start_slide.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"nutrition_page_00\": {\r\n            \"id\": \"nutrition_page_00\",\r\n            \"name\": \"nutrition_page_00\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/nutrition_page_00/nutrition_page_00.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/nutrition_page_00/nutrition_page_00.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/nutrition_page_00/nutrition_page_00.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"nutrition_page_01\": {\r\n            \"id\": \"nutrition_page_01\",\r\n            \"name\": \"nutrition_page_01\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/nutrition_page_01/nutrition_page_01.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/nutrition_page_01/nutrition_page_01.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/nutrition_page_01/nutrition_page_01.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"nutrition_page_02\": {\r\n            \"id\": \"nutrition_page_02\",\r\n            \"name\": \"nutrition_page_02\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/nutrition_page_02/nutrition_page_02.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/nutrition_page_02/nutrition_page_02.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/nutrition_page_02/nutrition_page_02.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"nutrition_page_03\": {\r\n            \"id\": \"nutrition_page_03\",\r\n            \"name\": \"nutrition_page_03\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/nutrition_page_03/nutrition_page_03.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/nutrition_page_03/nutrition_page_03.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/nutrition_page_03/nutrition_page_03.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"nutrition_page_04\": {\r\n            \"id\": \"nutrition_page_04\",\r\n            \"name\": \"nutrition_page_04\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/nutrition_page_04/nutrition_page_04.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/nutrition_page_04/nutrition_page_04.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/nutrition_page_04/nutrition_page_04.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"nutrition_page_05\": {\r\n            \"id\": \"nutrition_page_05\",\r\n            \"name\": \"nutrition_page_05\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/nutrition_page_05/nutrition_page_05.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/nutrition_page_05/nutrition_page_05.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/nutrition_page_05/nutrition_page_05.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"nutrition_page_06\": {\r\n            \"id\": \"nutrition_page_06\",\r\n            \"name\": \"nutrition_page_06\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/nutrition_page_06/nutrition_page_06.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/nutrition_page_06/nutrition_page_06.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/nutrition_page_06/nutrition_page_06.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"nutrition_page_07\": {\r\n            \"id\": \"nutrition_page_07\",\r\n            \"name\": \"nutrition_page_07\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/nutrition_page_07/nutrition_page_07.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/nutrition_page_07/nutrition_page_07.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/nutrition_page_07/nutrition_page_07.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"nutrition_page_08\": {\r\n            \"id\": \"nutrition_page_08\",\r\n            \"name\": \"nutrition_page_08\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/nutrition_page_08/nutrition_page_08.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/nutrition_page_08/nutrition_page_08.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/nutrition_page_08/nutrition_page_08.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"folic_acid_page_00\": {\r\n            \"id\": \"folic_acid_page_00\",\r\n            \"name\": \"Folic_acid_page_00\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/folic_acid_page_00/folic_acid_page_00.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/folic_acid_page_00/folic_acid_page_00.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/folic_acid_page_00/folic_acid_page_00.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"folic_acid_page_01\": {\r\n            \"id\": \"folic_acid_page_01\",\r\n            \"name\": \"Folic_acid_page_01\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/folic_acid_page_01/folic_acid_page_01.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/folic_acid_page_01/folic_acid_page_01.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/folic_acid_page_01/folic_acid_page_01.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"folic_acid_page_02\": {\r\n            \"id\": \"folic_acid_page_02\",\r\n            \"name\": \"Folic_acid_page_02\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/folic_acid_page_02/folic_acid_page_02.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/folic_acid_page_02/folic_acid_page_02.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/folic_acid_page_02/folic_acid_page_02.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"folic_acid_page_03\": {\r\n            \"id\": \"folic_acid_page_03\",\r\n            \"name\": \"Folic_acid_page_03\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/folic_acid_page_03/folic_acid_page_03.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/folic_acid_page_03/folic_acid_page_03.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/folic_acid_page_03/folic_acid_page_03.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"folic_acid_page_04\": {\r\n            \"id\": \"folic_acid_page_04\",\r\n            \"name\": \"Folic_acid_page_04\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/folic_acid_page_04/folic_acid_page_04.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/folic_acid_page_04/folic_acid_page_04.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/folic_acid_page_04/folic_acid_page_04.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"iron_page_00\": {\r\n            \"id\": \"iron_page_00\",\r\n            \"name\": \"Iron_page_00\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/iron_page_00/iron_page_00.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/iron_page_00/iron_page_00.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/iron_page_00/iron_page_00.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"iron_page_01\": {\r\n            \"id\": \"iron_page_01\",\r\n            \"name\": \"Iron_page_01\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/iron_page_01/iron_page_01.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/iron_page_01/iron_page_01.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/iron_page_01/iron_page_01.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"iron_page_02\": {\r\n            \"id\": \"iron_page_02\",\r\n            \"name\": \"Iron_page_02\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/iron_page_02/iron_page_02.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/iron_page_02/iron_page_02.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/iron_page_02/iron_page_02.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"iron_page_03\": {\r\n            \"id\": \"iron_page_03\",\r\n            \"name\": \"Iron_page_03\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/iron_page_03/iron_page_03.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/iron_page_03/iron_page_03.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/iron_page_03/iron_page_03.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"iron_page_04\": {\r\n            \"id\": \"iron_page_04\",\r\n            \"name\": \"Iron_page_04\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/iron_page_04/iron_page_04.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/iron_page_04/iron_page_04.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/iron_page_04/iron_page_04.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"iron_page_05\": {\r\n            \"id\": \"iron_page_05\",\r\n            \"name\": \"Iron_page_05\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/iron_page_05/iron_page_05.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/iron_page_05/iron_page_05.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/iron_page_05/iron_page_05.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"iodine_page_00\": {\r\n            \"id\": \"iodine_page_00\",\r\n            \"name\": \"Iodine_page_00\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/iodine_page_00/iodine_page_00.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/iodine_page_00/iodine_page_00.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/iodine_page_00/iodine_page_00.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"iodine_page_01\": {\r\n            \"id\": \"iodine_page_01\",\r\n            \"name\": \"Iodine_page_01\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/iodine_page_01/iodine_page_01.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/iodine_page_01/iodine_page_01.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/iodine_page_01/iodine_page_01.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"iodine_page_02\": {\r\n            \"id\": \"iodine_page_02\",\r\n            \"name\": \"Iodine_page_02\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/iodine_page_02/iodine_page_02.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/iodine_page_02/iodine_page_02.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/iodine_page_02/iodine_page_02.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"iodine_page_03\": {\r\n            \"id\": \"iodine_page_03\",\r\n            \"name\": \"Iodine_page_03\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/iodine_page_03/iodine_page_03.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/iodine_page_03/iodine_page_03.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/iodine_page_03/iodine_page_03.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"formulation_page_00\": {\r\n            \"id\": \"formulation_page_00\",\r\n            \"name\": \"Formulation_page_00\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/formulation_page_00/formulation_page_00.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/formulation_page_00/formulation_page_00.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/formulation_page_00/formulation_page_00.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"formulation_page_01\": {\r\n            \"id\": \"formulation_page_01\",\r\n            \"name\": \"Formulation_page_01\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/formulation_page_01/formulation_page_01.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/formulation_page_01/formulation_page_01.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/formulation_page_01/formulation_page_01.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"formulation_page_02\": {\r\n            \"id\": \"formulation_page_02\",\r\n            \"name\": \"Formulation_page_02\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/formulation_page_02/formulation_page_02.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/formulation_page_02/formulation_page_02.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/formulation_page_02/formulation_page_02.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"formulation_page_03\": {\r\n            \"id\": \"formulation_page_03\",\r\n            \"name\": \"Formulation_page_03\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/formulation_page_03/formulation_page_03.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/formulation_page_03/formulation_page_03.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/formulation_page_03/formulation_page_03.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"tablet_comparison_page_01\": {\r\n            \"id\": \"tablet_comparison_page_01\",\r\n            \"name\": \"Tablet_comparison_page_01\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/tablet_comparison_page_01/tablet_comparison_page_01.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/tablet_comparison_page_01/tablet_comparison_page_01.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/tablet_comparison_page_01/tablet_comparison_page_01.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"the_elevit_range_page_00\": {\r\n            \"id\": \"the_elevit_range_page_00\",\r\n            \"name\": \"The_Elevit_Range_page_00\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/the_elevit_range_page_00/the_elevit_range_page_00.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/the_elevit_range_page_00/the_elevit_range_page_00.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/the_elevit_range_page_00/the_elevit_range_page_00.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"the_elevit_range_page_01\": {\r\n            \"id\": \"the_elevit_range_page_01\",\r\n            \"name\": \"The_Elevit_Range_page_01\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/the_elevit_range_page_01/the_elevit_range_page_01.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/the_elevit_range_page_01/the_elevit_range_page_01.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/the_elevit_range_page_01/the_elevit_range_page_01.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"the_elevit_range_page_02\": {\r\n            \"id\": \"the_elevit_range_page_02\",\r\n            \"name\": \"The_Elevit_Range_page_02\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/the_elevit_range_page_02/the_elevit_range_page_02.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/the_elevit_range_page_02/the_elevit_range_page_02.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/the_elevit_range_page_02/the_elevit_range_page_02.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"the_elevit_range_page_03\": {\r\n            \"id\": \"the_elevit_range_page_03\",\r\n            \"name\": \"The_Elevit_Range_page_03\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/the_elevit_range_page_03/the_elevit_range_page_03.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/the_elevit_range_page_03/the_elevit_range_page_03.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/the_elevit_range_page_03/the_elevit_range_page_03.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"the_elevit_range_page_04\": {\r\n            \"id\": \"the_elevit_range_page_04\",\r\n            \"name\": \"The_Elevit_Range_page_04\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/the_elevit_range_page_04/the_elevit_range_page_04.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/the_elevit_range_page_04/the_elevit_range_page_04.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/the_elevit_range_page_04/the_elevit_range_page_04.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"resources_page_01\": {\r\n            \"id\": \"resources_page_01\",\r\n            \"name\": \"Resources_page_01\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"slides/resources_page_01/resources_page_01.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"slides/resources_page_01/resources_page_01.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"slides/resources_page_01/resources_page_01.css\"\r\n                ]\r\n            }\r\n        }\r\n    },\r\n    \"modules\": {\r\n        \"ag-slide-analytics\": {\r\n            \"name\": \"Agnitio Slide Analytics\",\r\n            \"description\": \"Save data about the slides visited. Will call ag.submit.slide(data).\",\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ag-slide-analytics/ag-slide-analytics.js\"\r\n                ]\r\n            },\r\n            \"version\": \"0.7.0\"\r\n        },\r\n        \"ag-auto-menu\": {\r\n            \"name\": \"Agnitio Auto Menu\",\r\n            \"description\": \"Menu that automatically builds.\",\r\n            \"files\": {\r\n                \"styles\": [\r\n                    \"modules/ag-auto-menu/ag-auto-menu.css\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ag-auto-menu/ag-auto-menu.js\"\r\n                ]\r\n            },\r\n            \"version\": \"0.8.5\"\r\n        },\r\n        \"ag-slide-popup\": {\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ag-slide-popup/ag-slide-popup.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ag-slide-popup/ag-slide-popup.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ag-video\": {\r\n            \"name\": \"Agnitio Video Module\",\r\n            \"description\": \"Wrapper for HTML5 video tag to add states and custom controls\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ag-video/ag-video.html\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ag-video/ag-video.css\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ag-video/ag-video.js\"\r\n                ]\r\n            },\r\n            \"version\": \"0.9.6\"\r\n        },\r\n        \"ag-viewer\": {\r\n            \"id\": \"ag-viewer\",\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ag-viewer/ag-viewer.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ag-viewer/ag-viewer.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ag-builder\": {\r\n            \"id\": \"ag-builder\",\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ag-builder/ag-builder.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ag-builder/ag-builder.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-toolbar\": {\r\n            \"id\": \"ap-toolbar\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-toolbar/ap-toolbar.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-toolbar/ap-toolbar.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-toolbar/ap-toolbar.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-overview\": {\r\n            \"id\": \"ap-overview\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-overview/ap-overview.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-overview/ap-overview.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-overview/ap-overview.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-module\": {\r\n            \"id\": \"ap-module\",\r\n            \"files\": {\r\n                \"templates\": [],\r\n                \"scripts\": [\r\n                    \"modules/ap-module/ap-module.js\"\r\n                ],\r\n                \"styles\": []\r\n            }\r\n        },\r\n        \"ap-auto-menu-handle\": {\r\n            \"id\": \"ap-auto-menu-handle\",\r\n            \"files\": {\r\n                \"templates\": [],\r\n                \"scripts\": [\r\n                    \"modules/ap-auto-menu-handle/ap-auto-menu-handle.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-auto-menu-handle/ap-auto-menu-handle.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-custom-collections-storage\": {\r\n            \"id\": \"ap-custom-collections-storage\",\r\n            \"files\": {\r\n                \"templates\": [],\r\n                \"scripts\": [\r\n                    \"modules/ap-custom-collections-storage/ap-custom-collections-storage.js\"\r\n                ],\r\n                \"styles\": []\r\n            }\r\n        },\r\n        \"ap-custom-collections-menu\": {\r\n            \"id\": \"ap-custom-collections-menu\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-custom-collections-menu/ap-custom-collections-menu.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-custom-collections-menu/ap-custom-collections-menu.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-custom-collections-menu/ap-custom-collections-menu.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-custom-collections\": {\r\n            \"id\": \"ap-custom-collections\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-custom-collections/ap-custom-collections.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-custom-collections/ap-custom-collections.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-custom-collections/ap-custom-collections.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-content-groups\": {\r\n            \"id\": \"ap-content-groups\",\r\n            \"files\": {\r\n                \"templates\": [],\r\n                \"scripts\": [\r\n                    \"modules/ap-content-groups/ap-content-groups.js\"\r\n                ],\r\n                \"styles\": []\r\n            }\r\n        },\r\n        \"ap-auto-references\": {\r\n            \"id\": \"ap-auto-references\",\r\n            \"files\": {\r\n                \"templates\": [],\r\n                \"scripts\": [\r\n                    \"modules/ap-auto-references/ap-auto-references.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-auto-references/ap-auto-references.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-auto-references-popup\": {\r\n            \"id\": \"ap-auto-references-popup\",\r\n            \"files\": {\r\n                \"templates\": [],\r\n                \"scripts\": [\r\n                    \"modules/ap-auto-references-popup/ap-auto-references-popup.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-auto-references-popup/ap-auto-references-popup.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-auto-side-clip\": {\r\n            \"id\": \"ap-auto-side-clip\",\r\n            \"files\": {\r\n                \"templates\": [],\r\n                \"scripts\": [\r\n                    \"modules/ap-auto-side-clip/ap-auto-side-clip.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-auto-side-clip/ap-auto-side-clip.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-media-repository\": {\r\n            \"id\": \"ap-media-repository\",\r\n            \"files\": {\r\n                \"templates\": [],\r\n                \"scripts\": [\r\n                    \"modules/ap-media-repository/ap-media-repository.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-media-repository/renderers.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-back-navigation\": {\r\n            \"id\": \"ap-back-navigation\",\r\n            \"files\": {\r\n                \"templates\": [],\r\n                \"scripts\": [\r\n                    \"modules/ap-back-navigation/ap-back-navigation.js\"\r\n                ],\r\n                \"styles\": []\r\n            }\r\n        },\r\n        \"ap-favorite-presentations-buttons\": {\r\n            \"id\": \"ap-favorite-presentations-buttons\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-favorite-presentations-buttons/ap-favorite-presentations-buttons.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-favorite-presentations-buttons/ap-favorite-presentations-buttons.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-favorite-presentations-buttons/ap-favorite-presentations-buttons.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-media-library\": {\r\n            \"id\": \"ap-media-library\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-media-library/ap-media-library.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-media-library/ap-media-library.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-media-library/ap-media-library.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-reference-library\": {\r\n            \"id\": \"ap-reference-library\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-reference-library/ap-reference-library.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-reference-library/ap-reference-library.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-reference-library/ap-reference-library.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-video-library\": {\r\n            \"id\": \"ap-video-library\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-video-library/ap-video-library.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-video-library/ap-video-library.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-video-library/ap-video-library.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-specific-product-characteristics\": {\r\n            \"id\": \"ap-specific-product-characteristics\",\r\n            \"files\": {\r\n                \"templates\": [],\r\n                \"scripts\": [\r\n                    \"modules/ap-specific-product-characteristics/ap-specific-product-characteristics.js\"\r\n                ],\r\n                \"styles\": []\r\n            }\r\n        },\r\n        \"ap-follow-up-mail\": {\r\n            \"id\": \"ap-follow-up-mail\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-follow-up-mail/ap-follow-up-mail.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-follow-up-mail/ap-follow-up-mail.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-follow-up-mail/ap-follow-up-mail.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-frequently-asked-questions\": {\r\n            \"id\": \"ap-frequently-asked-questions\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-frequently-asked-questions/ap-frequently-asked-questions.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-frequently-asked-questions/ap-frequently-asked-questions.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-frequently-asked-questions/ap-frequently-asked-questions.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-notepad\": {\r\n            \"id\": \"ap-notepad\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-notepad/ap-notepad.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-notepad/ap-notepad.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-notepad/ap-notepad.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ap-slide-indicator\": {\r\n            \"id\": \"ap-slide-indicator\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ap-slide-indicator/ap-slide-indicator.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ap-slide-indicator/ap-slide-indicator.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ap-slide-indicator/ap-slide-indicator.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"bhc-gpc-number\": {\r\n            \"id\": \"bhc-gpc-number\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/bhc-gpc-number/bhc-gpc-number.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/bhc-gpc-number/bhc-gpc-number.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/bhc-gpc-number/bhc-gpc-number.css\"\r\n                ]\r\n            }\r\n        },\r\n        \"ah-cleanup\": {\r\n            \"name\": \"Anthill Cleanup Module\",\r\n            \"description\": \"Clean up slides not in use\",\r\n            \"type\": \"global\",\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ah-cleanup/ah-cleanup.js\"\r\n                ]\r\n            },\r\n            \"version\": \"0.9.0\"\r\n        },\r\n        \"ag-overlay\": {\r\n            \"name\": \"Agnitio Overlay\",\r\n            \"type\": \"universal\",\r\n            \"description\": \"Creates an overlay to the presentation.\",\r\n            \"files\": {\r\n                \"styles\": [\r\n                    \"modules/ag-overview/ag-overview.css\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ag-overview/ag-overview.js\"\r\n                ]\r\n            },\r\n            \"version\": \"0.5.1\"\r\n        },\r\n        \"ah-popup-management\": {\r\n            \"name\": \"Popup management\",\r\n            \"type\": \"global\",\r\n            \"description\": \"Management for popups in BHC.\",\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ah-popup-management/ah-popup-management.js\"\r\n                ]\r\n            },\r\n            \"version\": \"0.0.1\"\r\n        },\r\n        \"ah-history\": {\r\n            \"id\": \"ah-history\",\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ah-history/ah-history.js\"\r\n                ]\r\n            },\r\n            \"version\": \"2.0.1\"\r\n        },\r\n        \"ah-storage-data\": {\r\n            \"id\": \"ah-storage-data\",\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ah-storage-data/ah-storage-data.js\"\r\n                ]\r\n            },\r\n            \"version\": \"1.0.1\"\r\n        },\r\n        \"ah-tab-group\": {\r\n            \"id\": \"ah-tab-group\",\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ah-tab-group/ah-tab-group.js\",\r\n                    \"modules/ah-tab-group/tab-states.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ah-tab-group/ah-tab-group.css\"\r\n                ]\r\n            },\r\n            \"version\": \"0.0.2\"\r\n        },\r\n        \"ah-tour\": {\r\n            \"id\": \"ah-tour\",\r\n            \"files\": {\r\n                \"styles\": [\r\n                    \"modules/ah-tour/tour.css\",\r\n                    \"modules/ah-tour/custom-popover.css\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ah-tour/ah-tour.js\",\r\n                    \"modules/ah-tour/tutorial-tour.js\"\r\n                ],\r\n                \"templates\": [\r\n                    \"modules/ah-tour/custom-popover.html\",\r\n                    \"modules/ah-tour/template.html\"\r\n                ]\r\n            },\r\n            \"version\": \"0.0.2\"\r\n        },\r\n        \"ah-tour-configurator\": {\r\n            \"id\": \"ah-tour-configurator\",\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ah-tour-configurator/ah-tour-configurator.js\",\r\n                    \"modules/ah-tour-configurator/tutorial-configurator.js\"\r\n                ]\r\n            },\r\n            \"version\": \"0.0.2\"\r\n        },\r\n        \"ah-tutorial\": {\r\n            \"id\": \"ah-tutorial\",\r\n            \"files\": {\r\n                \"templates\": [\r\n                    \"modules/ah-tutorial/tutorial.html\"\r\n                ],\r\n                \"scripts\": [\r\n                    \"modules/ah-tutorial/ah-tutorial.js\",\r\n                    \"modules/ah-tutorial/tutorial.js\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ah-tutorial/tutorial.css\"\r\n                ]\r\n            },\r\n            \"version\": \"0.0.2\"\r\n        },\r\n        \"ah-tutorial-controls\": {\r\n            \"id\": \"ah-tutorial-controls\",\r\n            \"files\": {\r\n                \"scripts\": [\r\n                    \"modules/ah-tutorial-controls/ah-tutorial-controls.js\"\r\n                ],\r\n                \"templates\": [\r\n                    \"modules/ah-tutorial-controls/ah-tutorial-controls.html\"\r\n                ],\r\n                \"styles\": [\r\n                    \"modules/ah-tutorial-controls/ah-tutorial-controls.css\"\r\n                ]\r\n            },\r\n            \"version\": \"0.0.2\"\r\n        }\r\n    },\r\n    \"structures\": {\r\n        \"start\": {\r\n            \"name\": \"Home page\",\r\n            \"id\": \"start\",\r\n            \"content\": [\r\n                \"start_slide\"\r\n            ],\r\n            \"type\": \"slideshow\",\r\n            \"shareable\": {}\r\n        },\r\n        \"nutrition_page\": {\r\n            \"name\": \"Nutrition\",\r\n            \"id\": \"nutrition_page\",\r\n            \"content\": [\r\n                \"nutrition_page_00\",\r\n                \"nutrition_page_01\",\r\n                \"nutrition_page_02\",\r\n                \"nutrition_page_03\",\r\n                \"nutrition_page_04\",\r\n                \"nutrition_page_05\",\r\n                \"nutrition_page_06\",\r\n                \"nutrition_page_07\",\r\n                \"nutrition_page_08\"\r\n            ],\r\n            \"type\": \"slideshow\",\r\n            \"shareable\": {}\r\n        },\r\n        \"folic_acid_page\": {\r\n            \"name\": \"Folic acid\",\r\n            \"id\": \"folic_acid_page\",\r\n            \"content\": [\r\n                \"folic_acid_page_00\",\r\n                \"folic_acid_page_01\",\r\n                \"folic_acid_page_02\",\r\n                \"folic_acid_page_03\",\r\n                \"folic_acid_page_04\"\r\n            ],\r\n            \"type\": \"slideshow\",\r\n            \"shareable\": {}\r\n        },\r\n        \"iron_page\": {\r\n            \"name\": \"Iron\",\r\n            \"id\": \"iron_page\",\r\n            \"content\": [\r\n                \"iron_page_00\",\r\n                \"iron_page_01\",\r\n                \"iron_page_02\",\r\n                \"iron_page_03\",\r\n                \"iron_page_04\",\r\n                \"iron_page_05\"\r\n            ],\r\n            \"type\": \"slideshow\",\r\n            \"shareable\": {}\r\n        },\r\n        \"iodine_page\": {\r\n            \"name\": \"Iodine\",\r\n            \"id\": \"iodine_page\",\r\n            \"content\": [\r\n                \"iodine_page_00\",\r\n                \"iodine_page_01\",\r\n                \"iodine_page_02\",\r\n                \"iodine_page_03\"\r\n            ],\r\n            \"type\": \"slideshow\",\r\n            \"shareable\": {}\r\n        },\r\n        \"formulation_page\": {\r\n            \"name\": \"Elevit Formulation\",\r\n            \"id\": \"formulation_page\",\r\n            \"content\": [\r\n                \"formulation_page_00\",\r\n                \"formulation_page_01\",\r\n                \"formulation_page_02\",\r\n                \"formulation_page_03\"\r\n            ],\r\n            \"type\": \"slideshow\",\r\n            \"shareable\": {}\r\n        },\r\n        \"tablet_comparison_page\": {\r\n            \"name\": \"Tablet Comparison\",\r\n            \"id\": \"tablet_comparison_page\",\r\n            \"content\": [\r\n                \"tablet_comparison_page_01\"\r\n            ],\r\n            \"type\": \"slideshow\",\r\n            \"shareable\": {}\r\n        },\r\n        \"the_elevit_range_page\": {\r\n            \"name\": \"The Elevit Range\",\r\n            \"id\": \"the_elevit_range_page\",\r\n            \"content\": [\r\n                \"the_elevit_range_page_00\",\r\n                \"the_elevit_range_page_01\",\r\n                \"the_elevit_range_page_02\",\r\n                \"the_elevit_range_page_03\",\r\n                \"the_elevit_range_page_04\"\r\n            ],\r\n            \"type\": \"slideshow\",\r\n            \"shareable\": {}\r\n        },\r\n        \"resources_page\": {\r\n            \"name\": \"Resources\",\r\n            \"id\": \"resources_page\",\r\n            \"content\": [\r\n                \"resources_page_01\"\r\n            ],\r\n            \"type\": \"slideshow\",\r\n            \"shareable\": {}\r\n        }\r\n    },\r\n    \"storyboard\": [\r\n        \"elevit\"\r\n    ],\r\n    \"storyboards\": {\r\n        \"elevit\": {\r\n            \"content\": [\r\n                \"start\",\r\n                \"nutrition_page\",\r\n                \"folic_acid_page\",\r\n                \"iron_page\",\r\n                \"iodine_page\",\r\n                \"formulation_page\",\r\n                \"tablet_comparison_page\",\r\n                \"the_elevit_range_page\",\r\n                \"resources_page\"\r\n            ],\r\n            \"name\": \"Elevit\",\r\n            \"linear\": false,\r\n            \"id\": \"elevit\",\r\n            \"type\": \"collection\"\r\n        }\r\n    }\r\n}");
app.cache.put("references.json","");
app.cache.put("media.json","{\r\n  \"content://National Health and Medical Research Council. Nutrient Reference Values for Australia and New Zealand Including Recommended Dietary Intakes, 2006.\": {\r\n    \"referenceKey\": \"ref_1\",\r\n    \"referenceId\": 1,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Centers for Disease Control and Prevention (2006). Recommendations to Improve Preconception Health and Health Care. Available at https://www.cdc.gov/mmwr/preview/mmwrhtml/rr5506a1.htm (Accessed October 2018).\": {\r\n    \"referenceKey\": \"ref_2\",\r\n    \"referenceId\": 2,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Gardiner PM, et al. Am J Obstet Gynecol 2008; 199(6 Suppl 2):S345-56.\": {\r\n    \"referenceKey\": \"ref_3\",\r\n    \"referenceId\": 3,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Malek L, et al. Public Health Nutr 2015; 19(7):1155-63.\": {\r\n    \"referenceKey\": \"ref_4\",\r\n    \"referenceId\": 4,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Conlin ML, et al. Aust NZ J Obstet Gynaecol 2006; 46:528-33.\": {\r\n    \"referenceKey\": \"ref_5\",\r\n    \"referenceId\": 5,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Data on file TLE Omnibus, Bayer Australia Ltd September 2009.\": {\r\n    \"referenceKey\": \"ref_6\",\r\n    \"referenceId\": 6,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://De-Regil L, et al. Cochrane Database of Systematic Reviews 2015; 12:CD007950. DOI: 10.1002/14651858.CD007950.pub3.\": {\r\n    \"referenceKey\": \"ref_7\",\r\n    \"referenceId\": 7,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Eastman C, RANZCOG membership magazine O&G 2005;7(1):65-6.\": {\r\n    \"referenceKey\": \"ref_8\",\r\n    \"referenceId\": 8,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Glinoer D, Public Health Nutr 2007; 10(12A):1542-6.\": {\r\n    \"referenceKey\": \"ref_9\",\r\n    \"referenceId\": 9,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Australian Bureau of Statistics. Australian Health Survey. Biomedical Results for Nutrients. Available at www.abs.gov.au (Accessed October 2018).\": {\r\n    \"referenceKey\": \"ref_10\",\r\n    \"referenceId\": 10,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Clifton VL, et al. Nutr J 2013; 12:32.\": {\r\n    \"referenceKey\": \"ref_11\",\r\n    \"referenceId\": 11,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Blot I, et al. Curr Opin Hematol 1999; 6(2):65-70.\": {\r\n    \"referenceKey\": \"ref_12\",\r\n    \"referenceId\": 12,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Australian Therapeutic Guidelines - Gastrointestinal Topic 2018.\": {\r\n    \"referenceKey\": \"ref_13\",\r\n    \"referenceId\": 13,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://World Health Organization, Daily iron and folic acid supplementation during pregnancy. 2018. Available at: http://www.who.int/nutrition/publications/micronutrients/guidelines/daily_ifa_supp_pregnant_women/en/ (Accessed September 2018).\": {\r\n    \"referenceKey\": \"ref_14\",\r\n    \"referenceId\": 14,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Food Standards Australia New Zealand. AUSNUT 2011-13 Food Nutrient Database. Available at www.foodstandards.gov.au (Accessed October 2018).\": {\r\n    \"referenceKey\": \"ref_15\",\r\n    \"referenceId\": 15,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Bramswig S, Int J Vitamin Nutr Res 2009; 79(2):61-70.\": {\r\n    \"referenceKey\": \"ref_16\",\r\n    \"referenceId\": 16,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Bayer Data on file, 2017.\": {\r\n    \"referenceKey\": \"ref_17\",\r\n    \"referenceId\": 17,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Lamers Y, Am J Clin Nutr 2006; 84:156-61.\": {\r\n    \"referenceKey\": \"ref_18\",\r\n    \"referenceId\": 18,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Daly LE, et al. JAMA 1995; 274(21):1698-702.\": {\r\n    \"referenceKey\": \"ref_19\",\r\n    \"referenceId\": 19,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Furness DLF, et al. Am J Obstet Gynecol 2008;199:276.e1-276.e8.\": {\r\n    \"referenceKey\": \"ref_20\",\r\n    \"referenceId\": 20,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Furness DLF, Folate in Pregnancy: An Update. 2015.\": {\r\n    \"referenceKey\": \"ref_21\",\r\n    \"referenceId\": 21,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Higgins JR, et al. Br J Obstet Gynaecol 2000; 107(9):1149-54.\": {\r\n    \"referenceKey\": \"ref_22\",\r\n    \"referenceId\": 22,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Greenburg JA, et al. Rev Obstet Gynecol 2011; 4(2):52-59.\": {\r\n    \"referenceKey\": \"ref_23\",\r\n    \"referenceId\": 23,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Velasco I, et al. J Clin Endocrinol Metab 2009; 94(9):3234-41.\": {\r\n    \"referenceKey\": \"ref_24\",\r\n    \"referenceId\": 24,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Australian Population Health Development Principal Committee (2007) The Prevalence and Severity of Iodine Deficiency in Australia. Report Commissioned by the Australian Health Minsters Advisory Committee.\": {\r\n    \"referenceKey\": \"ref_25\",\r\n    \"referenceId\": 25,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Hine T, et al. Aust N Z J Obstet Gynecol 2018; 1-7.\": {\r\n    \"referenceKey\": \"ref_26\",\r\n    \"referenceId\": 26,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Food Standards Australia New Zealand, Monitoring the Australian population’s intake of dietary iodine before and after mandatory fortification, June 2016.\": {\r\n    \"referenceKey\": \"ref_27\",\r\n    \"referenceId\": 27,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Gallego G, Goodall S and Eastman CJ, Med J Aust 2010; 192(8):461-463\": {\r\n    \"referenceKey\": \"ref_28\",\r\n    \"referenceId\": 28,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Picciano MF, J Nutr 2003; 133(6):1997S-2002S.\": {\r\n    \"referenceKey\": \"ref_29\",\r\n    \"referenceId\": 29,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Turner E, Nutrition during Pregnancy. In: Modern Nutrition in Health and Disease, 10th Edition. 2006.\": {\r\n    \"referenceKey\": \"ref_30\",\r\n    \"referenceId\": 30,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Australian Bureau of Statistics. Australian Health Survey. Usual Nutrient Intakes, 2011-12. Media release 2015. Available at www.abs.gov.au (Accessed October 2018).\": {\r\n    \"referenceKey\": \"ref_31\",\r\n    \"referenceId\": 31,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Khalafallah A, et al. J Int Med 2010; 268(3): 286-95.\": {\r\n    \"referenceKey\": \"ref_32\",\r\n    \"referenceId\": 32,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Milman N, et al. Acta Obstet Gynecol Scand 1999; 78:749-757.\": {\r\n    \"referenceKey\": \"ref_33\",\r\n    \"referenceId\": 33,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Cogswell ME, et al. Am J Clin Nutr 2003; 78:773-81.\": {\r\n    \"referenceKey\": \"ref_34\",\r\n    \"referenceId\": 34,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Sifakis S and Pharmakides G, Ann NY Acad Sci 2000; 900:125-36.\": {\r\n    \"referenceKey\": \"ref_35\",\r\n    \"referenceId\": 35,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Milman N, J Pregnancy 2012; 2012:514345.\": {\r\n    \"referenceKey\": \"ref_36\",\r\n    \"referenceId\": 36,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Milman N, Acta Obstet Gynecol Scand 2005; 84:238-247.\": {\r\n    \"referenceKey\": \"ref_37\",\r\n    \"referenceId\": 37,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Cziezel AE, N Engl J Med 1992. 39. BAYER HCP research, IPSOS 2017.\": {\r\n    \"referenceKey\": \"ref_38\",\r\n    \"referenceId\": 38,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://\": {\r\n    \"referenceKey\": \"ref_39\",\r\n    \"referenceId\": 39,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Aitken RJ, Int J Androl 2006; 92: 69-75.\": {\r\n    \"referenceKey\": \"ref_40\",\r\n    \"referenceId\": 40,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Donnelly ET, et al. Mutagenesis 1999; 14(5):505-11.\": {\r\n    \"referenceKey\": \"ref_41\",\r\n    \"referenceId\": 41,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Griswold TBD.\": {\r\n    \"referenceKey\": \"ref_42\",\r\n    \"referenceId\": 42,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Vutyavanich T, et. al. Obstet & Gynecol 2001; 97(4): 577-582.\": {\r\n    \"referenceKey\": \"ref_43\",\r\n    \"referenceId\": 43,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Sahakian V, et. al. Obstet & Gynecol 1991; 78(1); 33-6.\": {\r\n    \"referenceKey\": \"ref_44\",\r\n    \"referenceId\": 44,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Vutyavanich T, et. al. Am J Obstet & Gynecol 1994; 173(3) Part 1.\": {\r\n    \"referenceKey\": \"ref_45\",\r\n    \"referenceId\": 45,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"content://Smith C, et al. Ev-B Obstet & Gynecol 2005; 7: 60-61.\": {\r\n    \"referenceKey\": \"ref_46\",\r\n    \"referenceId\": 46,\r\n    \"allowDistribution\": false\r\n  },\r\n  \"shared/references/Nutrition-Pregnancy-Brochure.pdf\": {\r\n    \"title\": \"Nutrition & Pregnancy Booklet\",\r\n    \"allowDistribution\": true\r\n  },\r\n  \"shared/references/Menevit-leave-behind.pdf\": {\r\n    \"title\": \"Menevit Summary\",\r\n    \"allowDistribution\": true\r\n  },\r\n  \"shared/references/Elevit-Breastfeeding-leave-behind.pdf\": {\r\n    \"title\": \"Breastfeeding Summary\",\r\n    \"allowDistribution\": true\r\n  },\r\n  \"shared/references/Elevit-Leave-Behind.pdf\": {\r\n    \"title\": \"Elevit Summary\",\r\n    \"allowDistribution\": true\r\n  },\r\n  \"shared/references/Elevit-HCP-Range-Brochure.pdf\": {\r\n    \"title\": \"Elevit Range Brochure\",\r\n    \"allowDistribution\": true\r\n  }\r\n}");