
### Version 0.9.1 2018-03-29
- update styles according to csslint

### Version 0.9.0 2015-09-22
- Fixed link for nested array
- Added trigger property

### Version 0.8.5 2015-08-12
- Fixed placement settings
- Small fix to 'slideshows' setting

### Version 0.8.0 2015-08-11
- Added 'slideshows' setting
