### Version 0.6.0 2018-03-21
- Fixed wrong behaviour on exit state when unlock presentation.

### Version 0.6.1 2018-04-17
- Add functionality for set 'state-default' for popup (The same as slide). Variable for this class is defaultClassSlide and set this class if slide hasn't active state.
