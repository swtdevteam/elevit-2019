app.register('ag-overlay', function () {
	return {
		publish: {
			width: '80%',
			height: '80%',
			noBackground: false,
			noCloseBtn: false,
			content: 'No content available',
			delay: 0,
			type: 'popup',
			defaultClassSlide: 'state-default'
		},
		events: {
			'tap .ag-overlay-close': 'close'
		},
		states: [
			{
				id: 'ag-overlay-open',
				onEnter: function () {
					app.lock();
					app.trigger('opening:overlay', {id: this.id, slideId: this.slideId});

					if (this.slideId) {
						setTimeout(function () {
							this.runSlideMethod(this.slideId, 'onEnter');
							app.slide.trigger('slide:enter', {id: this.slideId, type: this.props.type});
							app.trigger('opened:overlay', {id: this.id, slideId: this.slideId});
						}.bind(this), this.props.delay);
					} else {
						app.trigger('opened:overlay', {id: this.id});
					}
				},
				onExit: function () {
					app.trigger('closing:overlay', {id: this.id, slideId: this.slideId});

					setTimeout(function (slideId) {
						if (slideId) {
							this.runSlideMethod(slideId, 'onExit');
							app.slide.trigger('slide:exit', {id: slideId, type: this.props.type});
							app.slide.remove(slideId, true);
						}
						delete this.slideId;
						app.unlock();
						app.trigger('closed:overlay', {id: this.id, slideId: slideId});
					}.bind(this, this.slideId), this.props.delay);
				}
			}
		],
		runSlideMethod: function (slideId, methodName) {
			var slideData = app.slide.get(slideId);
			var slideElement;

			if (slideData && slideData[methodName]) {
				slideElement = slideData.el;
				slideData[methodName](slideElement);
				if (!slideData.getState()) slideElement.classList.add(this.props.defaultClassSlide);
			}
		},
		close: function () {
			this.reset();
		},
		// Load a slide into the overlay
		_loadPopup: function (slideId) {
			this.slideId = slideId;
			this.$('.ag-overlay-content')[0].innerHTML = '';
			app.dom.insert([{id: this.slideId}], false, this.$('.ag-overlay-content')[0]);
			this.goTo('ag-overlay-open');
		},
		load: function (slideId) {
			if (slideId === this.slideId) return;

			if (this.slideId) {
				this.reset();
				if (this._currentLoad) {
					app.off('close:overlay', this._currentLoad, app);
				}
				this._currentLoad = this._loadPopup.bind(this, slideId);
				app.once('close:overlay', this._currentLoad, app);
			} else {
				this._loadPopup(slideId);
			}
		},
		// Open provided HTML
		open: function (content) {
			var localContent = content || this.props.content;
			if (localContent) {
				this.$('.ag-overlay-content')[0].innerHTML = localContent;
			}
			this.goTo('ag-overlay-open');
		},
		setDimensions: function (width, height) {
			var contentEl = this.$('.ag-overlay-content')[0];
			var closeBtn = this.$('.ag-overlay-x')[0];
			var wMargin;
			var hMargin;
			var wUnit = '%';
			var hUnit = '%';
			// Assume percentage
			if (width < 101) {
				wMargin = (100 - width) / 2;
			}
			// Assume percentage
			if (height < 101) {
				hMargin = (100 - height) / 2;
			}

			if (contentEl) {
				contentEl.style.top = hMargin + hUnit;
				contentEl.style.bottom = hMargin + hUnit;
				contentEl.style.left = wMargin + wUnit;
				contentEl.style.right = wMargin + wUnit;
			}
			if (contentEl) {
				closeBtn.style.top = (hMargin - 1) + hUnit;
				closeBtn.style.right = (wMargin - 1) + wUnit;
			}
		},
		onRender: function () {
			var content = this.el.innerHTML;
			var html = '';
			if (!this.props.noBackground) {
				html = '<div class="ag-overlay-background ag-overlay-close"></div>';
			}
			html += '<div class="ag-overlay-content">';
			html += content;
			html += '</div>';
			if (!this.props.noCloseBtn) {
				html += '<div class="ag-overlay-x ag-overlay-close" data-popover="closePopup/bottomCenterCustom1/bottom"></div>';
			}
			this.el.innerHTML = html;
		},
		onRemove: function () {
		},
		onEnter: function () {
		},
		onExit: function () {
		}
	};
});
