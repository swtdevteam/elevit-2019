### Version 0.10.1 2018-06-28
### Dryzhuk Andrii
- change ag to window.ag in monitor usage function

### Version 0.10.0 2016-05-06
- Fixed a bug that prevented using more than one instance of the video-module.
- Fixed positionting of the video-controls.

### Version 0.9.7 2015-11-02
- Fixed minor bug that could result in loop if removing element

### Version 0.9.5 2015-09-22
- Added monitoring
