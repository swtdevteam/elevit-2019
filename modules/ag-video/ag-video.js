app.register("ag-video", function() {

    // This module makes videos stateful so that they'll work remotely
    // ... and fix some layering issues that default controls cause
    // Usage: <div src="assets/myvideo.mp4" data-module="ag-video">

    var self = null;
    var videoEles = [];
    var initialized = false;
    return {
        publish: {
            src: "",
            poster: "",
            type: "video/mp4",
            monitor: false
        },
        events: {
            "playing video": function() {this.goTo('play')},
            "pause video": function() {this.goTo('pause')},
            "tap .ag-video-play-toggle": 'toggleVideo',
            "tap .ag-upper-click-layer": 'toggleVideo',
            "tap .ag-video-fullscreen-button": 'toggleFullscreen',
            "swipeleft .ag-video-controls": 'noSwipe',
            "swiperight .ag-video-controls": 'noSwipe',
            "swipeleft": 'checkIfSwipe',
            "swiperight": 'checkIfSwipe',
            "swipeup": 'checkIfSwipe',
            "swipedown": 'checkIfSwipe',
            "onDrop .ag-video-seek-ele": 'handleDrop'
        },
        states: [
            {
                id: 'play',
                onEnter: function() {
                    this.el.videoEle.play();
                }
            },
            {
                id: 'pause',
                onEnter: function() {
                    this.el.videoEle.pause();
                    if (this.props.monitor) {
                        window.ag.submit.data({
                            category: "Video viewed",
                            label: this.props.src,
                            value: this.formatTime(this.el.videoEle.currentPlayTime),
                            valueType: "time",
                            path: app.getPath(),
                            unique: true
                        });
                    }
                }
            }
        ],
        onRender: function(el) {
            self = this;

            this.el.videoEle = this.el.querySelector('.ag-video-element');
            this.el.videoEle.currentPlayTime = 0;
            this.el.videoEle.progressBar = this.el.querySelector('.ag-video-progress-bar');
            this.el.videoEle.currentTimeText = this.el.querySelector('.ag-video-current-time');
            this.el.videoEle.endTimeText = this.el.querySelector('.ag-video-total-time');
            this.el.videoEle.videoContainer = this.el.querySelector('.ag-video-container');
            this.el.videoEle.seekEle = this.el.querySelector('.ag-video-seek-ele');
            this.el.videoEle.progressBarContainer = this.el.querySelector('.ag-video-progress-container');

            // this.el.videoEle.addEventListener('error', this.fallBackSrc.bind(this));
            this.el.videoEle.src = this.props.src;
            this.el.videoEle.poster = this.props.poster;
            this.el.videoEle.type = this.props.type;
            this.el.videoEle.preload = "none";

            // Make sure play and pause is passed on if handled with default controls
            this.el.videoEle.addEventListener('playing', function(e) {
                if(e.target == self.el.videoEle){
                    if (self.stateIsnt('play')){
                        self.goTo('play');
                    }
                }
            });
            this.el.videoEle.addEventListener('pause', function(e) {
                if(e.target == self.el.videoEle){
                    if (self.stateIsnt('pause')){
                        self.goTo('pause');
                    }
                }
            });
            this.el.videoEle.addEventListener('timeupdate', this.setTime);

            self.el.videoEle.seekHandle = new Draggy(self.el.videoEle.seekEle, {
                restrictY: true, limitsX: [0, self.el.videoEle.progressBarContainer.offsetWidth], onChange: self.moveSeekHandle
            });

            videoEles.push(this.el.videoEle);

            setTimeout(function(){
                self.updateSeekHandle();
            },100);

            if(!initialized){
                app.listenTo(app, 'close:overlay', this.cleanOnOverlayExit);
                document.addEventListener('webkitfullscreenchange', this.addFullscreenClass, false);
                document.addEventListener('mozfullscreenchange', this.addFullscreenClass, false);
                document.addEventListener('fullscreenchange', this.addFullscreenClass, false);
                document.addEventListener('MSFullscreenChange', this.addFullscreenClass, false);
                window.addEventListener('resize', this.updateSeekHandle, false);
                initialized = true;
            }
        },
        // If can't load src as specified, then try to load from "slides/[parentId]/[src specified]"
        // This is to make it work if lazyloaded
        // TODO: use slides path in config
        fallBackSrc: function(event) {
            var el = event.target;
            // el.removeEventListener('error', this.fallBackSrc.bind(this));
            el.src = "slides/" + this.parentId + "/" + this.props.src;
        },
        cleanOnOverlayExit: function(e) {
            // Checks that the exited slide from the overlay has videoelements and if so removes them from videoEles on exit
            var slideEle = null, videoEle = null, videos = null;
            if(e.slideId){
                slideEle = app.slide.get(e.slideId);
                if(slideEle){
                    videos = slideEle.el.querySelectorAll('.ag-video-element');
                    if(videos[0]){
                        for (var i = 0; i < videos.length; i++) {
                            videos[i].removeEventListener('timeupdate', self.setTime);
                            videoEles.pop();
                        };
                    }
                }
            }
        },
        onEnter: function(el) {

        },
        onExit: function(el) {
            if (this.stateIs('play')) this.goTo('pause');
            this.reset();
        },
        onRemove: function() {
        },
        noSwipe: function(e){
            e.stopPropagation();
        },
        checkIfSwipe: function(e){
            // prevent swiping in fullscreen mode
            for(ele in videoEles){
                if(videoEles[ele].classList.contains('ag-video-fullscreen'))
                    self.noSwipe(e);
            }
        },
        toggleVideo: function(e){
            if(this.stateIs('play'))
                this.goTo('pause');
            else
                this.goTo('play');
        },
        setTime: function(e) {
            e.target.currentPlayTime = e.target.currentTime;
            self.updateProgress(e.target);
        },
        updateProgress: function(ele, time) {
            ele.currentPlayTime = time || ele.currentPlayTime;
            var seekHandlePos = 0;
            var value = 0;

            if (ele.currentPlayTime > 0) {
                value = Math.floor((100 / ele.duration) * ele.currentPlayTime);
            }

            var videoCurrentTime = this.formatTime(ele.currentPlayTime);
            var videoEndTime = this.formatTime(ele.duration);

            ele.currentTimeText.innerHTML = videoCurrentTime;
            ele.endTimeText.innerHTML = videoEndTime;

            ele.progressBar.style.width = value + "%";

            seekHandlePos = value * 0.01 * ele.progressBarContainer.offsetWidth;

            ele.seekHandle.moveTo(seekHandlePos);
        },
        formatTime: function(seconds) {
            var s = Math.floor(seconds % 60),
                m = Math.floor(seconds / 60 % 60),
                h = Math.floor(seconds / 3600);

            if (h > 0) {
                h = ((h < 10) ? "0" + h : h) + ":";
            }
            else {
                h = "00:";
            }

            if (m > 0) {
                m = ((m < 10) ? "0" + m : m) + ":";
            }
            else {
                m = "00:";
            }

            // Check if leading zero is need for seconds
            s = (s < 10) ? "0" + s : s;

            return h + m + s;
        },
        toggleFullscreen: function(e){
            var videoEle = self.getVideoEle(e.target);
            var fullscreenEle = document.fullscreenElement || document.msFullscreenElement ||
                                document.mozFullScreenElement || document.webkitCurrentFullScreenElement;

            var userAgent = navigator.userAgent || navigator.vendor || window.opera;

            if(userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i )){
                if (videoEle.webkitEnterFullscreen){
                    videoEle.webkitEnterFullscreen();
                }
            }
            else{
                if (videoEle.videoContainer.requestFullscreen) {
                    if(fullscreenEle)
                        document.exitFullscreen();
                    else
                        videoEle.videoContainer.requestFullscreen();
                }
                else if (videoEle.videoContainer.msRequestFullscreen) {
                    if(fullscreenEle)
                        document.msExitFullscreen();
                    else
                        videoEle.videoContainer.msRequestFullscreen();
                }
                else if (videoEle.videoContainer.mozRequestFullScreen) {
                    if(fullscreenEle)
                        document.mozCancelFullScreen();
                    else
                        videoEle.videoContainer.mozRequestFullScreen();
                }
                else if (videoEle.videoContainer.webkitRequestFullscreen) {
                    if(fullscreenEle)
                        document.webkitCancelFullScreen();
                    else
                        videoEle.videoContainer.webkitRequestFullscreen();
                }
            }
        },
        addFullscreenClass: function(){
            var fullscreenEle = document.fullscreenElement || document.msFullscreenElement ||
                                document.mozFullScreenElement || document.webkitCurrentFullScreenElement;
            for(ele in videoEles){
                if (fullscreenEle) {
                    videoEles[ele].classList.add('ag-video-fullscreen');
                }
                else{
                    videoEles[ele].classList.remove('ag-video-fullscreen');
                    self.updateSeekHandle();
                }
            }
        },
        updateSeekHandle: function(){
            // draggy - changes limits and position on window resize
            for(ele in videoEles){
                videoEles[ele].seekHandle.config.limitsX[1] = videoEles[ele].progressBarContainer.offsetWidth;
                var seekHandlePos = videoEles[ele].currentTime / videoEles[ele].duration * videoEles[ele].progressBarContainer.offsetWidth;
                videoEles[ele].seekHandle.moveTo(seekHandlePos);
            }
        },
        moveSeekHandle: function(x, y){
            // updates progressbar
            var videoEle = self.getVideoEle(this);
            var currentPos = x / videoEle.progressBarContainer.offsetWidth;
            self.updateProgress(videoEle, videoEle.duration * currentPos);
        },
        handleDrop: function(e) {
            // moves handle
            var videoEle = self.getVideoEle(e.target);
            var pos = e.target.position;
            var currentPos = pos[0] / videoEle.progressBarContainer.offsetWidth;
            videoEle.currentTime = videoEle.duration * currentPos;
        },
        getVideoEle: function (el) {
            while ((el = el.parentElement) && !el.classList.contains("ag-video-container"));
            var vid = el.querySelector('.ag-video-element');
            return vid;
        }
    }

});
