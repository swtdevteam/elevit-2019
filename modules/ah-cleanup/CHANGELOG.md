### Version 1.0.1
### 2018-03-06 - A.Shapovalova
- slide:enter event replaced with update:anthill-current to fix appearing slide without background, which then appears some time later

### Version 1.0.0
### 2018-02-27 - Nina Koval
- module was updated according to jslint rules

### Version 0.5.0
### 2016-06-22 - Roman Kovalenko
- version of module ag-cleanup, on which ah-cleanup is based
