ah-cleanup
============

Module cleans number of visited slides (number is set in property limit) from stack every time period (time period is set in timeout property)

## How to add to the project:
  Copy folder `ah-cleanup` to `app/modules/rainmaker_modules` then add this markup `div(data-module="ah-cleanup")` to `index.pug`.

Add links to `platforms/rainmaker/presentation.json` to modules object:
```
"ah-cleanup": {
  "id": "ah-cleanup",
  "files": {
    "scripts": [
      "modules/rainmaker_modules/ah-cleanup/ah-cleanup.js"
    ]
  }
}
```
