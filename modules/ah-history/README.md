# ah-history module
-------------------------------------
### Description
##### This module the triggered event when current view (slide or popup) updated.
-------------------------------------

### How to use:

##### Include module to presentation.json
```
"ah-history": {
    "id": "ah-history",
    "files": {
        "scripts": [
            "modules/ah-history/ah-history.js"
        ]
    }
 }
 ```    
##### Include module to index.html
```
      <div data-module="ah-history" id="ahHistory"></div>
```


### Author
Agent Developer

## Changelog

```
*   v.2.0.1
        01.10.2018 - Coffee converted to js, module renamed to ah-history
*   v.2.0.0
        Created ah-history module
```