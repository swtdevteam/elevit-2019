ah-popup-management
=========

Module for management popups in BHC.

## Usage

## Settings

The following property can be set on the above element:
- overlay STRING for setting overlay id
Note: delay is by default as in ag-overlay.

1) Include following tag in master template (index.html):

    <div data-module="ag-overlay" id="ag-overlay-for-bhc" width="100%" height="100%"></div>
    <div id="ah-popup-management" data-module="ah-popup-management"></div>

2) Open a popup as following:

-
    ahPopup = app.module.get('ah-popup-management')
    ahPopup.openPopup('popupId')
