ah-storage-data
=========

Module for saving and loading data from local storage.

## Usage

#### 1) Include module to presentation.json 

```

"ah-storage-data": {
    "id": "ah-storage-data",
    "files": {
        "scripts": [
            "modules/ah-storage-data/ah-storage-data.js"
        ]
    }
}

 ```   
 

#### 2) Include following tag in Accelerator element in master template:

```#apStorageData(data-module="ah-storage-data" storage-id="test")```

#### 3) Get module: 

```apStorage = app.module.get 'apStorageData'```

#### 4) Use the following functions:

```All data saves in local storage in field whith key specified by storage-id. All next data will be there. Path template: localStorage[storage-id][your key]```

- ```apStorage.add(key,data)``` (will add data to local storage in filed with defined key)
- ```apStorage.remove(key)``` (will remove filed with following key)
- ```apStorage.getStorageData(key)``` (will return all data as object, or data by key if it defined)
- ```apStorage.init()``` (will initialize local storage)

### Author
Andrii Romanyshyn

## Changelog

```
*   v.1.0.1
        01.10.2018 - Module renamed to ah-storage-data
*   v.1.0.0
        Created ah-storage-data module
```