/**
 * Storage data module
 * -------------------------------------
 *
 * Module for saving and loading data from local storage.
 *
 * @module ah-storage-data.js
 * @author Agent Developer
 */
app.register('ah-storage-data', function () {
	return {
		publish: {
			storageId: 'BHCTemplateDefaultStorageId'
		},
		onRender: function () {
			this.init();
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-storage-data',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		init: function () {
			if (!this.getStorageData()) {
				sessionStorage.setItem(this.props.storageId, '{}');
			}
		},
		getStorageData: function (key) {
			var data = JSON.parse(sessionStorage.getItem(this.props.storageId));
			if (key) {
				data = data[key];
			}
			return data;
		},
		add: function (key, data) {
			var sd = this.getStorageData();
			if (typeof key !== 'string' && typeof key !== 'number') {
				// eslint-disable-next-line
				console.error('Wrong key');
				return;
			}
			sd[key] = data;
			this.save(sd);
		},
		save: function (data) {
			try {
				sessionStorage.setItem(this.props.storageId, JSON.stringify(data));
			} catch (e) {
				// eslint-disable-next-line
				console.error('Storage is not available!');
			}
		},
		remove: function (key) {
			var sd = this.getStorageData();
			if (!sd[key]) {
				// eslint-disable-next-line
				console.error('Wrong key');
				return;
			}
			delete sd[key];
			this.save(sd);
		}
	};
});
