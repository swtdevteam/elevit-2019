# ah-tab-group module

## Usage
**This module is part of training overlay and is working in conjunction with "ap-exit-popup", "ah-tour", "ah-tour-configurator", "ah-tutorial" and "ah-tutorial-controls" modules**

- Add the following code to the presentation json file
```
"ah-tab-group": {
    "id": "ah-tab-group",
    "files": {
        "scripts": [
            "modules/ah-tab-group/ah-tab-group.js",
            "modules/ah-tab-group/tab-states.js"
        ],
        "styles": [
            "modules/ah-tab-group/ah-tab-group.css"
        ]
    }
}

#### How using
     Example:
``` 
#tabInstanceId.tab-group(data-module="ah-tab-group")
    nav.tg-nav
        button.tg-tab(data-tab="first") tab 1
        button.tg-tab(data-tab="second") tab 2
    .tg-content(data-tab="first")
    .tg-content(data-tab="second")
```

#### For interaction states, use class 'TabStates' on your slide (It is not necessarily)
state IDs must be same in attr 'data-tab'
```
class SlideClass
  constructor: ()->
    @states = [
      {id: 'first'}
      {id: 'second'}
    ]

  onRender: (element)->
    new TabStates 'tabInstanceId', element.id
```    
    

### Author
Agent Developer

## Changelog

```
*   v.0.0.2
        01.10.2018 - Coffee, styl converted to js, css, module renamed to ah-tab-group
*   v.0.0.1
        16.12.2016 - Created tab-group module
```