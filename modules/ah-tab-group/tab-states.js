function TabStates(tabInstanceId, slideId) {
	this.tabInstanceId = tabInstanceId;
	this.slideId = slideId;
	this.slide = app.slide.get(this.slideId);
	this.tabGroup = app.module.get(this.tabInstanceId);
	this.references = app.module.get('references');
	setTimeout(function () {
		this._init();
	}.bind(this), 0);
}

TabStates.prototype._init = function () {
	this.slideStatesIds = this.slide.states.map(function (state) {
		return state.id;
	});
	this.tabGroupStatesIds = Object.keys(this.tabGroup.map);
	this._curTime = new Date();
	this.tabGroup.change = function (tabId) {
		if (this.slideStatesIds.indexOf(tabId) !== -1) {
			this.slide.goTo(tabId);
		}
	}.bind(this);
	return app.slide.on('state:enter', function (data) {
		if (data.view === this.slideId && this.tabGroupStatesIds.indexOf(data.id) !== -1) {
			this.tabGroup.goTo(data.id);
			app.trigger('tabstate:change', this);
		}
	}.bind(this));
};