ah-tour-configurator
======================
This module allows to set Tutorial Tour configuration.

## Usage
**This module is part of training overlay and is working in conjunction with "ap-exit-popup", "ah-tab-group", "ah-tour", "ah-tutorial" and "ah-tutorial-controls" modules**

- Add the following code to the presentation json file
```
"ah-tour-configurator": {
    "id": "ah-tour-configurator",
    "files": {
        "scripts": [
            "modules/ah-tour-configurator/ah-tour-configurator.js",
            "modules/ah-tour-configurator/tutorial-configurator.js"
        ]
    }
}
 ```
- Add the following tag to a desired location before "ah-tour", "ah-tutorial" modules:
```
      <div id="ahTourConfigurator" data-module="ah-tour-configurator"></div>
```

## Changelog
### Version 0.0.2 28.09.2018
- Renaming to ah-tour, converting coffee to js, styl to css, pug to html, fixing jslint, stylelint errors
### Version 0.0.1 29.12.2017
- Initial creation
