ah-tour
======================
This module allows to create Tutorial Tour.

## Usage
**This module is part of training overlay and is working in conjunction with "ap-exit-popup", "ah-tab-group", "ah-tour-configurator", "ah-tutorial" and "ah-tutorial-controls" modules**

- Add the following code to the presentation json file
```
"ah-tour": {
    "id": "ah-tour",
    "files": {
        "styles": [
            "modules/ah-tour/tour.css",
            "modules/ah-tour/custom-popover.css"
        ],
        "scripts": [
            "modules/ah-tour/ah-tour.js",
            "modules/ah-tour/tutorial-tour.js"
        ],
        "templates": [
            "modules/ah-tour/custom-popover.html",
            "modules/ah-tour/template.html"
        ]
    }
}
 ```
- Add the following tag to a desired location with the "ah-tour-configurator" module already placed, "ah-tutorial" should follow "ah-tour":
```
      <div id="ahTour" data-module="ah-tour"></div>
```

# Tutorial tour module description

### Data
#### Text (tour-strings.json)
##### Example json
```json
{ 
    "keyText":"text"
}
```
#### Tour tips data (tour-tips.json)
##### Example json
```json
{
    "offsets": {
        "center":{
            "top":50,
            "left":50,
            "x":0,
            "y":0
        }
    },
    "points":{
        "key": {
            "title": "keyText",
            "gestures": "longTouch",
            "content": "keyTextContent"
        }
    }
}
```
    'top' - value in percent relative to the element
    'left' - value in percent relative to the element
    'x' - additional pixel offset (optional parameter)
    'y' - additional pixel offset (optional parameter)
    'key' - identifies key
    'title' - text key
    'gestures' - will set this class (CamelCase)
    'content' - text key
#### Binding point
##### Example html
```html
<element data-popover="key/center"></element>
<element data-popover="key/center/right"></element>


```
    'key' - key points
    'center' - key offsets
    'right' - Specifies the popover position. (by default 'auto')
###### Additional options
```html
<element class="scroll-container">
    <element class="scroll-wrapper">
        <element data-popover="key/center" data-visibility-area=".scroll-container"></element>
    </element>
</element>
```
    'data-visibility-area' - Defines parent selector. Popover will be created if the point will be in visibility area of '.scroll-container'
```html
    <element data-popover="key/center" data-point-class-"className"></element>
```
    data-placement="left" - className placement-left will added to point button

#### Create instanced in setup.js
##### Example javascript
```javascript
  app.tutorialTour = new TutorialTour({
    viewport: {
      selector: "#presentation",
      padding: 10
    },
    getActiveContainers: function(){}
  });
```
    'viewport' - Keeps the popover within the bounds of this element.
    'getActiveContainers' - should return array of elements of which is generated points when tour will be opened
    
#### Layout popover in template.html (should be follow the rules Bootstrap Popover)
##### Example html
```html
  <div class="popover" role="tooltip">
    <div class="arrow"></div>
    <h3 class="popover-title"></h3>
    <div class="popover-content"></div>
  </div>
```

## Changelog
### Version 0.0.2 27.09.2018
- Renaming to ah-tour, converting coffee to js, styl to css, pug to html, fixing jslint, stylelint errors
### Version 0.0.1 29.12.2017
- Initial creation
