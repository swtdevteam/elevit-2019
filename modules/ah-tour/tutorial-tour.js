var toSnakeCase = function (str) {
	return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
};
var setUnit = function (value, unit) {
	var unit_ = unit || 'px';
	return value + unit_;
};

function TutorialTour(options) {
	var $wrapper;
	this.ahHistory = app.module.get('ahHistory');
	this.viewport = {
		selector: options.viewport.selector || 'body',
		padding: options.viewport.padding || 0
	};
	this.popovers = [];
	this.readData();
	this.getActiveContainers = options.getActiveContainers || function () {
		return [app.slideElements[app.currentSlide]];
	};
	this.attr = 'data-popover'; // Should be prefix 'data-'
	this.attrShower = 'data-state-popover'; // Should be prefix 'data-'
	this.attrReferences = 'data-overlay-reference';
	this.$overlayWrapper = this.createOverlayWrapper('overlayWrapper');
	this.$overlayCustomWrapper = this.createOverlayWrapper('overlayCustomWrapper');
	$wrapper = $(this.viewport.selector);
	$wrapper.append(this.$overlayCustomWrapper);
	$wrapper.append(this.$overlayWrapper);
}

TutorialTour.prototype.createOverlayWrapper = function (id) {
	var wrapper = $('<div>');
	wrapper.prop('id', id);
	return wrapper;
};
TutorialTour.prototype.getData = function (path, callback, isHTML) {
	var isHTML_ = isHTML || false;
	var _metadata = app.cache.get(path);
	if (_metadata) {
		callback(!isHTML_ ? JSON.parse(_metadata) : _metadata);
	} else {
		$.ajax({
			url: path,
			dataType: isHTML_ ? 'html' : 'json',
			success: function (data) {
				callback(data);
			},
			error: function (jqXHR, textStatus) {
				var err = {
					current: 'Error loading tutorial tour metadata file \'' + path + '\': ' + textStatus
				};
				throw err.current;
			},
			async: false
		});
	}
};
TutorialTour.prototype.readData = function () {
	if (app.isVeevaWide) {
		this.template = app.cache.get('modules/veeva_modules/ah-tour/template.html');
		this.customPopover = app.cache.get('modules/veeva_modules/ah-tour/custom-popover.html');
		this.tips = JSON.parse(app.cache.get('modules/veeva_modules/ah-tour/tour-tips.json'));
		this.references = JSON.parse(app.cache.get('modules/veeva_modules/ah-tour/overlay-references-strings.json'));
		this.strings = JSON.parse(app.cache.get('modules/veeva_modules/ah-tour/tour-strings.json'));
	}
	this.getData('modules/ah-tour/template.html', function (data) {
		this.template = data;
	}.bind(this), true);
	this.getData('modules/ah-tour/custom-popover.html', function (data) {
		this.customPopover = data;
	}.bind(this), true);
	this.getData('modules/ah-tour/tour-strings.json', function (data) {
		this.strings = data;
	}.bind(this));
	this.getData('modules/ah-tour/tour-tips.json', function (data) {
		this.tips = data;
	}.bind(this));
	this.getData('modules/ah-tour/overlay-references-strings.json', function (data) {
		this.references = data;
	}.bind(this));
};
TutorialTour.prototype.getTemplate = function (className, customClasslist) {
	var $template = $(this.template);
	$template.addClass(className);
	if (customClasslist) {
		$template.addClass(customClasslist);
	}
	return $template;
};
TutorialTour.prototype.getPosition = function ($parent, pointOffset) {
	var slideParameters = $('.present').get(0).getBoundingClientRect();
	var parentParameters = $parent.get(0).getBoundingClientRect();
	var y = pointOffset.y ? +pointOffset.y : 0;
	var x = pointOffset.x ? +pointOffset.x : 0;
	return {
		top: (parentParameters.top - slideParameters.top) + y + (parentParameters.height * pointOffset.top / 100),
		left: (parentParameters.left - slideParameters.left) + x + (parentParameters.width * pointOffset.left / 100)
	};
};
TutorialTour.prototype.getAutoPlacement = function ($parent) {
	var ICON_HEIGHT = 60;
	var $element = $parent.get(0);
	var className = $element.dataset.placement;
	var deviceParameters = document.getElementsByTagName('body')[0].getBoundingClientRect();
	var parentParameters = $element.getBoundingClientRect();
	return className || (parentParameters.bottom + ICON_HEIGHT > deviceParameters.bottom ? 'right-placement' : void 0);
};
TutorialTour.prototype.checkPlacement = function ($parent) {
	var placement = $parent.get(0).dataset.placement;
	if (placement) {
		return (placement + '-placement') || this.getAutoPlacement($parent);
	}
	return '';
};
TutorialTour.prototype.setPosition = function ($pointElement, $parent, pointOffset) {
	var position = this.getPosition($parent, pointOffset);
	$pointElement.addClass(this.checkPlacement($parent));
	if (pointOffset.placement) {
		$pointElement.addClass(pointOffset.placement);
	}
	position.top = setUnit(position.top);
	position.left = setUnit(position.left);
	$pointElement.css(position);
};
TutorialTour.prototype.createPoint = function (isCustomType) {
	var $pointElement = $('<div>');
	var className = isCustomType ? 'tour-custom-point' : 'tour-point';
	$pointElement.addClass(className);
	if (isCustomType) {
		this.$overlayCustomWrapper.append($pointElement);
	} else {
		this.$overlayWrapper.append($pointElement);
	}
	return $pointElement;
};
TutorialTour.prototype.addEvent = function ($element, isCustomType) {
	if (isCustomType) {
		$element.on('shown.bs.popover', function () {
			this.initScroll();
		}.bind(this));
	} else {
		$element.bind('tap', function (event) {
			event.stopPropagation();
			$element.popover('toggle');
		});
		$element.on('show.bs.popover', function () {
			var currentOpenedPopover = this.$currentOpenedPopover;
			if (!currentOpenedPopover || !currentOpenedPopover.is($element)) {
				if (currentOpenedPopover) {
					this.$currentOpenedPopover.popover('hide');
				}
				this.$currentOpenedPopover = $element;
				this.$currentOpenedPopover.addClass('show');
			}
		}.bind(this));
		$element.on('hide.bs.popover', function () {
			if (this.$currentOpenedPopover) {
				this.$currentOpenedPopover.removeClass('show');
			}
			this.$currentOpenedPopover = null;
		}.bind(this));
	}
};
TutorialTour.prototype.initPoint = function ($element, itemParams, offset, placement) {
	var className;
	var customClassList;
	var isCustomType = itemParams.type === 'custom';
	var $pointElement = this.createPoint(isCustomType);
	this.setPosition($pointElement, $element, offset);
	this.addEvent($pointElement, isCustomType);
	className = toSnakeCase(itemParams.gestures);
	customClassList = itemParams.classList;
	$pointElement.popover({
		placement: placement || 'auto',
		title: this.strings[itemParams.title],
		html: true,
		template: this.getTemplate(className, customClassList),
		trigger: 'manual',
		content: this.getContent(itemParams, isCustomType),
		container: this.viewport.selector,
		viewport: this.viewport
	});
	this.popovers.push($pointElement);
	if (isCustomType) {
		if (this.timerCustomPopover) {
			clearTimeout(this.timerCustomPopover);
		}
		this.timerCustomPopover = setTimeout(function () {
			$pointElement.popover('show');
		}, 150);
	}
};
TutorialTour.prototype.closeAll = function () {
	this.popovers.forEach(function ($pointElement) {
		$pointElement.popover('destroy');
		$pointElement.unbind();
		$pointElement.remove();
	});
};
TutorialTour.prototype.overlayToggle = function (isShow) {
	this.toggleShow(this.$overlayWrapper, isShow);
	this.toggleShow(this.$overlayCustomWrapper, isShow);
};
TutorialTour.prototype.toggleShow = function (wrapper, isShow) {
	wrapper[isShow ? 'addClass' : 'removeClass']('show');
};
TutorialTour.prototype.isPointInContainer = function ($element, offset, container) {
	var containerParameters;
	var pointPosition;
	var container_ = $element.closest(container).get(0);
	if (!container_) {
		throw new Error('Parent is not found');
	}
	pointPosition = this.getPosition($element, offset);
	containerParameters = container_.getBoundingClientRect();
	return pointPosition.top >= containerParameters.top && pointPosition.top <= containerParameters.bottom && pointPosition.left >= containerParameters.left && pointPosition.left <= containerParameters.right;
};
TutorialTour.prototype.checkIsVisible = function (element) {
	var elementComputedStyle = getComputedStyle(element);
	var anyInvisibleCase = [
		{
			name: 'display',
			value: 'none'
		}, {
			name: 'opacity',
			value: '0'
		}, {
			name: 'visibility',
			value: 'hidden'
		}
	].filter(function (data) {
		return elementComputedStyle[data.name] === data.value;
	})[0];
	return !anyInvisibleCase;
};
TutorialTour.prototype.show = function (element) {
	this.isShow = true;
	this.overlayToggle(this.isShow);
	this.getActiveContainers(element).forEach(function (wrapper) {
		var $wrapper = $(wrapper);
		var elements = $wrapper.find('[' + this.attr + ']').toArray();
		if ($wrapper.attr(this.attr)) {
			elements.push(wrapper);
		}
		elements.forEach(function (element_) {
			var showerAttr;
			var slide;
			var slideId;
			var arg = element_.dataset[$.camelCase(this.attr.replace('data-', ''))].split('/');
			var $element = $(element_);
			var itemParams = this.tips.points[arg[0]];
			var offset = this.tips.offsets[arg[1]];
			var placement = arg[2];
			if (element_.dataset.visibilityArea) {
				// eslint-disable-next-line
				console.log(this.isPointInContainer($element, offset, element_.dataset.visibilityArea));
				if (!this.isPointInContainer($element, offset, element_.dataset.visibilityArea)) {
					return;
				}
			}
			if (!this.checkIsVisible(element_)) {
				return;
			}
			showerAttr = element_.dataset[$.camelCase(this.attrShower.replace('data-', ''))];
			slideId = this.ahHistory.getCurrent() ? this.ahHistory.getCurrent().id : '';
			slide = app.slide.get(slideId);
			if (showerAttr && slide.getState(slide._state) && showerAttr.split(',').indexOf(slide.getState(slide._state).id) < 0) {
				return;
			}
			this.initPoint($element, itemParams, offset, placement);
		}.bind(this));
	}.bind(this));
};
TutorialTour.prototype.hide = function () {
	this.isShow = false;
	this.overlayToggle(this.isShow);
	this.closeAll();
};
TutorialTour.prototype.getContent = function (itemParams, isCustomType) {
	return isCustomType ? this.getCustomContent(itemParams.content) : this.strings[itemParams.content];
};
TutorialTour.prototype.getCustomContent = function (key) {
	return this.generateCustomContent({
		content: this.generateCustomString(key)
	});
};
TutorialTour.prototype.generateCustomString = function (key) {
	var content = this.strings[key];
	var result = [];
	Object.keys(content).forEach(function (key_) {
		content[key_].classes = this.parseClasses(key_);
		result.push(content[key_]);
	}.bind(this));
	return result;
};
TutorialTour.prototype.parseClasses = function (key) {
	var array = key.split('-');
	array.shift();
	return array.join(' ') || '';
};
TutorialTour.prototype.generateCustomContent = function (myViewModel) {
	var htmlObject = document.createElement('div');
	htmlObject.innerHTML = this.customPopover;
	window.ko.applyBindings(myViewModel, htmlObject);
	this.injectReferences(htmlObject);
	return htmlObject.innerHTML;
};
TutorialTour.prototype.initScroll = function () {
	var scrollsElements = Array.prototype.slice.call(document.querySelectorAll('.custom-popover-wraptutorialper'), 0);
	var scroll;
	var scrolls = [];
	// eslint-disable-next-line
	console.log(scrollsElements);
	scrollsElements.forEach(function (element) {
		var scroller = element.children[0];
		if (scroller.offsetHeight > element.offsetHeight) {
			/* eslint no-new: "error"*/
			scroll = new IScroll(element, {
				scrollY: true,
				scrollbars: 'custom',
				interactiveScrollbars: true,
				snap: '.item'
			});
			scrolls.push(scroll);
			['swipeup', 'swipedown'].forEach(function (eventName) {
				element.addEventListener(eventName, function (event) {
					event.stopPropagation();
				});
			});
			element.classList.add('show-scroll');
		}
	});
};
TutorialTour.prototype.injectReferences = function (el) {
	var refEls = el.querySelectorAll('[' + this.attrReferences + ']');
	Array.prototype.forEach.call(refEls, function (refEl) {
		var refKey = refEl.getAttribute(this.attrReferences);
		refEl.innerText = this.references[refKey];
	}.bind(this));
};
