/**
 * Training overlay button functionality on menu toolbar
 * -------------------------------------
 *
 * This module allows to set button functionality on menu toolbar for training overlay.
 * Training overlay modules:
 *    ap-exit-popup
 *    ah-tab-group
 *    ah-tour
 *    ah-tour-configurator
 *    ah-tutorial
 *    ah-tutorial-controls
 *
 * @module ah-tutorial-controls.js
 * @requires ap-toolbar.js, training overlay modules
 * @author Serhii.Zv, Mykola Fant
 */
app.register('ah-tutorial-controls', function () {
	return {
		publish: {
			'active': 'active',
			'disable': 'disable-bhc'
		},
		events: {
			'tap .tutorial': 'toggleTour'
		},
		states: [],
		onRender: function () {
			this.tutorial = this.el.getElementsByClassName('tutorial')[0];
			this.presentation = document.getElementById('presentation');
			this.toolbarModule = app.module.get('apToolbar');
			app.on('close:tour', function () {
				this.resetTour();
			}.bind(this));
			if (app.isVeevaWide) {
				app.once('view-enter', function () {
					if (this.isActiveTour()) {
						this.setControlActive();
					}
				}.bind(this));
			}
			this.monitorUsage();
		},
		resetTour: function () {
			this.resetActive();
			this.enableBhc();
		},
		isActiveTour: function () {
			var storageData;
			storageData = app.tutorialConfigurator.storageData;
			return storageData.isActiveTour || storageData.isActiveTutorial;
		},
		setControlActive: function () {
			this.toggleActive();
			this.toggleDisableBhc();
		},
		toggleTour: function () {
			app.trigger('toggle:tour');
			this.toolbarModule.hide();
			this.setControlActive();
		},
		toggleDisableBhc: function () {
			this.presentation.classList.toggle(this.props.disable);
		},
		enableBhc: function () {
			this.presentation.classList.remove(this.props.disable);
		},
		toggleActive: function () {
			this.tutorial.classList.toggle(this.props.active);
		},
		setActive: function () {
			this.tutorial.classList.add(this.props.active);
		},
		resetActive: function () {
			this.tutorial.classList.remove(this.props.active);
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-tutorial-controls',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		}
	};
});
