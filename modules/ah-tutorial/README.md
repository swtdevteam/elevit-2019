ah-tutorial
======================
This module allows to create Tutorial tour

## Usage
**This module is part of training overlay and is working in conjunction with "ap-exit-popup", "ah-tab-group", "ah-tour", "ah-tour-configurator" and "ah-tutorial-controls" modules**

- Add the following code to the presentation json file
```
"ah-tutorial": {
    "id": "ah-tutorial",
    "files": {
        "templates": [
            "modules/ah-tutorial/tutorial.html"
        ],
        "scripts": [
            "modules/ah-tutorial/ah-tutorial.js",
            "modules/ah-tutorial/tutorial.js"
        ],
        "styles": [
            "modules/ah-tutorial/tutorial.css"
        ]
    }
}
 ```
- Add the following tag to a desired location with the "ah-tour", "ah-tutorial" module already placed:
```
      <div id="tutorial" data-module="ah-tutorial"></div>
```

## Changelog
### Version 0.0.2 28.09.2018
- Renaming to ah-tutorial, converting coffee to js, stylelint errors fix
### Version 0.0.1 29.12.2017
- Initial creation
