/**
 * Creates Tutorial instance.
 * ---------------------------------
 * This module allows to create Tutorial tour.
 * Training overlay modules:
 *    ap-exit-popup
 *    ah-tab-group
 *    ah-tour
 *    ah-tour-configurator
 *    ah-tutorial
 *    ah-tutorial-controls
 *
 * @module ah-tutorial.js
 * @requires tutorial.js, knockout.js, training overlay modules
 * @author Serhii.Zv, Mykola Fant
 */
app.register('ah-tutorial', function () {
	return {
		onRender: function () {
			var tutorialStrings = {};
			var getData = window.TutorialTour.prototype.getData;
			getData('modules/ah-tutorial/tutorial-strings.json', function (data) {
				tutorialStrings = data;
			});
			app.tutorial = new window.Tutorial(tutorialStrings);
			window.ko.applyBindings(app.tutorial, this.el);
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-tutorial',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		}
	};
});
