function Tutorial(data) {
	this.data = data;
	this.tour = app.module.get('ahTour');
	this.tutorialStep = window.ko.observable();
	this.activeTutorial = window.ko.observable();
	this.tutorialWrapper = document.querySelector('#tutorial');
	app.on('toggle:tour', function () {
		this.toggleTutorial();
	}.bind(this));
	app.on('close:tour', function () {
		this.closeTour();
	}.bind(this));
}

Tutorial.prototype.closeTour = function () {
	app.tutorialTour.hide();
	this.trigger();
};

Tutorial.prototype.goTour = function () {
	this.closeTutorial();
	event.stopPropagation();
	this.tour.openTour();
};

Tutorial.prototype.goInfoStep = function () {
	this.tutorialStep('tutorial_1');
};

Tutorial.prototype.toggleTutorial = function () {
	if (this.tour.activeTour()) {
		return;
	}
	this.activeTutorial(!this.activeTutorial());
	this.goInfoStep();
	this.tutorialWrapper.classList.toggle('active-tutorial');
	this.trigger();
};

Tutorial.prototype.closeTutorial = function () {
	this.activeTutorial(false);
	this.tutorialStep('');
	this.tutorialWrapper.classList.remove('active-tutorial');
	this.trigger();
};

Tutorial.prototype.fastCloseTour = function () {
	this.closeTutorial();
	app.trigger('close:tour');
};

Tutorial.prototype.trigger = function () {
	return app.trigger('tutorial:toggle', {
		isActive: this.activeTutorial()
	});
};
