### Version 0.0.1 2018-06-28
- Initial version

### Version 1.0.2 2018-10-09
- Removing code duplication with ap-auto-references module
- Creating popup on render and populating it with content on slide enter
- Receiving reference indexes from ap-auto-references module on 'refindexes:created' event
- Refactoring
