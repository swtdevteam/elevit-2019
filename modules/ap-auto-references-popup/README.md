# ap-auto-references-popup

##### Implements auto reference popups. 
##### Adds tap handlers to every element that has the [data-reference-id] data attribute.
##### On tap a popup with a list of all references in the slide appears.
##### The module is loaded within index.html
##### @requires jquery.js, touchy.js

### How to use:

```
 "ap-auto-references-popup": {
            "id": "ap-auto-references-popup",
            "files": {
                "templates": [],
                "scripts": [
                    "modules/ap-auto-references-popup/ap-auto-references-popup.js"
                ],
                "styles": [
                    "modules/ap-auto-references-popup/ap-auto-references-popup.css"
                ]
            }
        }
```

## Changelog
```
* v.1.0.1
    2018-07-17 - Oleksandr Melnyk
        refactoring:
            fix css linter errors
            
* v.1.0.2
    2018-07-17 - Vladyslav Kodenko
        Use in conjunction with 'ap-auto-references' module with flag 'injectrefstopopup'
        added to 'ap-auto-references' module
        
        update:
          Removing code duplication with ap-auto-references module
          Creating popup on render and populating it with content on slide enter
          Receiving reference indexes from ap-auto-references module on 'refindexes:created' event
```
