# ap-auto-side-clip

##### Implements auto sideclips.

  Listens to agnitios "slideEnter" event. When this event is triggered, this module looks for DOM elements with the "auto-side-clip" class:
 ```
       <div class="auto-side-clip"> SIDE CLIP CONTENT </div>
 ```
  and builds a side clip around it.
  The generated side clips adapt their width and height to their content up to a limit. After that, they start being scrollable.

 ##### @requires jquery.js, touchy.js
