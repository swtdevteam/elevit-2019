/**
 * Provides a function to navigate backwards
 * ---------------------------------------------------------
 *
 * Navigates backwards through the sequence of app.goTo calls.
 *
 * @module ap-back-navigation.js
 * @requires agnitio.js
 * @author David Buezas, antwerpes ag
 */
app.register('ap-back-navigation', function () {
	var self;
	return {
		publish: {},
		events: {
			'tap': 'back'
		},
		states: [],
		history: [],
		active: false,
		path: null,
		onRender: function () {
			self = this;
			app.$.BackNavigation = this;
			app.slideshow.on('update:current', function (data) {
				if (data.prev.id !== data.current.id) {
					this.save();
				}
			}.bind(this));
			app.slideshow.on('load', function () {
				this.clearHistory();
			}.bind(this));
			this.update();
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-back-navigation',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		// Save the path of the slide we are currently on
		update: function () {
			this.path = app.getPath();
		},
		save: function () {
			if (!this.active) {
				this.history.push(this.path);
			}
			this.active = false;
			this.update();
		},
		storeLastCollection: function (data) {
			self.prevCollection = data.id;
		},
		storePrevSlide: function (data) {
			self.prevSlide = data.prev.id;
		},
		setPrevCollection: function (id) {
			self.prevCollection = id;
		},
		back: function () {
			if (this.nextSlideHistory()) {
				this.active = true;
				var path = this.history.pop();
				if (path) {
					app.goTo(path);
				}
			}
		},
		nextSlideHistory: function () {
			return !this.history.length < 1;
		},
		clearHistory: function () {
			this.history = [];
			this.update();
			this.active = true;
		}
	};
});
