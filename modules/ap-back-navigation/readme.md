## ap-back-navigation

### This module writes to slides history and provides a function to navigate backwards.
### Navigates backwards through the sequence of app.goTo calls.

## How to use

##### Include module to presentation.json
```
"ap-back-navigation": {
    "id": "ap-back-navigation",
    "files": {
        "templates": [],
        "scripts": [
            "modules/ap-back-navigation/ap-back-navigation.js"
        ],
        "styles": []
    }
}
```
### Include module to jade/pug/html file or template
```
<div data-module="ap-back-navigation"></div>
```
