/**
 * Provides a function to lock and connect slides
 * ---------------------------------------------------------
 * *
 * * This module allows the creation of groups of different slides.
 * These will be connected either horizontally or vertically and stay connected when creating custom collections.
 *
 * Usage:
 *
 * To define content groups, you need to create a `contentGroups.json` next to the `presentation.json` file.
 *
 * Remember to make sure that all slideshows in the `presentation.json` file match the rules defined by the different content groups.
 *
 * If they don't match, the group definition will be ignored and a warning will be given in the javascript console.
 *
 * e.g.:
 *
 * 'test-group-1': {
 *       'orientation': 'horizontal',
 *       'slides': ['radiology_slide_1', 'radiology_slide_2']
 *   },
 * 'test-group-2': {
 *       'orientation': 'vertical',
 *       'slides': ['radiology_slide_1', 'radiology_slide_2', 'radiology_slide_3']
 *   }
 *
 * The content groups will be checked on each start
 *
 * The module is loaded within index.html
 *
 * @module ap-content-groups.js
 * @requires agnitio.js, contentGroups.json, ap-custom-collections.js
 * @author Marc Lutz, antwerpes ag
 */
app.register('ap-content-groups', function () {
	var self;
	return {
		definition: null,
		json: {},
		publish: {},
		events: {},
		states: [],
		onRender: function () {
			self = this;
			app.$.contentGroups = this;
			$.get('contentGroups.json').done(function (def) {
				var groupDefinition = def;
				if (typeof groupDefinition === 'string') {
					groupDefinition = JSON.parse(groupDefinition);
				}
				self.json = groupDefinition;
			});
			this.validateContentgroups();
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (!app.isVeevaWide) {
				if (window.ag) {
					window.ag.submit.data({
						label: 'Registered Module',
						value: 'ap-content-groups',
						category: 'BHC Template Modules',
						isUnique: true,
						labelId: 'bhc_registered_module',
						categoryId: 'bhc_template_modules'
					});
				}
			}
		},
		collectionContainsSlides: function (collection, groupDefinition) {
			/**
			 * Implements a function to check if the collection contains a slide from the contentGroup.json
			 * --------------------------------------------------------------------------------------------
			 *
			 * Tests if the collection needs validation
			 * by checking if the collection has any slide from the contentGroup
			 *
			 * @class validateContentgroups
			 * @constructor
			 */
			// get all slides which are used in this collection
			var collectionClone = $.extend(true, {}, collection);
			var slidesUsed;
			if (collection.content) {
				if (collectionClone.content.length > 0) {
					$.each(collectionClone.content, function (i, slideshowName) {
						collectionClone.content[i] = app.model.getStructure(slideshowName).content;
					});
					slidesUsed = collectionClone.content.reduce(function (a, b) {
						return a.concat(b);
					});
				}
			} else {
				slidesUsed = collectionClone.slides;
			}
			// check if the collection contains any slide from this contentGroup
			var collectionContainsSlide = false;
			if (slidesUsed) {
				$.each(groupDefinition.slides, function (i, slide) {
					if (~slidesUsed.indexOf(slide)) {
						collectionContainsSlide = true;
					}
				});
			}
			return collectionContainsSlide;
		},
		validateContentgroups: function () {
			// validate groups
			/**
			 * Implements a function to validate the content groups
			 * -----------------------------------------------------------
			 *
			 * @class validateContentgroups
			 * @constructor
			 */
			// var customCollections = app.$.customCollectionsStorage.getAll();
			// $.extend(true, app.model.get().storyboards, customCollections);
			$.get('contentGroups.json').done(function (def) {
				var groupDefinition = def;
				if (typeof groupDefinition === 'string') {
					groupDefinition = JSON.parse(groupDefinition);
				}
				function createHorizontalCollection() {
					$.each(app.model.get().storyboards, function (i, collection) {
						if (collection.type === 'collection') {
							var collectionContainsSlide = self.collectionContainsSlides(collection, definition);
							// if it contains a slide, validate if its been used correctly
							if (collectionContainsSlide) {
								var firstSlides = [];
								if (collection.content) {
									// normal Collections
									$.each(collection.content, function (item, slideshow) {
										if (definition.slides.indexOf(app.model.getStructure(slideshow).content[0]) > -1) {
											if (app.model.getStructure(slideshow).content.length === 1) {
												// okay
											} else {
												// eslint-disable-next-line
												console.warn('horizontal contentGroups may not have more than one vertical slides @%s', slideshow);
											}
										}
										if (typeof app.model.getStructure(slideshow).content === 'string') {
											firstSlides.push(app.model.getStructure(slideshow).content);
										} else {
											firstSlides.push(app.model.getStructure(slideshow).content[0]);
										}
									});
									if (~firstSlides.join(':').indexOf(definition.slides.join(':'))) {
										// eslint-disable-next-line
										console.log('contentGroup %s valid for presentation %s', groupName, collection.id);
										// write the matching contentgroups to the structures for use in the custom-collections
										var storyboard = app.model.hasStoryboard(collection.id) ? app.model.getStoryboard(collection.id) : app.model.getStoryboard(collection.name);
										if (storyboard) {
											if (!storyboard.contentGroups) {
												storyboard.contentGroups = {};
											}
											storyboard.contentGroups[groupName] = definition;
										}
									} else {
										// eslint-disable-next-line
										console.warn('contentGroup %s is invalid for presentation %s', groupName, collection.id);
										// eslint-disable-next-line
										console.warn('the group definition will be ignored!');
										delete groupDefinition[groupName];
									}
								} else {
									// custom Collections
									$.each(collection.slideshows, function (item, slideshow) {
										// if the slideshow contains a slide from the group definition
										if (definition.slides.indexOf(slideshow.content[0]) > -1) {
											// and is only 1 slide long vertically
											if (slideshow.content.length === 1) {
												// okay
											} else {
												// eslint-disable-next-line
												console.warn('horizontal contentGroups may not have more than one vertical slides @%s', slideshow);
											}
										}
										if (typeof app.model.getStructure(slideshow).content === 'string') {
											firstSlides.push(app.model.getStructure(slideshow).content);
										} else {
											firstSlides.push(app.model.getStructure(slideshow).content[0]);
										}
									});
									if (~firstSlides.join(':').indexOf(definition.slides.join(':'))) {
										// eslint-disable-next-line
										console.log('contentGroup %s valid for presentation %s', groupName, collection.id);
										// write the matching contentgroups to the structures for use in the custom-collections
										var $storyboard = app.model.hasStoryboard(collection.id) ? app.model.getStoryboard(collection.id) : app.model.getStoryboard(collection.name);
										if ($storyboard) {
											if (!$storyboard.contentGroups) {
												$storyboard.contentGroups = {};
											}
											$storyboard.contentGroups[groupName] = definition;
										}
									} else {
										// eslint-disable-next-line
										console.warn('contentGroup %s is invalid for presentation %s', groupName, collection.id);
										// eslint-disable-next-line
										console.warn('the group definition will be ignored!');
										delete groupDefinition[groupName];
									}
								}
							}
						}
					});
				}
				function createVerticalCollection() {
					$.each(app.model.get().storyboards, function (i, collection) {
						if (collection.type === 'collection') {
							var collectionContainsSlide = self.collectionContainsSlides(collection, definition);
							if (collectionContainsSlide) {
								var allSlides = [];
								if (collection.content) {
									$.each(collection.content, function (item, slideshow) {
										var slideshowSlides = app.model.getStructure(slideshow).content;
										allSlides.push(slideshowSlides);
									});
								} else {
									// custom Collection
									$.each(collection.slideshows, function (item, slideshow) {
										allSlides.push(slideshow.content);
									});
								}
								if (~allSlides.join(':').indexOf(definition.slides.join(','))) {
									// eslint-disable-next-line
									console.log('contentGroup %s valid for presentation %s', groupName, collection.id);
									// write the matching contentgroups to the structures for use in the custom-collections
									var storyboard = app.model.hasStoryboard(collection.id) ? app.model.getStoryboard(collection.id) : app.model.getStoryboard(collection.name);
									if (storyboard) {
										if (!storyboard.contentGroups) {
											storyboard.contentGroups = {};
										}
										storyboard.contentGroups[groupName] = definition;
									}
								} else {
									// eslint-disable-next-line
									console.warn('contentGroup %s is invalid for presentation %s', groupName, collection.id);
									// eslint-disable-next-line
									console.warn('the group definition will be ignored!');
									delete groupDefinition[groupName];
								}
							}
						}
					});
				}
				for (var property in groupDefinition) {
					if (groupDefinition.hasOwnProperty(property)) {
						var groupName = property;
						var definition = groupDefinition[property];
						if (definition.orientation === 'horizontal') {
							createHorizontalCollection();
						} else if (definition.orientation === 'vertical') {
							createVerticalCollection();
						} else {
							// eslint-disable-next-line
							console.log('unknown ContentGroup definition type');
						}
					}
				}
				self.definition = groupDefinition;
			});
		}
	};
});
