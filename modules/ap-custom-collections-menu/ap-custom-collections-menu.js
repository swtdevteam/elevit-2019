/**
 * Implements Custom Collections Menu.
 * -----------------------------------
 *
 * This module allows the creation / naming / deletion and loading of custom collections.
 *
 * @module ap-custom-collections-menu.js
 * @requires jquery.js, iscroll.js, custom-collections-storage.js
 * @author David Buezas, antwerpes ag
 */
app.register('ap-custom-collections-menu', function () {
	var translation = {
		'ENTER_PRESENTATION_NAME': 'Please enter a name for your presentation',
		'PRESENTATION_NAME': 'Presentation name',
		'NAME_ALREADY_EXISTS': 'Another presentation exists with the same name, please choose a new one',
		'$PRESENTATION_NAME$_WILL_BE_REMOVED': '\'$PRESENTATION_NAME$\' will be erased',
		'OK': 'OK',
		'CANCEL': 'Cancel',
		'YES': 'Yes',
		'NO': 'No',
		'PRESENTATION_IS_EMPTY': 'This presentation is empty.',
		'NOT_ALLOWED_TO_DELETE': 'Deleting the current presentation is not allowed'
	};
	/**
	 * Implements Custom Collections Menu
	 * ------------------------------------
	 * This module allows the creation / naming / deletion and loading of custom collections.
	 *
	 * @class ap-custom-collections-menu
	 * @constructor
	 */
	var self;
	var daDefaultDataPath = './da-default-data.json';
	var daDefaultData = {};
	return {
		publish: {},
		events: {
			'tap .newPresentation': 'createNewPresentation',
			'tap .trash': 'deletePresentation',
			'tap .rename': 'renamePresentation',
			'tap .presentation': 'openPresentation',
			'tap .edit': 'editPresentation',
			'tap .favorite': 'favorisePresentation'
		},
		states: [
			{
				id: 'visible'
			}
		],
		onRender: function () {
			self = this;
			$.ajax(daDefaultDataPath)
				.done(function (data) {
					var parsedData = typeof data === 'string' ? JSON.parse(data) : data;
					generateDefaultStructure(parsedData);
				});
			function generateDefaultStructure (data) {
				daDefaultData = data;
				var structures = app.model.get().structures;
				var slideshows = daDefaultData.slideshows.map(function (slideshowId) {
					return structures[slideshowId] || false;
				});
				daDefaultData.slideshows = slideshows;
			}
			app.$.customCollectionsMenu = this;
			app.$.on('open:ap-custom-collections-menu', function () {
				this.show();
			}.bind(this));
			app.$.on('close:ap-custom-collections-menu', function () {
				this.hide();
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				this.hide();
			}.bind(this));
			self.appriseDefaults = {
				textOk: translation.OK,
				textCancel: translation.CANCEL,
				textYes: translation.YES,
				textNo: translation.NO
			};
			var presentationsList = $('.presentationsContainer');
			presentationsList.sortable({
				axis: 'y',
				items: '.row:not(.newPresentationButtonContainer)',
				scroll: true,
				update: function () {
					self.updateCustomPresentationOrder();
				}
			});
			function checkTouchOutX(element, eventName, e) {
				var customEvent = eventName || 'out:touchout';
				var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
				var elementPosition = $(element).offset();
				var x = Math.floor(touch.pageX - elementPosition.left);
				if (x >= $(element).width() || x <= 0) {
					element.trigger(customEvent);
				}
			}
			presentationsList.on('touchmove', checkTouchOutX.bind(this, presentationsList, 'out-x:sortable-list'));
			presentationsList.on('out-x:sortable-list', presentationsList.trigger.bind(presentationsList, 'mouseup'));
			$.each(app.$.customCollectionsStorage.getAll(), function (index, presentationObject) {
				self._addPresentationToView(presentationObject.name, presentationObject, /* animated: */false);
			});
		},
		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {},
		hide: function () {
			app.unlock();
			this.reset();
		},
		show: function () {
			app.lock();
			this.goTo('visible');
		},
		/**
		 * Adds a new entry to the menu
		 * -----------------------------------
		 *
		 * @param {string} presentationName - presentation name
		 * @param {object} presentationObject - presentation object
		 * @private
		 * @method _addPresentationToView
		 * @returns {void} - returns nothing
		 */
		_addPresentationToView: function (presentationName, presentationObject) {
			var $self = this;
			var $row = $('.row.template').clone();
			$row.removeClass('template');
			$row.data('presentationName', presentationName);
			$row.find('.presentation .name').text(presentationName);
			if (presentationObject.isFavorite) {
				$row.find('.favorite').addClass('selected');
			}
			$row.insertBefore($self.$('.newPresentationButtonContainer'));
		},
		/**
		 * Updates the current order of custom presentations
		 * @public
		 * @method updateCustomPresentationOrder
		 * @returns {void} - returns nothing
		 */
		updateCustomPresentationOrder: function () {
			var rowsArray = [];
			$('.presentationsContainer').children('.row').each(function () {
				var presentationName = $(this).data('presentationName');
				if (presentationName) {
					if (app.$.customCollectionsStorage.getFavorites().length < 3) {
						var isFavorite = $(this).find('.favorite').hasClass('selected');
						var collection = app.$.customCollectionsStorage.get(presentationName);
						collection.isFavorite = isFavorite;
						app.$.customCollectionsStorage.add(presentationName, collection);
					}
					rowsArray.push(presentationName);
				}
			});
			if (rowsArray.length > 1) {
				app.$.customCollectionsStorage.updateOrder(rowsArray);
			}
			app.$.trigger('update:favorites');
		},
		/**
		 * creates a new presentation
		 * @public
		 * @method createNewPresentation
		 * @returns {void} - returns nothing
		 */
		createNewPresentation: function () {
			function askName(defaultName) {
				var optn = $.extend({}, self.appriseDefaults, {input: defaultName});
				window.apprise(translation.ENTER_PRESENTATION_NAME, optn, function (newName) {
					if (newName === false || newName === '') {
						return; // user canceled
					} else if (app.$.customCollectionsStorage.get(newName)) {
						window.apprise(translation.NAME_ALREADY_EXISTS, self.appriseDefaults, function () {
							askName(newName); // ask again
						});
					} else {
						var collectionId = 'custom-collection-' + Math.floor((Math.random() * 10000) + 1);
						var presentationObject = {
							id: collectionId,
							name: newName,
							type: 'collection',
							slideshows: daDefaultData.slideshows,
							slides: daDefaultData.slides,
							presentation: daDefaultData.presentation,
							isCustomPresentation: true,
							isFavorite: false
						};
						app.$.customCollectionsStorage.add(newName, presentationObject);
						self._addPresentationToView(newName, presentationObject, true);
					}
				});
			}
			var standardName = translation.PRESENTATION_NAME;
			askName(standardName);
		},
		/**
		 * deletes the presentation
		 * @public
		 * @method deletePresentation
		 * @param {object} event - event object
		 * @returns {void} - returns nothing
		 */
		deletePresentation: function (event) {
			var $row = $(event.target).parent();
			var presentationName = $row.data('presentationName');
			var confirmationTitle = translation.$PRESENTATION_NAME$_WILL_BE_REMOVED.replace('$PRESENTATION_NAME$', presentationName);
			var optn = $.extend({}, self.appriseDefaults, {confirm: true});
			var current = app.slideshow.getId();
			var collections = app.$.customCollectionsStorage.getAll();
			var collectionData = collections[presentationName];
			if (collectionData && collectionData.id !== current) {
				window.apprise(confirmationTitle, optn, function (answer) {
					if (answer) {
						app.$.customCollectionsStorage.delete(presentationName);
						$row.animate({height: 0}, function () {
							$(this).remove();
						});
					}
				});
				app.$.trigger('update:favorites');
			} else {
				window.apprise(translation.NOT_ALLOWED_TO_DELETE, self.appriseDefaults);
			}
		},
		/**
		 * renames the presentation
		 * @public
		 * @method renamePresentation
		 * @param {object} event - event object
		 * @returns {void} - returns nothing
		 */
		renamePresentation: function (event) {
			var $row = $(event.target).parent();
			function askName(defaultName) {
				var optn = $.extend({}, self.appriseDefaults, {input: defaultName});
				window.apprise(translation.ENTER_PRESENTATION_NAME, optn, function (newName) {
					if (newName === false || newName === oldName || newName === '') {
						return; // user canceled
					} else if (app.$.customCollectionsStorage.get(newName)) {
						window.apprise(translation.NAME_ALREADY_EXISTS, self.appriseDefaults, function () {
							askName(newName); // ask again
						});
					} else {
						app.$.customCollectionsStorage.rename(oldName, newName);
						$row.data('presentationName', newName);
						var $name = $row.find('.name');
						$name.css({opacity: 0});
						setTimeout(function () {
							$name.text(newName);
							$name.css({opacity: 1});
						}, 500);
					}
				});
			}
			var oldName = $row.data('presentationName');
			askName(oldName);
		},
		/**
		 * open the presentation
		 * @public
		 * @method openPresentation
		 * @param {object} event - event object
		 * @returns {void} - returns nothing
		 */
		openPresentation: function (event) {
			var presentationName = $(event.target).parent().parent().data('presentationName');
			if (!presentationName) {
				presentationName = $(event.target).parent().data('presentationName');
			}
			var collection = app.$.customCollectionsStorage.get(presentationName);
			// app.menu.linksConfig[homeSlideName] = {title: '<div class='homeIcon' />', classname: 'home'}
			if (collection.slideshows.length > 0) {
				var slideshowIdArray = [];
				$.each(collection.slideshows, function (i, slideshow) {
					slideshowIdArray.push(slideshow.id);
					var slidesArrary = [];
					$.each(slideshow.content, function (item, slide) {
						slidesArrary.push(slide);
					});
					var temp = {
						id: slideshow.id,
						name: slideshow.name,
						content: slidesArrary,
						type: 'slideshow'
					};
					if (!app.model.hasStructure(slideshow.id)) {
						app.model.addStructure(slideshow.id, temp);
					}
				});
				var storyboardData = {
					id: collection.id,
					name: collection.name,
					content: slideshowIdArray
				};
				if (!app.model.hasStoryboard(collection.id)) {
					app.model.addStoryboard(collection.id, storyboardData);
				}
				if (app.slideshow.resolve().slideshow === collection.id) {
					app.slideshow.first();
				} else {
					app.slideshow.init(collection.id);
					app.slideshow.load(collection.id);
				}
				app.$.toolbar.hide();
				app.unlock();
			} else {
				window.apprise(translation.PRESENTATION_IS_EMPTY, self.appriseDefaults);
			}
		},
		/**
		 * edit the presentation
		 * @public
		 * @method editPresentation
		 * @param {object} event - event object
		 * @returns {void} - returns nothing
		 */
		editPresentation: function (event) {
			// load custom collections editor into own container
			/*
			 new CustomCollections({
			 $container: self.$container,
			 presentationName: $(this).parent().data('presentationName')
			 });
			 */
			self.hide();
			var trigger = 'open:ap-custom-collections';
			var presentationName = $(event.target).parent().data('presentationName');
			app.$.trigger(trigger, {presentationName: presentationName});
		},
		/**
		 * favorise the presentation
		 * @public
		 * @method favorisePresentation
		 * @param {object} event - event object
		 * @returns {void} - returns nothing
		 */
		favorisePresentation: function (event) {
			// set favorite
			var $this = $(event.target);
			var presentationName = $this.parent().data('presentationName');
			var collection = app.$.customCollectionsStorage.get(presentationName);
			if (app.$.customCollectionsStorage.getFavorites().length < 3 || collection.isFavorite) {
				$this.toggleClass('selected');
				collection.isFavorite = !collection.isFavorite;
				app.$.customCollectionsStorage.add(presentationName, collection);
			} else {
				var $popup = $('#maxFavoritesPopUp');
				$popup.fadeIn();
				$popup.delay(1600).fadeOut();
			}
			app.$.trigger('update:favorites');
		}
	};
});
