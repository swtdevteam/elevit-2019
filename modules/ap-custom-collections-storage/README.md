ap-custom-collections-storage
==============================
This module manages the storage (localStorage) for custom collections.

## Usage
**This module is working in conjunction with "ap-custom-collections" and "ap-custom-collections-menu"**

- Add the following code to the presentation json file
```
"ap-custom-collections-storage": {
         "id": "ap-custom-collections-storage",
         "ignoreVeeva": true,
            "files": {
                "scripts": [
                   "**SOME-DIRECTORY**/ap-custom-collections-storage/ap-custom-collections-storage.js"
                ]
            }
       }
 ```
- Add the following tag to index.pug:
```
div(data-module='ap-custom-collections-storage')
```
