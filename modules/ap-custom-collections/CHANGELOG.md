### Version 0.0.8 2018-09-27
### Dryzhuk Andrii
- change ag to window.ag in monitor usage function

### Version  0.0.7 16.07.18 Oleksandr Melnyk
- fix css linter warnings

### Version 0.0.6 26.06.2018
- fix issue with getting data in ajax response
- fix issue with wrong slides naming

### Version 0.0.5 06.06.2018
- Fixed issue with adding linked slides
- Added mandatory parameter

### Version 0.0.4 06.06.2018
- Fix of the bug with wrong location slides to collection when moving the slide fast

### Version 0.0.3 05.06.2018
- Fix semantic

### Version 0.0.2 31.05.2018
- Update getting data for default custom collection
- Fix Linters warnings (css, js)
- Update collection editing flow (first and last slides could be deleted, but not last slide or last group in collection)

### Version 0.0.1 16.04.2018
- Fix of the bug that led to a slide image freezing while being dragged with another slide grabbed, the bug that led to a discarded result of a movement because of an another mouseUp event detection, the bug that led to a state change of the .o_slide because of a click on the .edit button from the "ap-custom-collections-menu", the bug that led to a wrong touch being used
