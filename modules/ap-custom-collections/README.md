ap-custrom-collections
======================
This module allows the edition of custom collections directly on the client (iPad / Browser).

## Usage
**This module is working in conjunction with "ap-custom-collections-menu", "ap-overview", "ap-load" and da-default-data.json in app folder**

- Add the following code to the presentation json file
```
"ap-custom-collections": {
         "id": "ap-custom-collections",
         "ignoreVeeva": true,
            "files": {
                "templates": [
                     "**SOME-DIRECTORY**/ap-custom-collections/ap-custom-collections.html"
                ],
                "scripts": [
                   "**SOME-DIRECTORY**/ap-custom-collections/ap-custom-collections.js"
                ],
                "styles": [
                   "**SOME-DIRECTORY**/ap-custom-collections/ap-custom-collections.css"
                ]
            }
       }
 ```
- Add the following tag to a desired location with the "ap-custom-collections-menu", "ap-overview", "ap-load" modules already placed. Example of da-default-data.json attached.
```
<div data-module="ap-custom-collections" hide ></div>
```
- Add mandatory=true parameter for ap-custom-collections-storage if default slides are mandatory (should not be removed from created presentations):
```
<div data-module="ap-custom-collections-storage" mandatory=true></div>
```
