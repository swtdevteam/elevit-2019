# ap-favorite-presentations-buttons

##### Implements favorite presentations buttons

 adds up to three buttons to a slide, which reference the favorite custom collections

##### requires jquery.js

## Changelog
```
*   v.1.0.1
        2018-07-17 - Oleksandr Melnyk
            refactoring:
                fix css linter errors

```