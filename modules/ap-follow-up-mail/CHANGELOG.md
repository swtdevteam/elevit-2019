### Version 1.0.6 2018-09-27
### Dryzhuk Andrii
- change ag to window.ag in monitor usage function

### Version 1.0.5 2018-07-16
### Oleksandr Melnyk
- fix coffee linter errors.

### Version 1.0.4 2018-03-16
### Oleksandr Melnyk
- fix coffee linter errors.

### Version 1.0.3 2018-05-31
### Oleksandr Tkachov
- Update code to use current ap-media-repository module functionality.

### Version 1.0.2 2018-04-16
### Vladyslav Voit
- Fix styles for input search field.

### Version 1.0.1 2018-04-13
### Vlad Kodenko
- Fixing an issues with Helvetica font
- Changing the positioning property of the .mail section from top to bottom.
