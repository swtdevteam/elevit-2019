# ap-media-library

##### This module implements a media library section.

### How to use:

##### Include module to presentation.json
```
"ap-media-library": {
    "id": "ap-media-library",
    "files": {
        "templates": [
            "modules/ap-media-library/ap-media-library.html"
        ],
        "scripts": [
            "modules/ap-media-library/ap-media-library.js"
        ],
        "styles": [
            "modules/ap-media-library/ap-media-library.css"
        ]
    }
}
 ```
##### Include module to jade/pug/html file or template
```
    <div data-module="ap-media-library"></div>
```

##### Custom module properties
```
    <div data-module="ap-media-library" prefiltersearchterms="" prefilterattributestobesearched="allowDistribution" metadatafiltrationopts="filterbytrack"></div>
```
    - prefiltersearchterms: limits basic set of metadata entries: what is to be searched (leave undefined to show all). Search terms should be separated by whitespaces.
    - prefilterattributestobesearched: limits basic set of metadata entries: where is it to be found. Meta attribute keys, whose values should be searched for search terms. If not specified, all attributes are being searched.
    - renderoptions: object with options to be passed to renderers
    - metadatafiltrationopts: metadata filtration options which will be applied before it renders. If more than one, values should be separated with comma. At this moment only 'filterbytrack' option is availbale which filters metadata by current active presentation flow (property "flow" with collection id(s) should be present in the media.json for metadata item, for example - "flow": "test_collection_1,test_collection_2").

##### Changing module state through attributes
```
    <div data-module="ap-media-library" hide></div>
```
There is a possibility of changing module state through attributes. Available attributes values are: hide, attachment, followupmail.
