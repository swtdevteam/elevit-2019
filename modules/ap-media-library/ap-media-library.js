/**
 * Provides a media library section.
 * ---------------------------------
 * Provides a user interface for searching and viewing arbitrary
 * media meta data and content from the media repository.
 *
 * @module ap-media-library.js
 * @requires jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register('ap-media-library', function () {
	/**
	 * Implements a media library section.
	 * -----------------------------------
	 * Implements a user interface for searching and viewing arbitrary
	 * media meta data and content from the media repository.
	 * Shows all media entries by default.
	 * The basic set of listed entries may be optionally limited by some constraint,
	 * e.g. 'all media entries that are attachable to emails'
	 * or 'all media entries that have the 'pdf' and 'reference' tags in it'.
	 *
	 * @class ap-media-library
	 * @constructor
	 * @param {object} options Valid properties are:
	 *     - preFilterSearchTerms: limits basic set of entries: what is to be searched (leave undefined to show all)
	 *     - preFilterAttributesToBeSearched: limits basic set of entries: where is it to be found
	 *     - renderOptions: options to be passed to renderers
	 */
	return {
		publish: {
			prefiltersearchterms: void 0,
			prefilterattributestobesearched: void 0,
			metadatafiltrationopts: void 0,
			renderOptions: {},
			attachment: false,
			followupmail: false,
			hide: false
		},
		events: {},
		states: [
			{
				id: 'hide'
			},
			{
				id: 'visible'
			},
			{
				id: 'followupmail'
			},
			{
				id: 'attachment'
			}
		],
		onRender: function (el) {
			app.$.mediaLibrary = this;
			this.load(el);
			if (this.props.hide) {
				this.hide();
			}
			if (this.props.attachment) {
				this.goTo('attachment');
			}
			if (this.props.followupmail) {
				this.goTo('followupmail');
			}

			app.$.on('open:ap-media-library', function () {
				if (this.stateIs('hide')) {
					this.show();
				}
			}.bind(this));

			app.$.on('close:ap-media-library', function () {
				if (this.stateIs('visible')) this.hide();
			}.bind(this));

			app.$.on('toolbar:hidden', function () {
				if (this.stateIs('visible')) this.hide();
				this.unload();
			}.bind(this));

			app.$.on('media-library:refresh', function () {
				if (this.stateIs('attachment')) this.scroll.refresh();
			}.bind(this));

			app.listenTo(app.slideshow, 'load', function () {
				this.clearSearchInput();
				this.load(this.el);
			}.bind(this));

			this.monitorUsage();
		},
		onRemove: function () {
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-media-library',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},

		hide: function () {
			app.unlock();
			this.goTo('hide');
		},

		show: function () {
			app.lock();
			this.goTo('visible');
			this.load();
		},
		unload: function () {
			if (this.stateIs('visible')) {
				if (this.scroll) this.scroll.destroy();
			}
		},
		refreshScroll: function () {
			this.scroll.refresh();
		},
		clearSearchInput: function (){
			$(this.el).find('input').val('');
		},
		load: function (el) {
			var $list = $(el).find('ul');
			$list.empty();
			if (this.scroll) this.scroll.destroy();
			this.scroll = new IScroll(this.$('.scroll')[0], {scrollbars: true});


			// Fill the list with a basic set of media entries (e.g. 'all media entries that are attachable to emails'):


			/**
			 * Implements a media library section.
			 * -----------------------------------
			 * Implements a user interface for searching and viewing arbitrary
			 * media meta data and content from the media repository.
			 * Shows all media entries by default.
			 * The basic set of listed entries may be optionally limited by some constraint,
			 * e.g. 'all media entries that are attachable to emails'
			 * or 'all media entries that have the 'pdf' and 'reference' tags in it'.
			 *
			 * @param {object} options Valid properties are:
			 *     - preFilterSearchTerms: limits basic set of entries: what is to be searched (leave undefined to show all)
			 *     - preFilterAttributesToBeSearched: limits basic set of entries: where is it to be found
			 *     - renderOptions: options to be passed to renderers
			 */

			var media = window.mediaRepository.find(this.props.prefiltersearchterms, this.props.prefilterattributestobesearched);
			if (media) {
				media = window.mediaRepository.updateAndGetRenderedMetadata(media, this.props.metadatafiltrationopts);
				$.each(media, function (file, meta) {
					var listElement = window.mediaRepository.render(file, meta, this.renderOptions);
					if (listElement) {
						$list.append(listElement);
					}
				});
			}

			// Set up live-search (on each key stroke):
			var $listElements = $(this.el).find('ul li');
			var $input = $(this.el).find('input');
			$input.keyup(function (e) {
				// Dismiss on enter:
				if (e.keyCode === 13) {
					$input.blur();
					return;
				}
				// Search and update list:
				var searchString = $input.val().toLowerCase();
				var searchTerms = searchString.split(/\s+/g);
				$listElements.each(function () {
					var $listElement = $(this);
					var found = searchTerms.reduce(function (foundEl, searchTerm) {
						return foundEl && ($listElement.text().toLowerCase().indexOf(searchTerm) !== -1);
					}, true);
					$listElement.toggleClass('hidden', !found);
				});
				this.scroll.refresh();
			}.bind(this));
			this.scroll.refresh();
		}
	};
});
