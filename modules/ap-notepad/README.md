# ap-notepad

##### Implements the Notepad Class.

Enables the user to draw over other content via touch/mouse events.
	 
The module is loaded within index.html
	 
## Changelog
```
*   v.1.0.1
        2018-07-17 - Oleksandr Melnyk
            refactoring:
                fix css linter errors
```
