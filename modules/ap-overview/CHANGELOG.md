### Version 0.0.3 2018-06-28
### Dryzhuk Andrii
- change ag to window.ag in monitor usage function

### Version 0.0.2 26.06.2018
- fix issue with getting data in ajax response

### Verson 0.0.1 31.05.2018
- Update getting data for default custom collection
- Fix Linters warnings (css, js)
