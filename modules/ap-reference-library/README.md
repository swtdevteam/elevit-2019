# ap-reference-library

##### This module implements a user interface for viewing media entries that have a 'referenceId' attribute.

### How to use:

##### Include module to presentation.json
```
"ap-reference-library": {
    "id": "ap-reference-library",
    "files": {
        "templates": [
            "modules/ap-reference-library/ap-reference-library.html"
        ],
        "scripts": [
            "modules/ap-reference-library/ap-reference-library.js"
         ],
         "styles": [
            "modules/ap-reference-library/ap-reference-library.css"
         ]
     }
}
 ```
##### Include module to jade/pug/html file or template
```
    <div data-module="ap-reference-library"></div>
```