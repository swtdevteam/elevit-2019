# ap-slide-indicator

##### Implements navigation joystick

 updates the joystick component
 
 The module is loaded within index.html

##### requires jquery.js, agnitio.js

## Changelog

```
*   v.1.0.1
        2018-07-17 - Oleksandr Melnyk
            refactoring:
                fix css, js linter errors
```