/**
 * Implements a function that will open the specific-product-characteristics.pdf file with the in-app PDF viewer.
 * -----------------------------------------------------------------
 * @module ap-specific-product-characteristics.js
 * @author Andreas Tietz, antwerpes ag
 */
app.register('ap-specific-product-characteristics', function () {
	/**
	 * Implements a function that will open the specific-product-characteristics.pdf file with the in-app PDF viewer.
	 * ----------------------------------------------------------------------------------------------------------------------
	 *
	 * @class ap-specific-product-characteristics.js
	 * @constructor
	 */
	return {
		publish: {},
		events: {},
		states: [],
		onRender: function () {
			app.$.specificProductCharacteristics = this;
			this.monitorUsage();
		},
		onRemove: function () {},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-specific-product-characteristics',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},

		openPdf: function () {
			// eslint-disable-next-line
			console.log('openPDF(\'shared/references/specific-product-characteristics.pdf\', \'Specific Product Characteristics\'');
			ag.openPDF('shared/references/specific-product-characteristics.pdf', 'Specific Product Characteristics');
			var $joystick = $('.joystick');
			var $addSlideButton = $('[data-module=\'ap-add-slide-button\']');
			$joystick.fadeIn();
			$addSlideButton.fadeIn();
		},

		openPopup: function (overlay, popup) {
			if (app.module.get(overlay) && app.module.get(overlay).load) {
				app.module.get(overlay).load(popup);
			}
		}
	};
});
