# ap-video-library

##### This module implements a user interface for viewing media entries that have the 'video' tag.

### How to use:

##### Include module to presentation.json
```
"ap-video-library": {
    "id": "ap-video-library",
    "files": {
        "templates": [
            "modules/ap-video-library/ap-video-library.html"
        ],
        "scripts": [
            "modules/ap-video-library/ap-video-library.js"
         ],
         "styles": [
            "modules/ap-video-library/ap-video-library.css"
         ]
     }
}
 ```
##### Include module to jade/pug/html file or template
```
    <div data-module="ap-video-library"></div>
```
