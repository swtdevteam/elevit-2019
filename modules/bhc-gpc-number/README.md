# bhc-gpc-number

BHC Template Module: GPC Number

A module that reads the 'gpc' setting in config.json and displays it.

### How to use:

Insert 
```
<div data-module="bhc-gpc-number"></div> 
```
where number should be displayed

## Changelog
```

*   v.1.0.1
        2018-07-17 - Oleksandr Melnyk
            refactoring:
                fix css linter warnings
```
