app.register('bhc-gpc-number', function () {
	return {
		template: false,
		publish: {},
		events: {},
		states: [],
		onRender: function () {
			var gpc = app.config.get('gpc');
			var existingText = this.el.innerHTML;
			if (!gpc || gpc === '999-999-999') {
				gpc = 'Not Applied';
			}
			if (gpc) {
				this.el.innerHTML = existingText + ' ' + gpc;
			}
			this.monitorUsage();
		},
		onRemove: function () {},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'bhc-gpc-number',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		}
	};
});
