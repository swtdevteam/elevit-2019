app.register("formulation_page_01", function() {

    var mainClass = "#formulation_page_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {
    // Set times for each animation
    var time =0;
    var setTime =450;

        // $(mainClass + ".ani-1").delay(0).fadeIn(1000); 
        $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-1").delay(0).show( "slide",{direction: "down"}, 650);
        $(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "left"}, 650);
        $(mainClass + ".ani-3").delay(time+=setTime).fadeIn(450);
        // $(mainClass + ".ani-3").delay(time+=setTime).show( "slide",{direction: "right"}, 1000);
        // $(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);
        // $(mainClass + ".ani-1").delay(0).animate({
        //     bottom:'40px', 
        //     opacity:"show"
        // }, 1000);                 
        // $(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "left"}, 1000);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 
        // $(mainClass + ".ani-2").delay(0).animate({
        //     bottom:'0px'
        // }, 1000);  
    }
  }

});