app.register("home_slide", function () {
	return {
		events: {
			'tap [data-popup]': 'openPopup'
		},
		states: [],
		onRender: function (el) {
			this.ahPopup = app.module.get('ah-popup-management');
		},
		openPopup: function (el) {
			this.ahPopup.openPopup(el.target.attributes["data-popup"].value);
		},
		onRemove: function (el) {

		},
		onEnter: function (el) {


		},
		onExit: function (el) {
		}
	}

});
