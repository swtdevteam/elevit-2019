app.register("nutrition_page_01", function() {

    var mainClass = "#nutrition_page_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =0;
    var setTime =0;

        // $(mainClass + ".ani-1").delay(0).fadeIn(150);
        $(mainClass + ".ani-2").delay(450).fadeIn(150);
        // $(mainClass + ".ani-2").delay(1000).animate({
        //     height:'166px',
        //     opacity:"show"
        // }, 650);   
        $(mainClass + ".ani-3").delay(time+=setTime).animate({
            height:'224px',
            opacity:"show"
        }, 650); 

        $(mainClass + ".g-s01 > img").click(function(){
            $(mainClass + ".ani-4").delay(0).animate({
                height:'280px',
                opacity:"show"
            }, 650);                
        });
        //$("#side-tabs").fadeIn(500);
        //$(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);
        //$(mainClass + ".ani-1").delay(time+=setTime).show( "slide",{direction: "left"}, 1000);
        //$(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "right"}, 1000);
        //$(mainClass + ".ani-1").delay(time+=setTime).show( "slide",{direction: "right"}, 1000);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        // $(mainClass + ".ani-2").delay(0).animate({
        //     height:'0px',
        // }, 350);

        $(mainClass + ".ani-3").delay(0).animate({
            height:'0px',
        }, 350);

        $(mainClass + ".ani-4").delay(0).animate({
            height:'0px',
        }, 350);
 

    }
  }

});