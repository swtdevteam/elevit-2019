app.register("nutrition_page_03", function() {

    var mainClass = "#nutrition_page_03 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =200;
    var setTime =500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        $(mainClass + ".ani-2").delay(0).animate({
            width:'491px', 
            opacity:"show"
        }, 650); 

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 

        $(mainClass + ".ani-2").delay(0).animate({
            width:'0px'
        }, 650);

    }
  }

});