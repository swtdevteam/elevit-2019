app.register("resources_page_01", function() {

    var mainClass = "#resources_page_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {
        // Set times for each animation
        var time = 150;
        var setTime = 500;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);
        // $(mainClass + ".ani-3").delay(time+=setTime).fadeIn(1000);
        // $(mainClass + ".ani-4").delay(time+=setTime).fadeIn(1000);
        // $(mainClass + ".ani-5").delay(time+=setTime).fadeIn(1000);
        // $(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);

        // openPDF   
        $(mainClass + ".popup-pdf").click(function() {
            var file = $(this).data("file");
            var title = $(this).text().trim();
            ag.openPDF(file, title);
            return false;
        });          

        // $("#frame").attr("src", "shared/references/BA3929_YAZ_Patient_Booklet_Final.pdf");

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});