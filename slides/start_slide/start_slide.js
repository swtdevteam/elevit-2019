app.register("start_slide", function() {

    var mainClass = "#start_slide ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    var time =0;
    var setTime =0;

        // $(mainClass + ".ani-1").delay(0).fadeIn(450);
        // $(mainClass + ".ani-2").delay(0).animate({
        //     bottom:'40px', 
        //     opacity:"show"
        // }, 1000);                 
        $(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "down"}, 550);
        $(mainClass + ".ani-3").delay(time+=setTime).show( "slide",{direction: "left"}, 550);
        $(mainClass + ".ani-4").delay(time+=setTime).show( "slide",{direction: "right"}, 550);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide(); 
        // $(mainClass + ".ani-2").delay(0).animate({
        //     bottom:'0px'
        // }, 1000);        
    }
  }

});