app.register("test_slide_3", function() {

  return {
    events: {
        "tap .custom-attachment-button": "addCustomAttachment"
    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    },
    addCustomAttachment: function() {
        // In this demo we get the input from the textarea but it can be anything
        var textarea = this.$(".custom-attachment-text");
        if (textarea.length) {
            // Update the media repository entry (media.json)
            window.mediaRepository.setMetada("content://Calculator result", "content", textarea[0].value);
            // Tell the Follow Up Mail module to attach the media entry
            app.$.trigger("attach:ap-follow-up-mail", {"key":"content://Calculator result"});
        }
    }
  };

});
