app.register("the_elevit_range_page_01", function() {

    var mainClass = "#the_elevit_range_page_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {
        // Set times for each animation
        var time =0;
        var setTime =150;

            $(mainClass + ".ani-1").delay(0).fadeIn(1000); 
            $(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);
            $(mainClass + ".ani-3").delay(time+=setTime).fadeIn(1000);
            $(mainClass + ".ani-4").delay(time+=setTime).fadeIn(1000);
            $(mainClass + ".ani-5").delay(time+=setTime).fadeIn(1000);
            $(mainClass + ".ani-6").delay(time+=setTime).fadeIn(1000);
            // $(mainClass + ".ani-1").delay(0).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-3").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-4").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-5").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-1").delay(0).fadeIn(1000); 
            // $(mainClass + ".ani-2").delay(time+=setTime).fadeIn(1000);
            // $(mainClass + ".ani-3").delay(time+=setTime).fadeIn(1000);
            // $(mainClass + ".ani-4").delay(time+=setTime).fadeIn(1000);
            // $(mainClass + ".ani-5").delay(time+=setTime).fadeIn(1000);
            // $(mainClass + ".ani-6").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);
            // $(mainClass + ".ani-6").delay(time+=setTime).fadeIn(1000);
    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});