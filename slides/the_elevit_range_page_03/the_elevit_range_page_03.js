app.register("the_elevit_range_page_03", function() {

    var mainClass = "#the_elevit_range_page_03 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

        $(mainClass + ".ani-1").delay(0).show( "slide",{direction: "right"}, 650);

    },
    onExit: function(el) {

        $(mainClass + "[class*='ani-'] ").hide();

    }
  }

});